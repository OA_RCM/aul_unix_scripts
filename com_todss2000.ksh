#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXCOM712                CALL            *
#COMMENT *  Script Name  : com_todss2000.ksh                         *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to read report strings and create     *
#COMMENT *                 commission data to send to DSS            *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 06/26/2001                                *
#COMMENT *  Environment  : ENVINTF                                   *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : COM2000E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 06/26/2001  SMR: PEN????  By: Mike Lewis            U
#COMMENT U Reason: Created to run the program COM2000E               U
#COMMENT U Date: 05/10/2005  SMR: CC8191   By: Mike Lewis            U
#COMMENT U Reason: Change delivery point of output files and         U
#COMMENT U         functionalize coppy to POSTOUT.                   U
#COMMENT U Date: 03/24/2006  SMR: CC7955   By: Rick Sica             U
#COMMENT U Reason: New input file to process                         U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function send_and_copy
{
  cat EXTRTEA >> $EBSXFER/comm/COMMFILE2
  cat EXTRTEE >> $EBSXFER/comm/COMMFILE2
  cat EXTRTTA >> $EBSXFER/comm/COMMFILE2

  $copy_to_postout && copy_to_postout
}

function copy_to_postout
{
  cp EXTRTEA $AULOUT
  cp EXTRTEE $AULOUT
  cp EXTRTTA $AULOUT
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
  prog=$(basename $0)
  export integer NumParms=$#
  export JUSER=${JUSER:-$LOGNAME}
  export JGROUP=${JGROUP:-01}
  export JNAME=${prog%.ksh}
  export JDESC="OMNIPLUS/UX COMMISSION INDICATIVE TO DSS PROCESS"
  export ENVFILE=ENVINTF
  export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_daily_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
  . JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1 -- define files
### ******************************************************************
  cp $EBSCTRL/DUMBTRAN.DAT $XJOB
  cp $EBSCTRL/DUMBTRN2.DAT $XJOB
  touch $XJOB/DUMMY3.DEF
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_TRANIN=$XJOB/DUMBTRAN.DAT
  export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
  export dd_SEQAHST=$XJOB/SEQAHST
  export dd_HELPER2=$AULDATA/HELP2

  if [ "$JSTN" = "YES" ]
  then
   export dd_RSTRIN=$AULOUT/rsfile.${JUSER}
   export dd_INEXTPLAN=$AULOUT/planval.${JUSER}
  else
   export dd_RSTRIN=$AULOUT/rsfile.txt
   export dd_INEXTPLAN=$AULOUT/planval.txt
  fi
  export dd_INEXTPART=$AULOUT/certificates.txt
  export dd_EXTRTEA=$XJOB/EXTRTEA
  export dd_EXTRTEE=$XJOB/EXTRTEE

  if [ -f $dd_INEXTPLAN ]
  then
     echo 'planval.txt exists'
  else
     touch $dd_INEXTPLAN
  fi

  if [ -f $dd_INEXTPART ]
  then
     echo 'certificates.txt exists'
  else
     touch $dd_INEXTPART
  fi

  if [ -f $dd_RSTRIN ]
  then
     echo 'rsfile.txt exists'
  else
     touch $dd_RSTRIN
  fi

### ******************************************************************
### TASK 2 -- Run the COBOL program to extract commission data from the
###  Omni report strings to the extract files EXTRTEA and EXTRTEE.
### ******************************************************************

cd $XJOB

if [[ -s $dd_INEXTPLAN ]] || [[ -s $dd_RSTRIN ]] || [[ -s $dd_INEXTPART ]]
then
  PNAME=COM2000E
  JDESC="Run COBOL program $PNAME"
  execute_program
  JDESC="Extract files  EXTRTEA and EXTRTEE written"
  display_message
  $send_and_copy && send_and_copy
else
  JDESC="no reports strings or planval data"
  display_message
fi

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  create_html
  return 0
