#!/bin/ksh 
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name       : exp_distfee.ksh         CALL            *
#COMMENT *  SchedXref Name : rsuxexp701.ksh          OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Description  : Report and create Cash Expense Billing    *
#COMMENT *                 for plans that have Distribution Fee      *
#COMMENT *                 AND Annual Contribution Fee               *
#COMMENT *                 AND Manual Deposit Fee                    *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 01/03/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxexp701.ksh                            *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : EXP1000C                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : approx 1-1.5 hours                        *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Cannot be re-started until       R
#COMMENT R    system is evaluated to see if any updating was done.   R
#COMMENT R    Cash Expense Bill History is written and must be       R
#COMMENT R    removed OR can give the job a start plan so job starts R
#COMMENT R    on a plan after it had aborted.  The log or the report R
#COMMENT R    will tell what plan it was on. If the job does abort   R
#COMMENT R    make sure what was reported gets to document manager.  R
#COMMENT R                                                           R
#COMMENT R    If re-running after the 22nd of a month for the        R
#COMMENT R    previous month, then must override run date by putting R
#COMMENT R    "export opt_d=yyyymmdd" in the ini file. It is on the  R
#COMMENT R    the 22nd when the DATEMTHEND file is changed.          R
#COMMENT R                                                           R
#COMMENT R   To restart/re-run one particular fee out of all the     R
#COMMENT R   billed fees this process does, refer to the ini file.   R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 01/03/2005  CC: 5795   By: Rick Sica                U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 10/11/2005  CC: 8165&7648   By: Rick Sica           U
#COMMENT U Reason: Add Manual Deposit and Annual Contribution Fees   U
#COMMENT U                                                           U
#COMMENT U Date: 20110318  Proj: WMS3939     By: Paul Lewis          U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT U Date: 08/24/2011  WMS06689    By: Louis Blanchette        U
#COMMENT U Reason: Add good and bad email notification               U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function local_error_handling
{
   echo "Expense Billing Job failed  at $(date)"  >> $badmsgfile
   $send_email_notification && fnc_notify re_errors
}
function send_good_email_message
{
   echo "Expense Billing Job completed successfully at $(date)"  >> $goodmsgfile
   fnc_notify re_success
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=exp_distfee     
export JDESC="OMNIPLUS/UX BILLED FEES"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables
 
######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input 

make_output_directories

######################
### Override daily date and rundate 
### if one is passed in
######################
fnc_set_monthend_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Run the COBOL program                           
### ******************************************************************

export dd_RSTROUT=$XJOB/rs.tmp
export dd_EXPREPORT=$XJOB/${EBSRPTPFX}BILLEDFEERPT
export dd_FEEAPPLIEDIDX=$AULDATA/FEEAPPLIEDIDX
export dd_PAYROLLIDX=$XJOB/PAYROLLIDX.tmp

PNAME=EXP1000C
PARM="${fee_call}${start_plan}${num_of_plans}"
JDESC="Run of COBOL Program $PNAME"
execute_program

### ******************************************************************
### TASK 2 - Send Report to document manager
### ******************************************************************

$send2DocMgr && fnc_send2DocMgr


### ******************************************************************
### TASK 3 - Send good email message
### ******************************************************************
if [[ $cond = 0 ]]; then
    $send_email_notification && send_good_email_message
fi    


### ******************************************************************
### TASK 4 - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
