#!/bin/ksh

######################################################################
#                                                                    #
#                           Initialization                           #
#                                                                    #
######################################################################

prog=$(basename $0)
dir=$(cd $(dirname $0); pwd)
name=$(basename $prog .ksh)

######################################################################
#                                                                    #
#                        Function Definitions                        #
#                                                                    #
######################################################################

# display the parameters used for this run
function show_info
{
  echo "global folder card: $cardfile_create"
  echo "propagation card: $cardfile_prop"
  echo "process step: $procstep"
  echo "folder name: $fileid"
  echo "parameter data: $pdata"
  echo "run date: $rundate"
}

# define giveback review folder in global plan with T966 to call
# PLP-GBREVIEW calculator
function create_gbreview_folder
{
  echo "creating global giveback review folder"
  export gentemp_plan="000001"
  export gentemp_folder="$fileid"
  export gentemp_procstep="$procstep"
  gentemp.pl -o $cardfile_create DEFINEFOLDER || return 1
  export gentemp_date="$rundate"
  export gentemp_jobname="$calculator"
  export gentemp_planid="000001"
  gentemp.pl -a -o $cardfile_create RUNCALC || return 1
  cp $cardfile_create $EBSCARD || return 1
  echo "running JOBVTUT with $cardfile_create"
  JOBVTUT $JUSER $(basename $cardfile_create) || return 1
  return 0
}

# propagate giveback review folder to appropriate plans
function prop_gbreview_folder
{
  echo "propagating plp review folder to plans"
  export gentemp_fileid="$fileid"
  export gentemp_paramdata="$pdata"
  gentemp.pl -o $cardfile_prop PROPVTH || return 1
  cp $cardfile_prop $EBSCARD || return 1
  echo "running JOBVTUT with $cardfile_prop"
  JOBVTUT $JUSER $(basename $cardfile_prop) || return 1
  return 0
}

######################################################################
#                                                                    #
#                                MAIN                                #
#                                                                    #
######################################################################

. FUNCTIONSFILE
. DATEFUNC

export JNAME=$name
export JUSER=$LOGNAME
export today=$(date +%Y%m%d)
export tmptime=$(date +%H%M%S)

make_output_directories

log=$XJOB/$name.log

exec >$log
exec 2>&1

cardfile_create=$XJOB/gb-define
cardfile_prop=$XJOB/gb-prop
procstep=M4
fileid=GBREVIEW
calculator=PLP-GBREVIEW
pdata="G***"
datefile=$AULDATA/DATEBUSLAST

echo "$prog start at $(date)"

if grep -q "^$rundate\$" $datefile ; then
  show_info
  create_gbreview_folder || return 1
  prop_gbreview_folder || return 1
else
  echo "Giveback review not scheduled to run on $rundate"
fi

echo "$prog stop at $(date)"

return 0
