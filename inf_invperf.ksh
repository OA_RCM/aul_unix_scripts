#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : RSUXINF010             NO CALL         *
#COMMENT *  Schedule Name   : rsuxinf010.ksh         ASU-OMNI        *
#COMMENT *  Unix Script     : inf_invperf.ksh                        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will create the Fund Performance *
#COMMENT *                 files needed for marketing                *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - LQB                                 *
#COMMENT *  Created      : 02/24/2009                                *
#COMMENT *  Environment  : ENVUNIF                                   *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE, ODBC (RTS32.AUL)               *
#COMMENT *  COBOL Calls  : INF1003R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : Less than 5 minutes                       *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time.      R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date:             WMS:          By:                       U
#COMMENT U 03/21/2014        8043          Danny Hagans              U
#COMMENT U Increased size of the report file in perl function        U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS INVESTMENTS PERFORMANCES PROCESSING MODULE"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input

###################################
### Override daily date and rundate
### if one is passed in
###################################
fnc_set_monthend_date

. JOBDEFINE
###########################
# Move to the JOB directory
###########################
cd $XJOB

### ******************************************************************
### TASK 1 - Define files needed
### ******************************************************************
  export JDESC="Defines files needed by Cobol program"
  display_message nobanner

  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_RPTHEADER=$XJOB/RPTHEADER
  export dd_RPTHEADER_S=$XJOB/RPTHEADER_S
  export dd_INVPERF=$XJOB/INVPERF.CSV
  export dd_TMPPERFTABLE=$XJOB/performance_table.tmp
  
### ***********************************************************************
### TASK 2 - Run the COBOL program to produce Investment Performance files.
### ***********************************************************************

  PMSG="Produce Investment Performance files"

  export COBSAVE=$COBRUN
  export COBRUN=$(dirname $COBRUN)/$AUL_RTS

  PNAME=INF1003R
  JDESC="Run COBOL program $PNAME - Performance file creation"
  execute_program

  export COBRUN=$COBSAVE

  JDESC="Completed $PMSG - Performance file creation"
  display_message nobanner
  unset PMSG

### ******************************************************************
### TASK 3 - Split the file by product
### ******************************************************************
JDESC="Split file $dd_INVPERF"
display_message nobanner

export HEADER=$(cat $dd_RPTHEADER)
export HEADER_S=$(cat $dd_RPTHEADER_S)

perl -ne '
  chomp;
  $file = substr $_, 0, 5;
  $file .= "_perf.csv";
  if (/^.{4}S/i) 	{
	/(^.{188})(.{1212})(.*$)/;
	$_ = $1 . $3;
  }
  s/^.{5}//g;
  open FD, ">>$file" or die "Failed to open $file, stopped ";
  if (! defined $has_header{$file})   {
          if ($file =~ /^.{4}S/i) {
		print FD $ENV{HEADER_S}, "\n";
	  } else {
          	print FD $ENV{HEADER}, "\n";
	  }
	  $has_header{$file} = 1;
  }
  print FD $_, "\n";
  close FD;
' $dd_INVPERF

JDESC="Split file $dd_INVPERF - COMPLETED"
display_message nobanner

### ******************************************************************
### TASK 4 - Copy files for network transfer
### ******************************************************************
JDESC="Copy files to $EBSXFER"
display_message nobanner

# Annualized performances
cp *A_perf.csv $EBSXFER
JDESC="Annualized files copied to $EBSXFER"
display_message nobanner

# Standardized performances quarterly only
export perfmonth=$(echo $rundate | cut -c5-6)
case "$perfmonth" in
      "03"|"06"|"09"|"12")
      cp *S_perf.csv $EBSXFER
      JDESC="Standardized files copied to $EBSXFER : $perfmonth"
      display_message nobanner
esac

JDESC="Copy files to $EBSXFER - COMPLETED"
display_message nobanner

### ******************************************************************
### TASK 4 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
