package Folder;
use strict;

use Tran;

sub new
{
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{HEADER} = Tran->new();
  $self->{TRANS} = [Tran->new()];
  bless($self, $class);
  return $self;
}

sub add_rec
{
  my $self = shift;
  my $rec = shift;
  my $code = substr($rec, 0, 5);
  my $rc = 1;
  if ($code =~ /^002/)
  {
    $rc = $self->{HEADER}->add_rec($rec);
  }
  else
  {
    my $tran = $self->{TRANS}->[-1];
    while (!$tran->add_rec($rec))
    {
      push(@{$self->{TRANS}}, Tran->new());
      $tran = $self->{TRANS}->[-1];
    }
  }
  return $rc;
}

sub add_tran
{
  my $self = shift;
  my $tran = shift;
  my @recs = split(/\n/, $tran->tostr());
  while (@recs)
  {
    $self->add_rec(shift(@recs));
  }
}

sub name
{
  my $self = shift;
  if (@_)
  {
    $self->{HEADER}->upd_rec('00201', 18, 20, shift);
  }
  my $name = substr($self->{HEADER}->rec('00201'), 18, 20);
  for ($name)
  {
    # s/^\s+//;
    s/\s+$//;
  }
  return $name;
}

sub plan
{
  my $self = shift;
  if (@_)
  {
    $self->{HEADER}->upd_rec('00200', 15, 6, shift);
  }
  my $plan = substr($self->{HEADER}->rec('00200'), 15, 6);
  return $plan;
}

sub tran_count
{
  my $self = shift;
  return scalar(@{$self->{TRANS}});
}

sub tran
{
  my $self = shift;
  my $index = 0;
  $index = shift if (@_);
  return $self->{TRANS}->[$index];
}

sub trans
{
  my $self = shift;
  return $self->{TRANS};
}

sub header
{
  my $self = shift;
  return $self->{HEADER};
}

sub empty
{
  my $self = shift;
  return 1 if ($self->{HEADER}->empty());
  #return 1 if ($self->{TRANS}->[0]->empty());
  return 0;
}

sub tostr
{
  my $self = shift;
  my $str = $self->{HEADER}->tostr();
  foreach my $tran (@{$self->{TRANS}})
  {
    $str .= $tran->tostr() unless ($tran->empty());
  }
  return $str;
}

use constant COPY_ALL => 0;
use constant COPY_HEADER => 1;

sub copy
{
  my $self = shift;
  my $copy = COPY_ALL;
  $copy = shift if (@_);
  my @recs = ();
  if ($copy == COPY_ALL)
  {
    @recs = split(/\n/, $self->tostr());
  }
  elsif ($copy == COPY_HEADER)
  {
    @recs = split(/\n/, $self->{HEADER}->tostr());
  }
  my $newf = Folder->new();
  foreach my $rec (@recs)
  {
    $newf->add_rec($rec);
  }
  return $newf;
}

my @buffer = ();

use constant KEEP_ALL => 0;
use constant DISCARD_99 => 1;

sub read_folder
{
  my $file = shift;
  my $keep = KEEP_ALL;
  $keep = shift if (@_);
  my $folder = Folder->new();
  while (@buffer)
  {
    $folder->add_rec(shift(@buffer));
  }
  while (<$file>)
  {
    next if (/^\d{3}99/ && $keep == DISCARD_99);
    chomp;
    s/\000\000/ /g;
    s/\s*$//;
    next if ($folder->add_rec($_));
    push(@buffer, $_);
    last;
  }
  return $folder unless ($folder->empty());
  return undef;
}

1;

__END__

=head1 NAME

Folder - Class to represent a VTRAN entry in OMNI

=head1 SYNOPSIS

  # load the class module
  use Folder;

  # create a new folder
  $folder = Folder->new();

  # add a record to a folder
  $folder->add_rec($text);

  # add a transaction to a folder
  $folder->add_tran($tran);

  # check if a folder is empty
  if ($folder->empty()) { ... do stuff ... }

  # show the folder's associated plan number
  print $folder->plan(), "\n";

  # show the folder's name
  print $folder->name(), "\n";

  # a folder's plan and name can be set but why??
  $folder->name('SOMENAME');
  $folder->plan('XXXXXX');

  # how many transactions in the folder?
  $nt = $folder->tran_count();

  # look at first transaction
  $tran = $folder->tran(0);

  # look at last transaction
  $tran = $folder->tran(-1);

  # get the entire list of transactions
  $tranlist = $folder->trans();

  # get the header transaction
  $header = $folder->header();

  # retrieve a string representation of the folder
  # (in card format)
  print $folder->tostr(), "\n";

  # read a list of folders from an input stream
  # using provided read_folder function
  while (my $folder = Folder::read_folder($handle))
  {
    ... do stuff ...
  }

  # read a list of folders as before, but drop
  # any 99 sequences (i.e. 96699)
  while (my $folder = Folder::read_folder($handle, DISCARD_99))
  {
    ... do stuff ...
  }

  # make a copy of the folder
  $new_folder = $folder->copy();

  # make a copy of the folder, but only the header
  $new_folder = $folder->copy(COPY_HEADER);

=head1 DESCRIPTION

A folder represents a VTRAN entry in OMNI.  When a folder is
extracted from VTRAN, it is represented by a card deck containing
a 002 header transaction and a variable number of subsequent
transactions which are the folder's contents.

This class and associated read function are intended to provide easy
parsing and handling of a card deck in a read-only manner.  Although
this class can be used to construct new data, no mechanisms are provided
to create individual records.  The name and plan methods do allow
the header record information to be updated for those two items only.

A function is provided to read a folder from an input stream.
It returns a reference to a folder object (i.e. it calls Folder->new).
If the folder is empty, undef is returned.  This is how EOF is detected.
The while loops shown in the SYNOPSIS above demonstrate this.
The first argument is a reference to a file handle from which to read
records and the second optional argument specifies whether cards with
a sequence code of 99 should be kept or discarded.  The default behavior
keeps all records.  A value of 0 will cause records with a sequence
code of 99 to be discarded.
