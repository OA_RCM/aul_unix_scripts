#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxadb704.ksh            CALL            *
#COMMENT *  Script Name  : adb_taxyearchg.ksh                        *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will look for Disbursements that *
#COMMENT *                 are being run on the last business day of *
#COMMENT *                 the year and switch the tax year of the   *
#COMMENT *                 disbursement to the following year.       *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 06/22/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Scheduler                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : ADB1000U                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On Request                                *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 06/22/2001  SMR: 3279     By: Mike Lucas            U
#COMMENT U Reason: Created to run the program ADB1000U               U
#COMMENT U Date: 07/31/2006  SMR: CC3279   By: Mike Lewis            U
#COMMENT U Date: 08/06/2006   CC: 10404    By: Rick Sica             U
#COMMENT U Reason: Put report in Vista properly                      U
#COMMENT U Date: 20110317  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

######################################################################
##FUNCTIONS################   functions   ############################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

######################################################################
##MAIN##################### main section  #################SECTION####
######################################################################
export integer NumParms=$#
clear

[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=adb_taxyearchg
export JDESC="OmniPlus/UX Year-End DB Tax Year Change"
export ENVFILE=ENVBATCH

######################
## source in common functions
######################
. FUNCTIONSFILE
 
######################
## setup generic variables
## to initial values
######################
set_generic_variables

######################
## check for required parameters
## and request for help
######################
check_input

######################
## create output directories
######################
make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - set up and run process
### ******************************************************************

export dd_DBUPDRPT=$XJOB/${EBSRPTPFX}DBUPDRPT

if $runprog; then
   export dd_RSTROUT=$XJOB/rs.tmp
   export dd_DATEBUSEND=$AULDATA/DATEBUSEND

   PNAME=ADB1000U
   JDESC="Run of COBOL Program $PNAME"
   execute_program
fi

### ******************************************************************
### TASK 2 - Send Report to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 3 - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
