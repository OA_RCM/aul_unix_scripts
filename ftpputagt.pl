#!/opt/perl5/bin/perl

$PLCY="$ENV{'PLCYFILE'}";
$ADDR="$ENV{'ADDRFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
&check2;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputagt.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $PLCY`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $PLCY file to 9000 successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $PLCY file to 9000\n"; 
    }
}

sub check2
{
    $result=`$NCFTPPUT -d ftpputagt2.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $ADDR`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $ADDR file to 9000 successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $ADDR file to 9000\n"; 
    }
}
