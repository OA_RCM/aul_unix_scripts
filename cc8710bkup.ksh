#!/usr/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  ECS JOBNAME   : RSUXUTL399                CALL           *
#COMMENT *  SchedXref Name: rsuxutl399.ksh            OMNI-ASU       *
#COMMENT *  UNIX script   : cc8710bkup.ksh                           *
#COMMENT *                                                           *
#COMMENT *  Description  : Backup master files utilizing bcv splits; *
#COMMENT *                 manage OMNI ports.                        *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX - all versions.             *
#COMMENT *  Resides      : Found via PATH.                           *
#COMMENT *  Author       : AUL - SKL                                 *
#COMMENT *  Created      : 06/13/2005                                *
#COMMENT *  Environment  : Not applicable.                           *
#COMMENT *  Called by    : ECS Scheduler,child process,or manually   *
#COMMENT *  Script Calls : FUNCTIONSFILE, JOBMIRROR,                 *
#COMMENT *  COBOL Calls  : None                                      *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Multiple times per night.                 *
#COMMENT *  Est.Run Time : Half an hour to hours depending on process*
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S  This process should never be run without fully reviewing S
#COMMENT S  the OMNI environment and the data within the BCV to be   S
#COMMENT S  refreshed!                                               S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T  1 - check if selected bcv mount point is mounted         T
#COMMENT T  2 - check if selected bcv has current activity           T
#COMMENT T  3 - shutdown all ports for current OMNI region           T
#COMMENT T  4 - run script to resync selected bcv to OMNI dataset    T
#COMMENT T  5 - startup all / web ports for current OMNI region      T
#COMMENT T  6 - perform jobmirror backup in background               T
#COMMENT T  7 - perform validation of snapcopy [optional]            T
#COMMENT T  LAST - End of job and housekeeping                       T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 06/13/2005  SMR: 8573/8710    By: Steve Loper       U
#COMMENT U Reason: Initial development.                              U
#COMMENT U                                                           U
#COMMENT U Date: 08/23/2005  SMR: 8401     By: Steve Loper           U
#COMMENT U Reason: Install resync logic.                             U
#COMMENT U                                                           U
#COMMENT U Date: 11/02/2005  SMR: 8404     By: Steve Loper           U
#COMMENT U Reason: Restart CSR ports on all bcv backups unless just  U
#COMMENT U taking backup just prior to UNIFIED1 (pre2) processing.   U
#COMMENT U                                                           U
#COMMENT U Date: 01/19/2006  SMR: 9719     By: Mike Lewis            U
#COMMENT U Reason: Manage what the last bcv backup is and            U
#COMMENT U         put case statement for bcvId in .ini.             U
#COMMENT U                                                           U
#COMMENT U Date: 02/08/2006  CC:8403       By: Tony Ledford          U
#COMMENT U Reason: Convert old port syntax to new syntax for ddms v3 U
#COMMENT U                                                           U
#COMMENT U Date: 09/18/2006  CC:10670      By: Steve Loper           U
#COMMENT U Reason: Correct port up/down logic.                       U
#COMMENT U                                                           U
#COMMENT U Date: 03/10/2013  WMS: 9015 Phase III By: Steve Loper     U
#COMMENT U Reason: Standardize for use in any OMNI environment.      U
#COMMENT U Reason: Standardize use of fnc_log and fnc_exit.          U
#COMMENT U Reason: Read bcvType parms from SHSQL data file.          U
#COMMENT U Reason: Leave ports up when abend condition is detected.  U
#COMMENT U Reason: Allow for abend if unrealized EBSBKUP files.      U
#COMMENT U Reason: When files are copied to EBSBKUP, gzip them.      U
#COMMENT U Reason: Allow for exceptions to BCV backup when "true".   U
#COMMENT U                                                           U
#COMMENT U Date: 08/27/2013  WMS: 9987           By: Steve Loper     U
#COMMENT U Reason: validate snapshot prior to exit.                  U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##  functions                                                       ##
######################################################################
function check_input
{
  fnc_log "In function: $0" 
  [[ -z $bcvType ]] && { mno=3; check_mno; }
}
function check_overrides
{
  fnc_log "In function: $0" 
  typeset bcvTypeOrig=$bcvType cond=0
  get_backup_parameters || cond=$?
  [[ $cond = 99 ]] && { bcvType=adhoc; fnc_log "Default to type: adhoc"; get_backup_parameters; }
  [[ $cond = 1 ]] && fnc_exit "Cannot find either $bcvTypeOrig or adhoc entries in bcvRegions datafile."
  [[ -n $debug ]] && print "DEBUGGING INFORMATION: RECORD FROM DATAFILE"
  [[ -n $debug ]] && print "Btype\t Allow\t bcvID\t Event\t bkupYN\t SubDir\t Desc"
  [[ -n $debug ]] && print "$Btype\t $Allow\t $bcvID\t $Event\t $bkupYN\t $SubDir\t $Desc"
  [[ -n $SubDir ]] && SubDir="/$SubDir"
  export EBSBCV=/${OMNICUST}_${bcvID}_bcv
  export EBSBCV_Data=${EBSBCV}${SubDir}
  fnc_log "Using BCV: $EBSBCV. Data is in: $EBSBCV_Data."
}
function get_backup_parameters
{
  fnc_log "In function: $0" 
  unset Btype Allow bcvID Event bkupYN SubDir Desc
  get_shsql_record $sqlLog $DBfile $DBfield $bcvType
  typeset cond=$?
  if [[ $cond = 0 ]];then
     :
  elif [[ $cond = 99 ]];then
     [[ $bcvType = adhoc ]] && cond=1
  else
     fnc_exit "Input error:Check parms: DBfile[$DBfile] DBfield[$DBfield] bcvType[$bcvType]" 
  fi
  return $cond
}
function check_for_bcv_mount
{
  fnc_log "In function: $0" 
  bdf 2>/dev/null | { grep -q $EBSBCV || fnc_exit "BCV $EBSBCV not mounted!"; }
  fnc_log "BCV $EBSBCV found mounted - must resync before using it"
}
function check_for_bcv_activity
{
  fnc_log "In function: $0" 
  xx=`sudo /usr/sbin/fuser -c $EBSBCV|cut -f2 -d ":"` ## set xx to any pid(s) running in selected bcv

  ## if there is something running in bcv region then check for bkup in progress flag.
  ## if hit timeout w/o bkup finishing, abend. Else, resync and continue split operations.
  [[ -n $xx && -f $EBSOUT/$JUSER/$today/jobmirror_bkup_inProgress ]] && loop_while_bkup

  ## loop exhausted - re-check activity flag to see if resync is possible. Abend if not.
  ## write list of objects in FS mount point before abend [when abend condition exists].
  if [[ -n $xx ]]; then
     for x in $(echo $xx);do
       fnc_log "CANNOT RESYNC! The following processes are active in ${EBSBCV}!"
       ps -ef|grep $x|grep -v grep >>$JNAME.LOG
       fnc_exit "Process aborted."
     done
  else
     fnc_log "Mounted bcv inactive - can be resync'd"
  fi
}
function resync_mounted_bcv
{
  fnc_log "$0:Call refresh_bcv script to resync $bcvID"
  [[ $verify_checkFile = true ]] && set_checkFile
  sudo /scripts/omnibcv/refresh_bcv.sh $bcvID && cond=0 || cond=99
  if [[ $cond != 0 ]]; then
     fnc_log "$0:ERROR: BCV $EBSBCV resync failed! Restart ports."
     pf_restart_ports $Event
     fnc_exit "$0:ABNORMAL TERMINATION: BCV ${EBSBCV}: RESYNC FAILURE"
  fi
  [[ $Btype = post || $Btype = ENDCYCLE ]] && print "ENDCYCLE" > $LastBkupInfoFile || print "$Btype" > $LastBkupInfoFile
  print "$EBSBCV_Data" >> $LastBkupInfoFile
}
function loop_while_bkup
{
  fnc_log "In function: $0"
  integer total=0
  until [ $total -ge $limit ]
  do
     if [[ -f $EBSOUT/$JUSER/$today/jobmirror_bkup_inProgress ]]
     then
       fnc_log "Prior backup from mounted bcv still in progress"
       sleep $delay
       (( total += delay ))
     else
       xx=`sudo /usr/sbin/fuser -c $EBSBCV|cut -f2 -d ":"`
       total=$limit
     fi
  done
}
function backup_snapshot
{
  fnc_log "In function: $0"
  typeset skipIt=false
  [[ $backupBCV != true ]] && { [[ $bkupYN != Y ]] && skipIt=true; }
  [[ $skipIt = true ]] && { fnc_log "$Btype bcv: Skip data backup."; return 0; }
  take_jobmirror_backup &
}
function take_jobmirror_backup
{
  fnc_log "In function: $0"
  export EBSDATA_orig=$EBSDATA
  export EBSDATA=$EBSBCV_Data
  fnc_log "$Btype bcv: Calling JOBMIRROR to backup $EBSDATA [background process]"
  touch $EBSOUT/$JUSER/$today/jobmirror_bkup_inProgress

  PNAME=JOBMIRROR
  JDESC="Backup $EBSDATA"
  JPRM1=$JUSER
  JPRM2=$Btype
  execute_job

  rm $EBSOUT/$JUSER/$today/jobmirror_bkup_inProgress
  touch $EBSOUT/$JUSER/$today/jobmirror_bkup_${Btype}_Ok
  export EBSDATA=$EBSDATA_orig

  ## go to backup directory to set ThisBkupDir variable
  ## used when resetting daily backup pointers
  . /usr/local/bin/lastbkup
  export ThisBkupDir=$PWD
  fnc_log "In backup directory: $ThisBkupDir"

  ## if switch to zip backup files is on, zip up files in bkup directory
  if [[ $zip_bkup_files = true ]]; then
     fnc_log "Begin $maxZips parallel gzip processes to compress files."
     ls -1S|parallel -P $maxZips -t gzip
     if [[ $? = 0 ]];then
        fnc_log "Compression of files completed Ok."
     else
        typset errHdr="$EBSFSET:$Btype jobmirror backup [from $bcvID] failure"
        typset errMsg="File(s) in the backup directory were not compressed properly! Review and correct."
        fnc_log -m "$errHdr|$errMsg|$errAddr" "$errHdr"
     fi
  else
     fnc_log "Skip compression of files backed up from $bcvID [$Btype backup]."
  fi

  ## signal veritas based on switch to do so or not (settings below valid ONLY FOR PROD)!!!
  [[ $signal_veritas != true ]] && return 0

  ## populate snapshot directory and signal veritas backup for selected backup types
  case "$Btype" in
    "pre")
        veritas_signal="$veritas_path -ADD BACKUP1_READY ODAT"
        bkupPointer="Daily_PreBkup"
        set_pointer_signal_backup
        ;;
    "pre1")
        veritas_signal="$veritas_path -ADD UNF292_TO_B0008_OK ODAT"
        bkupPointer="Daily_PreBkup"
        set_pointer_signal_backup
        ;;
    "post")
        [[ $JUSER = NIGHTLY ]] && veritas_signal="$veritas_path -ADD UNF302_TO_BOMNI_OK ODAT"
        [[ $JUSER = SATNITE ]] && veritas_signal="$veritas_path -ADD BACKUP2_READY ODAT"
        [[ $JUSER = SUNNITE ]] && veritas_signal="$veritas_path -ADD BACKUP2_READY ODAT"
        bkupPointer="Daily_PostBkup"
        set_pointer_signal_backup
        ;;
  esac
}
function set_pointer_signal_backup
{
  fnc_log "In function: $0"
  cd $EBSBKUP
  typeset cond=0
  rm -f $bkupPointer || set_pointer_error removing for
  ln -s $ThisBkupDir $bkupPointer || set_pointer_error creating to
  [[ $cond = 1 ]] && return 0
  fnc_log "Send signal for Veritas BCE backup:"
  $veritas_signal && cond=0 || cond=1
  [[ $cond = 1 ]] && set_pointer_error signaling for
  [[ $cond = 1 && $abend_on_bkup_error = true ]] && fnc_exit "Backup error [${Btype}/${bcvID}]: must correct!"
  fnc_log "Success: BCE [$rundate $Btype] backup ready signal received ok."
}
function set_pointer_error
{
  fnc_log "In function: $0"
  fnc_log "Error $1 $bkupPointer $2 $Btype backup!"
  fnc_log "Backup in: $ThisBkupDir"
  cond=1
}
## executed just before sudo execution of refresh.sh
function set_checkFile
{
  cd $EBSDATA || fnc_exit "$0:cd:Could not access EBSDATA"
  date > $CheckBcvFile || fnc_exit "$0:Could not capture system date to file in EBSDATA"
  md5sum $CheckBcvFile > $XJOB/checksum.file || fnc_exit "$0:Could not capture checksum data to file in XJOB"
  fnc_log "$0:Created $CheckBcvFile and checksum.file Ok"
  cd $OLDPWD
}
## executed just before eoj_housekeeping
function verify_checkFile
{
  cd $EBSBCV_Data || fnc_exit "$0:cd:Could not access EBSBCV_Data"
  md5sum --status -c $XJOB/checksum.file || fnc_exit "$0:md5sum:$CheckBcvFile failed checksum"
  fnc_log "$0: $CheckBcvFile checksum matched!"
  rm -f $EBSDATA/$CheckBcvFile || fnc_log -m \
      "ERROR:BCV CHECK FILE|Remove $CheckBcvFile from EBSDATA|$ASUEMAIL" \
      "$0:rm:$CheckBcvFile removal failed!"
  cd $OLDPWD
}
#######################################
#### main  section
#######################################
prog=$(basename $0)
integer NumParms=$#
export bcvType=$1
export bcvType=${bcvType:-adhoc}

export JNAME=${prog%.ksh}
export JUSER=${JUSER:-$LOGNAME}
export JDESC="OMNIPLUS/AUL BCV BACKUP UTILITY"
export ENVFILE=ENVNONE
export EBSDATA_orig=$EBSDATA

. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
### TASK 1 - check if selected bcv mount point is mounted
$check_for_bcv_mount && check_for_bcv_mount
$check_for_bcv_mount || fnc_log "Skip check_for_bcv_mount"

### TASK 2 - check if selected bcv has current activity - and why
$check_for_bcv_activity && check_for_bcv_activity
$check_for_bcv_activity || fnc_log "Skip check_for_bcv_activity"

### TASK 3 - shutdown all ports for current OMNI region
$shutdown_ports && pf_stop_ports $Event
$shutdown_ports || fnc_log "Skip pf_stop_ports"

### TASK 4 - run script to resync selected bcv to OMNI dataset
$resync_mounted_bcv && resync_mounted_bcv
$resync_mounted_bcv || fnc_log "Skip resync_mounted_bcv"

### TASK 5 - startup all / web ports for current OMNI region [Only web if pre-backup]
$startup_ports && pf_restart_ports $Event
$startup_ports || fnc_log "Skip pf_restart_ports"

### TASK 6 - perform jobmirror backup in background
$backup_snapshot && backup_snapshot
$backup_snapshot || fnc_log "Skip backup_snapshot"

### TASK 7 - perform validation of snapcopy
$verify_checkFile && verify_checkFile
$verify_checkFile || fnc_log "Skip verify_checkFile"
fnc_log "BCV $EBSBCV resync'd Ok"

### TASK LAST - End of job and housekeeping
eoj_housekeeping
return 0
