#!/usr/bin/ksh
################################################################
##################### Functions ################################
################################################################
function run_utl130_process
{
 JDESC="Running UTL130 Process at $(date +%H:%M)"
 display_message nobanner
 rsuxutl130.ksh U <<EOF
$RUNDATE
$RUNDATE
****AULVIP
EOF
 cond=$?
 [[ $cond != 0 ]] && JDESC="ABEND during run of UTL130 Process"
 [[ $cond != 0 ]] && display_message nobanner
 [[ $cond != 0 ]] && exit 99
 JDESC="UTL130 Process Complete"
 display_message nobanner
}
function run_utl140_process_all
{
 for file in PRICES.*
 do
   suffix=`print ${file#PRICES.}`
   run_utl140_process $suffix
 done
}
function run_utl140_process
{
 AULtype=$1
 AULprefix=AUL
 PRCID="${AULprefix}${AULtype}"
 MNEval=$(eval "echo \$P${AULtype}_DATA" | cut -c1-5)
 REGind=$(eval "echo \$P${AULtype}_DATA" | cut -c6)
 JDESC="PROCESS TO CALC HIST PRICES FOR $PRCID SUBMITTED at $(date +%H:%M)"
 display_message nobanner
 rsuxutl140.ksh <<EOF
$RUNDATE
$AULACCT
$PRCID
$MNEval
$REGind
EOF
 cond=$?
 [[ $cond != 0 ]] && JDESC="ABEND during run of UTL140 Process"
 [[ $cond != 0 ]] && display_message nobanner
 [[ $cond != 0 ]] && exit 99
 JDESC="UTL140 Process Complete for $PRCID at $(date +%H:%M)"
 display_message nobanner
 find_output
 cat $dname/HISTT029COPYCARDS >> $XJOB/TOTAL.NEW.PRICE.HISTORY
}
function find_output
{
  cd $EBSOUT/$LOGNAME/$today
  dname=`ls -1tr|tail -1`
  export dname=$EBSOUT/$LOGNAME/$today/$dname
}
function load_new_prices
{
  for file in PRICES.*
  do
    suffix=`print ${file#PRICES.}`
    JDESC="PROCESS TO LOAD $suffix PRICES SUBMITTED at $(date +%H:%M)"
    display_message nobanner
    export EBSCARD=$XJOB
    JOBUNIC $LOGNAME $file 
  done
}
function load_prices_history
{
 JDESC="PROCESS TO LOAD HISTORICAL PRICES SUBMITTED at $(date +%H:%M)"
 display_message nobanner
 export EBSCARD=$XJOB
 JOBUNIC $LOGNAME HISTPRICES.ALL
}
################################################################
##################### main code ################################
################################################################
dir=$(cd $(dirname $0); pwd)
##. $dir/omniprof.txt

JNAME=build_price_history
JUSER=$LOGNAME
JDESC="BUILD PRICE HISTORY PROCESS"

. FUNCTIONSFILE
set_generic_variables
make_output_directories

display_message
JDESC="$JNAME submitted by $JUSER in $EBSFSET at $(date)"
display_message nobanner

## task 1 - parse ini file for job parameters
[[ -f $dir/$JNAME.ini ]] && . $dir/$JNAME.ini

## task 2 - run price extraction process
[[ $run_rsuxutl130 = true ]] && run_utl130_process
[[ $run_rsuxutl130 = true ]] && find_output || dname=$PWD

## task 3 - copy output of utl130 to XJOB for this process
cp $dname/T029COPYCARDS $XJOB
cp $AULDATA/*.list $XJOB
cd $XJOB

## task 4 - remove 002 records from utl130 output
cat T029COPYCARDS | grep -v '^002' > workfile1
JDESC="COMPLETED 002 STRIP PROCESS"
display_message nobanner

## task 5 - remove non-registered-only funds from workfile1
stripfunds.pl nonreg.list workfile1 > workfile2
JDESC="COMPLETED NONREG STRIP PROCESS"
display_message nobanner

## task 6 - create AULVPA file from workfile2
sed '1,$s/AULVIP/AULVPA/g' workfile2 > PRICES.VPA
JDESC="COMPLETED VIP-TO-VPA CHANGE PROCESS"
display_message nobanner

## task 7 - create AULVPB, AULVPC, and AULVPD files from AULVPA file
sed '1,$s/AULVPA/AULVPB/g' PRICES.VPA > PRICES.VPB
sed '1,$s/AULVPA/AULVPC/g' PRICES.VPA > PRICES.VPC
sed '1,$s/AULVPA/AULVPD/g' PRICES.VPA > PRICES.VPD
JDESC="COMPLETED VPA-TO-VPB/C/D CHANGE PROCESS"
display_message nobanner

## task 8 - remove registered-only funds from workfile1
stripfunds.pl reg.list workfile1 > workfile3
JDESC="COMPLETED REG STRIP PROCESS"
display_message nobanner

## task 9 - create AUL150 price file from workfile3
sed '1,$s/AULVIP/AUL150/g' workfile3 > PRICES.150
JDESC="COMPLETED VIP-TO-150 CHANGE PROCESS"
display_message nobanner

## task 10 - load all new AULxxx files in background
[[ $run_vtut_load = true ]] && load_new_prices
wait

[[ $run_vtut_load = true ]] && JDESC="ALL PRICE LOAD PROCESSES DONE at $(date +%H:%M)"
[[ $run_vtut_load = true ]] && display_message nobanner

## task 12 - run price history build process
[[ $run_rsuxutl140 = true ]] && run_utl140_process_all

## task 13 - remove 002 records from utl140 output
cd $XJOB
cat TOTAL.NEW.PRICE.HISTORY | grep -v '^002' > HISTPRICES.ALL

## task 14 - load all new price history in background
[[ $run_vtut_load = true ]] && load_prices_history

JDESC="COMPLETED NEW FUND HISTORY CREATE PROCESS at $(date +%H:%M)"
display_message nobanner
return
