#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXCNF704                CALL            *
#COMMENT *  UNIX POINTER : rsuxcnf704.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : cnf_withdrawal.ksh                        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce participant         *
#COMMENT *                 withdrawal confirmations                  *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 10/08/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : CNF1007R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  30 min                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Define files needed by Sungard and AUL routines.T
#COMMENT T  TASK 2 - Run the COBOL program to produce deposit        T
#COMMENT T           confirmations.                                  T
#COMMENT T  TASK 3 - Prepare files for transfer to IBM.              T
#COMMENT T  TASK 4 - Send reports to document manager.               T
#COMMENT T  TASK 5 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date:             SMR: PEN      By: Patty Wamsley         U
#COMMENT U Reason: Created to run the program CNF1007R               U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2001  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U                                                           U
#COMMENT U Date: 04/29/2002  SMR: CC2944   By: Mike Lewis            U
#COMMENT U Reason: Breakout to individual CD script.                 U
#COMMENT U                                                           U
#COMMENT U Date: 08/25/2006  SMR: CC9056   By: Mike Lewis            U
#COMMENT U Reason: Modifications for HOP                             U
#COMMENT U                                                           U
#COMMENT U Date: 07/18/2007  SMR: CC11966  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U                                                           U
#COMMENT U Date: 04/16/2009 WMS: 1943      By: Gary Pieratt          U
#COMMENT U Reason: Vista-only confirmations                          U
#COMMENT U                                                           U
#COMMENT U Date: 05/26/2009 WMS: 2464      By: Gary Pieratt          U
#COMMENT U Reason: Copy input file to CIFS                           U
#COMMENT U                                                           U
#COMMENT U Date: 20110318  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function run_program
{
  cd $XJOB
  if [[ -s $dd_WITHDRAWAL ]]
  then
    PNAME=CNF1007R
    JDESC="Run $PNAME to produce participant withdrawal confirmations"
    execute_program
  fi
}
function prepare_files_for_transfer
{
  JDESC="Preparing Participant Withdrawal Confirmations for Transfer"
  display_message nobanner

### WMS 1943 begin
  export FRFILE=$1
  export V=$2
  create_file
### WMS 1943 end

  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying Participant Withdrawal Confirmation report to $AULOUT for ftp job "
    display_message nobanner
    ##cat $AULDATA/jcl/$JCL >> $XJOB/$CDFILE   ### Not needed by ControlM ...rml
    cat $XJOB/$FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No Withdrawal Confirmation report created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH120W
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP120W
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ120W
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"

### WMS 1943 begin
  if $V ; then
     CDFILE="${CDFILE}V"
  fi
### WMS 1943 end

  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
### WMS 1943 begin
function send_file_to_nt_informatica
{
  cond=0
  JDESC="Copy $JOUTPUT to shared path"
  display_message nobanner
  case $EBSFSET in
    "hop")
      cp $JOUTPUT $HOPTGT || cond=98
    ;;
    *)
      cp $JOUTPUT $TGT || cond=99
    ;;
  esac
  case $cond in 
    "0")
      JDESC="Sucessfully sent file $JOUTPUT to NT"
      display_message nobanner
    ;;
    "98")
      JDESC="ERROR copying $JOUTPUT to shared path $HOPTGT"
      display_message nobanner
      return $cond
    ;;
    "99")
      JDESC="ERROR copying $JOUTPUT to shared path $TGT"
      display_message nobanner
      return $cond
    ;;
  esac
  display_message nobanner
}
### WMS 1943 end
function end_of_job
{
  cd $XJOB
  eoj_housekeeping
  copy_to_masterlog
  create_html
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Printing Participant Withdrawal Confirmations"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

cd $XJOB
#######################################################################
### TASK 1 - Define files needed by Sungard and AUL routines
#######################################################################
cp $EBSCTRL/DUMBTRAN.DAT $XJOB
cp $EBSCTRL/DUMBTRN2.DAT $XJOB
touch $XJOB/DUMMY3.DEF
export dd_RSTROUT=$XJOB/rs.tmp
export dd_TRANIN=$XJOB/DUMBTRAN.DAT
export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
if [ "$JSTN" = "yes" ]; then
  export dd_WITHDRAWAL=$AULOUT/withdrawal.${JUSER}
else
  export dd_WITHDRAWAL=$AULOUT/withdrawal.txt
fi

export dd_WDCNFRPT=WDCNFRPT
### WMS 1943 begin
export dd_WDCNFRPTV=WDCNFRPTV
### WMS 1943 end
export dd_WDCNFERR=${EBSRPTPFX}WDCNFERR

######################################################################
### TASK 2 - Run the COBOL program to produce deposit confirmations
######################################################################

$run_program && run_program

######################################################################
### TASK 3 - Prepare files for transfer to IBM
######################################################################

### WMS 1943 begin
$prepare_files && prepare_files_for_transfer $dd_WDCNFRPT false
$prepare_files && prepare_files_for_transfer $dd_WDCNFRPTV true
### WMS 1943 end

######################################################################
### TASK 4 - Send reports to document manager.
######################################################################

$send2DocMgr && fnc_send2DocMgr

export JOUTPUT=$dd_WITHDRAWAL
$send_to_nt && send_file_to_nt_informatica

################################################################
### TASK 5 - clean up and say goodbye
################################################################
end_of_job
return 0
