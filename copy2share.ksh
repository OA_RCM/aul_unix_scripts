#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  Cntl-M Job Name : N/A                       N/A          *
#COMMENT *  UNIX Job Pointer: N/A                       N/A          *
#COMMENT *  UNIX Script Name: copy2share.ksh                         *
#COMMENT *                                                           *
#COMMENT * DESCRIPTION:                                              *
#COMMENT * this script will simply capture two input parms and then  *
#COMMENT * copy 1st name to 2nd name (location). the two parms are:  *
#COMMENT *   [ 1 ] name of file - fully qualify if not in current    *
#COMMENT *         XJOB location.                                    *
#COMMENT *   [ 2 ] fully qualified name of target location - must be *
#COMMENT *         accessible by calling job (owner)                 *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.80+                       *
#COMMENT *  Resides      : Found via PATH                            *
#COMMENT *  Author       : AUL - SKL                                 *
#COMMENT *  Created      : 05/12/2010                                *
#COMMENT *  Environment  : As selected; no envxxxx file              *
#COMMENT *  Called by    : Utility job called manually or by parent  *
#COMMENT *                                                           *
#COMMENT *  Script Calls : FUNCTIONSFILE                             *
#COMMENT *                                                           *
#COMMENT *  COBOL Calls  : None                                      *
#COMMENT *  Frequency    : Determined by end user                    *
#COMMENT *  Est.Run Time : less than 1 minute                        *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions : None                              S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE: ----------------------------------------- T
#COMMENT T  Input - Perform initial logic checks of input parms      T
#COMMENT T  001 - copy specified file to location copy_routine       T
#COMMENT T  End - end process and housekeeping                       T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U Update History:                                           U
#COMMENT U Date: 05/12/2010   WMS: 3479     By: Steve Loper          U
#COMMENT U Reason: Initial version.                                  U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
##############################################################################
####FUNCTIONS############# functions section #####################SECTION#####
##############################################################################
##############################################################################
## typically, overrides are checked after input, but since logfile is used in
## input function, then need to check for XJOB override now.
##############################################################################
function check_overrides
{
  return 0
}
function check_input
{
  cd $XJOB
  rm -rf $logfile
  export cond=0 fcond=0 dcond=0
  ## show input parameters
  print "Filename to copy is: $fname" | tee -a $logfile
  print "Target directory is: $tgtdir" | tee -a $logfile

  ## check input parameters
  [[ $NumParms -lt 2 ]] && { print "Not enough parms on call" | tee -a $logfile; cond=99; }
  [[ -z $fname ]] && { print "Filename not provided" | tee -a $logfile; fcond=99; }
  [[ -d $fname ]] && { print "[Type Error] Filename is a directory" | tee -a $logfile; fcond=99; }
  [[ -r $fname ]] || { print "Filename not found or is not readable" | tee -a $logfile; fcond=99; }
  [[ -z $tgtdir ]] && { print "Target directory not provided" | tee -a $logfile; dcond=99; }
  [[ -f $tgtdir ]] && { print "[Type Error] Target directory is a file" | tee -a $logfile; dcond=99; }
  [[ -d $tgtdir ]] || { print "Target directory not found or not readable" | tee -a $logfile; dcond=99; }
  [[ $dcond = 0 ]] && { touch $tgtdir/$ppidTag 2>/dev/null && rm -f $tgtdir/$ppidTag || dcond=88; }
  [[ $dcond = 88 ]] && { print "Directory not writable" | tee -a $logfile; dcond=99; } 
  [[ $dcond != 0 || $fcond != 0 ]] && cond=99
}
function copy_routine
{
  JDESC="Too many Errors! Abort copy operation!"   ## assume early errors
  if [[ $cond = 0 ]]
  then
     cp $fname $tgtdir && cond=0 || cond=99
     [[ $cond = 0 ]] && JDESC="Copy Successful" || JDESC="ERROR during copy process!"
  fi
  print "$JDESC" | tee -a $logfile
  [[ $cond != 0 ]] && error_routine
}
function error_routine
{
  ## send message to OPS for ticket creation 
  ## DO NOT SEND BAD RETURN CODE TO CALLING PROCESSES
  [[ -z $ADDR ]] && return 0
  print >> $logfile
  print "*******************  MESSAGE TO OPERATIONS GROUP  *******************************" | tee -a $logfile
  print "  Please create alteris ticket and assign to OMNI_ASU group.  Please paste       " | tee -a $logfile
  print "  contents of this email into body of ticket.                                    " | tee -a $logfile
  print "  Thank you.                                                                     " | tee -a $logfile
  print "*********************************************************************************" | tee -a $logfile
  cat $logfile | mailx -s "ERROR: copy2share: [$fname]" $ADDR
}
##############################################################################
####MAIN##################   main section   ######################SECTION#####
##############################################################################
prog=$(basename $0 .ksh)
integer NumParms=$#
ppid=$$; ppidTag=tmp${ppid}
export fname=$1
export tgtdir=$2

export JNAME=$prog
export JUSER=${JUSER:-$LOGNAME}
export MKOUTDIR=current

. FUNCTIONSFILE
set_generic_variables
make_output_directories
check_overrides
check_input

########################
### Processing logic ###
########################
### TASK 1 - copy specified file to location
copy_routine

### TASK End - end process and housekeeping
return 0
