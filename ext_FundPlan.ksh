#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : RSUXEXT001             CALL            *
#COMMENT *  Schedule Name   : rsuxext001.ksh                         *
#COMMENT *  Unix Script     : ext_FundPlan.ksh                       *
#COMMENT *                                                           *
#COMMENT *  Description  : Omni - SEC Rule 22-C2 Extract             *
#COMMENT *                 Plan Investments to an index file         *
#COMMENT *                 ordered by fund-id                        *
#COMMENT *  Author       :                                           *
#COMMENT *  Created      : 03/11/2007                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : EXT1020E.CBL                              *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily, On Demand                          *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 06/30/2009  WMS02451      By: Louis Blanchette      U
#COMMENT U Reason: Rebuild idx file as text and copy to Informatica  U
#COMMENT U                                                           U
#COMMENT U Date: 09/13/2013  WMS08043      By: Louis Blanchette      U
#COMMENT U Reason: Update the sort because of changes to INVFUNDPLAN U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME="ext_FundPlan"
export JDESC="OMNIPLUS/UX SEC RULE 22-C2 PRE-PROCESSOR MODULE"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Create the file
### ******************************************************************
export dd_RSTROUT=$XJOB/rs.tmp
export dd_INVESTIDX=$XJOB/INVFUNDPLANX
PNAME=EXT1020E
JDESC="run COBOL program $PNAME"
execute_program

### ******************************************************************
### TASK 2 - Move ISAM file to $AULDATA
### ******************************************************************
[[ -s $dd_INVESTIDX ]] && mv $dd_INVESTIDX $AULDATA/INVFUNDPLANX

### ******************************************************************
### TASK 3 - REBUILD ISAM FILE AS TEXT FILE
### ******************************************************************
rebuild $AULDATA/INVFUNDPLANX,$AULDATA/INVFUNDPLAN.NEW -o'IND,LSEQ'

### ******************************************************************
### TASK 4 - CHANGE NULLS TO NO-SPACE
### ******************************************************************
perl -pe 's/\000\000//g;s/([\x80-\xff])/ /g' $AULDATA/INVFUNDPLAN.NEW > $AULDATA/INVFUNDPLAN.TXT

### ******************************************************************
### TASK 5 - SORTING THE TEXT FILE BY PLAN AND INVESTMENT
### ******************************************************************
#sort -o $AULDATA/INVFUNDPLAN.NEW -k .3,.8 -k .1,2 $AULDATA/INVFUNDPLAN.TXT
sort -o $AULDATA/INVFUNDPLAN.NEW -k .5,.10 -k .1,4 $AULDATA/INVFUNDPLAN.TXT

### ******************************************************************
### TASK 6 - FORMAT FILE FOR DOS/WINDOWS
### ******************************************************************
ux2dos  $AULDATA/INVFUNDPLAN.NEW > $AULDATA/INVFUNDPLAN.TXT
rm $AULDATA/INVFUNDPLAN.NEW

### ******************************************************************
### TASK 7 - Copy file to informatica
### ******************************************************************
$send_to_nt && cp $AULDATA/INVFUNDPLAN.TXT $TGT

### ******************************************************************
### TASK 8 - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
