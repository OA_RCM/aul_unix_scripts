#!/usr/bin/ksh
rptname=$EBSPROC/work/$1
rm -rf $rptname
for dname in /usr/local/bin $EBSMSTRSCPT $EBSMSTRCALC $EBSMSTRPROC $EBSSCPT $EBSCALC $EBSPROC
do
  print "Checking $dname"
  print "***** IN [ $dname ] *********" >> $rptname
  cd $dname
  for file in *
  do
    while read pname
    do
      pname=$(awk -F "." '{print $1}')
      grep -q "$pname" $file && grep '^#COMMENT' $file >> $rptname
    done<$EBSPROC/findUTVADR.srt
  done
  print "============================================" >> $rptname
done
banner DONE
return 0
