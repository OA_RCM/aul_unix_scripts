#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : RSUXACT001             NON-CALL        *
#COMMENT *  Schedule Name   : rsuxact001.ksh         OMNI-ASU        *
#COMMENT *  Unix Script     : act_sfr.ksh                            *
#COMMENT *                                                           *
#COMMENT *  Description  : Shadow fund audit report creation         *
#COMMENT *                                                           *
#COMMENT *  Author       : LQB                                       *
#COMMENT *  Created      : 06/18/2009                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  : ACT2000R.CBL                              *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily, On Demand                          *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : <1m                                       *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T    1 - Run COBOL program to create the Report             T
#COMMENT T    2 -                                                    T
#COMMENT T    3 - Standard EOJ housekeeping tasks                    T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U  Date: 06/18/2009  WMS: 01763    By: Louis Blanchette     U
#COMMENT U  Reason:  New                                             U
#COMMENT U                                                           U
#COMMENT U  Date: 20110317   Proj: WMS3939  By: Paul Lewis           U
#COMMENT U  Reason: Standardize document manager calls to a function.U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
###   function declarations
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function create_report
{
   PMSG="function create_report"
   export dd_RSTROUT=$XJOB/rs.tmp
     
   PNAME=ACT2000R
   # STBVL layout
   # PLAN-NUM         PIC X(06). 
   # PART-NUM         PIC 9(09). 
   # AS-OF-DATE       PIC 9(08).
   # INVESTMENT       PIC X(02).
        
   JDESC="run COBOL program $PNAME"
   execute_program
         
   JDESC="Completed $PMSG "
   display_message nobanner
   unset PMSG
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS SHADOW FUND REPORTING MODULE"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

###########################
# Move to the JOB directory
###########################
cd $XJOB

### **********************************************************************
### TASK 1 - Gather requests
### **********************************************************************

   export dd_STBVL=$XJOB/STBVL
   rptlist=/tmp/stbvllist$$
   export JDESC="gathering control files from $WIZARDDIR"
   display_message nobanner
     
   ls $WIZARDDIR | grep STBVL >$rptlist
   if [[ -s $rptlist ]]; then
      #
      # sleep just to make sure no FTP session is going on any of the files
      #
      sleep 5
      for f in $(cat $rptlist) ; do
         dos2ux $WIZARDDIR/$f >$XJOB/$f
         $delete_files && rm $WIZARDDIR/$f
         cat $XJOB/$f >>$dd_STBVL
      done
      sort -o ${dd_STBVL}.tmp -k .1,.25 $dd_STBVL
      cp ${dd_STBVL}.tmp $dd_STBVL
   else
      echo
      echo No Wizard requests to process
      export JDESC="No requests to process"
      display_message 
   fi
   rm -f $rptlist

### **********************************************************************
### TASK 2 - Run COBOL program to create transactions.
### **********************************************************************
if [[ -s $dd_STBVL ]]; then
   create_report
fi   

### ******************************************************************
### TASK 3 - Send report to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 4 - Standard EOJ housekeeping tasks
### ******************************************************************
JDESC="Process complete !"
display_message nobanner
eoj_housekeeping
return 0
