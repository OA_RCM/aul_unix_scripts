#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : ftp_put.ksh                 CALL          *
#COMMENT *                                                           *
#COMMENT *                                             ASU           *
#COMMENT *                                                           *
#COMMENT *  Description  : This job send a file to the HP3000        *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 07/12/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : inf_fundperf_send.ksh, ...                *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Depends on what calls it                  *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : Depends on size of file                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S  If run manually as a stand-alone, must give it a first   S
#COMMENT S  parameter of the original XJOB/name of file and a second S
#COMMENT S  parameter of the to file name and a third parmeter for   S
#COMMENT S  record length of the to file.                            S
#COMMENT S                                                           S
#COMMENT S  The to file must have a different name then the from fileS
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time.      R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 07/12/2004  CC: 6054      By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 08/02/2004  CC: 6054      By: Rick Sica             U
#COMMENT U Reason: Change to cause ftp to use rhost file             U
#COMMENT U Date: 09/25/2007  CC12197   By: Zera Holladay             U
#COMMENT U Reason: Changed insecure protocols to secure.             U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 3 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
clear
######################
## setup standard local variables
######################
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=ftp_put 
export JDESC="OMNIPLUS/UX FTP PUT FILE"
export ENVFILE=ENVBATCH
   
######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables
 
######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input 

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1 - copy file to HP3000 
### ******************************************************************
putfile=$1
if [[ -s $putfile ]]; then
  tofile=$2
  tofilereclen=$3

  if [[ $OMNIsysID = "prod" ]] ; then
    hp3000sys=aulhpb
  else
    hp3000sys=aulhpa
  fi

/usr/bin/ftp $hp3000sys  <<EOF
verbose
put $putfile $tofile;rec=-$tofilereclen,,f,ascii
get $tofile
quit
EOF
 
### ******************************************************************
### TASK 2 - check if was completed okay
### ******************************************************************
  cksum1=`cksum $putfile | cut -f1 -d" "`
  if [[ -s $tofile ]]; then
    cksum2=`cksum $tofile | cut -f1 -d" "`
    if [ $cksum1 != $cksum2 ] ; then
      JDESC="ftp of $putfile file failed; files still different on both boxes"
      display_message nobanner
      return 99
    fi
  else
    JDESC="ftp of $putfile file failed; $tofile not found"
    display_message nobanner
    return 99
  fi

### ******************************************************************
### TASK 3 - send a done file so other system knows when process is done
###        - the other system has responsibility of delete the done file
###          when the process on its side is done.
### ******************************************************************
  donefile7=`echo $tofile | cut -c1-7`
  donefile="${donefile7}D"
  [[ $donefile = $tofile ]] && donefile="${donefile7}2"
  print "DONE" > $donefile

/usr/bin/ftp $hp3000sys  <<EOF
verbose
put $donefile $donefile
quit
EOF

else
  JDESC="ftp from file $putfile does not exist"
  display_message nobanner
  return 99
fi

### ******************************************************************
### TASK 4 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
