#!/bin/ksh
#xOMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXEXP702                CALL            *
#COMMENT *  UNIX POINTER : rsuxexp702.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : exp_allbills.ksh                          *
#COMMENT *                                                           *
#COMMENT *  Description  : Expense Bill Report to IBM for printing   *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 11/19/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxexp702.ksh                            *
#COMMENT *  Script Calls : JOBDEFINE JOBCALC                         *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Create Expense Bill Report                      T
#COMMENT T  TASK 2 - Prepare file for transfer                       T
#COMMENT T  TASK 3 - Send reports to document manager.               T
#COMMENT T  TASK 4 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date:02/26/2003   SMR: CC4213   By:  MIKE LEWIS           U
#COMMENT U Reason: Created to print the MONTHLY Expense Bills on the U
#COMMENT U         IBM                                               U
#COMMENT U Date:11/02/2005   SMR: CC8110   By:  Rick Sica            U
#COMMENT U Reason: Rewrite - Functionality for CC does not work the  U
#COMMENT U         way this script runs the OmniScript               U
#COMMENT U Date: 09/25/2007  SMR: CC12194  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U                                                           U
#COMMENT U Date: 20110318  Proj:  WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT U Date: 20111205  Proj:  WMS6887  By: Danny Hagans          U
#COMMENT U Reason: Modify code to send EXP2451A to document mgr.   . U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function create_report
{
	rm  $AULOUT/EXP2451A
	touch  $AULOUT/EXP2451A
  export JDESC="Call JOBCALC to create Expense Bill Report"
  display_message nobanner
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=EXP-ALLBILL.txt
  JDESC="Run JOBCALC $JPRM2 at $(date +%H:%M)"
  execute_job

  JDESC="JOBCALC $JPRM2 complete at $(date +%H:%M)"
  display_message nobanner
}
function prepare_file_for_transfer
{
  JDESC="Preparing Expense Bills for Transfer"
  display_message nobanner

#  export FRFILE=EXP2451A
  export FRFILE=${EBSRPTPFX}EXP2451A

  if [ -s $AULOUT/EXP2451A ]
     then
       cp $AULOUT/EXP2451A $FRFILE
  fi

  create_file
  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying Expense Bill Report to $AULOUT for ftp job "
    display_message nobanner
    cat $XJOB/$FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
#    if [[ $OMNIsysID = "dev" ]]; then
#       cp EXP2451A $XJOB/${EBSRPTPFX}EXP2451A
#    fi
  else
    JDESC="No Expense Bill report created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH1500
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP1500
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ1500
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"
  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}

function send2DocMgr
{
  fnc_send2DocMgr  $XJOB/${EBSRPTPFX}EXP2451A
  export JDESC="Expense bills sent to vista - ${EBSRPTPFX}EXP2451A"
  display_message nobanner
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JGROUP=${JGROUP:-01}
export JDESC="OMNIPLUS/UX Monthly Expense Bill Creation"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start
check_input
. JOBDEFINE

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_monthend_date

cd $XJOB

### ******************************************************************
### TASK 1 - Create Expense Bill Report
### ******************************************************************

$create_report && create_report

### ******************************************************************
### TASK 2 - Prepare file for transfer
### ******************************************************************

$prepare_file && prepare_file_for_transfer

### ******************************************************************
### TASK 3 - Send reports to document manager.
### ******************************************************************

$send2DocMgr && send2DocMgr

### ******************************************************************
### TASK 4 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
