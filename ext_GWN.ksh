#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTM Job Name  : RSUXEXT711               NO CALL         *
#COMMENT *  SchedXref Name: rsuxext711.ksh                           *
#COMMENT *  Script Name   : ext_GWN.ksh              OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce transaction extract *
#COMMENT *		   for the TPA GWN (Kades)                             *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 05/11/2009                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : TPA1000E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily                                     *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 05/11/2009  #: W2370     By: Rick Sica              U
#COMMENT U Reason: Original                                          U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################

function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

############################################################################
###   main section
############################################################################
prog=$(basename $0)
integer NumParms=$#

### ******************************************************************
### define job variables
### ******************************************************************
  export JUSER=${JUSER:-$LOGNAME}
  export JNAME=${prog%.ksh}
  export ENVFILE=ENVBATCH
  export AULOUT=$POSTOUT
  export JDESC="Create Extracts for GWN"

### ******************************************************************
### Standard script setup
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
check_input
. JOBDEFINE ## standard omniplus master file/environment definition script
fnc_log_standard_start
cd $XJOB

### ******************************************************************
### TASK 1 Get the parameters needed to produce the reports
### ******************************************************************
  export tpatpa=7
  export tpaend=${TPAEND:-$rundate}
  export tpabeg=${TPABEG:-$rundate}
  export PARM=$tpatpa$tpabeg$tpaend

### ******************************************************************
### TASK 2 Run the COBOL program to produce TPA Extracts
### ******************************************************************
  export dd_TPATRANO=$XJOB/GWNTRANO

  export PNAME=TPA1000E
  export JDESC="Run COBOL program $PNAME"
  execute_program

  cp $dd_TPATRANO $pickupLoc/GWN_AUL_${rundate}.psv

### ******************************************************************
### TASK 3 clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
