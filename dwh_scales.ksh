#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *  CNTL-M Job Name   : RSUXDWH004              NON-CALL               *
#COMMENT *  UNIX Pointer Name : rsuxdwh004.ksh          OMNI-ASU               *
#COMMENT *  UNIX Script Name  : dwh_scales.ksh                                 *
#COMMENT *                                                                     *
#COMMENT *  Runtime syntax    : dwh_scales.ksh                                 *
#COMMENT *  Description       : Run DWH-SCALES.txt. No input parms.            *
#COMMENT *                                                                     *
#COMMENT *  Author            : AUL - Gary Pieratt                             *
#COMMENT *  Created           : 05/23/2011                                     *
#COMMENT *  Called by         : Scheduled or Manually.                         *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       : None.                                          *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Daily                                          *
#COMMENT *  Est. Run Time     : 15 min.                                        *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S  None.                                                              S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Must be reviewed by ASU support personnel prior to restart.        R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT T TASK SEQUENCE:                                                      T
#COMMENT T    1 - Run Omniscript program to extract data                       T
#COMMENT T    2 - Build single extract file without nulls                      T
#COMMENT T    3 - Copy single extracted datafile to share directory            T
#COMMENT T LAST - Standard end-of-job processes                                T
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Gary Pieratt     WMS5501   05/18/2011  original                    U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   12/21/2011                              U
#COMMENT U  Reason: correct header. simplify CPATH determination. Add error    U
#COMMENT U  Reason: detection to sed/consolidate and copy (to cifs) tasks.     U
#COMMENT U  Reason: run JOBCALC as child to dwh_scales XJOB.                   U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   01/06/2012                              U
#COMMENT U  Reason: remove debugging code.                                     U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   01/09/2012                              U
#COMMENT U  Reason: allow for override of EBSDATA (source).                    U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   03/06/2012                              U
#COMMENT U  Reason: add functionality to split HOP and Prod recs; for testing, U
#COMMENT U          manipulate EBSFSET to send HOP file to test hop cifs.      U
#COMMENT U          Use fnc_log & fnc_exit functions to standardize messaging. U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   04/02/2012                              U
#COMMENT U  Reason: Add error checking to extract process.                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
################################################################################
#### FUNCTIONS ####### functions section ##################### SECTION #########
################################################################################
function check_input
{
   return 0 ## future use and standards
}
function check_overrides
{
   export EBSDATA=$EBSDATAovrd
   fnc_log "Derive data from: $EBSDATA"
   export MKOUTDIR=child
}
function extract_data
{
  fnc_log "In function $0 to run $OSname"
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=$OSname
  JDESC="Running $JPRM2..."
  display_message
  execute_job || fnc_exit "$OSname data extract failed."
  fnc_log "Return from function $0 Ok"
}
################################################################################
####   MAIN    #######   main  section   ##################### SECTION #########
################################################################################
export integer NumParms=$#
export JNAME=$(basename $0 .ksh)
export JUSER=${JUSER:-$LOGNAME}
export JDESC="ONEAMERICA DWH Scales Extraction Process"
export ENVFILE=ENVNONE

# standard script setup
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
##############################################
# TASK 1 - Run Omniscript program to extract data
##############################################
$extract_data && extract_data
$extract_data || fnc_log "extract_data function: OFF"

##############################################
# TASK 2 - build single extract file without nulls
##############################################
$sed_and_consolidate && dwh_sed_and_consolidate ${tem_name_out}
$sed_and_consolidate || fnc_log "filter and consolidate function: OFF"

##############################################
# TASK 3 - copy single extracted datafile to share directory
##############################################
$copy_file && dwh_copy_file $EXTFILE
$copy_file || fnc_log "copy EXTFILE file to cifs: OFF"

##############################################
# TASK LAST - Standard end-of-job processes
##############################################
touch rs.tmp
eoj_housekeeping
return 0
