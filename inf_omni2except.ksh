#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS Job Name : RSUXINF706                CALL            *
#COMMENT *  Unix Job Name: rsuxinf706.ksh                            *
#COMMENT *  Script Name  : inf_omni2except.ksh       OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job gets participant data from ExAcct*
#COMMENT *                 plan located in OmniPlus.  This data will *
#COMMENT *                 then be picked up by ExCCEPT.             *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $EBSPROC                                  *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 09/25/2000                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : INF1000E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  5 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions : IN THE EVENT THAT THIS JOB FAILS, S
#COMMENT S                         PLEASE ENSURE THAT THE TIMESTAMPS S
#COMMENT S                         OF THE FILES IN THE               S
#COMMENT S                         $EBSARCH/EXCfiles DIRECTORY DO    S
#COMMENT S                         NOT GET ALTERED IN ANY WAY !!!    S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T --------------------------------------------------------- T
#COMMENT T 1 - Run COBOL program; extract basic participant info     T
#COMMENT T 2 - Find/Unzip prev bus day archived ExACCT TOE/TOS files T
#COMMENT T 3 - Run COBOL pgm to write participant extract diffs file T
#COMMENT T 4 - Run COBOL pgm to write part src extract diff file     T
#COMMENT T 5 - Copy extracts to xfer directory for pickup            T
#COMMENT T F - Clean up and say goodbye                              T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2000  SMR: PEN3224  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program INF1000E               U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2001  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U                                                           U
#COMMENT U Date: 10/23/2002  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Clean up of static information.                   U
#COMMENT U                                                           U
#COMMENT U Date: 03/30/2005  SMR: CC7661   By: Mike Lewis            U
#COMMENT U Reason: Added a second Cobol program INF2000E;            U
#COMMENT U         Concantenate two seq files;                       U
#COMMENT U         Sort new sequential history file.(Revoked)        U
#COMMENT U                                                           U
#COMMENT U Date: 07/21/2005  SMR: CC7661   By: Dave Laski            U
#COMMENT U Reason: Eliminated Cobol program INF2000E                 U
#COMMENT U         due to missing 801 transactions                   U
#COMMENT U         in sequential history file.                       U
#COMMENT U         Added new programs to create small differences    U
#COMMENT U         files containing additions and changes from       U
#COMMENT U         previous business day's large extracts            U
#COMMENT U                                                           U
#COMMENT U  Date: 07/08/2008  PROJ00000680  By: Zera Holladay        U
#COMMENT U  Reason: Need to replace Reflections as the file transfer U
#COMMENT U  tool for ExCCEPT.                                        U
#COMMENT U                                                           U
#COMMENT U  Date: 11/11/2008  CC 12454      By: Louis Blanchette     U
#COMMENT U  Reason: Added error report from INF1000E sent to Vista   U
#COMMENT U                                                           U
#COMMENT U  Date: 03/27/2009  PROJ0001856   By: Zera Holladay        U
#COMMENT U  Reason: Remove EOF character and add '.txt' extension.   U
#COMMENT U                                                           U
#COMMENT U  Date: 20110318   Proj: WMS3939  By: Paul Lewis           U
#COMMENT U  Reason: Standardize document manager calls to a function.U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################

function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function archive_files
{
  saveDir=$PWD
  cd $archDir
  for label in $archList; do
      if [ -f $label ]; then
          mv $label $EBSTMPDIR/ || { JDESC="Problem with Archiving file in CIFS"; display_message nobanner; exit 99; }
      fi
  done
  export archDir=$EBSTMPDIR # Changed due to CIFS issue with gzip
  cd $archDir
  for label in $(ls $archList)
  do
     mv $label ${label}.$rundate.$tmptime
     archive ${label}.$rundate.$tmptime
     JDESC="$archName Completed"
     display_message nobanner
  done
  cd $saveDir
}

function extract_part_data
{
  PNAME=INF1000E
  JDESC="Extract all Participant Data"
  export dd_EXACCTOP=$XJOB/EXACCTOP.tmp
  export dd_EXACCTOC=$XJOB/EXACCTOC
  export dd_EXACCTOE=$XJOB/OEXACCTOE   ## original big extract
  export dd_EXACCTOS=$XJOB/OEXACCTOS   ## original big extract
  export dd_EXACCTOL=$XJOB/EXACCTOL
  export dd_EXRPTERR=$XJOB/Omni2ExcceptRpt.tmp
  export dd_RSTROUT=$XJOB/rs.tmp
  execute_program

  sort -o $XJOB/EXACCTOP -k .1,.6 $XJOB/EXACCTOP.tmp
  [[ -f $XJOB/EXACCTOP ]] && rm -f $XJOB/EXACCTOP.tmp
  
  sort -o $XJOB/${EBSRPTPFX}Omni2ExcceptRpt -k .1,.25 $XJOB/Omni2ExcceptRpt.tmp
  [[ -f $XJOB/${EBSRPTPFX}Omni2ExcceptRpt ]] && rm -f $XJOB/Omni2ExcceptRpt.tmp
  
}

function find_prev_EXCfiles
{
  export cond=0
  cd $oldCopyDir
  export lastOEXACCTOE=`ls -1tr OEXACCTOE.txt.20*|tail -1`
  export lastOEXACCTOS=`ls -1tr OEXACCTOS.txt.20*|tail -1`
  [[ -n $lastOEXACCTOE ]] && unzip_previous $lastOEXACCTOE || cond=88
  [[ -n $lastOEXACCTOS ]] && unzip_previous $lastOEXACCTOS || cond=88
  [[ $cond = 0 ]] && lastOEXACCTOE=${lastOEXACCTOE%.gz}
  [[ $cond = 0 ]] && lastOEXACCTOS=${lastOEXACCTOS%.gz}
  cd $OLDPWD
  case "$cond" in
    "0")
       JDESC="Found-unzipped last TOE file: $lastOEXACCTOE"
       display_message nobanner
       JDESC="Found-unzipped last TOS file: $lastOEXACCTOS"
       display_message nobanner
       export dd_OEXCEPTO=$oldCopyDir/$lastOEXACCTOE
       export dd_OEXCEPTOSS=$oldCopyDir/$lastOEXACCTOS
       return 0
       ;;
    "88")
       JDESC="ERROR: Could not find last TOE and/or TOS file(s)"
       display_message nobanner
       exit 99
       ;;
    "99")
       JDESC="ERROR: Could not unzip last TOE and/or TOS file(s)"
       display_message nobanner
       exit 99
       ;;
  esac
}

function unzip_previous
{
  fnameLong=$1
  cond=0
  fnameExt=$(print $fnameLong | awk -F "." '{ print $NF }')
  [[ $fnameExt = gz ]] && \
       { JDESC="Unzipping $fnameLong"; display_message nobanner; }
  [[ $fnameExt = gz ]] && { gunzip -v $fnameLong || cond=99; }
  return $cond
}

function create_part_diffs_file
{
  PNAME=INF3000E
  JDESC="Create participant diffs file"
  export dd_OEXACCTOENS=$dd_EXACCTOE
  export dd_OEXACCTOEOS=$dd_OEXCEPTO
  export dd_MISMATCH=$XJOB/EXACCTOE
  execute_program
}

function create_partsrc_diffs_file
{
  PNAME=INF4000E
  JDESC="Create participant source diffs file"
  export dd_OEXACCTOENSS=$dd_EXACCTOS
  export dd_OEXACCTOEOSS=$dd_OEXCEPTOSS
  export dd_MISMATCHS=$XJOB/EXACCTOS
  execute_program
}

function copy_files_for_transfer
{
  cond=1
  if [ -d $EBSCNV ]
  then
     cond=0 
     ux2dos EXACCTOC | perl -pe 's/\032//g;' > $EBSCNV/EXACCTOC.txt || cond=99
     ux2dos EXACCTOE | perl -pe 's/\032//g;' > $EBSCNV/EXACCTOE.txt || cond=99
     ux2dos EXACCTOL | perl -pe 's/\032//g;' > $EBSCNV/EXACCTOL.txt || cond=99
     ux2dos EXACCTOP | perl -pe 's/\032//g;' > $EBSCNV/EXACCTOP.txt || cond=99
     ux2dos EXACCTOS | perl -pe 's/\032//g;' > $EBSCNV/EXACCTOS.txt || cond=99
     ux2dos OEXACCTOE | perl -pe 's/\032//g;' > $EBSCNV/OEXACCTOE.txt || cond=99
     ux2dos OEXACCTOS | perl -pe 's/\032//g;' > $EBSCNV/OEXACCTOS.txt || cond=99
     
  fi
  
  case "$cond" in
    "99") JDESC="ERROR copying files to $EBSCNV! Check!" 
          display_message nobanner
          exit $cond
          ;;
    "0") JDESC="Successfully copied files to $EBSCNV" 
         display_message nobanner
         ;;
    "1") JDESC="ERROR accessing $EBSCNV ! Copy aborted!"
         cond=99
         display_message nobanner
         exit $cond
         ;;
  esac
}

############################################################################
###   main section
############################################################################
prog=$(basename $0)
integer NumParms=$#

### ******************************************************************
### define job variables
### ******************************************************************
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT
export JDESC="Daily Creation of Indicative Changes from OMNI"

### ******************************************************************
### Standard script setup
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
check_input
. JOBDEFINE ## standard omniplus master file/environment definition script
fnc_log_standard_start
cd $XJOB

### ******************************************************************
### Archive previous run files
### TASK 1 - 
### ******************************************************************

$archive_files && archive_files

### ******************************************************************
### This is where real processing starts
### TASK 2 - Run COBOL program; extract basic participant info
### ******************************************************************
$extract_part_data && extract_part_data

### ******************************************************************
### TASK 2 - Find/Unzip prev business day archived ExACCT TOE/TOS files
### ******************************************************************
$find_prev_EXCfiles && find_prev_EXCfiles

### ******************************************************************
### TASK 3 - Run COBOL pgm to write the participant extract diffs file
### ******************************************************************
$create_part_diffs_file && create_part_diffs_file

### ******************************************************************
### TASK 4 - Run COBOL pgm to write participant src extract diff file
### ******************************************************************
$create_partsrc_diffs_file && create_partsrc_diffs_file

### ******************************************************************
### TASK 5 - Copy extracts to xfer directory for pickup
### ******************************************************************
$copy_files_for_transfer && copy_files_for_transfer

### ******************************************************************
### TASK 6 - Send Reports to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK LAST - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
