#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxrem019h.ksh           CALL            *
#COMMENT *  Script Name  : h_rem_cre8cards.ksh                       *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will read a file of contributions*
#COMMENT *                 and loan repayments and format T114,T385  *
#COMMENT *                 cards.                                    *
#COMMENT *                                                           *
#COMMENT *                 THIS IS A COPY OF rem_cre8cards.ksh with  *
#COMMENT *                 some HOP specific items in it. ANY CHANGE *
#COMMENT *                 TO THIS SCRIPT MAY NEED TO BE DONE TO     *
#COMMENT *                 THE OTHER.                                *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 12/09/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : none                                      *
#COMMENT *  COBOL Calls  : REM1000H                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : 7am workflow                              *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT #  Task List                                                #
#COMMENT # 1 - Setup files for REM1000H processing                   #
#COMMENT # 2 - Prepare files                                         #
#COMMENT # 3 - Run REM1000H process                                  #
#COMMENT # 4 - Load cards into Omni                                  #
#COMMENT # 5 - Archive Files                                         #
#COMMENT # 6 - Send Reports to document manager                      #
#COMMENT # 7 - Cleanup and Say Goodbye                               #
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 12/09/2005  CC#: 9219    By: R. Sica                U
#COMMENT U Reason: Set up to be used by HOP                          U
#COMMENT U Date: 08/25/2005  CC#: 9056    By: R. Sica                U
#COMMENT U Reason: Check for blank and zero files                    U
#COMMENT U Date: 08/01/2007  CC#: 11966   By: M. Lewis               U
#COMMENT U Reason: Remove none needed processes.                     U
#COMMENT U Date: 01/17/2008  CC#: 12475   By: M. Lewis               U
#COMMENT U Reason: Added archiving to prevent duplicate adds to OMNI U
#COMMENT U Date: 20110318   Proj: WMS3939    By: Paul Lewis          U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

######################################################################
###########################   functions   ############################
######################################################################

function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

#################################################
## notify personnel via email if files are not ##
## populated with data. Personnel depends if   ##
## if executing in dev or prod data regions.   ##
#################################################
function send_email
{
  integer sub=0
  until [ $sub -eq $limit ]
  do
     addr=$(eval "echo \${notify_${OMNIsysID}[$sub]}")
     cat $MSGFILE | mailx -s "ExCCEPT Warning" $addr
     JDESC="Error messages sent via email to $addr"
     display_message nobanner
     ((sub+=1))
  done
}
function archive_files
{
  cd $archDir
  for label in $archList
  do
     mv $label ${label}.$rundate.$tmptime
     archive ${label}.$rundate.$tmptime
     JDESC="$archName Completed"
     display_message nobanner
  done
}
######################################################################
##MAIN##################### main section  #################SECTION####
######################################################################
export integer NumParms=$#
prog=$(basename $0)
clear

[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=${prog%.ksh}
export JDESC="OmniPlus/UX HOP Contribution and Loan Repay Txns."
export ENVFILE=ENVBATCH
export PARENT=$JNAME
export AULOUT=$POSTOUT

######################
## source in common functions
######################
. FUNCTIONSFILE

######################
## setup generic variables
## to initial values
######################
set_generic_variables

######################
## check for required parameters
## and request for help
######################
check_input

######################
## create output directories
######################
make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Setup files for REM1000H processing
### ******************************************************************
export MSTRFILE=$XJOB/RPTFILE1260
export MSGFILE=$XJOB/error.msg
rm -f $MSTRFILE $MSGFILE
integer errno=0

if $remfilesetup; then
   JDESC="Setup files for REM1000H processing"
   display_message nobanner

   cd $EBSCNV
   # AUL files
   ls HODEP?? >HOPfilelist.$today
   # RMS files
   ls RMDEP?? >>HOPfilelist.$today
   # Loan files
   ls LNREP?? >>HOPfilelist.$today
   if [[ -s HOPfilelist.$today ]]; then
      for f in $(cat HOPfilelist.$today) ; do
         cp $f $XJOB || ((erOCrno+=1))
         mv $f $f.$today || ((errno+=1))
      done
      cd $XJOB
   else
      cd $XJOB
      export JDESC="No new HOP Deposit files in $EBSCNV"
      display_message
      exit 0
   fi

   if [[ $errno -gt 0 ]]; then
     limit=$(eval "echo \${notify_${OMNIsysID}_nbr}")
     $error_notification && send_email
     cond=99
     check_status
   fi
fi

### ******************************************************************
### TASK 2 - Prepare files for REM1000H
### ******************************************************************

# This is necessary because UT80IN looks for both IN80 and IN802.
touch $XJOB/DUMMY2.tmp || ((errno+=1))
export dd_IN802=$XJOB/DUMMY2.tmp

if $runprog; then
   touch $XJOB/$CARDFILENAME
   # TROT file is opened in EXTEND mode
   export dd_TROT=$XJOB/$CARDFILENAME

   cp $EBSCTRL/DUMBTRAN.DAT $XJOB || ((errno+=1))
   cp $EBSCTRL/DUMBTRN2.DAT $XJOB || ((errno+=1))
   export dd_TRANIN=$XJOB/DUMBTRAN.DAT
   export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
   export dd_RSTROUT=$XJOB/rs.tmp
   export dd_HELPER2=$AULDATA/HELP2
fi

### ******************************************************************
### TASK 3 - Run REM1000L process
### ******************************************************************
for f in $(cat $EBSCNV/HOPfilelist.$today) ; do
   if [[ -s $f ]]; then
      export fline=`head -1 $f | cut -c3-11`

      # If first/only record has zeros for ssn, then like a blank file
      if [[ $fline != '000000000' ]]; then
         export RPTLISTING=$XJOB/${EBSRPTPFX}REM1000RPT.$f
         export ERRORRPT=$XJOB/${EBSRPTPFX}REM1000ERR.$f

         if $runprog; then
            export dd_IN80=$XJOB/$f
            export dd_DATEHOLIDAY=$AULDATA/DATEHOLIDAY

            PNAME=REM1000H
            # file name must be 7 characters in length
            PARM=$f
            export Mid=$f
            JDESC="Run REM1000H process for $f"
            execute_program
         fi

         [[ -s $RPTLISTING ]] && cat $RPTLISTING >> $MSTRFILE
         [[ -s $ERRORRPT ]] && cat $ERRORRPT >> $MSTRFILE
         [[ ! -s $MSTRFILE ]] && touch $MSTRFILE
      fi
   fi
done

### ******************************************************************
### TASK 4 - Load cards into Omni
### ******************************************************************
if $loadcards; then
   if $runprog; then
      cp $XJOB/$CARDFILENAME $CARDFILE
   fi

   PNAME=rem_loadcards.ksh
   JPRM1=$CARDFILENAME
   JDESC="Load VTRAN"
   export cond_limit=1
   execute_job

fi

rm -f $CARDFILE
### ******************************************************************
### TASK 5 - Archive files
### ******************************************************************
$archive_files && archive_files

### ******************************************************************
### TASK 6 - Send Reports to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 7 - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
