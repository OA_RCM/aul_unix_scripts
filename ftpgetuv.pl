#!/opt/perl5/bin/perl

$UNITVAL="$ENV{'UVFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPGET="/opt/ncftp/bin/ncftpget";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPGET -d ftpgetuv.log -f $PROC/ftpgetuv.cfg -E . $UNITVAL`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $UNITVAL file from 3000 successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $UNITVAL file from 3000\n"; 
    }
}
