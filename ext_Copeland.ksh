#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTM Job Name  : RSUXEXT705               CALL            *
#COMMENT *  SchedXref Name: rsuxext705.ksh                           *
#COMMENT *  Script Name   : ext_Copeland.ksh         OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce two reports to be   *
#COMMENT *                 sent to Third Party Administrator (TPA)   *
#COMMENT *                 Copeland.                                 *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : EBSPROC                                   *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 09/19/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : TPAMAIN TPAMVTP TPAPTTP TPAMVTP TPAPTTP   *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task Sequence                                            T
#COMMENT T                                                           T
#COMMENT T   TASK 1 Get the parameters needed to produce the reports T
#COMMENT T   TASK 2 Run the COBOL programs to produce TPA files      T
#COMMENT T   TASK 3 Sort TRAN & VALU files                           T
#COMMENT T   TASK 4 Run the COBOL programs to produce MVTP & PTTP    T
#COMMENT T            extracts                                       T
#COMMENT T   TASK 5 Send MVTP & PTTP files to document manager       T
#COMMENT T   TASK 6 clean up and say goodbye                         T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/08/2005  SMR: CC3039  By: Mike Lewis             U
#COMMENT U Reason: Redesigned from original script to bring up to    U
#COMMENT U         current standards.                                U
#COMMENT U                                                           U
#COMMENT U Date: 01/25/2006  CC: 9695     By: Sue Freas              U
#COMMENT U Reason: Add calls to create PTTP & MVTP files and extract U
#COMMENT U         only those two files. Also send files to Vista.   U
#COMMENT U                                                           U
#COMMENT U Date: 20110318  Proj: WMS3939  By: Paul Lewis             U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################

function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function prep_files
{
  if [ -s "$XJOB/TPAPTTPO" ] ; then
    cp $dd_TPAPTTPO $XJOB/${EBSRPTPFX}TPAPTTPO
    cp $dd_TPAMVTPO $XJOB/${EBSRPTPFX}TPAMVTPO
    export JDESC="Sending Copeland TPA files to document manager..."
    display_message
    fnc_send2DocMgr
  else
    export JDESC="Copeland TPA files are empty - nothing to send to document manager"
    display_message
  fi
}

############################################################################
###   main section
############################################################################
prog=$(basename $0)
integer NumParms=$#

### ******************************************************************
### define job variables
### ******************************************************************
  export JUSER=${JUSER:-$LOGNAME}
  export JNAME=${prog%.ksh}
  export ENVFILE=ENVBATCH
  export AULOUT=$POSTOUT
  export JDESC="Create TPA Extracts for Copeland"

### ******************************************************************
### Standard script setup
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
check_input
. JOBDEFINE ## standard omniplus master file/environment definition script
fnc_log_standard_start
fnc_set_monthend_date
cd $XJOB

### ******************************************************************
### TASK 1 Get the parameters needed to produce the reports
### ******************************************************************
  export tpatpa=1
  export tpaend=${TPAEND:-$rundate}
  export tpabeg=${TPABEG:-`echo $tpaend |cut -c1-6`"01"}
  export PARM=$tpatpa$tpabeg$tpaend

### ******************************************************************
### TASK 2 Run the COBOL programs to produce TPA files
### ******************************************************************
#  export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
#  export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
  export dd_TPAPARTO=$XJOB/COPPARTO;
  export dd_TPATRANO=$XJOB/COPTRANO;
  export dd_TPAVALUO=$XJOB/COPVALUO;
  export dd_TPAMVTPO=$XJOB/COPMVTPO;
  export dd_TPAPTTPO=$XJOB/COPPTTPO;
  export dd_SRTTRANO=$XJOB/SRTTRANO;
  export dd_SRTVALUO=$XJOB/SRTVALUO;

  PNAME=TPAMAIN
  export JDESC="Run COBOL program $PNAME"
  execute_program

### ******************************************************************
### TASK 3 Sort the TRAN and VALU files
### ******************************************************************
  sort -o $XJOB/SRTTRANO -k .1,.6 -k .21,.29 -k .32,.43 $XJOB/COPTRANO
  sort -o $XJOB/SRTVALUO -k .1,.6 -k .21,.29 $XJOB/COPVALUO

  mv $XJOB/SRTTRANO $XJOB/COPTRANO
  mv $XJOB/SRTVALUO $XJOB/COPVALUO

### ******************************************************************
### TASK 4 Run the programs to produce mvtp & pttp extracts
### ******************************************************************
  PNAME=TPAMVTP
  export JDESC="Run COBOL program $PNAME"
  execute_program

  PNAME=TPAPTTP
  export JDESC="Run COBOL program $PNAME"
  execute_program

  cp $dd_TPAPTTPO $pickupLoc
  cp $dd_TPAMVTPO $pickupLoc

### ******************************************************************
### Task 5 Send files to document manager
### ******************************************************************
$send2DocMgr && prep_files

### ******************************************************************
### TASK 6 clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
