#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTM Job Name  : RSUXHOP001                  CALL         *
#COMMENT *  SchedXref Name: rsuxhop001.ksh           OMNI-ASU        *
#COMMENT *  Unix Script   : h_addr_cng.ksh                           *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : batch job for Data Express                *
#COMMENT *                 HOP address changes.                      *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20/5.50                   *
#COMMENT *  Resides      : $EBSPROC                                  *
#COMMENT *  Author       : AUL - JWY                                 *
#COMMENT *  Created      : 02/10/2006                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M Scheduler                       *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : DERUN OPMSGSORT                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : daily                                     *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 - 10 min                               *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task Information :                                       T
#COMMENT T                                                           T
#COMMENT T  TASK 1 -- Define files needed by all processes           T
#COMMENT T  TASK 2 -- Run the COBOL driver program for Data Express  T
#COMMENT T  TASK 3 -- Load output file to OMNI Vtran System          T
#COMMENT T  TASK 4 -- Sort the Data Express Message Log File         T
#COMMENT T  TASK 5 -- Send Logs to document manager                  T
#COMMENT T  TASK 6 -- Copy logs to $AULOUT                           T
#COMMENT T  TASK 7 -- Save address change file                       T
#COMMENT T  TASK 8 -- Send status email notification (optional)      T
#COMMENT T  TASK 9 -- clean up and say goodbye                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 02/10/2006  SMR: CC8949   By: Joe Young             U
#COMMENT U Reason: Created to process address changes for HOP.       U
#COMMENT U                                                           U
#COMMENT U Date: 20110318  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##FUNCTIONS################   functions   ############################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function create_cards
{
  if [ -s $HOPADDRFILE ]
  then
    export cond_limit=12
    export PNAME=DERUN
    export JDESC="run COBOL program $PNAME to create cards"
    execute_program
    print "HOP Address Change Cards successfully created." >> goodmsgfile
  else
    export JDESC="file $HOPADDRFILE does not exist - no further action required"
    display_message
    return 0
  fi
}

function load_cards_to_Omni
{
  if [ -s $OPTRANFILE ]
  then
     rm $EBSCARD/OPTRANHA.DAT
     cp $OPTRANFILE $EBSCARD/OPTRANHA.DAT
     export JDESC="Load output file to OMNI Vtran System"
     export PNAME=JOBVTUT
     export JPRM1=$JUSER
     export JPRM2=OPTRANHA.DAT
     execute_job
     print "Hop Address Change Cards successfully loaded to Omni" >> goodmsgfile
  else
     export JDESC="file $OPTRANFILE does not exist"
     display_message
     print "No $OPTRANFILE -- Possible problem exists at ${date}" >> badmsgfile
  fi
}

function sort_message_log
{
  if [ -s $DEMSGLOG ]
  then
    export cond_limit=12
    export PNAME=OPMSGSORT
    export JDESC="run COBOL program $PNAME to sort $DEMSGLOG"
    execute_program
  else
     export JDESC="file $DEMSGLOG has nothing to sort on"
     display_message
  fi
}

function prepare_logs_for_document_manager
{
  if [ -s $DEMSGLOG ]
  then
    export JDESC="look in document manager file ${EBSRPTPFX}DEMSGLOGHOP for messages"
    display_message
  else
    export JDESC="No entries found in $DEMSGLOG "
    display_message
    print "No entries found in $DEMSGLOG " >> $DEMSGLOG
  fi

  if [ -s $DEJOBLOG ]
  then
    export JDESC="look in document manager file ${EBSRPTPFX}DEJOBLOGHOP for messages"
    display_message
  else
    export JDESC="No entries found in $DEJOBLOG "
    display_message
    print "No entries found in $DEJOBLOG " >> $DEJOBLOG
  fi
}

function copy_logs
{
  JDESC="Copy logs to $AULOUT"
  display_message
  cp $DEMSGLOG $AULOUT
  cp $DEJOBLOG $AULOUT
}

function save_addr_cng
{
  JDESC="Save $EBSCNV/HOP/HOADD4K"
  display_message
  mv $EBSCNV/HOP/HOADD4K $EBSCNV/HOP/hopaddrfile.$today.txt
}

function send_email_notification
{
  [[ -s $goodmsgfile ]] && fnc_notify re_success
  [[ -s $badmsgfile ]] && fnc_notify re_errors
}
######################################################################
##########################  MAIN CODE ################################
######################################################################
export integer NumParms=$#
prog=$(basename $0)

export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Process HOP address changes"
export ENVFILE=ENVBATCH
export AULOUT=$EDITOUT

###################################
## call standard setup functions
###################################
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
. JOBDEFINE
fnc_log_standard_start
export badmsgfile=$XJOB/bad_messages
export goodmsgfile=$XJOB/good_messages

cd $XJOB

### ******************************************************************
### TASK 1 -- Define files needed by all processes
### ******************************************************************
cp $EBSCTRL/DUMBTRAN.DAT $XJOB
cp $EBSCTRL/DUMBTRN2.DAT $XJOB
touch $XJOB/DUMMY3.DEF
export dd_RSTROUT=$XJOB/rs.tmp
export dd_TRANIN=$XJOB/DUMBTRAN.DAT
export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
export HOPADDRFILE=$EBSCNV/HOP/HOADD4K
export DEJOBLOG=$XJOB/${EBSRPTPFX}DEJOBLOGHOP
export DEMSGLOG=$XJOB/${EBSRPTPFX}DEMSGLOGHOP
export DEPGMNAME=$IBSEDE/HOPADDRCHG.EDE
export OPTRANFILE=$XJOB/OPTRANHA.DAT
export DESORTWK=$XJOB/DESORTWK

### ******************************************************************
### TASK 2 -- Run the COBOL driver program for Data Express
### ******************************************************************

$create_cards && create_cards

### ******************************************************************
### TASK 3 -- Load output file to OMNI Vtran System
### ******************************************************************

$load_cards && load_cards_to_Omni

### ******************************************************************
### TASK 4 -- Sort the Data Express Message Log File
### ******************************************************************

$sortmsglog && sort_message_log

### ******************************************************************
### TASK 5 -- Send Logs to document manager
### ******************************************************************

$send2DocMgr && prepare_logs_for_document_manager
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 6 -- Copy logs to $AULOUT
### ******************************************************************

$copy_logs && copy_logs

### ******************************************************************
### TASK 7 -- Save address change file
### ******************************************************************

$save_file && save_addr_cng

### ***************************************************
### TASK 8 -- Send status email notification (optional)
### ***************************************************

$send_email_notification && send_email_notification

### ******************************************************************
### TASK 9 -- clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0

