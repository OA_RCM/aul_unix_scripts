#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  CTM  JOB NAME     : RSUXEXT701                       NO CALL       *
#COMMENT *  UNIX SYMBOLIC LINK: rsuxext701.ksh                   OMNI-ASU      *
#COMMENT *  UNIX SCRIPT NAME  : ext_PatriotAct.ksh                             *
#COMMENT *                                                                     *
#COMMENT *  Description  : This job will create an extract necessary           *
#COMMENT *                 for the Patriot Act. It is a full extract           *
#COMMENT *                 of ALL OMNI data.                                   *
#COMMENT *                                                                     *
#COMMENT *  Author       : AUL - RPS                                           *
#COMMENT *  Created      : 08/09/2002                                          *
#COMMENT *  Environment  : ENVRMNT                                             *
#COMMENT *  Called by    : ECS Scheduler                                       *
#COMMENT *  Script Calls : JOBDEFINE                                           *
#COMMENT *  COBOL Calls  : EXT1001E                                            *
#COMMENT *                                                                     *
#COMMENT *  Frequency    : Daily at first then monthly                         *
#COMMENT *                                                                     *
#COMMENT *  Est.Run Time : Depends on number of active plans and               *
#COMMENT *                 and participants                                    *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status   : Compliant                                           *
#COMMENT *                                                                     *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Can be restarted at anytime.                                       R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE ---------------------------------------------------- T
#COMMENT T    1  - Run OmniScript                                              T
#COMMENT T    2  - optionally, xfer output to remote location                  T
#COMMENT T  LAST - end of job cleanup routine                                  T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U Date: 12/10/2003  CC: 5551      By: Rick Sica                       U
#COMMENT U Reason: PTF21 readiness                                             U
#COMMENT U                                                                     U
#COMMENT U Date: 07/31/2006  SMR: CC8403   By: Mike Lewis                      U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.               U
#COMMENT U                                                                     U
#COMMENT U Date: 05/02/2008  SMR: CC12779   By: Mike Lewis                     U
#COMMENT U Reason: Standardized and changed destination server name.           U
#COMMENT U                                                                     U
#COMMENT U Date: 20110603    WMS: 2998      By: Tony Ledford                   U
#COMMENT U Reason: Eliminate use of connect direct.                            U
#COMMENT U                                                                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

################################################################################
###FUNCTION################# function declarations #################SECTION#####
################################################################################
##*********************************************************
## check script input parameters if applicable
##*********************************************************
function check_input
{
  return 0
}
function run_program
{
  PNAME=EXT1001E
  JDESC="run COBOL program $PNAME"
  execute_program

  JDESC="Cobol program $PNAME completed successfully"
  display_message nobanner

}
function transfer_file
{
  fnc_ftpq $dd_PATRIOTACTEXT
}

###########################################################################
###########################  main section  ################################
###########################################################################
prog=$(basename $0)
integer NumParms=$#

###################################################
## setup local script variables
###################################################
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="PATRIOT ACT EXTRACT"
export ENVFILE=EBSBATCH

###################################################
## load global functions and variables; 
## verify input parameters
###################################################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export AULOUT=$POSTOUT
cd $XJOB

. JOBDEFINE
fnc_log_standard_start
check_input 

### ******************************************************************
### define files needed by Sungard I/O routines
### ******************************************************************
export JDESC="define Sungard local files..."
display_message
  cp $EBSCTRL/DUMBTRAN.DAT $XJOB
  cp $EBSCTRL/DUMBTRN2.DAT $XJOB
  touch $XJOB/DUMMY3.DEF
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_TRANIN=$XJOB/DUMBTRAN.DAT
  export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
  
### ******************************************************************
### define files needed by AUL programs
### ******************************************************************
export JDESC="define AUL local files..."
display_message
  export dd_PATRIOTACTEXT=$XJOB/patriotact.full.txt
  export dd_PATRIOTACTRPT=$XJOB/patriotactrpt.txt
  export dd_PATRIOTPARTIDX=$XJOB/patriotpartidx.tmp
  export dd_PATRIOTAAIDX=$XJOB/patriotaaidx.tmp

###################################################
## TASK 1 - Run the COBOL program
###################################################
$run_program && run_program

###################################################
## TASK 2 - xfer output to remote location
###################################################
$transfer_file && transfer_file

###################################################
## TASK LAST - end of job cleanup routine
###################################################
eoj_housekeeping
return 0
