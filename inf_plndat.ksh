#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXINF707                CALL            *
#COMMENT *  UNIX POINTER : rsuxinf707.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : inf_plndat.ksh                            *
#COMMENT *                                                           *
#COMMENT *  Description  : job to get plan data to be sent           *
#COMMENT *                 to the IBM for inquiry                    *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 03/16/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : INF1001E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Define files needed by Sungard and AUL routines.T
#COMMENT T  TASK 2 - Run the COBOL program to produce plan data      T
#COMMENT T           extract file.                                   T
#COMMENT T  TASK 3 - Prepare files for transfer to IBM.              T
#COMMENT T  TASK 4 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 03/16/2004   CC: 5939     By: Mike Lewis            U
#COMMENT U Reason: New creation using current standards.             U
#COMMENT U                                                           U
#COMMENT U Date: 09/28/2007  SMR: CC12194  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function run_program
{
  cd $XJOB
  PNAME=INF1001E
  JDESC="run COBOL program $PNAME"
  execute_program
}
function prepare_files_for_transfer
{
  JDESC="Preparing Plan Data Extract for Transfer"
  display_message nobanner

  export FRFILE=$dd_OREC

  create_file
  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying Plan Data Extract to $AULOUT for ftp job "
    display_message nobanner
    cat $FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No Plan Data Extract created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH1700
	    PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP1700
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ1700
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"
  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="SEND PLAN DATA TO IBM FOR PRINTING"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start
check_input
. JOBDEFINE

cd $XJOB

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_daily_date

#######################################################################
### TASK 1 - Define files needed by Sungard and AUL routines
#######################################################################
cp $EBSCTRL/DUMBTRAN.DAT $XJOB
cp $EBSCTRL/DUMBTRN2.DAT $XJOB
touch $XJOB/DUMMY3.DEF
export dd_RSTROUT=$XJOB/rs.tmp
export dd_TRANIN=$XJOB/DUMBTRAN.DAT
export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
export dd_OREC=$XJOB/isxfl1.txt

#######################################################################
### TASK 2 - Run the COBOL program to produce plan data extract file
#######################################################################

$run_program && run_program

######################################################################
### TASK 3 - Prepare files for transfer to IBM
######################################################################

$prepare_files && prepare_files_for_transfer

################################################################
### TASK 4 - clean up and say goodbye
################################################################
eoj_housekeeping
return 0
