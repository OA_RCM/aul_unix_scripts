#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Script Name       : inf_premiertrust.ksh     CALL -- OMNI ASU      *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run INF-PREMIERTRUST (Transaction automation)  *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.50                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 07/01/2011                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         : rsuxinf813.ksh (sched_xref)                    *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Daily                                          *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 15 min.                                        *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Create a help ticket.  May need to delete   R
#COMMENT R                         OMNI transactions.                          R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Glen McPherson   WMS4788   01/01/11   original                     U
#COMMENT U  Glen McPherson   WMS5456   07/01/11   Handle multiple trade files  U
#COMMENT U  Louis Blanchette WMS8139   09/27/12   Remove call to OmniScript    U
#COMMENT U                                        INF-PT-UPD.txt in run_pt_upd U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
# functions

function job_setup
{
  export run_pt_auto=false
  rm -f $outdir/*
  cd $indir
  x=$(ll RPT4746.P236*|wc -l)
  [[ $x = 0 ]] && { JDESC="Nothing to do"; display_message nobanner; return 0; }
  for x in RPT4*
  do
     create_file
  done
}

function create_file
{
    cp $x $XJOB/premiertrust
    PNAME=JOBCALC
    JPRM1=$JUSER
    JPRM2=INF-PT-BUILD.txt
    execute_job
    export run_pt_auto=true
}

function run_pt_auto
{
    export JDESC="Running INF-PT-TRANS calculator..."
    display_message
    export PNAME=JOBCALC
    export JPRM1=$JUSER
    export JPRM2=INF-PT-TRANS.txt
    execute_job

}

function run_pt_upd
{
  premtrf=$XJOB/PTCONFIRMREJECT
  if [ `cat $premtrf | wc -c` -gt 1 ]; then
     typeset -L100 frec; cat $XJOB/PTCONFIRMREJECT | while read frec; do
           print "$frec" >> $XJOB/PTCONFIRMFILE || export ekerror="Y"
         done
     cp $XJOB/PTCONFIRMFILE $outdir/DTSFTP.C4746.S01126
  fi
}

function cleanup_dir
{
  mv $indir/* $XJOB
}

function check_issue_file
{
  premiss=$XJOB/AA450.TXT
  if [ `cat $premiss | wc -c` -gt 5 ]; then
    export JDESC="AA450 issues with Premier Trust"
    display_message
    export e_message="Alert: AA450 issues with Premier Trust."
    export e_subject="Premier Trust: Trustee Correction Needed."
    echo $e_message | mailx -s "$e_subject" $e_address <$premiss
  fi
}

function report_file
{
  premrpt=$XJOB/PTTRADES.txt
  if [ `cat $premrpt | wc -c` -gt 1 ]; then
    export e_message="Premier Trust Trade Report"
    export e_subject="Premier Trust Trade Report"
    echo $e_message | mailx -s "$e_subject" $e_addr_pt <$premrpt  
    cp $XJOB/PTTRADES.txt $outdir
  fi
}

# main
export JNAME=inf_premiertrust
export JUSER=$LOGNAME

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

# Task 1 - do processing - Always create $XJOB/DTCCTRADES file even if empty
$job_setup && job_setup

# Task 2 - Process the trade orders run_pt_auto variable is set in job_setup
touch $XJOB/DTCCTRADES
$run_pt_auto && run_pt_auto

# Task 3 - Set the output file to 100 char length records
$run_pt_auto && run_pt_upd

# Task 4 - email report file
$report_file && report_file

# Task 5 - check issue file
$check_issue_file && check_issue_file

# Task 6 - clean up directories
$cleanup_dir && cleanup_dir

# Task 7 - standard exit function
eoj_housekeeping
