#!/usr/bin/env perl

use feature ':5.10';

use Data::Dumper;
use File::Basename;
use Getopt::Long;
use Pod::Usage;
use XML::Simple;
use POSIX qw(strftime);

########################################
# dispatch function:                   #
########################################

sub dispatch(@_)
{
    my $jobs = { ivic => sub { `JOBCALC $ENV{JUSER} ivic2xml.txt`; },
    };
    my ($job, $env) = @_;

    foreach (keys %{$env})
    {
	  $ENV{$_} = $env->{$_};
    }

    say $jobs->{$job}();

    die "$job failed to execute, stopped" if $? > 4;
}

########################################
# next_record()	                       #
########################################

sub next_record(@_)
{
    my ($tag, $fd) = @_;
    my $xmlchunk = "";

    while (<$fd>)
    {
	if (/<($tag)>/ .. /<\/($tag)>/)
	{
	    s/[^[:ascii:]]//g;
	    $xmlchunk .= $_;
	    last if /<\/($tag)>/;
	}
    }
    return undef if $xmlchunk eq "";
    return XMLin $xmlchunk;
}

########################################
# MAIN.                                #
########################################

MAIN: {
    my $basename = basename $0, ".pl";

    GetOptions ( 'outdir=s' => \$outdir,
                 'help|h' => \$help ) or pod2usage(1);

    pod2usage 1 unless -d $outdir;

    my $planfile = shift;

    open FD, "<$planfile" or die "Failed to open $planfile, stopped";
    @plans = <FD>;
    close FD;
    chomp @plans;

    ########################################
    # Generate from price headers:         #
    # T028 add cards.			                 #
    # T028 delete cards.                   #
    # T029 purge cards.                    #
    ########################################

    dispatch "ivic", { JUSER   => $ENV{LOGNAME},
		       FILEIN  => $planfile,
		       XMLOUT  => "$outdir/ivic.xml" };	# <--XXX: Priced header dump.

    open IVIC,    "<$outdir/ivic.xml" or die "Failed to open $outdir/ivic.xml, stopped";
    open FUNDS,   ">$outdir/fundslist.txt" or die "Failed to open $outdir/fundslist, stopped";
    open T028ADD, ">$outdir/t028.add" or die "Failed to open $outdir/t028.add, stopped";
    open T028DEL, ">$outdir/t028.del" or die "Failed to open $outdir/t028.del, stopped";
    open T029PURGE, ">$outdir/t029.purge" or die "Failed to open $outdir/t029.purge, stopped";

    my $todayyear = strftime("%Y", localtime);

    while (my $record = next_record("INVESTMENT", IVIC))
    {
	     my $plan = $record->{DE005};
	     my $priceid = $record->{DE220};
	     
	     my $startyear = substr $record->{DE222}, 0, 4;
	     my $endyear = $startyear + 4;
	     my $startdt = $startyear . '0101';
	     my $enddt = $endyear . '1231';
	     my $rangedt =  $startdt . $enddt;
	     	     
	     next unless $priceid =~ /($plan)$/;
	     say FUNDS $priceid; # <--XXX: this is a PLP fund, so we need to delete/add the DR
		format T028DEL =
02800   $TIHDR 000001000000000
02801   20@<<<<<<<<<<<<<<
$priceid
.
	format T029PURGE =
02900   $TIHDR 000001000000000
02901   PURGE               @<<<<<<<<<<<<<<<
$priceid
02902      @<<<<<<<<<<<<<<<<<
$rangedt 
.
	write T028DEL;
	 while ($startyear <= $todayyear)
       {
	       write T029PURGE;
	       
	       $startyear = $endyear + 1;
	       $endyear = $startyear + 4;
	       $startdt = $startyear . '0101';
	       $enddt = $endyear . '1231';
         $rangedt =  $startdt . $enddt;
       }

format T028ADD =
02800   $TIHDR 000001000000000
02801   10@<<<<<<<<<<<<<<
$priceid
.
  next unless defined $record->{HEADER}; # <---XXX: this PLP fund has a PRICE ID in the Global plan
  write T028ADD;
    }
    close IVIC;
    close FUNDS;
    close T028ADD;
    close T028DEL;
    close T029PURGE;

    exit;
}

__END__

=head1 NAME

gentermpriceid.pl

=head1 SYNOPSIS

gentermpriceid.pl --outdir /put/cards/here /file/containing/plan/list.txt

=head1 DESCRIPTION

Generate the T028 cards for term environment.

=back
