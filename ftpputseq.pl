#!/opt/perl5/bin/perl

$SEQ="$ENV{'SEQFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputseq.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $SEQ`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $SEQ file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $SEQ file to ODS\n"; 
    }
}
