#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXEXT710              CALL            *
#COMMENT *  SchedXref Name : rsuxext710.ksh                          *
#COMMENT *  UNIX Script    : ext_to_OmniTrade.ksh                    *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Extract of Plan, IC, FC, and prices       *
#COMMENT *                 to OmniTrade                              *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 07/24/2008                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxext710                                *
#COMMENT *  Script Calls : JOBDEFINE JOBCALC                         *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily                                     *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : about 40 minutes                          *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S   There are settings in the ini file that are input for   S
#COMMENT S   the OmniScript and are used to control output:          S
#COMMENT S     "priceload": What prices to load.                     S
#COMMENT S         CUR - Select Prices For Current Day (default)     S
#COMMENT S         yyyymmddyyyymmdd - Date low/high range            S
#COMMENT S         ALL - Select Prices For All Days                  S
#COMMENT S         Blank - Don't load prices
#COMMENT S     "planload": Whether or not to load plan level data    S
#COMMENT S                (default = YES)                            S
#COMMENT S     "plansecurity": Whether or not to load info at        S
#COMMENT S             plan/security ID level (default = YES)        S
#COMMENT S     "securityplan": Whether or not to load info at        S
#COMMENT S             security ID/plan level (default = YES)        S
#COMMENT S     "securityload": Whether or not to load info at        S
#COMMENT S             security ID level (default =YES)              S
#COMMENT S     "securitymapping": Whether or not to load security    S
#COMMENT S             ID/plan/investment ID records (default = YES) S
#COMMENT S     "fcload": Whether or not to load Fund                 S
#COMMENT S             Control/position info (default = YES)         S
#COMMENT S                                                           S
#COMMENT S  This script can be run as a command too.  If $1 parm is  S
#COMMENT S  not set, it will use the settings above that are in the  S
#COMMENT S  ini file.  If 'F', everything will be turned off but the S
#COMMENT S  fcload.  If 'N', everything will be turned on but the    S
#COMMENT S  fcload.                                                  S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 07/15/2008  W#: 234       By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 10/20/2014  W#:11196      By: Danny Hagans          U
#COMMENT U Reason: Send Postion File Report to Document Manager      U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function send2DocMgr
{
  cd $XJOB
  fname=${EBSRPTPFX}POSITION_FILE
  if [[ -s "$fname" ]]                                        
  then                                                                
      fnc_send2DocMgr $fname                       
     JDESC="Sent $fname to document manager"              
     display_message nobanner                                         
  else
     JDESC="Nothing to send for file $fname "              
     display_message nobanner                                                                                                             
  fi                                                                
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME="ext_to_OmniTrade"
export JDESC="OMNIPLUS/UX Extract to OmniTrade"
export ENVFILE=ENVBATCH
export fcloadovr=$1

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables
 
######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input 

make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Override check
### ******************************************************************

if [[ ! -z $fcloadovr ]]; then
  case "$fcloadovr" in
    "F")
       export priceload="   "
       export planload="NO"
       export plansecurity="NO"
       export securityplan="NO"
       export securityload="NO"
       export securitymapping="NO"
       export fcload="YES"
    ;;
    "N")
       export fcload="NO"
    ;;
  esac
fi

[[ $EBSFSET = "hop" ]] && export priceload="   "

### ******************************************************************
### TASK 2 - run Omniscript
### ******************************************************************

if $runcalc ; then
  export PNAME=JOBCALC
  export JPRM1=$JUSER
  export JPRM2=otextr20.ux.txt
  execute_job
fi

### ******************************************************************
### TASK 3 - copy extract files to mount-point (shared with O.T. Server)
### ******************************************************************

if $cpextract ;then
   for f in $XJOB/OTI*.DAT; do
      [[ -s $f ]] && { 
	  cp $f $OTMOUNT
	  if [ $? != 0 ]; then
              JDESC="Failed to cp $f to $OTMOUNT.  See the source code."
              abend
	  fi
      }
   done
fi

### ******************************************************************
### TASK 4 - Send file to document manager
### ******************************************************************
$send2DocMgr && send2DocMgr

### ******************************************************************
### TASK 5 - clean up and say goodbye
### ******************************************************************

eoj_housekeeping
copy_to_masterlog
return 0
