#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXCNF701                CALL            *
#COMMENT *  Unix Pointer : rsuxcnf701.ksh            OMNI-ASU        *
#COMMENT *  Script Name  : cnf_cre8empremcnfs.ksh                    *
#COMMENT *                                                           *
#COMMENT *  Description  : Job to produce employer level             *
#COMMENT *                 remittance confirmations                  *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 09/25/2000                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : FUNCTIONSFILE JOBDEFINE                   *
#COMMENT *  COBOL Calls  : CNF1001E  CNF1000E  CNF1000R              *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task List:                                               T
#COMMENT T                                                           T
#COMMENT T TASK 1 - sort the Sequential History File                 T
#COMMENT T TASK 2 - define files needed by AUL programs              T
#COMMENT T TASK 3 - Run the COBOL program to extract contrib info    T
#COMMENT T TASK 4 - sort the CNFEXT File                             T
#COMMENT T TASK 5 - Run the COBOL program to produce REMCON file     T
#COMMENT T TASK 5 - sort the REMCONX File                            T
#COMMENT T TASK 6 - Run the COBOL program to produce contrib confirmsT
#COMMENT T TASK 7 - Send Confirmations & Labels to IBM via FTP       T
#COMMENT T TASK 8 - Copy files to common output directory (AULOUT)   T
#COMMENT T TASK 9 - Housekeeping and EOJ routines                    T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2001  SMR: PEN3170  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program CNF1000E, CNF1000R     U
#COMMENT U Date: 08/21/2001  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U Date: 03/08/2002  CC: 2664      By: Mike Lucas            U
#COMMENT U Reason: REMCONF sort off by 2 positions on fields 1,2     U
#COMMENT U Date: 02/26/2004  CC: 4645,5002 By: Ellen Slovinski       U
#COMMENT U Reason: Change CNF1000E                                   U
#COMMENT U Date: 02/26/2004  CC: 3586      By: Ellen Slovinski       U
#COMMENT U Reason: Change CNF1000E, Change CNF1000R to add Div/Sub   U
#COMMENT U and Add new program CNF1001E to script.                   U
#COMMENT U Date: 02/26/2004  CC: 3586      By: Keith Danielowski     U
#COMMENT U Reason: Script Standardization                            U
#COMMENT U Date: 03/11/2004  CC:  3586     By: S.Loper               U
#COMMENT U Reason: Additional script standardization                 U
#COMMENT U Reason: Modify to use sendfile.ksh file xfer standard     U
#COMMENT U Date: 07/30/2004  CC:  3586     By: S.Loper               U
#COMMENT U Reason: Rename script. Create jclLabel variable.          U
#COMMENT U Reason: Use standard fnc send_files_to_vista function.    U
#COMMENT U Date: 08/24/2004  CC:           By: Ellen Slovinski       U
#COMMENT U Reason: Correct sort of REMCONX file.  Changed all sorts  U
#COMMENT U         to Optech sorts.                                  U
#COMMENT U Date: 03/29/2007  CC:10789      By: Mike Lewis            U
#COMMENT U Reason: Concantenated all files into the TEAMFILE for     U
#COMMENT U         HOP plans                                         U
#COMMENT U Date: 08/21/2007  SMR: CC11967  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U  Date: 04/08/2008  CC: 12686     By: Louis Blanchette     U
#COMMENT U  Reason:  Change sort order of the file CNFEXT to:        U
#COMMENT U           Plan,Folder,Record Type, Div Sub, Part id       U
#COMMENT U           and Post num to prevent omission of disability  U
#COMMENT U           amount on the Contribution confirm              U
#COMMENT U           There's no Div in the PL record !!!             U
#COMMENT U  Date: 04/16/2009 WMS: 1943      By: Gary Pieratt         U
#COMMENT U  Reason: Vista-only confirmations                         U
#COMMENT U  Date: 06/24/2009 WMS: 2342      By: Rick Sica            U
#COMMENT U  Reason: Rewrite report into Compuset                     U
#COMMENT U  Date: 20110317  Proj: WMS3939   By: Paul Lewis           U
#COMMENT U  Reason: Standardize document manager calls to a function.U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##FUNCTIONS################   functions   ############################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function transfer_file
{
  NumParms=$#
  if [[ $NumParms != 3 ]]
  then
    JDESC="Parameters required for file transfer not supplied (bypass)"
    display_message nobanner
    JDESC="Fromfile=$1  Tofile=$2 "
    display_message nobanner
    exit 99
  fi

  JDESC="Preparing Employer Remittance Confirmations $1 for Transfer"
  display_message nobanner

  export FRFILE=$1
  export FRTYPE=$2
### WMS 1943 begin
  export V=$3
### WMS 1943 begin

  create_file
  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying Employer Remittance Confirmations $1 to $AULOUT for ftp job "
    display_message nobanner
    cat $FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No Employer Remittance Confirmations $2 created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi

}
function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH
                  PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.${JCLFILE}${FRTYPE}"

### WMS 1943 begin
  if $V ; then
     CDFILE="${CDFILE}V"
  fi

### WMS 1943 end

  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
function copy_files_to_aulout
{
  cp $XJOB/${EBSRPTPFX}REMERR $AULOUT
  cp $XJOB/REMCONF $AULOUT
# WMS 1943 begin
  cp $XJOB/REMCONFV $AULOUT
# WMS 1943 end
}
function merge_files
{
  cp $XJOB/TEAMFILE $XJOB/TEAMFILE.ORIG
  cat $XJOB/MAILLB10 >> $XJOB/TEAMFILE
  cat $XJOB/MAILLB11 >> $XJOB/TEAMFILE
  cp $XJOB/TEAMFILEV $XJOB/TEAMFILEV.ORIG
  cat $XJOB/MAILLB10V >> $XJOB/TEAMFILEV
  cat $XJOB/MAILLB11V >> $XJOB/TEAMFILEV
}
######################################################################
##MAIN##################### main section  #################SECTION####
######################################################################
export integer NumParms=$#
prog=$(basename $0)

export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-01}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX AUL Create Employer Remittance Confirms"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

### ******************************************************************
### standard job setup procedure
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
export JNAME="cnf701"           ###To keep output dir name shorter
check_input
make_output_directories
fnc_log_standard_start

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

cd $XJOB
### ******************************************************************
### TASK 1 - sort the Sequential History File
### on Plan/Plan-Sequence, Part-ID, Trade-Date
### ******************************************************************
OUTPUTFILE=$AULOUT/SEQAHST
[[ $JSTN = YES ]] && OUTPUTFILE=$AULOUT/SEQAHST.${JUSER}

OTSORT $OUTPUTFILE $XJOB/SRTAHST "/S(2,8,CH,A,294,20,CH,A,11,17,CH,A) F(TXT) W($TMPDIR/) CORE(40000)"

### ******************************************************************
### TASK 2 - define files needed by AUL programs
### These DD's are used by the next 3 program calls
### ******************************************************************
export dd_SEQAHIN=$XJOB/SRTAHST
export dd_CNFEXT=$XJOB/CNFEXTX
export dd_OREC=$XJOB/REMCONX
export dd_HELPER2=$AULDATA/HELP2
export dd_TEMPFILE=$XJOB/TAGFILE.tmp
export dd_TEMPPARTFILE=$XJOB/TAGPARTFILE.tmp
export dd_REMCONF=$XJOB/REMCONF
export dd_REMERR=$XJOB/${EBSRPTPFX}REMERR
export dd_TEAMFILE=$XJOB/TEAMFILE
export dd_MAILLB10=$XJOB/MAILLB10
export dd_MAILLB11=$XJOB/MAILLB11
### WMS 1943 begin
export dd_CNFEXTV=$XJOB/CNFEXTXV
export dd_ORECV=$XJOB/REMCONXV
export dd_REMCONFV=$XJOB/REMCONFV
export dd_TEAMFILEV=$XJOB/TEAMFILEV
export dd_MAILLB10V=$XJOB/MAILLB10V
export dd_MAILLB11V=$XJOB/MAILLB11V
### WMS 1943 end

### ******************************************************************
### TASK 3 - Run the COBOL program to extract contribution information
### from the Omni Sequential History File to the flat file CNFEXT.
### ******************************************************************
PNAME=CNF1001E
JDESC="Extract contribution information from sequential history"
execute_program
JDESC="Extract file CNFEXT written"
display_message

### ******************************************************************
### TASK 4 - sort the CNFEXT File
### by Plan, Folder, Record Type, Div Sub, Part id, and Post Num
### ******************************************************************
JDESC="Sorting Extract file $dd_CNFEXT "
display_message nobanner

OTSORT $dd_CNFEXT $XJOB/CNFEXT "/S(3,6,CH,A,9,20,CH,A,1,2,CH,A,29,4,CH,A,33,17,CH,A) F(TXT) W($TMPDIR/) CORE(40000)" && cond=0 || cond=1

[[ $cond = 0 ]] && JDESC="Extract file CNFEXT sorted OK"
[[ $cond = 0 ]] && display_message nobanner
[[ $cond = 1 ]] && JDESC="ERROR sorting Extract file CNFEXT !!"
[[ $cond = 1 ]] && display_message nobanner
[[ $cond = 1 ]] && { PMSG="SORT CNFEXT"; export cond=99; check_status; }

### WMS 1943 begin
JDESC="Sorting Extract file $dd_CNFEXTV "
display_message nobanner

OTSORT $dd_CNFEXTV $XJOB/CNFEXTV "/S(3,6,CH,A,9,20,CH,A,1,2,CH,A,29,4,CH,A,33,17,CH,A) F(TXT) W($TMPDIR/) CORE(40000)" && cond=0 || cond=1

[[ $cond = 0 ]] && JDESC="Extract file CNFEXTV sorted OK"
[[ $cond = 0 ]] && display_message nobanner
[[ $cond = 1 ]] && JDESC="ERROR sorting Extract file CNFEXTV !!"
[[ $cond = 1 ]] && display_message nobanner
[[ $cond = 1 ]] && { PMSG="SORT CNFEXTV"; export cond=99; check_status; }
### WMS 1943 end

### ******************************************************************
### TASK 5 - Run the COBOL program to produce REMCON file
### ******************************************************************
PNAME=CNF1000E
JDESC="Produce REMCON file"
export dd_CNFEXT=$XJOB/CNFEXT
### WMS 1943 begin
export dd_CNFEXTV=$XJOB/CNFEXTV
### WMS 1943 end
execute_program

JDESC="REMCON file written"
display_message

### ******************************************************************
### TASK 5 - sort the REMCONX File
### ******************************************************************
JDESC="Sorting Remittance confirms file $dd_OREC"
display_message nobanner

OTSORT $dd_OREC $dd_REMCONF "/S(844,6,CH,A,2,6,CH,A,8,4,CH,A,12,20,CH,A,1,1,CH,A,49,50,CH,A) F(TXT) W($TMPDIR/) CORE(40000)" && cond=0 || cond=1

[[ $cond = 0 ]] && JDESC="File $dd_OREC sorted OK"
[[ $cond = 0 ]] && display_message nobanner
[[ $cond = 1 ]] && JDESC="ERROR sorting file $dd_OREC !!"
[[ $cond = 1 ]] && display_message nobanner
[[ $cond = 1 ]] && { PMSG="SORT $dd_OREC"; export cond=99; check_status; }

### WMS 1943 begin
JDESC="Sorting Remittance confirms file $dd_ORECV"
display_message nobanner

OTSORT $dd_ORECV $dd_REMCONFV "/S(844,6,CH,A,2,6,CH,A,8,4,CH,A,12,20,CH,A,1,1,CH,A,49,50,CH,A) F(TXT) W($TMPDIR/) CORE(40000)" && cond=0 || cond=1

[[ $cond = 0 ]] && JDESC="File $dd_ORECV sorted OK"
[[ $cond = 0 ]] && display_message nobanner
[[ $cond = 1 ]] && JDESC="ERROR sorting file $dd_ORECV !!"
[[ $cond = 1 ]] && display_message nobanner
[[ $cond = 1 ]] && { PMSG="SORT $dd_ORECV"; export cond=99; check_status; }
### WMS 1943 end

### ******************************************************************
### TASK 6 - Run the COBOL program to produce contribution confirmations
### ******************************************************************
PNAME=CNF1000R
JDESC="Produce contribution confirmations"
execute_program

### ******************************************************************
### TASK 7 - Send Confirmations & Labels to IBM via FTP
### ******************************************************************
jclLabel=$jclLabelProd
[[ $OMNIsysID = "dev" ]] && jclLabel=$jclLabelTest
if [[ $OMNICUST = "omnihop" ]]
then
   merge_files
   $transfer_confirms && transfer_file $dd_TEAMFILE $jclLabel false
   $transfer_confirms && transfer_file $dd_TEAMFILEV $jclLabel true
else
   $transfer_confirms && transfer_file $dd_TEAMFILE $jclLabel false
   $transfer_confirms && transfer_file $dd_MAILLB10 LB10 false
   $transfer_confirms && transfer_file $dd_MAILLB11 LB11 false
   $transfer_confirms && transfer_file $dd_TEAMFILEV $jclLabel true
   $transfer_confirms && transfer_file $dd_MAILLB10V LB10 true
   $transfer_confirms && transfer_file $dd_MAILLB11V LB11 true
fi

### ******************************************************************
### TASK 8 - Send specified files to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 9 - Copy files to common output directory (AULOUT)
### ******************************************************************
$copy_files_to_aulout && copy_files_to_aulout

### ******************************************************************
### TASK 10 - Housekeeping and EOJ routines
### ******************************************************************
export JNAME=${prog%.ksh}
eoj_housekeeping
typeset +f | grep -q copy_to_masterlog && copy_to_masterlog
typeset +f | grep -q create_html && create_html
return 0
