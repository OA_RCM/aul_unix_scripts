#!/opt/perl5/bin/perl

$PWR="$ENV{'PWRFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputpwr.log -f $PROC/ftpputpwr.cfg -E -a . $PWR`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $PWR to Power Image successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $PWR to Power Image\n"; 
    }
}
