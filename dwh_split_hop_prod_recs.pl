#!/usr/local/bin/perl -n
use strict;
use warnings;
BEGIN {
  my ($sec,$min,$hour,$eor) = localtime(time);
  print "*** [$hour:$min:$sec] Begin record segregation process.\n";
  open (PRDFILE, ">$ENV{'XJOB'}/PFILE") || die "Couldn't open PFILE for output: $!";
  open (HOPFILE, ">$ENV{'XJOB'}/HFILE") || die "Couldn't open HFILE for output: $!";
}
if (/\tPLPL011\tY\d{5}/o) { print HOPFILE; } else { print PRDFILE; }
END {
  close (PRDFILE) || die "Couldn't open INFILE properly: $!";
  close (HOPFILE) || die "Couldn't open INFILE properly: $!";
  my ($sec,$min,$hour,$eor) = localtime(time);
  print "*** [$hour:$min:$sec] Record segregation complete.\n";
}
