#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXCMP700              NO-CALL         *
#COMMENT *  SchedXref Name : rsuxcmp700.ksh                          *
#COMMENT *  UNIX Script    : cmp_topheavy.ksh        OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *  Description  : Split Compliance.Reports Top Heavy report *
#COMMENT *                 by plan.                                  *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 11/19/2007                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : CMP1000R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : < 10 minutes                              *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S  VP.COMPLIANCE.REPORTS needs to be in POSTOUT if one was  S
#COMMENT S  created for this to run properly.                        S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can be re-run if report file is  R
#COMMENT R    in POSTOUT still can be restarted from XJOB area if    R
#COMMENT R    VP.COMPLIANCE.REPORTS has already been copied there.   R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 12/13/2007  CC: 12376            By: Rick Sica      U
#COMMENT U Reason: Change original EBSXFER directory                 U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  ## for future use
  return 0
}

###########################################################################
##main#####################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX Top Heavy Reports by Plan"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
## Standard startup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
. JOBDEFINE
fnc_log_standard_start
check_input
check_overrides

           ###################################################
cd $XJOB   ## processing effectively starts after this point
           ###################################################

### ******************************************************************
### TASK 1 - Retrieve report file
### ******************************************************************
export COMPRPT=${EBSRPTPFX}COMPLIANCE.REPORTS
if $retrievefile ; then
   cp $POSTOUT/${EBSRPTPFX}COMPLIANCE.REPORTS $COMPRPT || { JDESC="No Compliance Reports"; display_message; return 0; }
fi

### ******************************************************************
### TASK 2 - Run extract program
### ******************************************************************
if $runsplit ; then
   export dd_RSTROUT=$XJOB/rs.tmp
   export TOPHEAVYPLANS=$XJOB/TH.PLANS

   PNAME=CMP1000R
   JDESC="Run program $PNAME to extract data $(date +%H%M%S)"
   execute_program
fi

### ******************************************************************
### TASK 3 - Prepare files to be FTP'd to LAN
### ******************************************************************
if $sendfiles ; then
   for file in `ls -1 THR*`; do
      actualfile=`echo $file | cut -c8-`
      thyear=`echo $file | cut -c4-7`
      if [[ ! -d "$EBSXFER/$THXFERDIR/$thyear" ]]; then 
         mkdir -p "$EBSXFER/$THXFERDIR/$thyear" || { JDESC="Error creating directory $EBSXFER/topheavy/$thyear"; abend; }
      fi
      cp $file "$EBSXFER/$THXFERDIR/$thyear/${actualfile}.txt"
   done
fi

### ******************************************************************
### TASK 4 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
