#!/opt/perl5/bin/perl -s

use XML::Simple;
use Data::Dumper;
use File::Basename;
use Pod::Usage;
use FindBin;
use lib $FindBin::Bin;
use Folder;

$name = basename($0, '.pl');
$dir = $FindBin::Bin;
$default_filter_file = "$dir/$name.xml";

$filter_file = $filter || $default_filter_file;

if (dirname($filter_file) eq "." && ! -f $filter_file)
{
  $filter_file = "$dir/$filter_file";
}

pod2usage("can't read $filter_file") unless (-f $filter_file);

$rules = XMLin($filter_file, forcearray => 1) || pod2usage("problem reading $filter_file");

if ($list)
{
  print Dumper($rules), "\n";
  exit(0);
}

$cardfile = shift || pod2usage(1);

$file_prefix = $prefix || $cardfile;

if ($log)
{
  if (open(LOG, ">$log"))
  {
    $logfile = *LOG;
  }
  else
  {
    print STDERR "couldn't write $log - defaulting to stderr\n";
    $logfile = *STDERR;
  }
}
else
{
  $logfile = *STDERR;
}

%outputs = ();

add_output($rules->{action}, \%outputs);
foreach $rule (@{$rules->{rule}})
{
  if ($rule->{action})
  {
    add_output($rule->{action}, \%outputs);
  }
}
$keep_file = $file_prefix . ".keep";
open(KEEPERS, ">$keep_file") || die "can't write $keepfile\n";

open(FILE, "$cardfile") || die "can't read $cardfile\n";
while ($folder = Folder::read_folder(*FILE))
{
  my $filter = "";
  print $logfile "checking plan/folder [", $folder->plan(), "/", $folder->name(), "]\n" if ($verbose);
  $filter = trans_match_rule($folder, $rules);
  print $logfile "action for plan/folder [", $folder->plan(), "/", $folder->name(), "]: ", $filter || "keep", "\n" if ($verbose);
  my $handle = $outputs{$filter} || *KEEPERS;
  print $handle $folder->tostr();
}
close(FILE);

close(LOG) if ($log);

sub add_output
{
  my $action = shift;
  my $outref = shift;
  print $logfile "adding output file for action [$action]\n" if ($verbose);
  my $fname = $file_prefix . "." . $action;
  my $handle = "HANDLE" . $action;
  open($handle, ">$fname") || die "can't write $fname\n";
  $outref->{$action} = $handle;
}

sub trans_match_rule
{
  my $folder = shift;
  my $rules = shift;
  foreach my $rule (@{$rules->{rule}})
  {
    print $logfile "using rule:\n", Dumper($rule) if ($verbose);
    my $action = $rule->{action} || $rules->{action};
    my $rule_tref = $rule->{tran};
    my $match_tot = 0;
    my $match_cnt = 0;
    print $logfile "action is $action\n" if ($verbose);
    foreach my $rule_tran (@$rule_tref)
    {
      ++$match_cnt if (tran_matches_criteria($folder, $rule_tran));
      ++$match_tot;
    }
    return $action if ($match_cnt == $match_tot);
  }
  return "";
}

sub tran_matches_criteria
{
  my $folder = shift;
  my $rule_tran = shift;
  TRAN: foreach my $tran ($folder->header(), @{$folder->trans()})
  {
    if (length($rule_tran->{code}) == 3)
    {
      next TRAN unless ($tran->tran_code() eq $rule_tran->{code});
    }
    elsif (length($rule_tran->{code}) == 5)
    {
      next TRAN unless ($tran->rec($rule_tran->{code}));
    }
    if ($rule_tran->{field})
    {
      foreach my $field (@{$rule_tran->{field}})
      {
	my $idx = $field->{index} - 1;
	my $len = $field->{length};
	my $str = $field->{content};
	next TRAN unless (substr($tran->rec($rule_tran->{code}), $idx, $len) =~ /$str/);
      }
    }
    return 1;
  }
  return 0;
}

__END__

=head1 NAME

filter.pl - Filter folders from OMNI cycle's active cards.

=head1 SYNOPSIS

filter.pl [options] [card file]

=head1 DESCRIPTION

This program filters transactions from a given file containing a list
of transactions (card file).  The default action is to ignore the
offending transactions according to the configuration file.  The
configuration file is in XML format and contains the list of transactions
to be ignored.  The purpose of the action is to enable the offending
transactions to be grouped in any way desirable.  Each set of transactions
go to a file grouped by action.  The name of the input file is used as
a base and the action is appended to the input file name to create
the new output file name.  For the ignore file, no further action is taken.
The keep file (denoted by .keep extension) should then be passed to whatever
process would normally read the original card file.

Any additional actions created will have their own output file as well. 
These files can then be acted upon in any manner desired.  For example,
say that a certain transaction should be placed in a different process
step when encountered.  The action might be called "pschange".  After
running the filter program, the filtered transactions that need a
process step change are in the <name>.pschange file.  This file can then
be passed to another program which will read the file's contents and
perform the process step change.

An important thing to keep in mind is that a matching rule will cause the
entire folder containing the offending transaction(s) to be ignored.  The
program doesn't posess the ability to eliminate individual transactions
from a folder.  Normally a folder contains related transactions so this is
a desired behavior.  In the odd case where it isn't true, however, the
entire folder will be ignored.  The transactions can be placed in separate
folders, but this is beyond the scope of this program.  Remember that if
you want a folder split because of an offending transaction, the rule can
have its own action of 'split'.  A separate program can then handle the
folder(s) in the split output file and do whatever is desired.

=head2 Configuration File

As previously mentioned, the configuration file is in XML format.  This
allows for a very flexible configuration that is easily parsed even for
complicated filtering rules.  The basic syntax is a rule consists of one
or more tran criteria.  Each tran criteria contains one or more field
definitions to describe what to match in each tran.  Each rule may have
its own action.  If the action is not specified, the default action is
used.

It helps to understand the format of OMNI card files as the
configuration file is a natural fit to the how card files are
structured.  Each tran criteria defines a code which matches the tran
code and tran sequence at the beginning of each line in the card file.
Field definitions correspond directly to fields within each card.  This
allows both simple and more complex filtering.

A match on any of the rules will cause action on a folder but all the 
tran criteria must be fufilled for the rule to match.

The following text describes a sample configuration file to further help
in understanding the filtering process.

Here is the sample configuration:

  <ruleset action="ignore">
    <rule>
      <tran code="96601">
	<field index="9" length="25">IND-PRIORVEST|RPT-PARTVEST</field>
      </tran>
    </rule>
    <rule>
      <tran code="180"/>
    </rule>
    <rule>
      <tran code="181"/>
    </rule>
    <rule action="hold">
      <tran code="00200">
	<field index="16" length="6">^G</field>
      </tran>
      <tran code="00201">
	<field index="19" length="20">\.T801$</field>
      </tran>
    </rule>
  </ruleset>

The following describes each line in the file:

  <ruleset action="ignore">

This line opens the set of rules and sets the default action to ignore.

    <rule>

This opens the first rule.

      <tran code="96601">

This line indicates that a 96601 transaction is what is being checked
for a match.

	<field index="9" length="25">IND-PRIORVEST|RPT-PARTVEST</field>

This is the field that will be matched on the 96601 card.  The content
of the field is used as a regular expression.  In this case, a T966
report name of IND-PRIORVEST or RPT-PARTVEST will indicate a match.
Note that since this is XML, special characters must be escaped as
follows:

=over 4

=item '&'

Escaped as '&amp;'

=item '>'

Escaped as '&gt;'

=item '<'

Escaped as '&lt;'

=item '"'

Escaped as '&quot;'

=back

Normally, these characters are not used in regular expressions, but be aware of
them just in case.

      </tran>

This is the end of the tran criteria.

    </rule>

This is the end of the first rule.

    <rule>
      <tran code="180"/>
    </rule>

The three lines above constitute a new rule.  Any transaction code that starts
with 180 will be ignored.

    <rule>

      <tran code="181"/>

    </rule>

The three lines above constitute a new rule.  Any transaction code that starts
with 181 will be ignored.

    <rule action="hold">
      <tran code="00200">
	<field index="16" length="6">^G</field>
      </tran>
      <tran code="00201">
	<field index="19" length="20">\.T801$</field>
      </tran>
    </rule>

This rule causes any folder that ends in .T801 with a plan number that starts
with G to be put in the hold output file.

  </ruleset>

The end of the rules.

=head1 OPTIONS

=over 4

=item -list

List contents of filter config file.

=item -filter=ffile

Sets the filter file to 'ffile' (default is filter.xml).

=item -verbose

Produces debugging output on STDERR.

=item -log=logfile

Sends debugging output to file 'logfile' instead of STDERR.

=back
