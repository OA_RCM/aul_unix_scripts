#!/bin/ksh 
function do_diff
{
 diff $EBSTCUCPY/$file1 $diffdir/$file1 > $HOME/tmpfile 2> $HOME/tmperr
 cond=$?
 diff_cond_check
}
function diff_cond_check
{
 if [[ $cond = "1" ]]
 then
   echo "--- Compare test to $toarea ---" > $HOME/TMPFILE
   echo "--------- $file1 ---------" >> $HOME/TMPFILE
   cat $HOME/tmpfile  >> $HOME/TMPFILE
   cat $HOME/TMPFILE
 else
   if [[ $cond = "0" ]]
   then
     echo "--- No Differences in $file1 ---" > $HOME/TMPFILE
     cat $HOME/TMPFILE
   else
     if [[ $cond = "2" ]] && [[ $diffcntr = 1 ]]
     then
       diffdir=$EBSCUSCPY
       toarea="custom"	  
       ((diffcntr=diffcntr + 1))
       do_diff
     else
       echo "--- Bad Return Condition = $cond ---" > $HOME/TMPFILE
       cat $HOME/TMPFILE
     fi  
   fi
 fi
}
 clear
 echo "  "
 echo "\tComparing Copybook from test ... \c"
 echo "  "
 echo "\tCopybook name to compare (no extension needed): \c"
 read file1?"  > "
 file1=${file1}.CPY
 if [ -z "$file1" ]
 then
   echo "Invalid response - script terminating...."
   return  
 fi
 diffcntr=1
 diffdir=$EBSRCUCPY
 toarea="release"
 do_diff
 echo
 echo "\tPrint results? (Y/N): \c"
 read prt1
 if [[ $prt1 = "Y" ]]
 then
   echo 
   echo "\tEnter printer number 4=ISPRT4 or 6=ISPRT6 or 19=rpsprt19:  \c"
   read prtr
   if [[ $prtr = "4" ]]
   then
     lp -dISPRT_PS4_2 $HOME/TMPFILE
   fi
   if [[ $prtr = "6" ]]
   then
     lp -disprt6 $HOME/TMPFILE
   fi
   if [[ $prtr = "19" ]]
   then
     lp -drpsprt19 $HOME/TMPFILE
   fi
 fi
 rm $HOME/TMPFILE
 rm $HOME/tmpfile
 rm $HOME/tmperr
