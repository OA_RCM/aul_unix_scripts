#!/opt/perl5/bin/perl

$CONTRIB="$ENV{'CONTRIBFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPGET="/opt/ncftp/bin/ncftpget";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPGET -d ftpgetsss.log -f $PROC/ftpgetsss.cfg -E . $CONTRIB`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $CONTRIB file from 3000 successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $CONTRIB file from 3000\n"; 
    }
}
