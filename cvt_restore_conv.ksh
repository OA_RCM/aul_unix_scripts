#!/bin/ksh

# functions

function restore_plans
{
  PNAME=JOBREST
  JPRM1=$JUSER
  JPRM2=$pbfile
  execute_job
}

function restore_vtran
{
  PNAME=JOBVTRS
  JPRM1=$JUSER
  JPRM2=$vtfile
  execute_job
}

function restore_persons
{
  export PERSON_DATFILE=$EBSBKUP/$pdfile
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=cvt_addperson
  execute_job
}

# main

prog=$(basename $0)
dir=$(cd $(dirname $0); pwd)
name=$(basename $prog .ksh)
okfile=$dir/$name.ok

# usage: cvt_restore_conv.ksh [plan backup file] [vtran backup file] [person dat file]

pbfile=$1
vtfile=$2
pdfile=$3

omniprofdir=/omnitest/profile

. $omniprofdir/omniprof conv3

unset omniprofdir

# override path to get SG standard JOBREST and JOBVTRS
export PATH=/home/n244skl/scripts:$PATH

export JNAME=cvt_restore_conv
export JUSER=$LOGNAME

. FUNCTIONSFILE

set_generic_variables
make_output_directories

rm -f $okfile

$restore_plans && restore_plans
$restore_vtran && restore_vtran
$restore_persons && restore_persons

touch $okfile
