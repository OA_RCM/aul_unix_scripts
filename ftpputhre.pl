#!/opt/perl5/bin/perl

$HRE="$ENV{'HREFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputhre.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $HRE`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $HRE file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $HRE file to ODS\n"; 
    }
}
