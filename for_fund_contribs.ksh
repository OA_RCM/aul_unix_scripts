#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Job Name          : rsuxfor002.ksh                                 *
#COMMENT *  Script Name       : for_fund_contribs.ksh      CALL                *
#COMMENT *                                                 OMNI-ASU            *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run Forf. Contributions                        *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.50                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 12/03/2010                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         :                                                *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : daily multiple times                           *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 15 min.                                        *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Can be restarted anytime                    R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U                                                                     U
#COMMENT U Date: 20111214   Proj:  WMS3939        By: Paul Lewis               U
#COMMENT U Standardize document manager calls to a function.                   U
#COMMENT U                                                                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# functions

function run_report
{
  export JDESC="Running calculator..."
  display_message
  export PNAME=JOBCALC
  export JPRM1=$JUSER
  export JPRM2=FOR-FUND-CONTRIBS.txt
  execute_job
}

function get_files_for_DM
{
    mv $POSTOUT/CNTRBOUT $XJOB/${EBSRPTPFX}FORFCREDITTRANSACTIONS
    mv $POSTOUT/CNTRBERR $XJOB/${EBSRPTPFX}FORFCREDITREJECTS
}

# main

prog=$(basename $0)
export JNAME=${prog%.ksh}
export JUSER=$LOGNAME

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
fnc_log_standard_start

# do custom stuff here
$run_report && run_report
$get_files_for_DM && get_files_for_DM
$send2DocMgr && fnc_send2DocMgr

# standard exit functions
eoj_housekeeping
return 0
