#!/bin/ksh 
function do_diff
{
 diff $EBSRCUSRC/$file1 $EBSCUSSRC/$file1 > $HOME/tmpfile 2> $HOME/tmperr
 cond=$?
 diff_cond_check
}
function diff_cond_check
{
 if [[ $cond = "1" ]]
 then
   echo "--- Compare release to custom ---" > $HOME/TMPFILE
   echo "--------- $file1 ---------" >> $HOME/TMPFILE
   cat $HOME/tmpfile  >> $HOME/TMPFILE
   cat $HOME/TMPFILE
 else
   if [[ $cond = "0" ]]
   then
     echo "--- No Differences in $file1 ---" > $HOME/TMPFILE
     cat $HOME/TMPFILE
   else
     echo "--- Bad Return Condition = $cond ---" > $HOME/TMPFILE
     cat $HOME/TMPFILE
   fi
 fi
}
 clear
 echo "  "
 echo "\tComparing Source between release and custom ... \c"
 echo "  "
 echo "\tSource name to compare (no extension needed): \c"
 read file1?"  > "
 file1=${file1}.CBL
 if [ -z "$file1" ]
 then
   echo "Invalid response - script terminating...."
   return  
 fi
 do_diff
 echo
 echo "\tPrint results? (Y/N): \c"
 read prt1
 if [[ $prt1 = "Y" ]]
 then
   echo 
   echo "\tEnter printer number 4=ISPRT4 or 6=ISPRT6 or 19=rpsprt19:  \c"
   read prtr
   if [[ $prtr = "4" ]]
   then
     lp -dISPRT_PS4_2 $HOME/TMPFILE
   fi
   if [[ $prtr = "6" ]]
   then
     lp -disprt6 $HOME/TMPFILE
   fi
   if [[ $prtr = "19" ]]
   then
     lp -drpsprt19 $HOME/TMPFILE
   fi
 fi
 rm $HOME/TMPFILE
 rm $HOME/tmpfile
 rm $HOME/tmperr
