#!/opt/perl5/bin/perl

$TGT="$ENV{'TGTFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPGET="/opt/ncftp/bin/ncftpget";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPGET -d ftpgetods.log -f $PROC/ftpgetods.cfg -E . $TGT`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $TGT file from ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $TGT file from ODS\n"; 
    }
}
