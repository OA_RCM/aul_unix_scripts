#!/opt/perl5/bin/perl
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U Date: 08/03/2006  SMR: CC8403   By: Steve Loper           U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 11/19/2007  CC:  12259    By: Steve Loper           U
#COMMENT U Reason: Minor mods for standardization and security.      U
#COMMENT U Reason: Changed for dual file loads to Unix/NT locations. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

$NAME="ftpputods";
$ODS="$ENV{'ODSFILE'}";
$CTRL="$ENV{'EBSCTRL'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d $NAME.log -f $CTRL/"."$NAME.cfg -E -a ./SrcFiles $ODS`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $ODS file to UNIX location successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $ODS file to UNIX location\n"; 
    }
}
