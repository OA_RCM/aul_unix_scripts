#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXDSH014              NO-CALL         *
#COMMENT *  SchedXref Name : rsuxdsh014.ksh     TICKET TO OMNI_DEV   *
#COMMENT *  UNIX Script    : dsh_403bextract.ksh                     *
#COMMENT *                                                           *
#COMMENT *  Description  : Extract 403b data needed for data sharing *
#COMMENT *                 to be placed in the data sharing hub      *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 01/27/2009                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxdsh014.ksh                            *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : DSH1000E DSH1001E DSH1002E                *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Weekly                                    *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 3 hours (depending on # of plans & sizes) *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S    To run plans on request, this must be run as a command S
#COMMENT S    with "O" given as a parameter                          S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 01/27/2009  W: 572        By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 10/24/2009  W: 6899       By: Rick Sica             U
#COMMENT U Reason: Simply Job Doc change from Monthly to Weekly      U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function generate_card
{
  print "$1"
}

function run_extract
{
  typeset file=$1
  export Mid=$2
  export dd_RSTROUT=$XJOB/rs${Mid}.tmp
  export DS403BPLANS=$XJOB/$file
  export DS403BDISTR=$XJOB/DS403BDISTR${Mid}.tmp
  PNAME=DSH1001E
  JDESC="Run program $PNAME to create extract for segment $segment"
  execute_program
  touch $XJOB/${file}.ok
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
runctrl=$1
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME="dsh_403bextract"
export JDESC="OMNIPLUS/UX 403b EXTRACT TO DATA SHARING HUB"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables
 
######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input 

make_output_directories

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Load in a source mapping text file
### ******************************************************************
export PICSRCTYPE=$XJOB/PICSRCTYPE.idx
export dd_RSTROUT=$XJOB/rs.tmp

if $loadsrctype; then
   PNAME=DSH1002E
   JDESC="Run program $PNAME to load source types"
   execute_program
fi

### ******************************************************************
### TASK 2 - Gather plans
### ******************************************************************

export DS403BPLANS=$XJOB/DS403BPLANS

if $gatherplans; then
   if [[ "$runctrl" = "O" ]]; then
      export WIZARDDIR=$EBSINPUT/$inputdir
      rptlist=/tmp/dsh403bextlist$$
      export JDESC="gather control files"
      display_message nobanner

      ls $WIZARDDIR | grep DSH403B >$rptlist
      if [[ -s $rptlist ]]; then
         # sleep just to make sure no FTP session is going on any of the files
         sleep 5
         for f in $(cat $rptlist) ; do
            dos2ux $WIZARDDIR/$f >$XJOB/$f
            rm $WIZARDDIR/$f
            cat $XJOB/$f >>$DSH403BPLANS
         done
      else
         echo
         echo No Wizard requests to process
         export JDESC="No requests to process"
         display_message 
         return 0
      fi
      rm -f $rptlist
   else
      PNAME=DSH1000E
      JDESC="Run program $PNAME to gather plans"
      execute_program
   fi
fi

### ******************************************************************
### TASK 3 - split plans
### ******************************************************************

export split_name=DS403BPLANSsplit

if $splitplans; then
   export PLANLISTFILE=$XJOB/DS403BPLANS
   if [ -s $PLANLISTFILE ]; then
      JDESC="Determine group sizes and split plans"
      display_message nobanner
      fnc_determine_group_sizes
      fnc_split_plans_by_row
   fi
fi

### ******************************************************************
### TASK 4 - run extract process
### ******************************************************************

if $runprocess; then
   num_files=$(ls ${split_name}* | wc -l)
   if ((num_files > 0)) ; then
      let segment=0
      for f in ${split_name}* ; do
         let segment=$segment+1
         JDESC="Running extract for segment $segment"
         display_message nobanner
         run_extract $f $segment &
      done
      JDESC="Waiting on extract(s) to complete"
      display_message nobanner
      wait
      num_ok_files=$(ls | grep '.ok$' | wc -l)
      if ((num_ok_files != num_files)) ; then
         ((num_failed = num_files - num_ok_files))
         JDESC="$num_failed segment(s) failed"
         display_message nobanner
         msg="$JDESC"
         send_job_abend_notification
         abend
      else
         JDESC="all segments finished ok"
         display_message nobanner
      fi
   else
      JDESC="No plan list"
      display_message nobanner
   fi
fi

### ******************************************************************
### TASK 5 - copy files to cifs directory for inbound data sharing hub
### ******************************************************************
if $copyfiles; then
   cat DS403B_* > DS403BFILE_${tmptime}.psv
   cp DS403BFILE_${tmptime}.psv $DSHCIFS
fi

### ******************************************************************
### TASK 6 - clean up and say goodbye
### ******************************************************************

eoj_housekeeping
copy_to_masterlog
return 0
