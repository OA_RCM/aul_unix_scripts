#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : inf_fundperf.ksh        NO -CALL          *
#COMMENT *                                                           *
#COMMENT *                                             ASU           *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will create the Fund Performance *
#COMMENT *                 files needed for Statements, Web, AULFORM *
#COMMENT *                 NewKirk, and Prospectus'                  *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 08/09/2001                                *
#COMMENT *  Environment  : ENVUNIF                                   *
#COMMENT *  Called by    : rsuxinf725.ksh                            *
#COMMENT *  Script Calls : JOBDEFINE  utl_getprices.ksh  JOBVTUT     *
#COMMENT *                 runfolder.ksh  inf_fundperf.send.ksh      *
#COMMENT *  COBOL Calls  : INF1002R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time.      R
#COMMENT R                                                           R
#COMMENT R   Before restart or any time re-run a previous month,     R
#COMMENT R   set 'loadperfprices' to 'false'. If tasks 1 and 2 ran   R
#COMMENT R   ran successfully the prices used for performance are    R
#COMMENT R   already loaded into plan 00PERF and do NOT need to be   R
#COMMENT R   done again. If Task 2 fails, then may need to clean     R
#COMMENT R   out the folder that may of been created by this step.   R
#COMMENT R                                                           R
#COMMENT R   If tasks 1 & 2 need to be re-run for any reason, then   R
#COMMENT R   refer to utl_getprices for pertinent ".ini" settings.   R
#COMMENT R                                                           R
#COMMENT R   This job could abort with a condition code 12 in Task 4 R
#COMMENT R   if there are missing prices. If this is the case,       R
#COMMENT R   approval is needed to continue with the dependant jobs  R
#COMMENT R   after this one.  The main job that is dependant is      R
#COMMENT R   rsuxstp710.ksh.  This approval would most likely come   R
#COMMENT R   from systems or admin area responsible for performance  R
#COMMENT R   If the missing price(s) is/are valid. It also may       R
#COMMENT R   require data being fixed. If this cannot be resolved    R
#COMMENT R   until the next day or business day, then DO NOT let     R
#COMMENT R   dependant jobs run. We,especially, do not want send out R
#COMMENT R   participant statements with missing performance.        R
#COMMENT R                                                           R
#COMMENT R   If something is corrected because of the price errors,  R
#COMMENT R   re-run this script. If the okay is given regardless     R
#COMMENT R   of the price errors, run the called script inf_fundperf.R
#COMMENT R   send.ksh to finish sending the reports and files to     R
#COMMENT R   their proper spot. It is okay to let rsuxstp710 go if   R
#COMMENT R   all other dependancies for stp710 have been met as well.R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 08/09/2001  SMR:          By: Mike Lucas            U
#COMMENT U Reason: Created to produce Fund Performance files         U
#COMMENT U Date: 08/09/2001  SMR:          By: Mike Lucas            U
#COMMENT U Reason: Created to produce Fund Performance files         U
#COMMENT U Date: 10/22/2003  CC: 5056      By: Rick Sica             U
#COMMENT U Reason: Add DSCLMRK file                                  U
#COMMENT U Date: 12/02/2003  CC: 5056      By: Rick Sica             U
#COMMENT U Reason: Production problem with DSCLMRK file              U
#COMMENT U Date: 12/02/2003  CC: 5851      By: Rick Sica             U
#COMMENT U Reason: Add a new file dd for the Cobol program and       U
#COMMENT U         send it to the LAN                                U
#COMMENT U Date: 04/02/2004  CC: 6076      By: Rick Sica             U
#COMMENT U Reason: Run new script to load prices onto 00PERF plan    U
#COMMENT U Date: 08/02/2004  CC: 6076      By: Rick Sica             U
#COMMENT U Reason: Fix sed - restore of T029 cards                   U
#COMMENT U Date: 10/30/2006  CC: 10965     By: Mike Lewis            U
#COMMENT U Reason: Added function for checking funds to ignore.      U
#COMMENT U Date: 07/06/2007  CC: 11637/11807 By: Rick Sica           U
#COMMENT U Reason: Get Expense Ratio from Global External IC file.   U
#COMMENT U Date: 09/25/2007  CC12197   By: Zera Holladay             U
#COMMENT U Reason: Changed insecure protocols to secure.             U
#COMMENT U Date: 20110526    WMS 2998  By: Tony Ledford              U
#COMMENT U Reason: Eliminate connect direct usage.                   U
#COMMENT U Date: 20111005   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT U Date: 20130220   Proj:  WMS8801        By: Rick Sica      U
#COMMENT U newfunds moved to AULDATA                                 U
#COMMENT U Date: 20131108   WMS8043         By: Louis Blanchette     U
#COMMENT U Reason: Inv ID 2 to 4  Change name of investment table    U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function check_for_funds
{
  if [ $cond -gt 4 ] ; then
    rm -f small.err.file smaller.err.file final.err.file
    grep "PRICE-ID"  $dd_ERRLOGO >> small.err.file
    cat small.err.file | cut -c66-69 >> smaller.err.file
    cond=0
    while read fund
    do
      grep -q "$fund" $AULDATA/newfunds
      [[ $? = 0 ]] || print $fund >> final.err.file
    done < smaller.err.file
    if [[ ! -f final.err.file ]] ; then
        cond=0
    else
        sort -o unique.err.file -u final.err.file
        cond=99
    fi
  fi
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
#clear
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX FUND PERFORMANCE"
export ENVFILE=ENVUNIF
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories
export MKOUTDIR=current

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_monthend_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Run the script to get prices from global 000001 and
###          create card file with prices set up to go to 00PERF.
### ******************************************************************
if $loadperfprices ; then
  PNAME=utl_getprices.ksh
  JDESC="running script $PNAME to get prices"
  execute_job

### ******************************************************************
### TASK 2 - Run JOBUNIC to load prices to 00PERF.
### ******************************************************************
  export perfcards=T029PERFCOPY
  export origcards=$XJOB/T029COPYCARDS
  cp $origcards $EBSCARD/$perfcards
  PNAME=JOBUNIC
  JPRM1=$JUSER
  JPRM2=$perfcards
  JPRM3=01
  JDESC="running JOBVTUT to load T029 cards"
  execute_job
fi

### ******************************************************************
### TASK 3 - Define files needed
### ******************************************************************
if $runperformance ; then
  export JDESC="Defines files needed by Cobol program"
  display_message nobanner

  export dd_INVTABLE=$AULDATA/investment_tbl.txt
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_PRTFLEO=$XJOB/PRTFLEO
  export dd_PRTFLIO=$XJOB/PRTFLIO
  export dd_PRTFLPO=$XJOB/PRTFLPO
  export dd_PRTRPTO=$XJOB/${EBSRPTPFX}FUNDPERFRPT
  export dd_ERRLOGO=$XJOB/FUNDPERFLOG
  export dd_TMPINVTABLE=$XJOB/investment_tbl.tmp

### ******************************************************************
### TASK 4 - Run the COBOL program to produce Fund Performance files.
### ******************************************************************

  PNAME=INF1002R
  JDESC="run COBOL program $PNAME"
  display_message nobanner
  logfile=$PNAME.log
  errfile=$PNAME.err

  if [[ $JUSER != "DEBUG" ]] ; then
    $COBRUN $PNAME > $XJOB/$logfile 2> $XJOB/$errfile
  else
    echo "logfile=$logfile"
    echo "errfile=$errfile"
    COBSW=+A$COBSW
    $COBRUN $PNAME
  fi

  ## check job status
  cond=$?
  $check_for_funds && check_for_funds

  if [ $cond -gt 4 ] ; then
    print "$PNAME Abended with cond=" $cond
    print "***************************************"
    print "**                                   **"
    print "**   $PNAME Ended Abnormally         **"
    print "**        in $SPROG                  **"
    print "**           Error Code = $cond      **"
    print "**                                   **"
    print "***************************************"

    cond=99
    if [[ $OMNIsysID = "prod" ]] ; then
      JDESC="special failure mode - queueing error files for ftp"
      display_message nobanner
      
      fromfile=$dd_ERRLOGO
      tofile="FUNDPERFLOG.$rundate.TXT"
      fnc_ftpq "$fromfile=$tofile"

      fromfile=$dd_PRTRPTO
      tofile="FUNDPERFRPT.$rundate.TXT"
      fnc_ftpq "$fromfile=$tofile"

      cond=$ftp_trigger_exit_code
    fi

    JDESC="Exit code: $cond"
    display_message nobanner
    exit $cond
  fi

  export JDESC="Run COBOL program $PNAME complete"
  display_message nobanner

### ******************************************************************
### TASK 5 - Call script that sends reports and files
### ******************************************************************

  if $sendperffiles ; then
    PNAME=inf_fundperf_send.ksh
    JPRM1="$XJOB"
    JPRM2="$rundate"
    JDESC="Running script to send fund performance files"
    execute_job
  fi
fi

### ******************************************************************
### TASK 6 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
