#!/usr/local/bin/perl

use Pod::Usage;
use FindBin;
use lib $FindBin::Bin;
use Folder;

pod2usage unless (@ARGV);

$lastEditTime = shift;

$folder = Folder->new();
while (<>)
{
  chomp;
  next if ($folder->add_rec($_));
  process_folder($folder);
  $folder = Folder->new();
  $folder->add_rec($_);
}
process_folder($folder) if (!$folder->empty());

# 00200   $TIHDR G30050000000000           00000000
# 00201   DEFINE    20050121.I541
# 00202         000000000          *         PF 00000000
# 00203                 00000000000000000000000000000000
# 54100   $TIHDR G30050000000000           20050121
# 54101      306525551           001
# 54102   2895505 UNIVERISTY PARK DR
# 54103   290APT D
# 54104
# 54105   293MISHAWAKA
# 54106
# 54107   295465451264
# 54108   $VTD2                20050121102611ESPPD
# 54199   $VTDEXT54120050121.I541         000000120050121102611   00000000

sub process_folder
{
  my $folder = shift;
  my $folder2 = Folder->new();
  $folder2->add_tran($folder->header());
  # print STDERR $folder2->tostr();
  my $trans = $folder->trans();
  TRAN: foreach my $tran (@$trans)
  {
    my $recs = $tran->recs();
    foreach my $rec (@$recs)
    {
      if ($rec =~ /\$VTD2/)
      {
        if (substr($rec, 29, 14) > $lastEditTime)
        {
          $folder2->add_tran($tran);
          next TRAN;
        }
      }
    }
  }
  if (!$folder2->trans()->[0]->empty())
  {
    print $folder2->tostr();
  }
}

__END__

=head1 NAME

def.pl - Filter old transactions out of a card deck

=head1 SYNOPSIS

def.pl [timespec] [card file]

=head1 DESCRIPTION

This program is used to filter transactions from the daily
edit card deck(s) so that previously edited transactions
won't be edited again.

The timespec argument is CCYYMMDDhhmmss where:

=over 4

=item CC

Century

=item YY

Year

=item MM

Month

=item DD

Day

=item hh

Hour

=item mm

Minute

=item ss

Seconds

=back
