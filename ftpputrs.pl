#!/opt/perl5/bin/perl

$RS03="$ENV{'RS03FILE'}";
$RS18="$ENV{'RS18FILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
&check2;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputrs.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $RS03`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $RS03 file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $RS03 file to ODS\n"; 
    }
}

sub check2
{
    $result=`$NCFTPPUT -d ftpputrs2.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $RS18`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $RS18 file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $RS18 file to ODS\n"; 
    }
}
