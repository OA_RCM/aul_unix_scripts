#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXCOM710                CALL            *
#COMMENT *  Script Name  : com_todss1000.ksh                         *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to read sequential history and create *
#COMMENT *                 financial transactions to send to DSS     *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 05/24/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : COM1000E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R  *** If restored from archive directory for rerunning,    R
#COMMENT R      file must be re-archived.                            R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 05/24/2001  SMR: PEN????  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program COM1000E               U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2002  SMR: CC3353   By: Mike Lucas            U
#COMMENT U Reason: Moved Folder-ID up in the sort of SEQAHST         U
#COMMENT U                                                           U
#COMMENT U Date: 09/19/2003  SMR: CC4485   By: Rick Sica             U
#COMMENT U Reason: Add new extract file for errors                   U
#COMMENT U                                                           U
#COMMENT U Date: 01/07/2004  SMR: CC5346   By: Rick Sica             U
#COMMENT U Reason: Add new input file                                U
#COMMENT U                                                           U
#COMMENT U Date: 05/10/2005  SMR: CC8191   By: Mike Lewis            U
#COMMENT U Reason: Change delivery point of output files and         U
#COMMENT U         functionalize copy to POSTOUT.                    U
#COMMENT U                                                           U
#COMMENT U Date: 02/02/2007  SMR: CC10698  By: Judy Stewart          U
#COMMENT U Reason: Change commission system to read additional input U
#COMMENT U         file that contains HWM & AAP amounts.             U
#COMMENT U                                                           U
#COMMENT U Date: 03/12/2008  SMR: CC11779  By: Danny Hagans          U
#COMMENT U Reason: Change the sort key for Seqential History to      U
#COMMENT U         match 5.50 record layout.                         U
#COMMENT U                                                           U
#COMMENT U Date: 20110607  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function send2DocMgr
{
  cat EXTRTNA >> $EBSXFER/comm/COMMFILE
  cat EXTRTMA >> $EBSXFER/comm/COMMFILE
  cat EXTRTFA >> $EBSXFER/comm/COMMFILE
  cat EXTRTTA >> $EBSXFER/comm/COMMFILE
  cat EXTRTEA >> $EBSXFER/comm/COMMFILE

  $copy_to_postout && copy_to_postout

  export COMERRDocMgr=${EBSRPTPFX}COMMERRFILE
  mv COMMERRFILE $COMERRDocMgr
  fnc_send2DocMgr
  JDESC="Sent $COMERRDocMgr to document manager"
  display_message nobanner
}

function copy_to_postout
{
  cp EXTRTNA $AULOUT
  cp EXTRTMA $AULOUT
  cp EXTRTFA $AULOUT
  cp EXTRTTA $AULOUT
  cp EXTRTEA $AULOUT
  cp COMMERRFILE $AULOUT
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
  prog=$(basename $0)
  export integer NumParms=$#
  export JUSER=${JUSER:-$LOGNAME}
  export JGROUP=${JGROUP:-01}
  export JNAME=${prog%.ksh}
  export JDESC="OMNIPLUS/UX COMMISSION FINANCIAL TO DSS PROCESS"
  export ENVFILE=ENVBATCH
  export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
check_input

make_output_directories

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_daily_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
  . JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1 - Define files
### ******************************************************************
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_SEQAHST=$XJOB/SEQAHST
  export dd_SEQAHIN=$XJOB/SRTAHST
  export dd_HELPER2=$AULDATA/HELP2
  export dd_EXTRTNA=$XJOB/EXTRTNA
  export dd_EXTRTMA=$XJOB/EXTRTMA
  export dd_EXTRTFA=$XJOB/EXTRTFA
  export dd_EXTRTTA=$XJOB/EXTRTTA
  export dd_EXTRTEA=$XJOB/EXTRTEA
  export dd_COMMERRFILE=$XJOB/COMMERRFILE
  export dd_DSSOPUPD=$AULDATA/DSSOPUPD
  export dd_HWMEXTIN=$XJOB/pcr_hwm_extract.txt

### ******************************************************************
### TASK 2 - Sort the Sequential History File
### on Plan/Plan-Sequence, Folder, Part-ID, Run-Date, Trade-Date
### ******************************************************************
 if [ "$JSTN" = "YES" ]; then
   sort -o $XJOB/SRTAHST -k .2,.10 -k .36,.43 -k .28,.35 -k .61,.66 -k .70,.79 -k .11,.27 $AULOUT/SEQAHST.${JUSER}
 else
   sort -o $XJOB/SRTAHST -k .2,.10 -k .36,.43 -k .28,.35 -k .61,.66 -k .70,.79 -k .11,.27 $AULOUT/SEQAHST
 fi

### ******************************************************************
### TASK 3 - Run the COBOL program to extract contribution information
### from the Omni Sequential History File to the Commission Files.
### ******************************************************************

cd $XJOB

 if [[ -f $EBSXFER/comm/pcr_hwm_extract.txt ]] ;
 then
   if [ "$OMNIsysID" = "prod" ];
   then
     JDESC="PRODUCTION - moving hwm extract file"
     display_message
     mv $EBSXFER/comm/pcr_hwm_extract.txt $XJOB
   else
     JDESC="TEST - copying hwm extract file"
     display_message
     cp $EBSXFER/comm/pcr_hwm_extract.txt $XJOB
   fi
 else
  JDESC="No $EBSXFER/comm/pcr_hwm_extract.txt - aborting"
  display_message
  return 99
 fi

PNAME=COM1000E
JDESC="Run COBOL program $PNAME"
execute_program

JDESC="Extract files  EXTRTNA,MA,FA,TA written"
display_message nobanner

### ******************************************************************
### TASK 4 - Send commission files to transfer area for CDFT pickup
### ******************************************************************

$send2DocMgr && send2DocMgr

### ******************************************************************
### TASK 5 - Clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  copy_to_masterlog
  create_html
  return 0
