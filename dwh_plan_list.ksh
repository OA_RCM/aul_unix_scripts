#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *  CONTRLM JOBNAME   : RSUXDWH001                 NON-CALL            *
#COMMENT *  UNIX Pointer Name : rsuxdwh001.ksh             OMNI-ASU            *
#COMMENT *  UNIX Script Name  : dwh_plan_list.ksh                              *
#COMMENT *                                                                     *
#COMMENT *  Description       : Create plan lists for dwh_driver.ksh           *
#COMMENT *                                                                     *
#COMMENT *  Author            : AUL - Gary Pieratt                             *
#COMMENT *  Created           : 05/23/2011                                     *
#COMMENT *  Called by         : Scheduled or manual
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       : None                                           *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Daily                                          *
#COMMENT *  Est. Run Time     : Less than 10 minutes                           *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S  None                                                               S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Must be looked at by ASU support personnel prior to restart.       R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT T TASK SEQUENCE ------------------------------------------------      T
#COMMENT T  1 - create plan lists in $LISTDIR                                  T
#COMMENT T  2 - standard exit function                                         T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Gary Pieratt     WMS5501   05/18/2011  original                    U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   12/20/2011                              U
#COMMENT U  Reason:  correct heading. allow for LISTDIR (plan lists location)  U
#COMMENT U  Reason:  remove redundant variables set in ini file.               U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   01/23/2012                              U
#COMMENT U  Reason:  correct consolidation of extracts into one final extract. U
#COMMENT U                                                                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
# functions
function generate_card
{
  echo $1
}
# main
export JNAME=$(basename $0 .ksh)
export JUSER=${JUSER:-$LOGNAME}
export ENVFILE=ENVNONE

# standard function calls
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start

cd $XJOB
# Task 1 - create plan lists in $LISTDIR
export PMSG="split planlist"
JDESC="Building plan lists..."
display_message nobanner

fnc_get_plan_list
cp $PLANLISTFILE $LISTDIR

for list_group in ${plan_list_split_array[@]} ; do
    export NUMGROUPS=$list_group
    fnc_determine_group_sizes   ## calculate number of plans per JOBUNIC run (per deck)
    fnc_split_plans_by_row      ## create actual card decks based on indicated method
    typeset -i mbr=1
    while (( $mbr <= $list_group )); do
      split_name_mbr=$split_name${mbr}.${rundate}
      split_name_final=${split_name}_${list_group}_${mbr}
      mv $split_name_mbr $split_name_final
      cp $split_name_final $LISTDIR
      ((mbr+=1))
    done
done

# Task 2 - standard exit function
touch $XJOB/rs.tmp  ## keeps eoj routine from generating err message re rs.tmp file
eoj_housekeeping
