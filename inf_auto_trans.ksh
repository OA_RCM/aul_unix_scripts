#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Script Name       : inf_auto_trans.ksh      NON-CALL -- OMNI ASU   *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run INF-AUTO-TRANS  (MidAmerica transaction    *
#COMMENT *                      automation)                                    *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.50                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 04/01/2008                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         : rsuxinf811.ksh (sched_xref)                    *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Daily                                          *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 15 min.                                        *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Create a help ticket.  May need to delete   R
#COMMENT R                         OMNI transactions.                          R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Glen McPherson   CC11334   04/01/08   original                     U
#COMMENT U  Glen McPherson   PROJ567   06/03/08   Empty file email/archiving   U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# functions

function sort_ma_file
{
  midamerf=$EBSXFER/midamerica/in/midamerica.txt
  midamert=$EBSXFER/midamerica/in/midamerica.tmp
  if [ `cat $midamerf | wc -c` -gt 10 ]; then
    JDESC="Sorting of MidAmerica input file"
    display_message nobanner
    PMSG="Error on sort step"
    sort -o $midamert -k .1,.16 $midamerf
    cond=$?; cond_limit=0; check_status
    mv $midamert $midamerf
  else
    export JDESC="MidAmerica file is empty - nothing to process"
    display_message
    export email_message="Alert: MidAmerica file is empty - nothing to process."
    export email_subject="MidAmerica file is empty."
    export email_address="M.AUL.RS.CMS.Team1.Unit2@oneamerica.com"
    echo $email_message | mailx -s "$email_subject" $email_address
  fi
}

function run_ma_auto
{
  if [ `cat $midamerf | wc -c` -gt 10 ]; then
     export JDESC="Running INF-AUTO-TRANS calculator..."
     display_message
     export PNAME=JOBCALC
     export JPRM1=$JUSER
     export JPRM2=INF-AUTO-TRANS.txt
     export INFILE=$EBSXFER/midamerica/in/midamerica.txt
     execute_job

     export email_message="MidAmerica process completed."
     export email_subject="MidAmerica file processed."
     export email_address="M.AUL.RS.CMS.Team1.Unit2@oneamerica.com"
     echo $email_message | mailx -s "$email_subject" $email_address
  fi
}

function run_upd_userid
{
  if [ `cat $midamerf | wc -c` -gt 10 ]; then
     export JDESC="Running INF-TRANS-UPD calculator..."
     display_message
     export PNAME=JOBCALC
     export JPRM1=$JUSER
     export JPRM2=INF-TRANS-UPD.txt
     export INFILE=$EBSXFER/midamerica/in/midamerica.txt
     execute_job
  fi
}

function archive_files
{
  if [ `cat $midamerf | wc -c` -gt 10 ]; then
     mv $INFILE $INFILE.$rundate.$tmptime
     archive $INFILE.$rundate.$tmptime
  fi
}

function local_error_handling
{
     export email_message="MidAmerica process failed."
     export email_subject="MidAmerica file did not process."
     export email_address="M.AUL.RS.CMS.Team1.Unit2@oneamerica.com"
     echo $email_message | mailx -s "$email_subject" $email_address
}

# main

export JNAME=inf_auto_trans
export JUSER=$LOGNAME

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

# Task 1 - sort input file
$sort_ma_file && sort_ma_file

# Task 2 - do Jobcalc here
$run_ma_auto && run_ma_auto

# Task 3 - do Jobcalc here
$run_upd_userid && run_upd_userid

# Task 4 - archive file
$archive_files && archive_files

# Task 5 - standard exit function
eoj_housekeeping
