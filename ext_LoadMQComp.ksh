#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : HSUXEXT005             CALL            *
#COMMENT *  Schedule Name   : rsuxext005.ksh         OMNI-ASU        *
#COMMENT *  Unix Script     : ext_LoadMQComp.ksh                     *
#COMMENT *                                                           *
#COMMENT *  Description  : OMNI LOAD MQ Series Completion Record     *
#COMMENT *                 (Rule 22c-2  aka MTAS)                    *
#COMMENT *  Author       : Louis Blanchette                          *
#COMMENT *  Created      : 03/19/2007                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : EXT1060L                                  *
#COMMENT *  Frequency    :                                           *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 60 seconds max                            *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/02/2009  #: W1519     By: Rick Sica              U
#COMMENT U Reason: Omni 5.8                                          U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=${prog%.ksh}
export JDESC="OMNI LOAD MQ Series Completion Record"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start


### ******************************************************************
### TASK 1 - Run Cobol program
### ******************************************************************
export dd_REQUEST=$AULDATA/MQREQUESTS

export COBSAVE=$COBRUN
export COBRUN=$(dirname $COBRUN)/$AUL_RTS

export PNAME=EXT1060L
export JDESC="Run COBOL program $PNAME"
export PARM="$PMQ_QMGR $PMQ_QNAME"
execute_program

export COBRUN=$COBSAVE

### ******************************************************************
### TASK 2 - Move the Requests file to XJOB
### ******************************************************************
mv $AULDATA/MQREQUESTS $XJOB

### ******************************************************************
### TASK 3 - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
