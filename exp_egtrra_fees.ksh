#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Job Name          : rsuxexp706.ksh                                 *
#COMMENT *  Script Name       : exp_egtrra_fees.ksh        NON-CALL            *
#COMMENT *                                                 OMNI-ASU            *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run EGTRRA Fee process                         *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.20                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 01/08/2009                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         :                                                *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : monthly                                        *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 5 min.                                         *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Can be restarted anytime                    R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U Date: 07/01/09   WMS:1519C     By: Rick Sica                        U
#COMMENT U Reason: Omni 5.8 Upgrade                                            U
#COMMENT U                                                                     U
#COMMENT U WMS1519 - 5.8 Upgrade            10/02/09   Glen McPherson          U   
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# functions

function run_report
{
  export JDESC="Running EXP-EGTRRA-FEES calculator..."
  display_message
  export PNAME=JOBCALC
  export JPRM1=$JUSER
  export JPRM2=EXP-EGTRRA-FEES.txt
  execute_job
}

function rename_file
{
  export JDESC="Renaming input file..."
  display_message
  ls /cifs/billing/EGTRRA*.csv > csvout.txt
  if [[ -s csvout.txt ]]; then
      for f in $(cat csvout.txt) ; do
         nfile=$f.sav
	 cp $f $POSTOUT
         mv $f $nfile
     done
  fi
}

# main

export JNAME=exp_egtrra_fees
export JUSER=$LOGNAME

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
fnc_log_standard_start

# do custom stuff here
$rename_file && rename_file
$run_report && run_report

# standard exit function
eoj_housekeeping