#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : exp_uab_fee.ksh           CALL            *
#COMMENT *  SchdXrf Name : rsuxexp703.ksh            OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Description  : Post unattractive business fee            *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - GJP                                 *
#COMMENT *  Created      : 12/18/2006                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE JOBCALC ASC-LOWASSETS-FEE.txt   *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Do not restart without           R
#COMMENT R                          programmer intervention          R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 20110321   Proj: WMS3939   By:  Paul Lewis          U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=exp_uab_fee
export JDESC="OMNIPLUS/UX Monthly UAB Bill Creation"
export ENVFILE=ENVBATCH

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
check_input 
make_output_directories
export MKOUTDIR=current

######################
### Override daily date and rundate 
### if one is passed in
######################
fnc_set_monthend_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1 - Create Expense Bill Report
### ******************************************************************

cd $XJOB
export JDESC="Call JOBCALC to create UAB fee"
display_message nobanner
PNAME=JOBCALC
JPRM1=$JUSER
JPRM2=ASC-LOWASSETS-FEE.txt
JDESC="Running JOBCALC $JPRM2 at $(date +%H:%M)"
execute_job

JDESC="JOBCALC $JPRM2 complete at $(date +%H:%M)"
display_message nobanner

### ******************************************************************
### TASK 2 - Send to document manager
### ******************************************************************

if $sendextract; then
   if [[ -s CC9759.log ]]; then
      mv CC9759.log ${EBSRPTPFX}BILLEDFEERPT2
      $send2DocMgr && fnc_send2DocMgr
   fi
fi

### ******************************************************************
### TASK 5 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
