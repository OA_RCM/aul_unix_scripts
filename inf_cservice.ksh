#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  ECS JOBNAME    : RSUXINF708              CALL            *
#COMMENT *  SchedXref Name : rsuxinf708.ksh          OMNI-ASU        *
#COMMENT *  UNIX Script    : inf_cservice.ksh                        *
#COMMENT *                                                           *
#COMMENT *  Description  : Populates the update file with new or     *
#COMMENT *                 changed records on the current days file  *
#COMMENT *                 compared to the previous days file        *
#COMMENT *                 for the CSERVICE & Enrollment Kit feeds.  *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 05/25/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : FUNCTIONSFILE JOBDEFINE                   *
#COMMENT *  COBOL Calls  : AULCSCMP  AULCSERV  EKT1000E              *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *  Est.Run Time : > 3 hours                                 *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R  Back-up files will need to be moved back into AULDATA    R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T   TASK SEQUENCE ------------------------------------------T
#COMMENT T   1 - Backup files                                        T
#COMMENT T   2 - Run Cservice current extract program                T
#COMMENT T   4 - Send CService files to ODS                          T
#COMMENT T   5 - Send specified files to document manager            T
#COMMENT T   6 - Reset files in AULDATA with updated data            T
#COMMENT T   LAST - Clean up and say goodbye                         T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 05/25/2001  SMR: ******** By: Patty Wamsley         U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 08/21/2001  SMR: ******** By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U Date: 10/04/2002  SMR: CC3522   By: Mike Lewis            U
#COMMENT U Reason: Clean up of large files after copies completed.   U
#COMMENT U Date: 07/31/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U Date: 11/02/2007  CC: 12259     By: M.Slinger/S.Loper     U
#COMMENT U Reason: Send file to NT                                   U
#COMMENT U Date: 20110318  Proj:  WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  return 0  ## for future use
}
# Send file to Informatica can be turned off in the ini file.
function send_file_to_informatica
{
  JDESC="Sending $ODSFILE to ODS [UNIX Server]"
  display_message nobanner
  ftpputods.pl || { JDESC="ERROR SENDING $ODSFILE to ODS [$EBSFSET]"; abend; }
}
# Send file to NT can be turned off in the ini file.
function send_file_to_nt_informatica
{
  PMSG="In function send_file_to_nt_informatica"
  JDESC="Copying $ODSFILE to shared path $XFER_TGT"
  display_message nobanner
  cp $ODSFILE $XFER_TGT && cond=0 || cond=99
  [[ $cond != 0 ]] && { JDESC="ERROR placing $ODSFILE in $XFER_TGT (ABORT!)"; abend; }
  JDESC="Sucessfully sent file $ODSFILE to NT [$XFER_TGT]"
  display_message nobanner
  unset PMSG
}
###########################################################################
##main#####################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
integer NumParms=$#

export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX C Service Extracts"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
## Standard startup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
. JOBDEFINE
fnc_log_standard_start
check_input
check_overrides

           ###################################################
cd $XJOB   ## processing effectively starts after this point
           ###################################################

### ******************************************************************
### TASK 1 - Backup files
### ******************************************************************
export dd_UTCUSTBASE=$XJOB/utcustprv.txt
if $backupcservfiles ; then
   if [[ -s ${dd_UTCUSTBASE}.gz ]]; then
      gunzip $dd_UTCUSTBASE
   else
      if [[ -s $AULDATA/utcustcur.txt ]]; then
         cp $AULDATA/utcustcur.txt $dd_UTCUSTBASE
      else
         touch $dd_UTCUSTBASE
      fi
   fi
fi

### ******************************************************************
### TASK 2 - Run Cservice current extract program
### ******************************************************************
export dd_RSTROUT=$XJOB/rs.tmp
export dd_UTCUSTOUT=$XJOB/utcustcur.txt

if $runextract ; then
   PNAME=AULCSERV
   JDESC="Run program $PNAME to extract data $(date +%H%M%S)"
   export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
   export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
   export dd_HELPER2=$AULDATA/HELP2
   execute_program
fi

### ******************************************************************
### Task 3 - Run CService Compare program
### ******************************************************************
export dd_UTCUSTOUT=$XJOB/utcustupd.txt
export dd_UTCUSTNEW=$XJOB/utcustcur.txt
if $runcservcompare ; then
   PNAME=AULCSCMP
   JDESC="Run program $PNAME to compare CService data $(date +%H%M%S)"
   execute_program
fi

### ******************************************************************
### TASK 4 - Send CService files to ODS 
### ******************************************************************
export cserverror="N"
export resetcservfiles=false
if $sendcservfiles ; then
  export ODSFILE=$dd_UTCUSTOUT
  $send_to_unix && send_file_to_informatica
  $send_to_nt && send_file_to_nt_informatica
  resetcservfiles=true
fi

### ******************************************************************
### TASK 5 - Send specified files to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 6 - Reset files in AULDATA with updated data
### ******************************************************************
if $resetcservfiles ; then
   export JDESC="Reseting C Service files in AULDATA"
   display_message nobanner
   cp $dd_UTCUSTOUT $AULOUT  && rm $XJOB/utcustupd.txt || { JDESC="ERROR on copy of update file to POSTOUT"; abend; }
   cp $dd_UTCUSTNEW $AULDATA && rm $XJOB/utcustcur.txt || { JDESC="ERROR on copy of update file to AULDATA"; abend; }
   nohup gzip $dd_UTCUSTBASE & 
fi

### ******************************************************************
### TASK LAST - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
