#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Script Name       : exp_dueunpaid_ext.ksh   NON-CALL -- OMNI ASU   *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run EXP-DUE-UNPAID-RPT.txt                     *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.80                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 06/04/2010                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         : rsuxext713.ksh (sched_xref)                    *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : 3rd Friday each month                          *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 15 min.                                        *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Create a help ticket.  May need to delete   R
#COMMENT R                         OMNI transactions.                          R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Glen McPherson   WMS3527  07/24/2010  original                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# functions

function run_rpt
{
  export JDESC="Running EXP-DUE-UNPAID-RPT calculator..."
  display_message
  export PNAME=JOBCALC
  export JPRM1=$JUSER
  export JPRM2=EXP-DUE-UNPAID-RPT.txt
  execute_job
}

function copy_rpt
{
  ux2dos $POSTOUT/DUEUNPAIDRPT.TXT > /cifs/billing/DUEUNPAID$rundate.txt
}

# main

export JNAME=exp_dueunpaid_ext
export JUSER=$LOGNAME
export rundate=`head -n 1 $AULDATA/DATENIGHT | cut -c1-8`

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

# Task 1 - do Jobcalc here
$run_rpt && run_rpt

# Task 2 - copy files
$copy_rpt && copy_rpt

# Task 3 - standard exit function
eoj_housekeeping
