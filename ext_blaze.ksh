#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  Job Name     : RSUXEXT702                    CALL        *
#COMMENT *  Script Name  : rsuxext702.ksh            OMNI-ASU        *
#COMMENT *  Script Name  : ext_blaze.ksh                             *
#COMMENT *                                                           *
#COMMENT *  Description  : Blaze extract                             *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 10/02/2002                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : FUNCTIONSFILE                             *
#COMMENT *  COBOL Calls  : EXT1002E                                  *
#COMMENT *  Frequency    : hourly                                    *
#COMMENT *  Est.Run Time : depends on number of plans in control file*
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                   R
#COMMENT R                                                           R
#COMMENT R  Before resubmitting job after a failure, the control     R
#COMMENT R  files need to be placed back in the WIZARDDIR so they    R
#COMMENT R  will be picked up again.                                 R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE ----                                       T
#COMMENT T Last - Clean up and say goodbye                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U UPDATE HISTORY                                            U
#COMMENT U                                                           U
#COMMENT U Date: 08/03/2006  SMR: CC8403   By: Steve Loper           U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 02/03/2011  WMS5718   By: Zera Holladay             U
#COMMENT U Reason: Corrections due to NAS changes.                   U
#COMMENT U                                                           U
#COMMENT U Date: 20110519  WMS: 2998     By: Tony Ledford            U
#COMMENT U Reason: Eliminate connect direct usage and bring script   U
#COMMENT U         up to standards.                                  U
#COMMENT U                                                           U
#COMMENT U Date: 20111005   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################

function log
{
  JDESC="$*"
  display_message nobanner
}

function gather_control_files
{
  log "Gathering control files..."

  ls $WIZARDDIR | grep blazectl >$rptlist
  if [[ -s $rptlist ]]
  then
     #
     # sleep just to make sure no FTP session is going on any of the files
     #
    sleep 5
    for f in $(cat $rptlist) ; do
      cat $WIZARDDIR/$f
    done >$blazectl
    for f in $(cat $rptlist) ; do
      mv $WIZARDDIR/$f $XJOB
    done
  else
    log "Nothing to process"
    process_control_files=false
  fi
}

function process_control_files
{
  cp $EBSCTRL/DUMBTRAN.DAT $XJOB
  cp $EBSCTRL/DUMBTRN2.DAT $XJOB
  touch $XJOB/DUMMY3.DEF
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_TRANIN=$XJOB/DUMBTRAN.DAT
  export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
  export dd_BLAZECTL=$blazectl
  PNAME=EXT1002E
  execute_program
}

function send_error_reports
{
  cd $XJOB
  for f in BLAZEERR*; do
    [[ -f $f ]] || continue
    mv $f $EBSRPTPFX$f
  done
  fnc_send2DocMgr
}

function send_extracts
{
  cd $XJOB
  fnc_ftpq BLAZEO*
}

###########################################################################
####MAIN###################  main section  #################SECTION########
###########################################################################
prog=$(basename $0)

######################
## setup standard local variables
######################
export JNAME=${prog%.ksh}
[[ -z $JUSER ]] && export JUSER=$LOGNAME
export JDESC="Blaze extract"
export ENVFILE=ENVBATCH

######################
### standard script setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start

cd $XJOB

. JOBDEFINE

export rptlist=$XJOB/blazelist
export blazectl=$XJOB/blazectl.txt

#
# Task 1 - gather control files
#

$gather_control_files && gather_control_files

#
# Task 2 - process control files
#

$process_control_files && process_control_files

#
# Task 3 - send error report(s) to a document manager
#

$send_error_reports && send_error_reports

#
# Task 4 - queue extract(s) for FTP
#

$send_extracts && send_extracts

### Task Last - Clean up and say goodbye
eoj_housekeeping
return $cond
