#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXINF717                CALL            *
#COMMENT *  UNIX POINTER : rsuxinf717.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : inf_cre8trxout.ksh                        *
#COMMENT *                                                           *
#COMMENT *  Description  : OMNI FINANCIAL EXTRACT                    *
#COMMENT *                 This job reads Sequential History and     *
#COMMENT *                 creates the extract file TRXOUT. It is    *
#COMMENT *                 then transfered to OF server.             *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.50                        *
#COMMENT *  Author       : Sungard - SKL                             *
#COMMENT *  Created      : 09/17/2001                                *
#COMMENT *  Environment  : ENVGL                                     *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : FUNCTIONSFILE JOBDEFINE SORTSEQGL         *
#COMMENT *  COBOL Calls  : AULEX001 and AULEXPRT                     *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  5 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S    Parameter is either supplied in full or none.          S
#COMMENT S    Has 2 parms: plan number - 6 digits, followed          S
#COMMENT S      by date in CCYYMMDD format.                          S
#COMMENT S    Either field may be set to "all" option by zeros.      S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T   1 - Extract GL Information from sorted sequential       T
#COMMENT T        history file                                       T
#COMMENT T   2 - Copy files to AULOUT                                T
#COMMENT T   LAST - End of job housekeeping                          T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/17/2001  SMR:          By: Steve Loper           U
#COMMENT U Reason: Omni Financial Extract                            U
#COMMENT U                                                           U
#COMMENT U Date: 11/02/2001  SMR:          By: Dan Dubinski          U
#COMMENT U Reason: Customized for client, AUL.                       U
#COMMENT U                                                           U
#COMMENT U Date: 11/12/2001  SMR:          By: Mike Lucas            U
#COMMENT U Reason: Standardized                                      U
#COMMENT U                                                           U
#COMMENT U Date: 12/02/2004  SMR: CC6212   By: Steve Loper           U
#COMMENT U Reason: Move to 2004 code standards.                      U
#COMMENT U Reason: Change dd_SEQAHST=$dd_SORTOUT                     U
#COMMENT U Reason: Manipulate EXTFH with IGNORELOCK code.            U
#COMMENT U                                                           U
#COMMENT U Date: 12/03/2004  SMR: CC7513   By: Steve Loper           U
#COMMENT U Reason: Abend job if zero byte SeqAhst exits.             U
#COMMENT U Reason: Bypass processing postout files if copy_xjob flag U
#COMMENT U      is off.                                              U
#COMMENT U                                                           U
#COMMENT U Date: 02/06/2006  SMR: CC9218   By: Tony Ledford          U
#COMMENT U Reason: Remove abend code on empty sequential history     U
#COMMENT U         file. FOR HOP ONLY!!!!!!!!                        U
#COMMENT U                                                           U
#COMMENT U Date: 08/01/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2006   CC: 9056     By: Steve Loper           U
#COMMENT U Reason: Changes required for HOP.                         U
#COMMENT U                                                           U
#COMMENT U Date: 08/30/2006  SMR: CC10569   By: Danny Hagans         U
#COMMENT U Reason: Added cfg parameters for and new program for      U
#COMMENT U         v5.5 changes.                                     U
#COMMENT U                                                           U
#COMMENT U Date: 10/20/2006  SMR: CC9650     By: Danny Hagans        U
#COMMENT U Reason: Added export for last business day of the year    U
#COMMENT U         file DATEBUSEND.                                  U
#COMMENT U                                                           U
#COMMENT U Date: 02/19/2007  SMR: CC11209  By: Mike Lewis            U
#COMMENT U Reason: Added call to universal archive script.           U
#COMMENT U                                                           U
#COMMENT U Date: 04/29/2008  SMR: CC11205  By: Danny Hagans          U
#COMMENT U Reason: Added Agent indexes for RIA project               U
#COMMENT U                                                           U
#COMMENT U Date: 07/30/2009  SMR: WMS 2532 By: Tony Ledford          U
#COMMENT U Reason: Split SEQAHST.COMBINE.srt into pieces and         U
#COMMENT U         run in parallel processes                         U
#COMMENT U                                                           U
#COMMENT U Date: 06/23/2010  SMR: WMS 4481 By: Danny Hagans          U
#COMMENT U Reason: Extract RIA transactions from SEQAHST.COMBINE and U
#COMMENT U         generate a separate report.                       U
#COMMENT U                                                           U
#COMMENT U Date: 20110609  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT U Date: 20110916  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Correct document manager calls to check for file. U
#COMMENT U Date: 20130821  Proj: 8043      By: Rick Sica             U
#COMMENT U Reason: 2 to 4 inv id - remove investment_table.txt       U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  tstuid=`echo $JUSER|tr "[:upper:]" "[:lower:]"`
  [[ $tstuid = "help" || $tstuid = "?" ]] && mno=1
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
##**********************************************************************
##   READ DAILY SEQUENTIAL ACTIVITY HISTORY FILE AND
##   GENERATE OUTPUT FILE FOR EXTERNAL PROCESSING
##*********************************************************************
function create_trxout_file
{
  JDESC="Beginning creation of TRXOUT file - $numsegs parallel processes"
  display_message nobanner
  typeset splitdir=$XJOB/seqsplit
  if [[ $numsegs -gt 1 ]]; then
    JDESC="Splitting $SEQAHST_FILE..."
    display_message nobanner
    utl_splitSeqahst.pl -n $((numsegs - 1)) -d $splitdir $SEQAHST_FILE && cond=0 || cond=1
    PMSG="problem splitting $SEQAHST_FILE"
    cond_limit=0
    check_status
    unset PMSG
    JDESC="Parallel processing split sequential history files"
    display_message nobanner
    aulexprt_run.pl -P $numjobs $splitdir/SEQAHST.CH $splitdir/SEQAHST.BR.* && cond=0 || cond=1
  else
    JDESC="numsegs is set to 1 - skipping splitting"
    display_message nobanner
    mkdir -p $splitdir
    cp $SEQAHST_FILE $splitdir/$(basename $SEQAHST_FILE)
    JDESC="Processing sequential history file"
    display_message nobanner
    aulexprt_run.pl $splitdir/$(basename $SEQAHST_FILE) && cond=0 || cond=1
  fi

  PMSG="Error running AULEXPRT segments"
  cond_limit=0
  check_status
  unset PMSG

  collect_files $XJOB/+([0-9])
  if [[ -s $XJOB/RPTOUT ]]; then
    PMSG="creating grand total for RPTOUT"
    rptout_grandtotal.pl $XJOB/RPTOUT && cond=0 || cond=1
    cond_limit=0
    check_status
    unset PMSG
  fi
}
function collect_files
{
  typeset xjob
  JDESC="collecting output from segments..."
  display_message nobanner
  while [[ $# -gt 0 ]]; do
    xjob=$1
    shift
    for f in $xjob/RPT* $xjob/SEQ* $xjob/TRX* $xjob/RIAH*; 
    do
      [[ -f $f ]] || continue
      cat $f >>$XJOB/$(basename $f)
      JDESC="collecting output from segments...$f"
      display_message nobanner
    done
##  Pickup file created by mne_calc.ksh
    for f in $AULDATA/TRXOUT.MNE*; do
      [[ -f $f ]] || continue
      JDESC="collecting output M&E TRXOUT.MNE file from AULDATA"
      display_message nobanner
      cat $f >>$XJOB/TRXOUT && rm $f
    done
  done
}
function copy_xjob_to_aulout
{
  cd $XJOB
  touch $AULOUT/TRXOUT
  [[ -s TRXOUT ]] && cond=0 || cond=$emptyTRXOUT_rc
  PMSG="Empty TRXOUT file produced"
  cond_limit=0
  check_status
  unset PMSG

  for file in $XJOB/RPT* $XJOB/SEQ* $XJOB/TRX* $XJOB/RIA*;
  do
    [[ -f $file ]] || continue
    JDESC="copying $file to $AULOUT..."
    cp $file $AULOUT && cond=0 || cond=1
    PMSG="Error copying $file to $AULOUT"
    cond_limit=0
    check_status
    unset PMSG
  done
  sleep 2
  cd $OLDPWD
}

function sort_for_ria
{
   export RIATIN=$XJOB/RIAHISTOUT
   export RIAOUT=$XJOB/SRT_RIAHST 
   JDESC="Sorting RIA sequential file - Start"
   display_message nobanner
   print "Sort Beginning..."
##   sort -o $XJOB/SRT_RIAHST -k .2,.6 -k .298,.22 -k .440,.1 -k .61,.3 -k .67,.2 $XJOB/RIAHISTOUT 
OTSORT $XJOB/RIAHISTOUT $XJOB/SRT_RIAHST "/S(2,6,CH,A,298,22,CH,A,440,1,CH,A,61,3,CH,A,67,2,CH,A) F(TXT) W($TMPDIR/) CORE(40000)"
   if [ $cond -gt 0 ] 
   then
     export JDESC="Sort of RIA sequential history file abended cond= $cond"
     display_message
     return $cond
   fi
   print "Sort Ending"
   mv $XJOB/SRT_RIAHST  $XJOB/RIAHISTOUT
   JDESC="Sorting RIA sequential file - END"
   display_message nobanner

}

function create_riaout_file
{
  export PNAME=AULEXPRT
  export PARM="000000RIA00000"
  export JDESC="Extract RIA data from Sorted/Unified RIA SEQAHST file"
  export dd_DATEBUSEND=$AULDATA/DATEBUSEND
  export dd_RPTOUT=$XJOB/RIARPT
  export dd_RPTOUTE=$XJOB/RIARPTE
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_SEQAHST=$XJOB/RIAHISTOUT
  export dd_SEQAHSTE=$XJOB/RIAAHSTE
  export dd_TRXOUT=$XJOB/RIAOUT
  export dd_TRANOUT=$XJOB/riatranout
  export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
  export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
  execute_program
}

function send2DocMgr
{
  typeset f
  JDESC="Setting up and sending RIA reports to document manager"
  display_message nobanner

  cd $XJOB
  PFX=${EBSRPTPFX}

  [[ -s RIARPT ]] || print "*** Omni Export RIA Report NOT produced ***" >>RPTOUT
  [[ -s RIARPTE ]] || print "*** Omni Export RIA Error Report NOT produced ***" >>RPTOUTE
  [[ -s RIAAHSTE ]] || print "*** Omni Export RIAAHSTE NOT produced ***" >>SEQAHSTE

  for f in RIARPT RIARPTE RIAAHSTE; do
    [[ -f $f ]] && {
      cp $f $AULOUT/${PFX}$f;
      fnc_send2DocMgr $AULOUT/${PFX}$f; }
  done

  cd $OLDPWD
}

###########################################################################
###########################  main section  ################################
###########################################################################
######################
## setup variables
######################
prog=$(basename $0)
export integer NumParms=$#

export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX SEQAHST GL EXTRACT"

### ******************************************************************
### standard job set up
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
. JOBDEFINE
fnc_log_standard_start

$check_input && check_input

##**********************************************************************
##  TASK 1 - Extract GL Information from sorted sequential history file
##**********************************************************************
$create_trxout && create_trxout_file

##**********************************************************************
##  TASK 2 - Extract RIA information and Generate report
##**********************************************************************
## $extract_riarec && extract_ria_recs
$sort_for_ria && sort_for_ria
$create_riaout && create_riaout_file
$send2DocMgr && send2DocMgr

##**********************************************************************
##  TASK 3 - Copy files to AULOUT
##**********************************************************************
$copy_xjob && copy_xjob_to_aulout

#########################################################################
##  TASK LAST - End of job housekeeping
#########################################################################
eoj_housekeeping
return 0
