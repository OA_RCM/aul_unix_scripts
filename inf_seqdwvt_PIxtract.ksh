#!/bin/ksh
#COMMENT ****************************************************************
#COMMENT *             Standard HP Job Documentation                 ****
#COMMENT ****************************************************************
#COMMENT *  CNTLM Job Name : RSUXINF711 / INF713         CALL           *
#COMMENT *  CNTLM Job Type : Command                     OMNI-ASU       *
#COMMENT *  UNIX Pointer   : rsuxinf711.ksh                             *
#COMMENT *  KSH Script Name: inf_seqdwvt_PIxtract.ksh                   *
#COMMENT *                                                              *
#COMMENT *  Description  : Job to send the deferred work file from      *
#COMMENT *     one-or-more JOBUNIF(s) to the Power Image System.        *
#COMMENT *                                                              *
#COMMENT *  Version      : OmniPlus/UNIX 5.80+                          *
#COMMENT *  Author       : AUL - MBL                                    *
#COMMENT *  Created      : 07/23/2001                                   *
#COMMENT *  Environment  : ENVINTF                                      *
#COMMENT *  Called by    : ECS Scheduler                                *
#COMMENT *  Script Calls : none                                         *
#COMMENT *  COBOL Calls  : none                                         *
#COMMENT *                                                              *
#COMMENT *  Frequency    : 5x daily  ( as of Jan 2010)                  *
#COMMENT *  Est.Run Time : 1 min                                        *
#COMMENT *  Y2K Status   : Compliant                                    *
#COMMENT ****************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  SPECIAL INSTRUCTIONS : SYNTAX:                              S
#COMMENT S  inf_seqdwvt_editout.ksh filename - where filename is        S
#COMMENT S  name of .info file with pointer to seqdwvt records to be    S
#COMMENT S  filtered for PI.                                            S
#COMMENT S  Accepted vaules now include DAYUNIF or NITEUNIF.            S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  RESTART INSTRUCTIONS :                                      R
#COMMENT R  If *ALL* of the following conditions are true, then         R
#COMMENT R ==>> wait 60 minutes after failure and run job again.        R
#COMMENT R  PLEASE DO NOT CALL ASU IN THIS INSTANCE.                    R
#COMMENT R  --------------------------                                  R    
#COMMENT R  Conditions for rerun:                                       R
#COMMENT R  --------------------------                                  R 
#COMMENT R  <+> failure is CNTLM job:  RSUXINF713                       R
#COMMENT R  <+> it is the first failure of this job for this cycle      R
#COMMENT R  <+> error messages are:                                     R
#COMMENT R  cp: cannot access /cifs/power_image: Permission denied      R
#COMMENT R  * Could not copy filter SEQDWVT file to CIFS * abend exit:  R 
#COMMENT R                                                              R
#COMMENT R  If any of the above conditions are not true, then please    R
#COMMENT R  respond to CALL/NON-CALL designation as per SOP.            R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                               T
#COMMENT T    1 - Filter found SEQDWVT file.                            T
#COMMENT T    2 - Delay loop. Gives EMS services time to terminate.     T
#COMMENT T    3 - Send filtered SEQDWVT to Power Image.                 T
#COMMENT T Last - Standard eoj and housekeeping logic.                  T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:  n/a                                  P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ      P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE      P
#COMMENT P                                                              P
#COMMENT P                                                              P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                             U
#COMMENT U Date: 07/23/2001  SMR: PEN????  By: Mike Lucas               U
#COMMENT U Reason: Created to send deferred work file to Power Image    U
#COMMENT U                                                              U
#COMMENT U Date: 08/01/2006  SMR: CC8403   By: Mike Lewis               U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.        U
#COMMENT U                                                              U
#COMMENT U Date: 11/15/2007  SMR: CC12361   By: Zera Holladay           U
#COMMENT U Reason: Modernized script.  Removed ncftp.                   U
#COMMENT U                                                              U
#COMMENT U Date: 01/06/2010  WMS: 003526    By: Steve Loper             U
#COMMENT U Reason: Adapted from inf_seqdwvt_editout.ksh (obsolete)      U
#COMMENT U Reason: (1) wait for EMS service to go down.                 U
#COMMENT U Reason: (2) lookup directory containing correct SEQDWVT      U
#COMMENT U Reason: (3) standardize with switches,functions, and doc.    U
#COMMENT U                                                              U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
########################################
## Functions section
########################################
function check_input
{
  mno=0
  [[ $NumParms -lt 1 ]] && mno=2
  check_mno
  [[ -z $JPARM ]] && mno=3
  check_mno
}
function check_overrides
{
  ## ------------------------------------------------------------------------------
  ##  If INFO_FILE is legit, then source it into the current environment in order to set 
  ##  AULOUT to directory containing input SDW file
  ##    NOGO - means abend if file is empty (0 bytes)
  ##    DONO - means simply bypass processing if file is empty (0 bytes)
  ## ------------------------------------------------------------------------------
  export INFO_FILE=$EBSCTRL/${JPARM}.info         ## file containing location of SDW file
  check_file $INFO_FILE NOGO && . $INFO_FILE
  export SDWK_FILE=$AULOUT/$SDW_FILE            ## fully qualified path and name of SDW file
  export DWFILE=$XJOB/$DWFILE                   ## set the output file for PI
  check_file $SDWK_FILE DONO
}
function check_file
{
  fname=$1
  emptyCD=$2
  print_msg "Checking file $fname:"
  errMsg1="$fname not found - cannot continue"
  errMsg2="$fname not readable - cannot continue"
  case "$emptyCD" in
    "NOGO") errMsg3="$fname is empty  - cannot continue"; cmd="abend";;
    "DONO") errMsg3="$fname is empty  - nothing to do  "; cmd="display_message nobanner";;
  esac
  [[ ! -f $fname ]] && { JDESC="$errMsg1"; $cmd; }
  [[ ! -r $fname ]] && { JDESC="$errMsg2"; $cmd; }
  [[ ! -s $fname ]] && { JDESC="$errMsg3"; $cmd; }
  print_msg "File $fname: Check complete."
  return 0
}
function filter_SEQDWVT
{
## check if SDW file is populated
  if [[ -s $SDWK_FILE ]]
  then
    JDESC="Filtering seq deferred work records from file: $SDWK_FILE"
    display_message nobanner
  else
    touch $DWFILE
    export delay_loop=false
    export send_file_to_PI=false
    return 0
  fi

## if so, then filter contents into output file
perl -ne '
	if (/^.{6}VTPOST/)
	{
	    print if /^.{232}[tT]\d{6}\w{4}/;
	}
	else
	{
	    print;
	}' $SDWK_FILE > $DWFILE
}
function send_file_to_PI 
{
  cp $DWFILE $PI_CIFS_SHARE || { JDESC="Could not copy $DWFILE file to $PI_CIFS_SHARE"; abend; }
}
function print_msg
{
  JDESC="$1"
  display_message nobanner
}
########################################
## Main section
########################################
prog=$(basename $0 .ksh)
integer NumParms=$#
export JPARM=$1

export JUSER=${JUSER:-"$LOGNAME"}
export JNAME=${prog%.ksh}
export JDESC="Filter SEQDWVT Records for PI"
export ENVFILE=ENVNONE

. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
########################################
# TASK 1 - Filter found SEQDWVT file.
########################################
filter_SEQDWVT || { JDESC="ERROR during filter process - abend"; abend; }

###############################################################
# TASK 2 - Delay loop. Gives EMS services time to terminate.
###############################################################
$delay_loop && { sleep $delay; } || { print_msg "Skipped delay_loop function"; }

########################################
# TASK 3 - Send filtered SEQDWVT to Power Image.
########################################
$send_file_to_PI && { send_file_to_PI; } || { print_msg "Skipped send_file_to_PI function"; }

########################################
# TASK Last - Finish.
########################################
eoj_housekeeping
return 0
