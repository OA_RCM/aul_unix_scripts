#!/usr/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : RSUXINF001             CALL            *
#COMMENT *  Schedule Name   : rsuxinf001.ksh         ASU-OMNI        *
#COMMENT *  Unix Script     : inf_EXIndica.ksh                       *
#COMMENT *                                                           *
#COMMENT *  Description  : Process all ExCCEPT indicative data.      *
#COMMENT *                 Including compensation for hours by       *
#COMMENT *                 payroll or cencus.                        *
#COMMENT *                                                           *
#COMMENT *  Author       : LQB                                       *
#COMMENT *  Created      : 02/27/2009                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBVTUT, ODBC                             *
#COMMENT *  COBOL Calls  : INF2000L.CBL                              *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily, On Demand                          *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : Dependant on file size(seconds,minutes ?) *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T    1 - Run COBOL program to create the X813,875,877 cards T
#COMMENT T    2 - Load 813 and 875  trans to VTRAN                   T
#COMMENT T    3 - Move serviceo and compenso to $EBSCNV              T
#COMMENT T    4 - Email msg for bad characters in address            T
#COMMENT T    5 - Standard EOJ housekeeping tasks                    T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U  Glen McPherson    WMS2106       Spring 2009 Release      U
#COMMENT U                                                           U
#COMMENT U  Louis Blanchette  WMS8252    Adding dd_RPTADDRESS        U
#COMMENT U                               Email and FTP if not empty  U
#COMMENT U                               (Bad Characters scrubbing)  U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
###   function declarations
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  ## for future use
  return 0
}
function create_X_cards
{
  PMSG="function create_X_cards"

  export dd_WORKFILE=$XJOB/WORKFILE.txt
  export dd_RSTROUT=$XJOB/rs.tmp

  export COBSAVE=$COBRUN
  export COBRUN=$(dirname $COBRUN)/$AUL_RTS
  
  PNAME=INF2000L
  JDESC="Run COBOL program $PNAME - Omni transaction creation"
  execute_program

  export COBRUN=$COBSAVE

  JDESC="Completed $PMSG - Omni transaction creation"
  display_message nobanner
  unset PMSG
}
function load_trans2VTRAN
{

   PMSG="function load_trans2VTRAN"
   cp $XJOB/X541IFILE.$rundate $EBSCARD
   cp $XJOB/X541NFILE.$rundate $EBSCARD
   cp $XJOB/X650CFILE.$rundate $EBSCARD
   cp $XJOB/X801NFILE.$rundate $EBSCARD
   cp $XJOB/X813IFILE.$rundate $EBSCARD
   cp $XJOB/X813CFILE.$rundate $EBSCARD
   cp $XJOB/X875CFILE.$rundate $EBSCARD

   if [ -s $EBSCARD/X541IFILE.$rundate ]
   then
      JDESC="Load X541I output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X541IFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X541IFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X541IFILE is empty...no X541I to load"
      display_message nobanner
   fi

   if [ -s $EBSCARD/X541NFILE.$rundate ]
   then
      JDESC="Load X541N output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X541NFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X541NFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X541NFILE is empty...no X541N to load"
      display_message nobanner
   fi

   if [ -s $EBSCARD/X650CFILE.$rundate ]
   then

      JDESC="Load X650 output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X650CFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X650CFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X650CFILE is empty...no X650C to load"
      display_message nobanner
   fi

   if [ -s $EBSCARD/X801NFILE.$rundate ]
   then
      JDESC="Load X801N output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X801NFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X801NFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X801NFILE is empty...no X801N to load"
      display_message nobanner
   fi

   if [ -s $EBSCARD/X813IFILE.$rundate ]
   then
      JDESC="Load X813I output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X813IFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X813IFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X813IFILE is empty...no X813I to load"
      display_message nobanner
   fi

   if [ -s $EBSCARD/X813CFILE.$rundate ]
   then
      JDESC="Load X813C output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X813CFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X813CFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X813CFILE is empty...no X813C to load"
      display_message nobanner
   fi

   if [ -s $EBSCARD/X875CFILE.$rundate ]
   then
      JDESC="Load X875C output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=X875CFILE.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file X875CFILE to VTRAN"
      display_message nobanner
   else
      JDESC="File X875CFILE is empty...no X875C to load"
      display_message nobanner
   fi

   unset PMSG
}
function move_files
{ 
	if [ -s $dd_SERVICEO ]
   then
      cp $dd_SERVICEO $EBSCNV  
  fi                                                           
  if [ -s $dd_COMPENSO ]
   then
      cp $dd_COMPENSO $EBSCNV   
  fi          
  if [ -s $dd_RPTADDRESS ]
   then
      cp $dd_RPTADDRESS $EBSXFER/ftp
  fi          
}
function email_msg
{ 
  if [ -s $dd_RPTADDRESS ]
  then
   
  PMSG="function email_reports"
  export RPTNAME=$(basename $dd_RPTADDRESS)

  mailx -s "ExACCT Omni Bad Address Report" $email_address <<-EOF
  Hi,

  This E-mail message is to notify you that the ExACCT process is complete
  in the $EBSFSET Omni area. You can look at the bad characters address
  report $RPTNAME on the lan in the $LANDEST folder.


  Best regards
  (Please do not REPLY to this email. It's an unattended mail box)

	EOF

  JDESC="Completed $PMSG- ExACCT Address report!"
  display_message nobanner
  unset PMSG
   
  fi          
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################'
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JPARM=`echo $1|tr "[:lower:]" "[:upper:]"`
export JNAME=${prog%.ksh}
export JDESC="$JPARM - OMNIPLUS SERVICEO AND COMPENSO PROCESSING MODULE"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides
. JOBDEFINE

###########################
# Move to the JOB directory
###########################
cd $XJOB

###########################
# Define files
###########################
export dd_SERVICEO=$XJOB/serviceo.$today.txt
export dd_COMPENSO=$XJOB/compenso.$today.txt
export dd_X541IFILE=$XJOB/X541IFILE.$rundate
export dd_X541NFILE=$XJOB/X541NFILE.$rundate
export dd_X650CFILE=$XJOB/X650CFILE.$rundate
export dd_X801NFILE=$XJOB/X801NFILE.$rundate
export dd_X813CFILE=$XJOB/X813CFILE.$rundate
export dd_X813IFILE=$XJOB/X813IFILE.$rundate
export dd_X875CFILE=$XJOB/X875CFILE.$rundate
export dd_RPTADDRESS=$XJOB/rpt_address$rundate.txt

### **********************************************************************
### TASK 1 - Run COBOL program to create Omni transactions cards
### **********************************************************************
$create_X_cards && create_X_cards

### ******************************************************************
### TASK 2 - Load transactions to VTRAN
### ******************************************************************
$load_trans2VTRAN && load_trans2VTRAN

### ******************************************************************
### TASK 3 -- Move files
### ******************************************************************
### copy files to $EBSCNV
$move_files && move_files

### ******************************************************************
### TASK 4 -- Email the msg if there is a rpt_address...
### ******************************************************************
$email_msg && email_msg

### ******************************************************************
### TASK 5 - Standard EOJ housekeeping tasks
### ******************************************************************
eoj_housekeeping
return 0
