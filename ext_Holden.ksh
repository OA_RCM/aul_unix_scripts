#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTM Job Name  : RSUXEXT706               CALL            *
#COMMENT *  SchedXref Name: rsuxext706.ksh                           *
#COMMENT *  Script Name   : ext_Holden.ksh           OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce three reports to be *
#COMMENT *		   sent to a Third party Administrator.      *
#COMMENT *                          Holden                           *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : EBSPROC                                   *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 09/20/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : TPAMAIN TPAQVTP                           *
#COMMENT *  Frequency    : Quarterly                                 *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task Sequence                                            T
#COMMENT T                                                           T
#COMMENT T   TASK 1 Get the parameters needed to produce the reports T
#COMMENT T   TASK 2 Run the COBOL programs to produce TPA Extracts   T
#COMMENT T   TASK 3 clean up and say goodbye                         T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/08/2005  SMR: CC3039  By: Mike Lewis             U
#COMMENT U Reason: Redesigned from original script to bring up to    U
#COMMENT U         current standards.                                U
#COMMENT U                                                           U
#COMMENT U Date: 11/21/2011  PROJECT: WMS6936  By: Louis Blanchette  U
#COMMENT U Reason: Define the format of qtrmm to avoid problems      U
#COMMENT U         calculating the start date.                       U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################

function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function define_quarter_yymm
{
  qtryy=`echo $tpaend |cut -c1-4`
  qtrmm=`echo $tpaend |cut -c5-6`
  if [[ $qtrmm > 02 ]]
  then
    ((qtrmm-=2))
  else
    ((qtryy-=1))
    ((qtrmm+=10))
  fi
}

############################################################################
###   main section
############################################################################
prog=$(basename $0)
integer NumParms=$#

### ******************************************************************
### define job variables
### ******************************************************************
  export JUSER=${JUSER:-$LOGNAME}
  export JNAME=${prog%.ksh}
  export ENVFILE=ENVBATCH
  export AULOUT=$POSTOUT
  export JDESC="Create Extracts for Holden"

### ******************************************************************
### Standard script setup
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
check_input
. JOBDEFINE ## standard omniplus master file/environment definition script
fnc_log_standard_start
fnc_set_monthend_date
cd $XJOB

### ******************************************************************
### TASK 1 Get the parameters needed to produce the reports
### ******************************************************************
  export tpatpa=4
  export tpaend=${TPAEND:-$rundate}
  typeset -Z2 qtrmm
  define_quarter_yymm
  export tpabeg=${TPABEG:-$qtryy$qtrmm"01"}
  export PARM=$tpatpa$tpabeg$tpaend
  echo $tpabeg ' ' $tpaend

### ******************************************************************
### TASK 2 Run the COBOL programs to produce TPA Extracts
### ******************************************************************
  export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
  export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
  export dd_TPAPARTO=$XJOB/HOLPARTO
  export dd_TPATRANO=$XJOB/HOLTRANO
  export dd_TPAVALUO=$XJOB/HOLVALUO

  PNAME=TPAMAIN
  export JDESC="Run COBOL program $PNAME"
  execute_program

  cp $dd_TPAPARTO $pickupLoc
  cp $dd_TPATRANO $pickupLoc
  cp $dd_TPAVALUO $pickupLoc

### ******************************************************************
### TASK 3 clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
