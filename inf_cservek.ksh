#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXINF708              CALL            *
#COMMENT *  SchedXref Name : rsuxinf708.ksh                          *
#COMMENT *  UNIX Script    : inf_cservek.ksh         OMNI-ASU        *
#COMMENT *                                                           *
#COMMENT *  Description  : Populates the update file with new or     *
#COMMENT *                 changed records on the current days file  *
#COMMENT *                 compared to the previous days file        *
#COMMENT *                 for the CSERVICE & Enrollment Kit feeds.  *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 05/25/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : AULCSCMP  AULCSERV  EKT1000E              *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : > 2 hours                                 *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R  Back-up files will need to be moved back into AULDATA    R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 05/25/2001  SMR: ******** By: Patty Wamsley         U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 08/21/2001  SMR: ******** By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U Date: 10/04/2002  SMR: CC3522   By: Mike Lewis            U
#COMMENT U Reason: Clean up of large files after copies completed.   U
#COMMENT U Date: 07/31/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U Date: 07/18/2007  CC: 11728     By: Rick Sica             U
#COMMENT U Reason: Add Enrollment Kits extract.                      U
#COMMENT U Date: 11/02/2007  CC: 12259     By: Mark Slinger/SLOPER   U
#COMMENT U Reason: Send file to NT                                   U
#COMMENT U Date: 12/10/2008  WMS: 1310     By: Gary Pieratt          U
#COMMENT U Reason: Age-targeted defaults                             U
#COMMENT U Date: 03/25/2010  WMS: 1308     By: Gary Pieratt          U
#COMMENT U Reason: Extend EKPLANOUT to 450 bytes                     U
#COMMENT U Date: 06/01/2010  WMS: 4504     By: Rick Sica             U
#COMMENT U Reason: Archive EBSXFER files                             U
#COMMENT U Date: 20110318   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U Date: 20130527   Proj: WMS7910  By: Danny Hagans          U
#COMMENT U Reason: Setup processing for Broadridge EK files          U
#COMMENT U Date: 20140218   Proj: WMS7910  By: Danny Hagans          U
#COMMENT U Reason: Send blank Broadridge files to Broadridge         U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  ## for future use
  return 0
}
function create_Bowne_EK
{
  export dd_INVTBLNEW=$AULDATA/investment_tbl.txt
  export dd_ASSETCATNEW=$AULDATA/Stmt_Option_Categories.prn
  export dd_INVTYPENEW=$AULDATA/Stmt_Investment_Types.prn

  export dd_EKCHGRPT=$XJOB/${EBSRPTPFX}EKCHANGERPT
  export dd_EKERRORRPT=$XJOB/${EBSRPTPFX}EKERRORRPT

  export dd_INVTBLOLD=$XJOB/EKINVTBLOLD
  echo "invtblold equals " $dd_INVTBLOLD
  JDESC="show $dd_INVTBLOLD to shared path $XJOB"
  display_message nobanner

  export dd_EKPLANOUT=$XJOB/EKPLANOUT
  export dd_EKFORMOUT=$XJOB/EKFORMOUT
  export dd_EKDIVOUT=$XJOB/EKDIVOUT
  export dd_EKSRCOUT=$XJOB/EKSRCOUT
  export dd_EKINVOUT=$XJOB/EKINVOUT
  export dd_EKVESTOUT=$XJOB/EKVESTOUT
  export dd_EKBRKROUT=$XJOB/EKBRKROUT
  export dd_EKTRIGGER=$XJOB/EKTRIGGER
  export dd_EKINVTBLO=$XJOB/EKINVTBLOUT

  export dd_INVTBLCHK=$XJOB/INVTBLCHK.tmp
  export dd_INVTBLCHK2=$XJOB/INVTBLCHK2.tmp
  PNAME=EKT1000E
  JDESC="Run program $PNAME to compare Enrollment Kit data $(date +%H%M%S)"
  execute_program

}

function create_Broadridge_EK
{
  cp $CIFSLOC/PerfDisclaimers.txt $XJOB/PerfDISCLAIMER.txt
# problems with reading/changing the tab delimiter in COBOL.
  sed 's/	/~/g' $XJOB/PerfDISCLAIMER.txt > $XJOB/PerfDISCLAIMER.txt2
  mv $XJOB/PerfDISCLAIMER.txt2 $XJOB/PerfDISCLAIMER.txt
  sed 's/"//g' $XJOB/PerfDISCLAIMER.txt > $XJOB/PerfDISCLAIMER.txt2
  mv $XJOB/PerfDISCLAIMER.txt2 $XJOB/PerfDISCLAIMER.txt

  export dd_INVTBLNEW=$AULDATA/investment_tbl.txt
  export dd_ASSETCATNEW=$AULDATA/Stmt_Option_Categories.prn
  export dd_INVTYPENEW=$AULDATA/Stmt_Investment_Types.prn
  export dd_PRTFLIO=$AULDATA/PRTFLIO

  export dd_BRPLANEW=$XJOB/BRPLANNEW
  export dd_BRSRCNEW=$XJOB/BRSRCNEW
  export dd_BRVESTNEW=$XJOB/BRVESTNEW
  export dd_BRDIVNEW=$XJOB/EKDIVNEW  
  export dd_BRFORMNEW=$XJOB/BRFORMNEW
  export dd_BRINVNEW=$XJOB/BRINVNEW
  export dd_BRGLOBALIVNEW=$XJOB/BRGLOBINVNEW
  export dd_BRCHGRPT=$XJOB/${EBSRPTPFX}BRCHANGERPT
  export dd_BRERRORRPT=$XJOB/${EBSRPTPFX}BRERRORRPT
  export dd_BRPLANOUT=$XJOB/BRPLAN.psv
  export dd_BRFORMOUT=$XJOB/BRFORM.psv
  export dd_BRDIVOUT=$XJOB/BRDIVISION.psv
  export dd_BRSRCOUT=$XJOB/BRSOURCE.psv
  export dd_BRINVOUT=$XJOB/BRINVESTMENT.psv
  export dd_BRVESTOUT=$XJOB/BRVESTING.psv
  export dd_BRTRIGGER=$XJOB/BRTRIGGER.psv
  export dd_BRASSETCAT=$XJOB/BRASSETCAT 
  export dd_BRINVTYPEOUT=$XJOB/BRINVTYPE.psv
  export dd_ASSETCATOUT=$XJOB/BRASSETCAT.psv
  export dd_BRGLOBALIVOUT=$XJOB/BRGLOBALINV.psv
  export dd_BRDISCLAIMOUT=$XJOB/BRDISCLAIMER.psv
  export dd_BRFUNDPERFOUT=$XJOB/BRFUNDPERF.psv
  export dd_INVTBLCHK=$XJOB/INVTBLCHK.idx
  export dd_BRERROUT=$XJOB/EKERROUT
  export dd_BRDISCLAIMER=$XJOB/PerfDISCLAIMER.txt
  export dd_EKCNTRLFLS=$XJOB/ekcntrlplans.idx
  PNAME=EKT1100E
  JDESC="Run program $PNAME to compare Enrollment Kit data $(date +%H%M%S)"
  execute_program

}
# Send file to Informatica can be turned off in the ini file.
function send_file_to_informatica
{
  JDESC="Sending $ODSFILE to ODS [UNIX Server]"
  display_message nobanner
  ftpputods.pl || { JDESC="ERROR SENDING $ODSFILE to ODS [$EBSFSET]"; display_message nobanner; export cserverror="Y"; }
}
# Send file to NT can be turned off in the ini file.
function send_file_to_nt_informatica
{
  PMSG="In function send_file_to_nt_informatica"
  JDESC="Copying $ODSFILE to shared path $XFER_TGT"
  display_message nobanner
  cp $ODSFILE $XFER_TGT && cond=0 || cond=99
  if [[ $cond != 0 ]]; then
     JDESC="ERROR placing $ODSFILE in $XFER_TGT (ABORT!)"
     display_message nobanner
     export cserverror="Y"
  else
     JDESC="Sucessfully sent file $ODSFILE to NT [$XFER_TGT]"
     display_message nobanner
  fi
  unset PMSG
}

function send_to_ods
{
export ekerror="N"
if $sendekfiles ; then
   export archName="Archive Enrollment Kit files"
   export archDir=$EBSXFER/$xferdir
   export archType="EK"
   export archList=*
   export archDate=$rundate

   export PNAME=utl_archive.ksh
   export JDESC="$archName"
   export JPRM1="inf_cservekt"
   execute_job

   if [[ -s $dd_EKPLANOUT ]] ; then
      export JDESC="Placing Enrollment Kit files in transfer area"
      display_message nobanner
      # cp $dd_EKPLANOUT $EBSXFER/$xferdir/EKPLAN.txt || export ekerror="Y"
      # Pad to 445 spaces to create fixed record length for EKPLAN.txt [WMS 1310]
      # Increase to 450 spaces [WMS 1308]
      rm -f $EBSXFER/$xferdir/EKPLAN.txt
      typeset -L450 frec; cat $dd_EKPLANOUT | while read frec; do
          print "$frec" >> $EBSXFER/$xferdir/EKPLAN.txt || export ekerror="Y"
      done
      cp $dd_EKFORMOUT $EBSXFER/$xferdir/EKFORM.txt || export ekerror="Y"
      cp $dd_EKDIVOUT $EBSXFER/$xferdir/EKDIVISION.txt || export ekerror="Y"
      cp $dd_EKSRCOUT $EBSXFER/$xferdir/EKSOURCE.txt || export ekerror="Y"
      cp $dd_EKINVOUT $EBSXFER/$xferdir/EKINVESTMENT.txt || export ekerror="Y"
      cp $dd_EKVESTOUT $EBSXFER/$xferdir/EKVESTING.txt || export ekerror="Y"
      cp $dd_EKBRKROUT $EBSXFER/$xferdir/EKBROKER.txt || export ekerror="Y"
      export EKINVTYPEXFER=$EBSXFER/$xferdir/EKINVTYPE.txt
      rm -f $EKINVTYPEXFER
      # Pad to 36 spaces since description is last field an variable
      typeset -L36 frec; cat $dd_INVTYPENEW | while read frec; do
         print "$frec" >> $EKINVTYPEXFER || export ekerror="Y"
      done
      cp $dd_ASSETCATNEW $EBSXFER/$xferdir/EKASSETCAT.txt || export ekerror="Y"
#      cp $dd_INVTBLNEW $EBSXFER/$xferdir/EKINVTABLE.txt || export ekerror="Y"
      cp $dd_EKINVTBLO $EBSXFER/$xferdir/EKINVTABLE.txt || export ekerror="Y"
      cp $dd_EKTRIGGER $EBSXFER/$xferdir/EKTRIGGER.tk || export ekerror="Y"
      [[ ekerror = "Y" ]] && { JDESC="Error copying files to Transfer Directory"; display_message; }
      export JDESC="Sent Bowne Enrollment Kits files to $xferdir2"
      display_message
   else
      export JDESC="Nothing to send for Enrollment Kits"
      display_message
   fi
   [[ ekerror = "Y" ]] && export resetekfiles=false
fi
}

function send2_to_ods
{
export ekerror="N"
if $sendekfiles ; then
   export archName="Archive Enrollment Kit files"
   export archDir=$EBSXFER/$xferdir2
   export archType="BR"
   export archList=*
   export archDate=$rundate

   export PNAME=utl_archive.ksh
   export JDESC="$archName"
   export JPRM1="inf_cservek"
   execute_job

   export JDESC="Placing Enrollment Kit files in transfer area"
   display_message nobanner
   cp $dd_BRFORMOUT $EBSXFER/$xferdir2/BRFORM.psv || export brerror="Y"
   cp $dd_BRDIVOUT $EBSXFER/$xferdir2/BRDIVISION.psv || export brerror="Y"
   cp $dd_BRSRCOUT $EBSXFER/$xferdir2/BRSOURCE.psv || export brerror="Y"
   cp $dd_BRINVOUT $EBSXFER/$xferdir2/BRINVESTMENT.psv || export brerror="Y"
   cp $dd_BRVESTOUT $EBSXFER/$xferdir2/BRVESTING.psv || export brerror="Y"
   cp $dd_BRPLANOUT $EBSXFER/$xferdir2/BRPLAN.psv || export brerror="Y"
   [[ brerror = "Y" ]] && { JDESC="Error copying files to Transfer Directory $EBSXFER/$xferdir2/"; display_message; }
   [[ brerror = "Y" ]] && export resetBRfiles=false
   cp $dd_BRTRIGGER $EBSXFER/$xferdir2/BRTRIGGER.psv || export brerror="Y"
   cp $dd_BRINVTYPEOUT $EBSXFER/$xferdir2/BRINVTYPE.psv || export brerror="Y"
   cp $dd_ASSETCATOUT $EBSXFER/$xferdir2/BRASSETCAT.psv || export brerror="Y"
   cp $dd_BRGLOBALIVOUT $EBSXFER/$xferdir2/BRGLOBALINV.psv || export brerror="Y"
   cp $dd_BRDISCLAIMOUT $EBSXFER/$xferdir2/BRDISCLAIMER.psv || export brerror="Y"
   cp $dd_BRFUNDPERFOUT $EBSXFER/$xferdir2/BRFUNDPERF.psv || export brerror="Y"
   cp $dd_BRCONTRIBLMT $EBSXFER/$xferdir2/BRCONTRIBLIMIT.psv || export brerror="Y"
   [[ brerror = "Y" ]] && { JDESC="Error copying files to Transfer Directory $EBSXFER/$xferdir2/"; display_message; }
   export JDESC="Sent Broadridge Enrollment Kits files to $xferdir2"
   display_message
   [[ brerror = "Y" ]] && export resetBRfiles=false
fi

}

function reset_ek_Bowne
{
   export JDESC="Reseting Bowne Enrollment Kit files in AULDATA"
   display_message nobanner
   cp $AULDATA/investment_tbl.idx $dd_EKINVTBL || { JDESC="ERROR on copy of Investment Table file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_ASSETCATNEW $dd_EKASSETCAT || { JDESC="ERROR on copy of Asset Category file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_INVTYPENEW $dd_EKINVTYPE || { JDESC="ERROR on copy of Investment Type file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKPLANNEW $AULDATA/EKPLANNEW && rm $dd_EKPLANNEW || { JDESC="ERROR on copy of Plan file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKFORMNEW $AULDATA/EKFORMNEW && rm $dd_EKFORMNEW || { JDESC="ERROR on copy of Form file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKDIVNEW $AULDATA/EKDIVNEW && rm $dd_EKDIVNEW || { JDESC="ERROR on copy of Division file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKSRCNEW $AULDATA/EKSRCNEW && rm $dd_EKSRCNEW || { JDESC="ERROR on copy of Source file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKINVNEW $AULDATA/EKINVNEW && rm $dd_EKINVNEW || { JDESC="ERROR on copy of Investment file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKVESTNEW $AULDATA/EKVESTNEW && rm $dd_EKVESTNEW || { JDESC="ERROR on copy of Vesting file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_EKBRKRNEW $AULDATA/EKBRKRNEW && rm $dd_EKBRKRNEW || { JDESC="ERROR on copy of Broker file to AULDATA"; display_message; ekerror="Y"; }
   nohup gzip $XJOB/EK*OLD &
}

function reset_ek_Broadridge
{

   export JDESC="Reseting Broadridge Enrollment Kit files in AULDATA"
   display_message nobanner
   cp $AULDATA/investment_tbl.idx $dd_EKINVTBL || { JDESC="ERROR on copy of Investment Table file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_ASSETCATNEW $dd_EKASSETCAT || { JDESC="ERROR on copy of Asset Category file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_INVTYPENEW $dd_EKINVTYPE || { JDESC="ERROR on copy of Investment Type file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_BRPLANNEW $AULDATA/BRPLANNEW && rm $dd_BRPLANNEW || { JDESC="ERROR on copy of Plan file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_BRFORMNEW $AULDATA/BRFORMNEW && rm $dd_BRFORMNEW || { JDESC="ERROR on copy of Form file to AULDATA"; display_message; ekerror="Y"; }
   if [[ -s ${dd_BRDIVNEW} ]]; then
      cp $dd_BRDIVNEW $AULDATA/EKDIVNEW && rm $dd_BRDIVNEW || { JDESC="ERROR on copy of Division file to AULDATA"; display_message; ekerror="Y"; }
   fi
   cp $dd_BRSRCNEW $AULDATA/BRSRCNEW && rm $dd_BRSRCNEW || { JDESC="ERROR on copy of Source file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_BRINVNEW $AULDATA/BRINVNEW && rm $dd_BRINVNEW || { JDESC="ERROR on copy of Investment file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_BRVESTNEW $AULDATA/BRVESTNEW && rm $dd_BRVESTNEW || { JDESC="ERROR on copy of Vesting file to AULDATA"; display_message; ekerror="Y"; }
   cp $dd_BRGLOBALIVNEW $AULDATA/BRGLOBALIVNEW && rm $dd_BRGLOBALIVNEW || { JDESC="ERROR on copy of Vesting file to AULDATA"; display_message; ekerror="Y"; }
  nohup gzip $XJOB/BR*OLD &

}
###########################################################################
##main#####################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX C Service & Enrollment Kit Extracts"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
## Standard startup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
. JOBDEFINE
fnc_log_standard_start
check_input
check_overrides

           ###################################################
cd $XJOB   ## processing effectively starts after this point
           ###################################################

### ******************************************************************
### TASK 1 - Backup files
### ******************************************************************
export dd_UTCUSTBASE=$XJOB/utcustprv.txt

if $reload_all ; then
   touch $dd_UTCUSTBASE
   export JDESC="Reload all - create EK files with all current data"
   display_message
else
   if $backupcservfiles ; then
      if [[ -s ${dd_UTCUSTBASE}.gz ]]; then
         gunzip $dd_UTCUSTBASE
      else
         if [[ -s $AULDATA/utcustcur.txt ]]; then
            cp $AULDATA/utcustcur.txt $dd_UTCUSTBASE
         else
           touch $dd_UTCUSTBASE
         fi
      fi
   fi
fi

export dd_EKPLANOLD=$XJOB/EKPLANOLD
export dd_EKFORMOLD=$XJOB/EKFORMOLD
export dd_EKDIVOLD=$XJOB/EKDIVOLD
export dd_EKSRCOLD=$XJOB/EKSRCOLD
export dd_EKINVOLD=$XJOB/EKINVOLD
export dd_EKVESTOLD=$XJOB/EKVESTOLD
export dd_EKBRKROLD=$XJOB/EKBRKROLD
export dd_EKINVTBL=$AULDATA/EK_investment_tbl.idx
export dd_INVTBLOLD=$XJOB/EKINVTBLOLD
export dd_EKASSETCAT=$AULDATA/EK_Stmt_Option_Categories.prn
export dd_ASSETCATOLD=$XJOB/EKASSETCATOLD
export dd_EKINVTYPE=$AULDATA/EK_Stmt_Investment_Types.prn
export dd_INVTYPEOLD=$XJOB/EKINVTYPEOLD
export dd_BRPLANOLD=$XJOB/BRPLANOLD
export dd_BRSRCOLD=$XJOB/BRSRCOLD
export dd_BRVESTOLD=$XJOB/BRVESTOLD
export dd_BRFORMOLD=$XJOB/BRFORMOLD
export dd_BRDIVOLD=$XJOB/EKDIVOLD
export dd_BRINVOLD=$XJOB/BRINVOLD

if $reload_all; then
   touch $dd_EKPLANOLD
   touch $dd_EKFORMOLD
   touch $dd_EKDIVOLD
   touch $dd_EKSRCOLD
   touch $dd_EKINVOLD
   touch $dd_EKVESTOLD
   touch $dd_EKBRKROLD

   touch $dd_BRPLANOLD
   touch $dd_BRSRCOLD
   touch $dd_BRVESTOLD
   touch $dd_BRFORMOLD
#   touch $dd_BRDIVOLD
   touch $dd_BRINVOLD

   cp $dd_EKINVTBL $dd_INVTBLOLD
   cp $dd_EKASSETCAT $dd_ASSETCATOLD
   cp $dd_EKINVTYPE $dd_INVTYPEOLD

   export JDESC="Reload all - generate empty  OLD files"
   display_message
else
   if $backupekfiles ; then
      if [[ -s ${dd_EKPLANOLD}.gz ]]; then
         gunzip $XJOB/EK*OLD.gz
      else
         if [[ -s $AULDATA/EKPLANNEW ]]; then
            cp $AULDATA/EKPLANNEW $dd_EKPLANOLD
            cp $AULDATA/EKFORMNEW $dd_EKFORMOLD
            cp $AULDATA/EKDIVNEW $dd_EKDIVOLD
            cp $AULDATA/EKSRCNEW $dd_EKSRCOLD
            cp $AULDATA/EKINVNEW $dd_EKINVOLD
            cp $AULDATA/EKVESTNEW $dd_EKVESTOLD
            cp $AULDATA/EKBRKRNEW $dd_EKBRKROLD

            cp $AULDATA/BRPLANNEW $dd_BRPLANOLD
            cp $AULDATA/BRFORMNEW $dd_BRFORMOLD
#            cp $AULDATA/BRDIVNEW $dd_BRDIVOLD
            cp $AULDATA/BRSRCNEW  $dd_BRSRCOLD
            cp $AULDATA/BRINVNEW $dd_BRINVOLD
            cp $AULDATA/BRVESTNEW $dd_BRVESTOLD
         else
            touch $dd_EKPLANOLD
            touch $dd_EKFORMOLD
            touch $dd_EKDIVOLD
            touch $dd_EKSRCOLD
            touch $dd_EKINVOLD
            touch $dd_EKVESTOLD
            touch $dd_EKBRKROLD

            touch $dd_BRPLANOLD
            touch $dd_BRFORMOLD
#            touch $dd_BRDIVOLD
            touch $dd_BRSRCOLD
            touch $dd_BRINVOLD
            touch $dd_BRVESTOLD
         fi
         cp $dd_EKINVTBL $dd_INVTBLOLD
         cp $dd_EKASSETCAT $dd_ASSETCATOLD
         cp $dd_EKINVTYPE $dd_INVTYPEOLD
      fi
   fi
fi
### ******************************************************************
### Get Plan resend requests sent from Batch Wizard
### ******************************************************************
  JDESC="Check to see if any Plans need to be resent"
  display_message nobanner
  export ANNRPTDIR=$EBSINPUT/wizard/
  rptlist=/tmp/csrvrpt$$
  ls $ANNRPTDIR | grep BRFORCE >$rptlist
  if [[ -s $rptlist ]]
  then
     #
     # sleep just to make sure no FTP session is going on any of the files
     #
     sleep 5
     for f in $(cat $rptlist) ; do
       cat $ANNRPTDIR/$f
     done >$XJOB/ekcntrlplans.txt
     for f in $(cat $rptlist) ; do
       mv $ANNRPTDIR/$f $XJOB
     done
    rm -f $rptlist
  else
     echo
     echo "Nothing to process"
     JDESC="No requests to process"
     display_message nobanner
     touch ekcntrlplans.txt
#     echo '      ' > ekcntrlplans.txt
     rm -f $rptlist
  fi
rebuild ekcntrlplans.txt, ekcntrlplans.idx  -o:lseq,ind -k:1+6 -r:F6
### ******************************************************************
### TASK 2 - Run extract program
### ******************************************************************
export dd_RSTROUT=$XJOB/rs.tmp
export dd_UTCUSTNEW=$XJOB/utcustcur.txt
export dd_EKPLANNEW=$XJOB/EKPLANNEW
export dd_EKFORMNEW=$XJOB/EKFORMNEW
export dd_EKDIVNEW=$XJOB/EKDIVNEW
export dd_EKSRCNEW=$XJOB/EKSRCNEW
export dd_EKINVNEW=$XJOB/EKINVNEW
export dd_BRINVNEW=$XJOB/BRINVNEW
export dd_EKVESTNEW=$XJOB/EKVESTNEW
export dd_EKBRKRNEW=$XJOB/EKBRKRNEW
export dd_EKERROUT=$XJOB/EKERROUT
export dd_BRPLANNEW=$XJOB/BRPLANNEW
export dd_BRSCNEW=$XJOB/BRSRCNEW 
export dd_BRINVNEW=$XJOB/BRINVNEW
export dd_BRVESTNEW=$XJOB/BRVESTNEW 
export dd_BRGLOBALIVNEW=$XJOB/BRGLOBINVNEW
export dd_BRCONTRIBLMT=$XJOB/BRCONTRIBLIMIT.psv
export dd_FCMASTER=$AULDATA/fc_master.txt
export dd_INVNAMEIDX=$AULDATA/investment_tbl.idx
export dd_BRINVNAME=$AULDATA/Stmt_Investment_Names.prn
if $runextract ; then
   export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
   export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
   export dd_HELPER2=$AULDATA/HELP2
   export COBSAVE=$COBRUN
   export COBRUN=$(dirname $COBRUN)/$AUL_RTS
   PARM=$xfer_target
   PNAME=AULCSERV
   JDESC="Run program $PNAME to extract data $(date +%H%M%S)"
   execute_program
fi

### ******************************************************************
### Task 3 - Run CService Compare program
### ******************************************************************
export dd_UTCUSTOUT=$XJOB/utcustupd.txt

if $runcservcompare ; then
   PNAME=AULCSCMP
   JDESC="Run program $PNAME to compare CService data $(date +%H%M%S)"
   execute_program
fi

### ******************************************************************
### Task 4 - Run Enrollment Kit Compare program
### ******************************************************************

  if $runkitcompare ; then
     if [[ $xfer_target = "BOTH" ]]; then
        create_Bowne_EK
        create_Broadridge_EK
     else
        if [[ $xfer_target = "Bowne" ]]; then
           create_Bowne_EK
        else
           if [[ $xfer_target = "Broadridge" ]]; then
              create_Broadridge_EK
           fi
       fi
     fi
  fi
### ******************************************************************
### TASK 5 - Send CService files to ODS and Enrollment Kit to EBSXFER
### ******************************************************************
if [[ $xfer_target = "BOTH" ]]; then
   send_to_ods
   send2_to_ods
else
   if [[ $xfer_target = "Bowne" ]]; then
      send_to_ods
   else
      if [[ $xfer_target = "Broadridge" ]]; then
         send2_to_ods
      fi
   fi
fi

export cserverror="N"
if $sendcservfiles ; then
  export ODSFILE=$dd_UTCUSTOUT
  $send_to_unix && send_file_to_informatica
  $send_to_nt && send_file_to_nt_informatica
  [[ cserverror = "Y" ]] && export resetcservfiles=false
fi

### ******************************************************************
### TASK 6 - Send specified files to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 7 - Reset files in AULDATA with updated data
### ******************************************************************
if $resetcservfiles ; then
   export JDESC="Reseting C Service files"
   display_message nobanner
   cp $dd_UTCUSTOUT $AULOUT && rm $dd_UTCUSTOUT || { JDESC="ERROR on copy of update file to POSTOUT"; display_message; cserverror="Y"; }
   cp $dd_UTCUSTNEW $AULDATA && rm $dd_UTCUSTNEW || { JDESC="ERROR on copy of current file to AULDATA"; display_message; cserverror="Y"; }
   nohup gzip $dd_UTCUSTBASE &
fi

if $resetekfiles ; then
   if [[ $xfer_target = "BOTH" ]]; then
       $reset_ek_Bowne && reset_ek_Bowne
       $reset_ek_Broadridge && reset_ek_Broadridge
   else
       if [[ $xfer_target = "Bowne" ]]; then
          $reset_ek_Bowne && $reset_ek_Bowne
       else
         if [[ $xfer_target = "Broadridge" ]]; then
           $reset_ek_Broadridge && reset_ek_Broadridge
         fi
      fi
   fi
fi

### ******************************************************************
### TASK 8 - Clean up and say goodbye
### ******************************************************************
if [[ $ekerror = "Y" || $cserverror = "Y" ]] ; then
   export JDESC="Aborted because of a previous error message"
   abend
fi
eoj_housekeeping
copy_to_masterlog
return 0
