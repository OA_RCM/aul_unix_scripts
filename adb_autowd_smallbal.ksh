#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXADB705                 CALL         *
#COMMENT *  SchedXref Name : rsuxadb705.ksh             OMNI-ASU     *
#COMMENT *  UNIX Script    : adb_autowd_smallbal.ksh                 *
#COMMENT *                                                           *
#COMMENT *  Description  : Automatic Withdrawal of smaill balances   *
#COMMENT *                 after a contribution of <= 200.           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 03/10/2010                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxadb705.ksh                            *
#COMMENT *  Script Calls : JOBDEFINE JOBVTUT                         *
#COMMENT *  COBOL Calls  : ADB1020U                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily                                     *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Writes 404 cards files and then  R
#COMMENT R    creates 404 folders. If JOBVTUT was started, then the  R
#COMMENT R    job cannot be restreamed until it is evaluated.        R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U Date: 03/10/2010  W: 3484       By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 04/29/2011  W: 6013       By: Gary Pieratt          U
#COMMENT U Reason: New report file to Powerimage EMS                 U
#COMMENT U Date: 20111005   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME="adb_autowd_smallbal"
export JDESC="AUTOMATIC WITHDRAWAL OF SMALL BALANCES"
export ENVFILE=ENVBATCH

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Run Auto Withdrawal process to create T404 cards
### ******************************************************************
export T404AUTOWD=$XJOB/${EBSRPTPFX}T404AUTOWD200
export T395AUTOWD=$XJOB/${EBSRPTPFX}T395AUTOWD200
export T404CARDS=$XJOB/$T404CARDFILE
export T813CARDS=$XJOB/$T813CARDFILE
export PICSRCTYPEIDX=$XJOB/PIC_SRC_TYPE.idx

if $runprog; then
   export AUTOWD200ORIG=$POSTOUT/AUTOWD200.txt

   if [[ -s $AUTOWD200ORIG ]]; then
      export AUTOWD200=$XJOB/AUTOWD200.txt
      mv $AUTOWD200ORIG $AUTOWD200

      export RSTROUT=$XJOB/rs.tmp
      export DATEHOLIDAY=$AULDATA/DATEHOLIDAY

      PNAME=ADB1020U
      JDESC="Run program $PNAME"
      execute_program
   fi
fi

### ******************************************************************
### TASK 2 - Load T404 cards
### ******************************************************************

if $load404cards; then
   if [[ -s $T404CARDS ]]; then
      cp $T404CARDS $EBSCARD/$T404CARDFILE

      export PNAME=JOBVTUT
      export JPRM1=$JUSER
      export JPRM2=$T404CARDFILE
      execute_job
   fi
fi

### ******************************************************************
### TASK 3 - Load T813 cards
### ******************************************************************

if $load813cards; then
   if [[ -s $T813CARDS ]]; then
      cp $T813CARDS $EBSCARD/$T813CARDFILE

      export PNAME=JOBVTUT
      export JPRM1=$JUSER
      export JPRM2=$T813CARDFILE
      execute_job
   fi
fi

### ******************************************************************
### TASK 4 - Send Report to a document manager
### ******************************************************************

$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 5 - Send Powerimage extract to PI EMS (WMS 6013)
### ******************************************************************

if $sendtoPI && [[ $EBSFSET != "hop" ]]; then
   cp AWD* $PI_CIFS
   cond=$?
   if [ $cond -gt 0 ]; then
      export JDESC="No PI EMS extract performed"
      display_message nobanner
   else
      export JDESC="PI EMS extract copied to $PI_CIFS"
      display_message nobanner
   fi
fi
### ******************************************************************
### TASK 6 - clean up and say goodbye
### ******************************************************************

eoj_housekeeping
copy_to_masterlog
return 0
