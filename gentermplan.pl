#!/usr/bin/env perl

use feature ':5.10';

use Data::Dumper;
use File::Basename;
use Getopt::Long;
use Pod::Usage;
use XML::Simple;

########################################
# MAIN.                                #
########################################

MAIN: {
    my $basename = basename $0, ".pl";

    GetOptions ( 'outdir=s' => \$outdir,
                 'help|h' => \$help ) or pod2usage(1);

    pod2usage 1 unless -d $outdir;

    my $planfile = shift;

    open FD, "<$planfile" or die "Failed to open $planfile, stopped";
    @plans = <FD>;
    close FD;
    chomp @plans;


    ########################################
    # Generate from the plan list:         #
    # T002 Tran delete cards.              #
    # T051 Plan delete cards.              #
    # T966 TST-PERSON cards.               #
    ########################################

    open T002, ">$outdir/t002.del" or die "Failed to open $outdir/t002.del, stopped";
    open T051, ">$outdir/t051.del" or die "Failed to open $outdir/t051.del, stopped";
    open T966, ">$outdir/t966.per" or die "Failed to open $outdir/t966.per, stopped";

    foreach my $plan (@plans)
    {
	format T002 =
00200   $TIHDR @<<<<<
$plan
00201   VTRNCM                        DELETEPLAN
.
	format T051 =
05100   $TIHDR @<<<<<000000000        ***
$plan
05101   PL          2
.
	format T966 =
96600   $TIHDR @<<<<<000000000           @<<<<<<<
$plan, $date
96601   UTL-PERSON                                         001 1
96602         TX021='A';
.
	write T002;
	write T051;
	write T966;
    }

    close T002;
    close T051;
    close T966;


    exit;
}

__END__

=head1 NAME

gentermplan.pl

=head1 SYNOPSIS

gentermplan.pl --outdir /put/cards/here /file/containing/plan/list.txt

=head1 DESCRIPTION

Generate T002, T051 and T966 plan cards for term environment.

=back
