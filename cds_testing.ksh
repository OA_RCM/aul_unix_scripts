#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxcds701.ksh            NO CALL         *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : CDS testing to document manager           *
#COMMENT *                 Run calculator INFFROMCDS                 *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $EBSMSTRPROC                              *
#COMMENT *  Author       : AUL - PAW                                 *
#COMMENT *  Created      : 09/17/2002                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : devmenu.ksh                               *
#COMMENT *  Script Calls : JOBCALC                                   *
#COMMENT *  COBOL Calls  : INF1090R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  5 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U  Date: 08/12/2003 SMR: CC4636  By: Mike Lewis             U
#COMMENT U  Reason: Added new script formatting to allow this to     U
#COMMENT U          work properly.                                   U
#COMMENT U  Reason: OmniHop  SMR: CC9056  By: Karen Lawson           U
#COMMENT U                                                           U
#COMMENT U Date: 08/03/2006  SMR: CC8403   By: Steve Loper           U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 20110610   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
####******************************************************************
### function declarations
####******************************************************************
function cre8file
{
  export JDESC="call JOBCALC to run INFFROMCDS calculator"
  display_message nobanner

  if [[ -s $CDSFILE ]]
  then
     #
     # sleep just to make sure no FTP session is going on any of the files
     #
     sleep 5
      mv $CDSFILE $XJOB/cdstfile.txt
      . JOBCALC $LOGNAME INFFROMCDS.txt 01
  else
     echo; echo Nothing to process
     export JDESC="Nothing to process"
     display_message nobanner
     return 0
  fi
  run1090R
}
function run1090R
{
  export PNAME=INF1090R
  export JDESC="run COBOL program $PNAME"; display_message nobanner
  export dd_INF1090E=$XJOB/INF1090E
  export dd_INF1090R=$XJOB/${EBSRPTPFX}INF1090R
  execute_program
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
### *******************************
### setup standard local variables
### *******************************
prog=$(basename $0)
export JNAME=${prog%.ksh}
export JUSER=${JUSER:-$LOGNAME}
export JDESC="CDS testing to document manager"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT
export MKOUTDIR=current

### *******************************
### standard setup
### *******************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
cd $XJOB

### ******************************************************************
### TASK 1: Run JOBCALC on INFROMCDS and run COBOL program INF1090R
### ******************************************************************

$cre8file && cre8file

### ******************************************************************
### TASK 2: Send Report to document manager
### ******************************************************************

$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK END: clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
