#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXADB702           NO  CALL            *
#COMMENT *  UNIX POINTER : rsuxadb702.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : adb_tax1099rpt.ksh                        *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce 1099 Tax Reports    *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 06/07/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : INF1015E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  30 min                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Define files needed by Sungard and AUL routines.T
#COMMENT T  TASK 2 - Run the COBOL program to produce report         T
#COMMENT T  TASK 3 - Prepare file1 for transfer to IBM.              T
#COMMENT T  TASK 4 - Prepare file2 for transfer to IBM.              T
#COMMENT T  TASK 5 - Send reports to document manager.               T
#COMMENT T  TASK 6 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 06/07/2001  SMR: SMR????  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program INF1015E               U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2001  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U                                                           U
#COMMENT U Date: 07/31/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: August 2006  CC: 9056     By: Steve Loper           U
#COMMENT U Reason: Modifications to create HOP version.              U
#COMMENT U                                                           U
#COMMENT U Date: June 2007    CC: 11860    By: Steve Loper           U
#COMMENT U Reason: DOCUMENTATION CHANGE ONLY - NO FUNCTIONAL CHANGES U
#COMMENT U Reason: Correct absence of 9056 documentation/sync PVCS.  U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2007  SMR: CC11967  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U                                                           U
#COMMENT U Date: 11/01/2007  SMR: CC12331  By: Mike Lewis            U
#COMMENT U Reason: Correct report name for second file.              U
#COMMENT U                                                           U
#COMMENT U Date: 20110317   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function run_program
{
  cd $XJOB
  PNAME=INF1015E
  JDESC="Run $PNAME to create 1099 tax reports"
  execute_program
}
function prepare_file1_for_transfer
{
  JDESC="Preparing 1099 Tax Extract for transfer"
  display_message nobanner

  export FRFILE=$TAXEXTRACT

  create_file1
  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying 1099 Tax Extract to $AULOUT for ftp job "
    display_message nobanner
    cat $FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No 1099 Tax Extract created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file1
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH3200
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP3200
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ3200
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"
  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
function prepare_file2_for_transfer
{
  JDESC="Preparing 1099 Tax Reports for transfer"
  display_message nobanner

  export FRFILE=$XJOB/RPTFILE3205
  rm $FRFILE
  [[ -s $TAXERRORS ]] && cat $TAXERRORS >> $FRFILE
  [[ -s $TAXANNERRORS ]] && cat $TAXANNERRORS >> $FRFILE
  [[ -s $TAXREPORT ]] && cat $TAXREPORT >> $FRFILE
  [[ -s $TAXANNREPORT ]] && cat $TAXANNREPORT >> $FRFILE
  [[ -s $TAXSUPPRESSRPT ]] && cat $TAXSUPPRESSRPT >> $FRFILE
  [[ -s $TAXANNSUPPRESSRPT ]] && cat $TAXANNSUPPRESSRPT >> $FRFILE


  create_file2
  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying 1099 Tax Reports to $AULOUT for ftp job "
    display_message nobanner
    cat $FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No 1099 Tax Reports created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file2
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH3205
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP3205
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ3205
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"
  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
function end_of_job
{
  cd $XJOB
  eoj_housekeeping
  copy_to_masterlog
  create_html
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Creating Tax 1099 Report"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

print "ENV_GLOBAL-MODE=EDIT" >> $XJOB/ENV

cd $XJOB
#######################################################################
### TASK 1 - Define files needed by Sungard and AUL routines
#######################################################################
cp $EBSCTRL/DUMBTRAN.DAT $XJOB
cp $EBSCTRL/DUMBTRN2.DAT $XJOB
touch $XJOB/DUMMY3.DEF
touch $XJOB/DUMMY2.DEF
export dd_RSTROUT=$XJOB/rs.tmp
export dd_TRANIN=$XJOB/DUMBTRAN.DAT
export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
export DATEMTHEND=$AULDATA/DATEMTHEND
export TAXERRORS=$XJOB/${EBSRPTPFX}TAXERRORS
export TAXANNERRORS=$XJOB/${EBSRPTPFX}TAXANNERRORS
export TAXERRORSORT=$XJOB/TAXERRORSORT
export INTERMEDIATETAXEXT=$XJOB/INTERMEDIATETAXEXT
export TAXEXTRACTSORT=$XJOB/TAXEXTRACTSORT
export TAXREPORT=$XJOB/${EBSRPTPFX}TAXREPORT
export TAXANNREPORT=$XJOB/${EBSRPTPFX}TAXANNREPORT
export TAXSUPPRESSRPT=$XJOB/${EBSRPTPFX}TAXSUPPRESSRPT
export TAXANNSUPPRESSRPT=$XJOB/${EBSRPTPFX}TAXANNSUPPRESSRPT
export TAXEXTRACT=$XJOB/TAXEXTRACT

######################################################################
### TASK 2 - Run the COBOL program to produce deposit confirmations
######################################################################

$run_program && run_program

######################################################################
### TASK 3 - Prepare file1 for transfer to IBM
######################################################################

$prepare_file1 && prepare_file1_for_transfer

######################################################################
### TASK 4 - Prepare file2 for transfer to IBM
######################################################################

$prepare_file2 && prepare_file2_for_transfer

######################################################################
### TASK 5 - Send reports to document manager.
######################################################################

$send2DocMgr && fnc_send2DocMgr

################################################################
### TASK 6 - clean up and say goodbye
################################################################
end_of_job
return 0
