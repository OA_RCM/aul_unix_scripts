#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT * Job Name:       rsuxhta701.ksh
#COMMENT * Script Name   : hta_disc2.ksh                             *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to run cobol programs that extract    *
#COMMENT *                 calendar period fixed fund data from Omni *
#COMMENT *                 and generates the Harris Trust Annual     *
#COMMENT *                 Disclosure letters.                       *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - DRH                                 *
#COMMENT *  Created      : 11/16/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : hta_disc.ksh                              *
#COMMENT *  Script Calls : JOBDEFINE JOBCALC                         *
#COMMENT *  COBOL Calls  : RPT1040E RPT1041R                         *
#COMMENT *  Omni Calc    : EXT-RATEOFRTRN.TXT                        *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Annual/Single plan on demand              *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 1 - 20 minutes depending on how many      *
#COMMENT *                 plans are in control file                 *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Refer to htannual_disclosure.ksh R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 04/16/2001  CC: RSS1109   By: Danny Hagans          U
#COMMENT U Reason: Created to run the program HT Annual Disclosure   U
#COMMENT U        Letters.                                           U
#COMMENT U Date: 01/24/2006  CC: RSS9554   By: Danny Hagans          U
#COMMENT U Reason: Modified sort to include addition of Companion    U
#COMMENT U        Plan numbers in extract file                       U
#COMMENT U Date: 12/22/2006  CC: RSS10567  By: Danny Hagans          U
#COMMENT U Reason: Modified script to match scripting standards      U
#COMMENT U        and run in HOP environment.                        U
#COMMENT U Date: 07/11/2007  CC: RSS11844  By: Danny Hagans          U
#COMMENT U Reason: Modified script to stop printing 'Estimated       U
#COMMENT U        Payout Report'.                                    U
#COMMENT U Date: 01/08/2010  CC: WMS03478  By: Louis Blanchette      U
#COMMENT U Reason: Remove MAAS                                       U
#COMMENT U Date: 20110607  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U Date: 20131211  Proj: WMS10125  By: Danny Hagans          U
#COMMENT U Reason: Added code for Audit - Fixed & Stable Value Rpt.  U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
### ******************************************************************
### define local functions
### ******************************************************************
### ******************************************************************
### Send all Harris Trust Annual Disclosure reports to document manager
### ******************************************************************
function send2DocMgr
{
  cd $XJOB

ll | sed '1d' | \
while read perm garb own grp size mm dd time fname
do

  FADname=`echo $fname | cut -c1-4`                                   
  if [[ "$FADname" = "AUDA" ]]                                        
  then                                                                
     cat $XJOB/$fname > $AULOUT/${EBSRPTPFX}$fname                    
     fnc_send2DocMgr $AULOUT/${EBSRPTPFX}$fname                       
     JDESC="Sent ${EBSRPTPFX}$fname to document manager"              
     display_message nobanner                                         
                                                                      
  fi                                                                  
  FADname=`echo $fname | cut -c1-4`
  if [[ "$FADname" = "RPTD" ]]
  then
     cat $XJOB/$fname > $AULOUT/${EBSRPTPFX}$fname
     fnc_send2DocMgr $AULOUT/${EBSRPTPFX}$fname
     JDESC="Sent ${EBSRPTPFX}$fname to document manager"
     display_message nobanner

  fi
  FADname=`echo $fname | cut -c1-4`
  if [[ "$FADname" = "DEFD" ]]
  then
     cat $XJOB/$fname > $AULOUT/${EBSRPTPFX}$fname
     fnc_send2DocMgr $AULOUT/${EBSRPTPFX}$fname
     JDESC="Sent ${EBSRPTPFX}$fname to document manager"
     display_message nobanner

  fi

  FADname=`echo $fname | cut -c1-4`
  if [[ "$FADname" = "RPTM" ]]
  then
     Prtflag=`echo $fname | cut -c14`
     if [[ "$Prtflag" = "Y" ]]
     then
        cat $XJOB/$fname >> $MVARPT
     fi
     cat $XJOB/$fname > $AULOUT/${EBSRPTPFX}$fname
     fnc_send2DocMgr $AULOUT/${EBSRPTPFX}$fname
     JDESC="Sent ${EBSRPTPFX}$fname to document manager"
     display_message nobanner

  fi
  FADname=`echo $fname | cut -c1-4`
  if [[ "$FADname" = "RPTE" ]]
  then
     Prtflag=`echo $fname | cut -c14`
     if [[ "$Prtflag" = "Y" ]]
        then
        cat $XJOB/$fname >> $MVARPT
     fi
     cat $XJOB/$fname > $AULOUT/${EBSRPTPFX}$fname
     fnc_send2DocMgr $AULOUT/${EBSRPTPFX}$fname
     JDESC="Sent ${EBSRPTPFX}$fname to document manager"
     display_message nobanner

  fi
done

 cat $XJOB/COMBINEDERR > $AULOUT/${EBSRPTPFX}COMBINEDERR
  fnc_send2DocMgr $AULOUT/${EBSRPTPFX}COMBINEDERR
 JDESC="Sent Combined Exception report - ${EBSRPTPFX}COMBINEDERR - to document manager"
 display_message nobanner

 cat $XJOB/COMBINEDWRN > $AULOUT/${EBSRPTPFX}COMBINEDWRN
  fnc_send2DocMgr $AULOUT/${EBSRPTPFX}COMBINEDWRN
 JDESC="Sent Combined Warning report - ${EBSRPTPFX}COMBINEDWRN - to document manager"
 display_message nobanner


  JDESC="Sent all Annual Disclosure reports to document manager"
  display_message nobanner
}
### ******************************************************************
### Send HTAD reports to IBM printer
### ******************************************************************
function print_annualdisc
{
JDESC="Calling Connect Direct for Annual Report"
display_message
  export REPORT1=$XJOB/ANNDISC
  if [[ -s $REPORT1 ]]
  then
   PNAME=rpt_maas4500.ksh
   JDESC="Calling Connect Direct for Annual Disclosure Report"
  execute_job
  fi
}

### ******************************************************************
### set variables to initial values
### ******************************************************************
  prog=$(basename $0)
  export JUSER=${JUSER:-$LOGNAME}
  export JGROUP=${JGROUP:-01}
  export JNAME=${prog%.ksh}
  export ENVFILE=ENVBATCH

  . FUNCTIONSFILE
  set_generic_variables
  export MKOUTDIR=current
  make_output_directories
### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
  . JOBDEFINE

  fnc_log_standard_start

  cd $XJOB

### ******************************************************************
### define job variables
### ******************************************************************
  export dd_ANNDISC=$XJOB/ANNDISC
  export MVARPT=$XJOB/mvarpt.$tmptime
  export dd_INFOTXT1=$AULDATA/anndisc1.txt
  export dd_INFOTXT2=$AULDATA/anndisc2.txt
  export dd_INFOTXT3=$AULDATA/anndisc3.txt
  export dd_GROSSRATEX=$AULDATA/GROSSRATEX
  export dd_RATEOFRTRN=$XJOB/RATEOFRTN.txt
  export dd_RATEOFRTRNIN=$XJOB/RATEIFRTRNIN
  export AULOUT=$POSTOUT
  export dd_HTACOMPF=$XJOB/HTADCOMPF
  export dd_HTACOMPFIN=$XJOB/HTADCOMPIN
  export dd_CONVTWARN=$XJOB/CONVTWARN
  export dd_CONVTEXCPT=$XJOB/CONVTEXCPT
### ******************************************************************
### define files needed by Sungard I/O routines
### ******************************************************************
  cp $EBSCTRL/DUMBTRAN.DAT $XJOB
  cp $EBSCTRL/DUMBTRN2.DAT $XJOB
  touch $XJOB/DUMMY3.DEF
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_TRANIN=$XJOB/DUMBTRAN.DAT
  export dd_TRANOUT=$XJOB/DUMBTRN2.DAT

### ******************************************************************
### define files needed by AUL programs
### ******************************************************************
  export dd_IN200=$XJOB/annctl.txt
  export dd_IN200RW=$XJOB/htannctl.txt

### ******************************************************************
### Run JOBCALC to extract the COMPANION PLAN data
### ******************************************************************

  PNAME=JOBCALC
  JPRM1=$LOGNAME
  JPRM2=HTADCOMP.txt
  JPRM3=$JGROUP
  JDESC="Run $PNAME to extract the Companion Case file"
   execute_job
  mv $EBSRPT/HTADCOMPEX $XJOB/.
  grep '*' HTADCOMPEX >> $XJOB/annctl.txt
  grep -v '*' HTADCOMPEX > $XJOB/HTADCOMPF
  rm $XJOB/HTADCOMPEX

### ******************************************************************
### Run JOBCALC to extract the  Rate of Return data
### ******************************************************************

  PNAME=JOBCALC
  JPRM1=$LOGNAME
  JPRM2=EXT-RATEOFRTRN.TXT 
  JPRM3=$JGROUP
  JDESC="Run $PNAME to extract the Rate of Return file"
  execute_job
### ******************************************************************
### Sort control file
### ******************************************************************
  sort -o $XJOB/annctl.tmp -k .1,.22 $XJOB/annctl.txt

  cond=$?
  if [ $cond -gt 0 ]
  then
     JDESC="$JNAME Abended in sort with cond= $cond"
     display_message nobanner
     return $cond
  fi

  cp $XJOB/annctl.tmp $XJOB/annctl.txt
  cond=$?
  if [ $cond -gt 0 ]
  then
     JDESC="$JNAME Abended in copy of control file with cond= $cond"
     display_message nobanner
     return $cond
  fi

  JDESC="Sort of annctl.txt complete"
  display_message nobanner

### ******************************************************************
### Run the COBOL program to create extracts
### ******************************************************************

  PNAME=RPT1040E;
  JDESC="Run COBOL program $PNAME"
  execute_program

### *********************************************************************
### Sort the extract data by Plan/Companion Case, transaction type and
### sub types
### ********************************************************************
sort -o $XJOB/ANNDSCEXT.srt -k .1,.18 -k .41,.49 -k .21,.23 $XJOB/ANNDSCEXT

  cond=$?
  if [ $cond -gt 0 ]
  then
     JDESC="$JNAME
     Abended in sort of ANNDSCEXT with cond= $cond"
     display_message nobanner
     return $cond
  fi

### *********************************************************************
### Sort the Exception/Warn files
### *********************************************************************

sort -o $XJOB/CONVTWARN.srt -k .9,.15 -k .1,.6 $XJOB/CONVTWARN

  cond=$?
  if [ $cond -gt 0 ]
  then
     JDESC="$JNAME
     Abended in sort of CONVTWARN with cond= $cond"
     display_message nobanner
     touch CONVTWARN.srt
  fi

sort -o $XJOB/CONVTEXCPT.srt -k .9,.15 -k .1,.6 $XJOB/CONVTEXCPT

  cond=$?
  if [ $cond -gt 0 ]
  then
     JDESC="$JNAME
     Abended in sort of CONVTEXCPT with cond= $cond"
     display_message nobanner
    touch CONVTEXCPT.srt
  fi


#########################################################################
### Run the COBOL program to generate the Harris trust report files
#########################################################################

  export dd_ANNDSCEXT=$XJOB/ANNDSCEXT.srt
  export dd_CONVTWARN=$XJOB/CONVTWARN.srt
  export dd_CONVTEXCPT=$XJOB/CONVTEXCPT.srt
  PNAME=RPT1041R
  JDESC="Run COBOL program $PNAME"
  execute_program

### ******************************************************************
### Send Consolidated Annual disclosure reports to IBM for printing.
### ******************************************************************
     $send2DocMgr && send2DocMgr
     $print_annualdisc && print_annualdisc
### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  return
