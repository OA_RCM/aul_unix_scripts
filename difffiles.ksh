#!/bin/sh 
#
if [[ -z $1 ]]; then
  echo
  echo "Enter FROM dir (HS/HC=home source/copy; TS/TC=TCU; RS/RC=RCU):"
  read dirfrom
  case $dirfrom in
    "HS") dirfrom=$HOME/source ;;
    "HC") dirfrom=$HOME/copy ;;
    "TS") dirfrom=$EBSTCUSRC ;;
    "TC") dirfrom=$EBSTCUCPY ;;
    "RS") dirfrom=$EBSRCUSRC ;;
    "RC") dirfrom=$EBSRCUCPY ;;
    "*") echo Invalid entry $dirfrom
         return 0
  esac
else
  dirfrom=$1
fi
if [[ "$dirfrom" = "" ]]; then
  echo A directory must be entered
  return 0
fi

if [[ -z $2 ]]; then
  echo
  echo "Enter TO dir (TS/TC=TCU; RS/RC=RCU; CS/CC=CUS):"
  read dirto
  case $dirto in
    "TS") dirto=$EBSTCUSRC ;;
    "TC") dirto=$EBSTCUCPY ;;
    "RS") dirto=$EBSRCUSRC ;;
    "RC") dirto=$EBSRCUCPY ;;
    "CS") dirto=$EBSCUSSRC ;;
    "CC") dirto=$EBSCUSCPY ;;
    "*") echo Invalid entry $dirto
         return 0
  esac
else
  dirto=$2
fi
if [[ "$dirto" = "" ]]; then
  echo A directory must be entered
  return 0
fi

origdir=$PWD
cd $dirfrom || { echo Invalid directory; return 0; }

### *****************************************************
### get names of programs to move and compile
### *****************************************************

echo
difflist=$HOME/diff.lst
rm $difflist
touch $difflist
book=1
while [ -n "$book" ]; do
  read book?"Enter full name to add to diff list: "
  if [ -n "$book" ]; then
    if [ ! -s $book ]; then
      echo "File does not exist in $dirfrom -- skipping"
    else
      if [ ! -s $dirto/$book ]; then
        echo "File does not exist in $dirto -- skipping"
      else
        echo "$book" >> $difflist
      fi
    fi
  fi  
done
if [[ ! -s $difflist ]]; then
  echo "You must enter a program name - aborting"
  return 0
fi

difffile=$HOME/difffile.txt
rm $difffile
touch $difffile

cat $difflist | \
while read book; do
   echo "Performing diff of $book"
   print "diff of $book between $dirfrom $dirto" >> $difffile
   diff $book $dirto >> $difffile
   print "END DIFF" >> $difffile
   print " " >> $difffile
   print " " >> $difffile
done

cd $origdir
