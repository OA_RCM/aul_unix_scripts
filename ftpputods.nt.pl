#!/opt/perl5/bin/perl

$ODS="$ENV{'ODSFILE'}";
$CTRL="$ENV{'HOME'}";
$LOGS="$ENV{'EBSLOGS'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit($?);

sub check
{
  $result=`$NCFTPPUT -d $LOGS/ftpputods.log -f $CTRL/"."ftpputods.cfg -E -a ./SrcFiles $ODS`;
  print $result;
  if ( $? == 0 )
  {
    print "$tody: ftp of $ODS file to 9000 successful\n";
  }
  else
  {
    print OUT "$tody ERROR: Could not ftp $ODS file to 9000\n"; 
  }
}
