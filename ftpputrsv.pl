#!/opt/perl5/bin/perl

$RSV="$ENV{'RSVFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputrsv.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $RSV`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $RSV file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $RSV file to ODS\n"; 
    }
}
