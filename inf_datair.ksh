#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : inf_datair.ksh            No-Call         *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : This program produces an Extract report   *
#COMMENT *                 from the stmt file and creates a file     *
#COMMENT *                 that will be formatted and used for       *
#COMMENT *                 DATAIR.  Replaces MAAS DATAIR             *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - PAW                                 *
#COMMENT *  Created      : 02/08/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxinf709.ksh                            *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  : INF1030R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On Request                                *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tasks                                                    T
#COMMENT T                                                           T
#COMMENT T TASK 1 - Gather user requested plans for report           T
#COMMENT T TASK 2 - define files needed by AUL programs              T
#COMMENT T TASK 3 - Run the COBOL program                            T
#COMMENT T TASK 4 -  Queue Report and Error files for FTP to LAN     T
#COMMENT T TASK 5 - clean up and say goodbye                         T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 02/08/2001  SMR: ******** By: Patty Wamsley         U
#COMMENT U Reason: Created to run the program INF1030R               U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2001  SMR: ******** By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U                                                           U
#COMMENT U Date: 05/08/2003  SMR: CC 3036  By: Rick Sica             U
#COMMENT U Reason: Still Original - needs to find Summary file       U
#COMMENT U                                                           U
#COMMENT U Date: 06/24/2003  SMR: CC 4696  By: Rick Sica             U
#COMMENT U Reason: Add Batch Wizard functionality to script          U
#COMMENT U         Fulfills part of num 4 of CC                      U
#COMMENT U                                                           U
#COMMENT U Date: 08/07/2003  SMR: CC 4696  By: Steve Loper           U
#COMMENT U Reason: Add Batch Wizard functionality to script          U
#COMMENT U         Complete CC as written for Datair reports.        U
#COMMENT U                                                           U
#COMMENT U Date: 03/01/2004  SMR:CC5940    By: Mike Lewis            U
#COMMENT U Reason: LAN migration path modifications.                 U
#COMMENT U                                                           U
#COMMENT U Date: 07/14/2005  SMR:CC7977    By: Rick Sica             U
#COMMENT U Reason: gunzip OSDSUMMARY files                           U
#COMMENT U                                                           U
#COMMENT U Date: 20110520    WMS: 2998     By: Tony Ledford          U
#COMMENT U Reason: Eliminate use of connect direct                   U
#COMMENT U                                                           U
#COMMENT U Date: 20111005   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Remove code that sends output to Vista+                   U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
########################################################
## send report file(s) to Datair folder on LAN        ##
########################################################
function send_file_to_lan
{
  typeset fname=$1

  fnc_ftpq $EBSSTMT/$fname.TXT
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME="${prog%.ksh}"
export JDESC="OMNIPLUS/UX DATAIR"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables
 
######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input 

make_output_directories

######################
### Override daily date and rundate 
### if one is passed in
######################
fnc_set_daily_date

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Gather user requested plans for report
### ******************************************************************
  export JDESC="gather control files"
  display_message

  rptlist=$XJOB/rptlist
  ls -1 $WIZARDDIR | grep datairctl >$rptlist
  if [[ -s $rptlist ]]
  then
     #
     # sleep just to make sure no FTP session is going on any of the files
     #
     sleep 5
     for file in $(cat $rptlist)
     do
       cat $WIZARDDIR/$file >> $XJOB/datairctl.txt
       mv $WIZARDDIR/$file $XJOB
     done
  else
     echo; echo Nothing to process
     export JDESC="Nothing to process"
     display_message
     return 0
  fi

### ******************************************************************
### TASK 2 - define files needed by AUL programs
### ******************************************************************
export JDESC="define AUL local files"
display_message
export JDESC="Summary file defined as $XJOB/$FSUM"
display_message

export dd_DATAIRCTL=$XJOB/datairctl.txt
export dd_LDFILE=$XJOB/LDFILE.DAT
export dd_SORTFILE=$XJOB/SORTFILE
export dd_SRTDFILE=$XJOB/SRTDFILE
export dd_STORFILE=$XJOB/STORFILE

### ******************************************************************
### TASK 3 - Run the COBOL program 
### ******************************************************************
cd $EBSSTMT

if [[ -s $dd_DATAIRCTL ]]
then
  for file in $(cat $dd_DATAIRCTL)
  do
    pfname="OSDSUMMARY${file}"
    gunzip $pfname
  done
fi

PNAME=INF1030R
JDESC="run COBOL program $PNAME"
execute_program
cd $XJOB

### ******************************************************************
### TASK 4 -  Queue Report and Error files for FTP to LAN
### ******************************************************************
if [[ -s $dd_DATAIRCTL ]]
then
  for file in $(cat $dd_DATAIRCTL)
  do
    send_file_to_lan $file
    pfname="OSDSUMMARY${file}"
    gzip $EBSSTMT/$pfname
  done
else
  export JDESC="No error reports to send to LAN"
  display_message
fi

### ******************************************************************
### TASK 5 - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
