#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Script Name  : hta_rename.ksh           CALL             *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Connect Direct to copy annual disclosure  *
#COMMENT *                 file to Unix from Maas.                   *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $EBSPROC                                  *
#COMMENT *  Author       : AUL - DRH                                 *
#COMMENT *  Created      : 01/25/2005                                *
#COMMENT *  Environment  :                                           *
#COMMENT *  Called by    : hta_Maascpy.ksh                           *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On Request                                *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                   R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date:             SMR: *******  By:                       U
#COMMENT U Reason:                                                  )U
#COMMENT U                                                           U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

###************ FUNCTIONS ********************************************
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function upshift_all_Plans
{

################## All Files  ***************************
ll -tr *| sed '1d' > compfile.txt
cat $EBSINPUT/htannual/compfile.txt/ | \
while read perm garb own grp size mm dd time fname
do
     l2u $fname
done

}

function rename_A_Plans
{

***************** A Files ******************************
cat compAfile.txt/ | \
while read perm garb own grp size mm dd time fname
do
     Fcase1=`echo $fname | cut -c3-7`
     Fcase2=`echo $fname | cut -c2-7`
     Fendchr=`echo $fname | cut -c8`
     Fcase3=`echo $fname  | cut -c24-28`
     display_message nobanner
     Ffromfile=L"$Fcase2$Fendchr";
     Ftofile=l"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
     Ffromfile=R"$Fcase2$Fendchr";
     Ftofile=r"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
     Ffromfile=E"$Fcase2$Fendchr";
     Ftofile=e"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
    export JDESC="$Ffromfile rename case $Ftofile"
done

}

function rename_C_Plans
{


###################### C Files ******************
cat compCfile.txt/ | \
while read perm garb own grp size mm dd time fname
do
     Fcase1=`echo $fname | cut -c3-7`
     Fcase2=`echo $fname | cut -c2-7`
     Fendchr=`echo $fname | cut -c8`
     Fcase3=`echo $fname  | cut -c24-28`
     display_message nobanner
     Ffromfile=L"$Fcase2$Fendchr";
     Ftofile=l"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
     Ffromfile=R"$Fcase2$Fendchr";
     Ftofile=r"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
     Ffromfile=E"$Fcase2$Fendchr";
     Ftofile=e"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
    export JDESC="$Ffromfile rename case $Ftofile"
done

}

function rename_D_Plans
{


################## D Files ###########################
cat compDfile.txt/ | \
while read perm garb own grp size mm dd time fname
do
     Fcase1=`echo $fname | cut -c3-7`
     Fcase2=`echo $fname | cut -c2-7`
     Fendchr=`echo $fname | cut -c8`
     Fcase3=`echo $fname  | cut -c24-28`
     display_message nobanner
     Ffromfile=L"$Fcase2$Fendchr";
     Ftofile=l"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
     Ffromfile=R"$Fcase2$Fendchr";
     Ftofile=r"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
     Ffromfile=E"$Fcase2$Fendchr";
     Ftofile=e"$Fcase2$Fendchr";
     mv $Ffromfile $Ftofile
    export JDESC="$Ffromfile rename case $Ftofile"
done

}

############## Find companion case file and rename according to Contract # line

function rename_Companions
{
cat hta-companions.txt/ | \
while read fname
  do
     Fcase1=`echo $fname | cut -c3-7`
     Fcase2=`echo $fname | cut -c2-7`
     Fendchr=`echo $fname | cut -c8`
     Fstart=`echo $fname | cut -c2`
     Fcase3=`echo $fname  | cut -c24-28`
     Ffromfile=l"$Fcase2$Fendchr";
     Ftofile=l"$Fstart$Fcase3"A;
     cp $Ffromfile $Ftofile
     Ffromfile=r"$Fcase2$Fendchr";
     Ftofile=r"$Fstart$Fcase3"A;
     cp $Ffromfile $Ftofile
     Ffromfile=e"$Fcase2$Fendchr";
     Ftofile=e"$Fstart$Fcase3"A;
     cp $Ffromfile $Ftofile
     export JDESC="$Ffromfile companion case $Ftofile - $Fstart $Fcase1 $Fcase3"
     display_message nobanner

  done
}

function rename_Companions2
{
cat hta-companions2.txt/ | \
while read fname e1 r1 o1 t1 a1 d1 r2 f1 c1 fcase
  do
     Fcase1=`echo $fname | cut -c3-7`
     Fcase2=`echo $fname | cut -c2-7`
     Fendchr=`echo $fname | cut -c8`
     Fstart=`echo $fname | cut -c2`
     Fcase3=`echo $fcase  | cut -b3-7`
     Ffromfile=l"$Fcase2$Fendchr";
     Ftofile=l"$Fstart$Fcase3"A;
     cp $Ffromfile $Ftofile
     Ffromfile=r"$Fcase2$Fendchr";
     Ftofile=r"$Fstart$Fcase3"A;
     cp $Ffromfile $Ftofile
     Ffromfile=e"$Fcase2$Fendchr";
     Ftofile=e"$Fstart$Fcase3"A;
     cp $Ffromfile $Ftofile
     export JDESC="$Ffromfile companion case $Ftofile - $Fstart $Fcase1 $Fcase3"
     display_message nobanner

  done
}

## ******************************************************************
### define job variables
### ******************************************************************
prog=$(basename $0)
  export JUSER=${JUSER:=$LOGNAME}
  export JNAME=${prog%.ksh}
  export JDESC="$JNAME.ksh to create and send annual reports"

### ******************************************************************
### standard job setup procedure
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start
### ******************************************************************
### Task 1 Transfer file renames
### ##################################################################
export JDESC="File rename (1) starting"
     display_message nobanner
cd $EBSINPUT/htannual

upshift_all_Plans

ll -tr *A > compAfile.txt

rename_A_Plans

ll -tr E*C > compCfile.txt

rename_C_Plans

ll -tr E*D > compDfile.txt

rename_D_Plans

## ******************************************************************
### Task 2 Search for Companion Cases and rename files
### ##################################################################
export JDESC="Companion case file rename (2) starting"
     display_message nobanner

grep 'Contract #' *C > hta-companions.txt
grep 'Contract #' *D >> hta-companions.txt
rename_Companions

grep 'CASE,' e*C > hta-companions2.txt
grep 'CASE,' e*D >> hta-companions2.txt
rename_Companions2
port JDESC="Rename and HTAD process Finished"
display_message nobanner

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  return
