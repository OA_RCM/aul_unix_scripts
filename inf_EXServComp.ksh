LOUIS WAS HERE AGAIN



#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : RSUXINF005             CALL            *
#COMMENT *  Schedule Name   : rsuxinf005.ksh         ASU-OMNI        *
#COMMENT *  Unix Script     : inf_EXServComp.ksh                     *
#COMMENT *                                                           *
#COMMENT *  Description  : CREATE 813 transactions in Omni from      *
#COMMENT *                 ExCCEPT serviceo file AND CREATE 875      *
#COMMENT *                 transactions from ExCCEPT compenso file   *
#COMMENT *                                                           *
#COMMENT *  Author       : LQB                                       *
#COMMENT *  Created      : 09/14/2007                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBVTUT, ODBC                             *
#COMMENT *  COBOL Calls  : INF1005E.CBL, INF1005L.CBL                *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily, On Demand                          *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : Dependant on file size(seconds,minutes ?) *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T    1 - Run COBOL program to create Serviceo and Compenso  T
#COMMENT T        file from a database if process not for Exacct     T
#COMMENT T    2 - Move the files to XJOB if EXACCT process           T
#COMMENT T    3 - Run COBOL program to create the X813,875,877 cards T
#COMMENT T    4 - Load 813 and 875  trans to VTRAN                   T
#COMMENT T    5 - Archive input files.                               T
#COMMENT T    6 - Standard EOJ housekeeping tasks                    T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U  CC10895  Added Hours by payroll 875 transactions         U
#COMMENT U                                                           U
#COMMENT U  Date: 02/04/2008  CC: 12475     By: Mike Lewis           U
#COMMENT U  Reason:  Add archiving of input files to prevent         U
#COMMENT U           duplications.                                   U
#COMMENT U                                                           U
#COMMENT U  Date: 05/08/2008  CC: 11190     By: Louis Blanchette     U
#COMMENT U  Reason:  Make ExCCEPT default and use parameter for      U
#COMMENT U           other interface like Internet and PowerImage    U
#COMMENT U                                                           U
#COMMENT U  Date: 07/08/2008  PROJ00000680  By: Zera Holladay        U
#COMMENT U  Reason: Need to replace Reflections as the file transfer U
#COMMENT U  tool for ExCCEPT.                                        U
#COMMENT U                                                           U
#COMMENT U  Date: 05/08/2008  CC: 11190     By: Louis Blanchette     U
#COMMENT U  Reason:  Make ExCCEPT default and use parameter for      U
#COMMENT U           other interface like Internet and PowerImage    U
#COMMENT U                                                           U
#COMMENT U  Date: 10/28/2008  CC: WMS1375   By: Louis Blanchette     U
#COMMENT U  Reason:  Added parameter for DESTINATION HOP or PROD     U
#COMMENT U                                                           U
#COMMENT U  Date: 11/04/2008  CC: 12288     By: Glen McPherson       U
#COMMENT U  Reason:  Blanking out dates; Add 877 cards               U
#COMMENT U  Date: 11/04/2008  CC: 12288     By: Louis Blanchette     U
#COMMENT U  Reason:  Blanking out dates; Add 877 cards               U


#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
###   function declarations
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  # DEFAULT IS ExCCEPT
  if [ "$JPARM" = "INTERNET" ]  || [ "$JPARM" = "POWERIMAGE" ];
  then
      export ORIGIN=$JPARM
      export create_files=true
      export move_files=false
      export archive_files=false
      JDESC="Override interface with $ORIGIN"
  else
      JDESC="Using default interface $ORIGIN"
  fi

  display_message nobanner
}
function create_files
{
   PMSG="function create_files"
   export COBSAVE=$COBRUN
   export COBRUN=$(dirname $COBRUN)/$AUL_RTS

   # PARMS defined in the ini file
  if [ "$JPARM" = "INTERNET" ]
    then
     PNAME=INF1005E
   PARM="$WEBDSN;$EXDSN;$DESTINATION;"
  fi
   if [ "$JPARM" = "POWERIMAGE" ]
    then
     PNAME=INF1006E
  fi
   JDESC="run COBOL program $PNAME with PARM: $PARM"
   execute_program
   export COBRUN=$COBSAVE

   JDESC="Completed $PMSG - using ODBC"
   display_message nobanner
   unset PMSG
}
function create_X_cards
{
  if [ ! -a $dd_SERVICEO ]
  then
     touch $dd_SERVICEO
     JDESC="$dd_SERVICEO NOT FOUND empty file created !"
     display_message nobanner
  fi

  if [ ! -a $dd_COMPENSO ]
   then
    touch $dd_COMPENSO
    JDESC="$dd_COMPENSO NOT FOUND empty file created !"
    display_message nobanner
  fi

  PMSG="function create_X_cards"

  export dd_WORKFILE=$XJOB/WORKFILE.txt
  export dd_RSTROUT=$XJOB/rs.tmp

  export COBSAVE=$COBRUN
  export COBRUN=$(dirname $COBRUN)/$AUL_RTS

  PNAME=INF1005L
  # The parm will indicate the Origin of the transaction
  # and will be displayed in the description of the folder.
  PARM="$ORIGIN;$EXDSN"
  JDESC="Run COBOL program $PNAME - $ORIGIN 813,875,877 file creation"
  execute_program

  export COBRUN=$COBSAVE

  JDESC="Completed $PMSG - $ORIGIN 813,875,877 file creation"
  display_message nobanner
  unset PMSG
}
function load_trans2VTRAN
{
   PMSG="function load_trans2VTRAN"

   if [ -s $dd_TROT ]
   then
      cp $dd_TROT $EBSCARD/$ORIGIN.813.$rundate
      JDESC="Load X813 output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=$ORIGIN.813.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file $dd_TROT to VTRAN"
      display_message nobanner
   else
      JDESC="File $dd_TROT is empty"
      display_message nobanner
   fi

   if [ -s $dd_X875FILE ]
   then
      cp $dd_X875FILE $EBSCARD/$ORIGIN.875.$rundate
      JDESC="Load X875 output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=$ORIGIN.875.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file $dd_X875FILE to VTRAN"
      display_message nobanner
   else
      JDESC="File $dd_X875FILE is empty"
      display_message nobanner
   fi

   if [ -s $dd_X877FILE ]
   then
      cp $dd_X877FILE $EBSCARD/$ORIGIN.877.$rundate
      JDESC="Load X877 output file to OMNI Vtran System"
      display_message nobanner
      PNAME=JOBVTUT
      JPRM1=$JUSER
      JPRM2=$ORIGIN.877.$rundate
      execute_job
      JDESC="Completed $PMSG - Load file $dd_X877FILE to VTRAN"
      display_message nobanner
   else
      JDESC="File $dd_X877FILE is empty"
      display_message nobanner
   fi

   unset PMSG
}
function archive_files
{
   for label in $archList; do
       export archDir=$XJOB
       mv $label ${label}.$rundate.$tmptime
       archive ${label}.$rundate.$tmptime
       JDESC="$archName Completed"
       display_message nobanner
   done
}

function move_files
{
   cd $EBSCNV || { JDESC="Could not cd to EBSCNV CIFS directory"; display_message nobanner; exit 99; }
   mv serviceo.$today.txt $XJOB
   mv compenso.$today.txt $XJOB
   cd $XJOB
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JPARM=`echo $1|tr "[:lower:]" "[:upper:]"`
export JNAME=${prog%.ksh}
export JDESC="$JPARM - OMNIPLUS SERVICEO AND COMPENSO PROCESSING MODULE"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides
. JOBDEFINE

###########################
# Move to the JOB directory
###########################
cd $XJOB

###########################
# Define files
###########################
export dd_SERVICEO=$XJOB/serviceo.$today.txt
export dd_COMPENSO=$XJOB/compenso.$today.txt
export dd_TROT=$XJOB/X813FILE.$rundate
export dd_X875FILE=$XJOB/X875FILE.$rundate
export dd_X877FILE=$XJOB/X877FILE.$rundate

### **********************************************************************
### TASK 1 - Run COBOL program to create SERVICEO and COMPENSO files
###          from a database
### **********************************************************************
$create_files && create_files

### **********************************************************************
### TASK 2 - Move the files to XJOB if EXACCT process
### **********************************************************************
$move_files && move_files

### **********************************************************************
### TASK 3 - Run COBOL program to create T813 and T875 cards
###          from the SERVICEO and COMPENSO files
### **********************************************************************
$create_X_cards && create_X_cards

### ******************************************************************
### TASK 4 - Load transactions to VTRAN
### ******************************************************************
$load_trans2VTRAN && load_trans2VTRAN

### DELETE THE FILES IN EBSCARD TO MAKE SURE THEY'RE NOT RELOADED
### rm -f $EBSCARD/$origin.*.$rundate

### ******************************************************************
### TASK 5 -- archive input files
### ******************************************************************
$archive_files && archive_files

### ******************************************************************
### TASK 6 - Standard EOJ housekeeping tasks
### ******************************************************************
eoj_housekeeping
return 0