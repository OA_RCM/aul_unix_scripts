#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Script Name  : hta_disc2ftp.ksh            CALL          *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to copy indiviual Harriss Trust Annual*
#COMMENT *                 Disclosure files from MAAS for            *
#COMMENT *                 converted plans.                          *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - DRH                                 *
#COMMENT *  Created      : 01/16/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : hta_disc2.ksh                             *
#COMMENT *  Script Calls : ftp_getMaas.ksh  ANNUALDISCCOMP.txt         *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Annual/Single plan on demand              *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 1 - 20 minutes depending on how many      *
#COMMENT *                 plans are in control file                 *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Refer to sta800.ksh              R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 04/16/2001  CC: RSS1109   By: Danny Hagans          U
#COMMENT U Reason: Created to run the program HT Annual Disclosure   U
#COMMENT U        Letters.                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
### ******************************************************************
### define job variables
### ******************************************************************
prog=$(basename $0)
  export JUSER=${JUSER:=$LOGNAME}
  export JNAME=${prog%.ksh}
  export JDESC="$JNAME.ksh to create and send annual reports"

### ******************************************************************
### standard job setup procedure
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

### ******************************************************************
### Run JOBCALC to extract the Plans list for Harris Trust letters
### ******************************************************************

   . JOBCALC $LOGNAME EXT-ANNDISCCOMP.txt 01

### ******************************************************************
### define job variables
### ******************************************************************
  export ENVFILE=ENVBATCH
  export XJOBHTANN=$EBSINPUT/htannual
  cd $XJOB
  rptlistX=/tmp/annrptX$$
  grep 'AUTO' $XJOB/annctl.txt >$rptlistX
  if [[ -s $rptlistX ]]
  then
        mv $XJOBHTANN/r* $XJOB
        mv $XJOBHTANN/l* $XJOB
        mv $XJOBHTANN/e* $XJOB
  else
    export INPUTSAV=$XJOB
    export INPUTSAV=$EBSINPUT
    export EBSINPUT=$XJOB
    cat $XJOB/ANNUALDISCCOMP.txt | \
     while read fname
     do
        Fcase=`echo $fname | cut -c1-7`
        if [[ $Fcase > '0' ]]
        then
           Ftemp=l"$Fcase";
           export Filename1="$Ftemp"
           ftp_getMaas.ksh $Filename1
           export Filename1=r"$Fcase"
           ftp_getMaas.ksh $Filename1
           export Filename1=e"$Fcase"
           ftp_getMaas.ksh $Filename1
        fi
     done

# ************************************************
# Companion Plans
# ************************************************
 grep 'Contract #' * > hta-contracts.txt
 cat hta-contracts.txt | \
     while read fname
      do
          Fcase1=`echo $fname | cut -c3-7`
          Fcase2=`echo $fname | cut -c2-7`
          Fendchr=`echo $fname | cut -c8`
          Fcase3=`echo $fname  | cut -c24-28`
          if [[ $Fcase1 = $Fcase3 ]]
          then
             export JDESC="$Fcase2 not a companion case"
#             display_message nobanner
          else
             export JDESC="G$Fcase1 companion case G$Fcase3"
             display_message nobanner
             Ffromfile=l"$Fcase2$Fendchr";
             Ftofile=lG"$Fcase3"A;
             cp $Ffromfile $Ftofile
             Ffromfile=r"$Fcase2$Fendchr";
             Ftofile=rG"$Fcase3"A;
             cp $Ffromfile $Ftofile
             Ffromfile=e"$Fcase2$Fendchr";
             Ftofile=eG"$Fcase3"A;
             cp $Ffromfile $Ftofile
          fi
       done
     export JDESC="rename companion cases finished"
     display_message nobanner
  fi

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  return
print "$SPROG Ending at " $tmptime >> $XJOB/$SLOG
