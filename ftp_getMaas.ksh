#!/usr/bin/ksh
function send_error
{
  [[ -s badmsg ]] && cat badmsg|mailx -s "FTP Errors" $ADDRESSEE
}
GETFILE=$1
rm -f goodmsg badmsg
[[ -z $GETFILE ]] && { print "No GETFILE specified" > badmsg; send_error; }
[[ -z $GETFILE ]] && exit 99

. FUNCTIONSFILE
export prog=$(basename $0)
export JNAME=${prog%.ksh}
set_generic_variables

##  Check location of process and look in proper MAAS ftp site for file
case $loc in
"omnihop")
    boxName=`grep host $CFGINH|cut -f2 -d " "|tr "[:lower:]" "[:upper:]"`
    print "FTP FETCH FOR $GETFILE FROM MAAS BOX $boxName" >> $LOGFILE
    $NCFTPGET -a -d $LOGFILE -f $CFGINH -E $EBSINPUT $GETFILE 1>>goodmsg 2>>badmsg
    cond=$?
    [[ $cond = 0 ]] && msg="FTP of $GETFILE from HP/3000 $boxName successful"
    [[ $cond != 0 ]] && msg="ERROR: Could not FTP $GETFILE from HP/3000 $boxName"
    ;;
"omniprod")
    boxName=`grep host $CFGINC|cut -f2 -d " "|tr "[:lower:]" "[:upper:]"`
    print "FTP FETCH FOR $GETFILE FROM MAAS BOX $boxName" >> $LOGFILE
    $NCFTPGET -a -d $LOGFILE -f $CFGINC -E $EBSINPUT $GETFILE 1>>goodmsg 2>>badmsg
    cond=$?
    if [[ $cond = 0 ]]
    then
      msg="FTP of $GETFILE from HP/3000 $boxName successful"
    else
      boxName=`grep host $CFGINB|cut -f2 -d " "|tr "[:lower:]" "[:upper:]"`
      print "FTP FETCH FOR $GETFILE FROM MAAS BOX $boxName" >> $LOGFILE
      $NCFTPGET -a -d $LOGFILE -f $CFGINB -E $EBSINPUT $GETFILE 1>>goodmsg 2>>badmsg
      cond=$?
      [[ $cond = 0 ]] && msg="FTP of $GETFILE from HP/3000 $boxName successful"
      [[ $cond != 0 ]] && msg="ERROR: Could not FTP $GETFILE from HP/3000 $boxName"
      [[ $cond != 0 ]] && print $msg >> $LOGFILE
      [[ $cond != 0 ]] && msg="HP3000 Servers may be down or file is not available"
    fi
    ;;
   *)
      boxName=`grep host $CFGINT|cut -f2 -d " "|tr "[:lower:]" "[:upper:]"`
      print "FTP FETCH FOR $GETFILE FROM MAAS BOX $boxName" >> $LOGFILE
      $NCFTPGET -a -d $LOGFILE -f $CFGINT -E $EBSINPUT $GETFILE 1>>goodmsg 2>>badmsg
      cond=$?
      [[ $cond = 0 ]] && msg="FTP of $GETFILE from HP/3000 $boxName successful"
      [[ $cond != 0 ]] && msg="ERROR: Could not FTP $GETFILE from HP/3000 $boxName"
    ;;
esac

##  Write last message and return
print $msg >> $LOGFILE
return $cond
