#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXINF722              CALL            *
#COMMENT *  SchedXref Name : rsuxinf722.ksh          OMNI-ASU        *
#COMMENT *  UNIX Script    : inf_fundproxy.ksh                       *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce a file to be used   *
#COMMENT *               : by MIS for proxy mailings.                *
#COMMENT *               :                                           *
#COMMENT *               : EXT1000E - an investment vehicle code and *
#COMMENT *               :  a proxy date are used to interrogate each*
#COMMENT *               :  plan to create a file of proxy info which*
#COMMENT *               :  is used by RPT1006R.                     *
#COMMENT *               :                                           *
#COMMENT *               : RPT1006R - formats selected data from the *
#COMMENT *               :  file created by EXT1000E to the current  *
#COMMENT *               :  MIS specifications. The same input file  *
#COMMENT *               :  used for EXT1000E is required to create  *
#COMMENT *               :  a separate file for each plan selected.  *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 10/13/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : EXT1000E  RPT1006R                        *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On Demand                                 *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 3 min                                     *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by ASU         R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 10/13/2001  SMR: ******** By: Mike Lewis            U
#COMMENT U Reason: Created to run the program EXTPROXY               U
#COMMENT U Date: 10/22/2001  SMR:          By: Tony Ledford          U
#COMMENT U Reason: Changed name of proxy input file and added        U
#COMMENT U         output of COBRUN to log file for analysis.        U
#COMMENT U Date: 03/07/2002  SMR           By: MID-RW                U
#COMMENT U Reason: Change EXTPROXY to EXT1000E. Change 'proxyin.txt' U
#COMMENT U   to 'parmsin.txt. Add job RPT1006R to stream.            U
#COMMENT U Date: 09/12/2002  CC1200        By: Mike Lewis            U
#COMMENT U Reason: Changes to EXT1000E, RPT1006R, and parmsin.txt    U
#COMMENT U         for various specifications and formatting.        U
#COMMENT U Date: 03/12/2004  SMR: CC5940   By: Mike Lewis            U
#COMMENT U Reason: Mods to LAN dirs forced C:D destination change.   U
#COMMENT U Date: 03/15/2004  SMR: CC5940   By: Mike Lewis            U
#COMMENT U Reason: More Mods to LAN directories for C:D.             U
#COMMENT U Date: 07/21/2006  CC: 10344  By: Rick Sica                U
#COMMENT U Reason: HOP                                               U
#COMMENT U Date: 10/04/2006  CC: 10822  By: Mike Lewis               U
#COMMENT U Reason: Production Problem - added prog= statement.       U
#COMMENT U Date: 07/07/2010  WMS 4519   By: Louis Blanchette         U
#COMMENT U Reason: Use of CIFS instead of CDFT                       U
#COMMENT U         Use $AULDATA/INVFUNDPLANX to get the plan list    U
#COMMENT U Date: 9/18/2013  WMS 8043   By: Louis Blanchette          U
#COMMENT U Reason: Use of Investment Short name instead of Inv. Code U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function wizard_gather
{
	PMSG="function wizard_gather"
  export WIZARDDIR=$EBSINPUT/$inputdir
  rm parmsin.txt
  touch parmsin.txt
  rm parmslist  
  ls $WIZARDDIR | grep '^prxy' >> parmslist
  if [ -s parmslist ] ; then
     cat parmslist | while read fname ; do
     cat $WIZARDDIR/$fname >> parmsin.txt
     $remove_wz_files &&  rm $WIZARDDIR/$fname
     done
     JDESC="Completed $PMSG - proxy request(s)"
     display_message nobanner
  else
     JDESC="Completed $PMSG - No Batch Wizard proxy request found"
     display_message nobanner
     # There's no need to run the next steps.
     export runextprog=false
     export runprxyprog=false
     export send_to_LAN=false
  fi
}
	
function run_ext_prog
{
  PMSG="function run_ext_prog"
  export dd_INVESTIDX=$AULDATA/INVFUNDPLANX
  export dd_SRTWORK=$XJOB/SRTWORK
  export dd_WORKFILE=$XJOB/WORKFILE.TXT
  export dd_WORKFILE2=$XJOB/WORKFILE2.TXT
  export dd_XXPROXYO=$XJOB/ALLPROXYO.TXT 
  export dd_EXCEPTIONS=$XJOB/PXYEXCEPTIONS.$today.TXT
  export dd_SRTPROXYO=$XJOB/SRTPROXYO
  PNAME=EXT1000E
  JDESC="Run of COBOL Program $PNAME; Writing Extract file(s) ALLPROXYO.TXT"
  execute_program
  
  JDESC="Completed $PMSG"
  display_message nobanner
  unset PMSG
}
function split_proxyo
# WMS8043 change from 2 to 4 chars
{
# First create empty files
cut -c1-4 parmsin.txt | sort -u | while read pfx ; do
   touch ${pfx}PROXYO
done

# Split the responses by investmment
/usr/local/bin/perl -n -e '
BEGIN {
  $oldpfx = "";
  $outfh = 0;
}
$pfx = substr($_,0 ,4);
if ($pfx ne $oldpfx) {
   close($outfh) if ($outfh);
   $outfile = $pfx . "PROXYO";
   $oldpfx = $pfx;
   open($outfh, ">>$outfile") || die "$outfile: $!\n";
}
print $outfh $_;' $1
}

function run_prxy_prog
{
  PMSG="function run_prxy_prog"
  PNAME=RPT1006R
  JDESC="Run of COBOL Program $PNAME; Writing Formatted files xxxxMISPXY"
  execute_program

  JDESC="Completed $PMSG"
  display_message nobanner
  unset PMSG
}

function send_to_LAN
{
	PMSG="function send_to_LAN"

  ls $XJOB | grep 'MISPXY$' >> $XJOB/mislist
  cat $XJOB/mislist | \
  while read mlist; do
      export fromfile=$XJOB/$mlist
      if [ "$EBSRPTPFX" = "HOP." ]; then
         export tofile=${proxydir}/${mlist}.${EBSRPTPFX}txt
      else
         export tofile=${proxydir}/${mlist}.txt
      fi
      ux2dos $fromfile > $tofile
  done
  ux2dos $dd_EXCEPTIONS > ${proxydir}/PXYEXCEPTIONS.$today.TXT
  JDESC="Completed $PMSG"
  display_message nobanner
  unset PMSG
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX FUND PROXIES"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

###########################
# Move to the JOB directory
###########################
cd $XJOB

### ******************************************************************
### TASK 1 - Gather user requests
### ******************************************************************
$wizardgather && wizard_gather

### ******************************************************************
### TASK 2 - Run the COBOL program to produce extract file
### ******************************************************************
export dd_RSTROUT=$XJOB/rs.tmp
export dd_PARMSIN=$XJOB/parmsin.txt;
$runextprog & run_ext_prog

### ******************************************************************
### TASK 3 - Split the ALLPROXYO.TXT into PROXYO for each fund.
### ******************************************************************
$runextprog & split_proxyo $dd_XXPROXYO

### ******************************************************************
### TASK 4 - Run the COBOL program to produce proxy files
### ******************************************************************
$runprxyprog && run_prxy_prog

### ******************************************************************
### TASK 5 - Send file to LAN (Copy to CIFS)
### ******************************************************************
$send_to_LAN && send_to_LAN

### ******************************************************************
### TASK 6 - clean up and say goodbye
### ******************************************************************
JDESC="Process complete !"
display_message nobanner
eoj_housekeeping
return 0
