#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Script Name       : ext_premiertrust.ksh     NON-CALL -- OMNI ASU  *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run EXT-PT-POSITION.txt                        *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.50                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 02/01/2011                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         : rsuxext714.ksh (sched_xref)                    *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Daily                                          *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 15 min.                                        *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Create a help ticket.  May need to delete   R
#COMMENT R                         OMNI transactions.                          R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Glen McPherson   WMS4788   01/01/11   original                     U
#COMMENT U  Glen McPherson   WMS5456   03/09/11   WT and Matrix                U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# functions

function run_pt_ext
{
  export JDESC="Running EXT-PT-POSITION calculator..."
  display_message
  export PNAME=JOBCALC
  export JPRM1=$JUSER
  export JPRM2=EXT-PT-POSITION.txt
  execute_job
}

function copy_pt_ext
{
     typeset -L100 frec; cat $XJOB/PTPOS | while read frec; do
           print "$frec" >> $XJOB/POSNFILE || export ekerror="Y"
         done

     if [[ $OMNIsysID = "prod" ]]; then
        cp $XJOB/POSNFILE $EBSXFER/premiertrust/out/DTSFTP.C4746.S$dtccprod
     else
        cp $XJOB/POSNFILE $EBSXFER/premiertrust/out/DTSUTF.C4746.S$dtccprod
     fi
     cp $XJOB/PTPRICE $EBSXFER/premiertrust/out/PTPRICE$rundate.txt
}

# main

export JNAME=ext_premiertrust
export JUSER=$LOGNAME

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

export rundate=`date +%Y%m%d`

# Task 1 - do Jobcalc here
$run_pt_ext && run_pt_ext

# Task 2 - move files
$copy_pt_ext && copy_pt_ext

# Task 3 - standard exit function
eoj_housekeeping
