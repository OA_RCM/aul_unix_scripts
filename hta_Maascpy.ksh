#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Script Name  : hta_Maascpy.ksh          CALL             *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Connect Direct to copy annual disclosure  *
#COMMENT *                 file to Unix from Maas.                   *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $EBSPROC                                  *
#COMMENT *  Author       : AUL - DRH                                 *
#COMMENT *  Created      : 01/25/2005                                *
#COMMENT *  Environment  :                                           *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : ftp_getMaas.ksh hta_rename.ksh            *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On Request                                *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                   R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date:             SMR: *******  By:                       U
#COMMENT U Reason:                                                  )U
#COMMENT U                                                           U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
## ******************************************************************
### define job variables
### ******************************************************************
prog=$(basename $0)
  export JUSER=${JUSER:=$LOGNAME}
  export JNAME=${prog%.ksh}
  export JDESC="$JNAME.ksh to create and send annual reports"

### ******************************************************************
### standard job setup procedure
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

### ******************************************************************
### set up the environment
### ******************************************************************
  export AULDATA=$AULDATA
  cd $XJOB
### ******************************************************************
### Run JOBCALC to extract the GIC interest table data
### ******************************************************************

   . JOBCALC $LOGNAME EXT-ANNDISCPLAN.txt 01

### ******************************************************************
### create directory and copy files from HP3000
### ******************************************************************
  export ENVFILE=ENVBATCH
  export INPUTSAV=$EBSINPUT
  export EBSINPUT=$EBSINPUT/htannual
  cat $XJOB/ANNUALDISCPLANS.txt | \
  while read fname
  do
     Fcase=`echo $fname | cut -c1-7`
     if [[ $Fcase > '0' ]]
     then
        Ftemp=l"$Fcase";
        export Filename1="$Ftemp"
        ftp_getMaas.ksh $Filename1
        export Filename1=r"$Fcase"
        ftp_getMaas.ksh $Filename1
        export Filename1=e"$Fcase"
        ftp_getMaas.ksh $Filename1
      fi
  done
  export EBSINPUT=$INPUTSAV
### ******************************************************************
### Search for Companion Cases and rename files
### ##################################################################
export JDESC="rename companion cases starting"
hta_rename.ksh
export JDESC="rename companion cases finished"
display_message nobanner
### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  return
