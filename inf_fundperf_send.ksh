#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : inf_fundperf_send.ksh       CALL          *
#COMMENT *                                                           *
#COMMENT *                                             ASU           *
#COMMENT *                                                           *
#COMMENT *  Description  : This job send the Fund Performance        *
#COMMENT *                 files needed for Statements, Web, AULFORM *
#COMMENT *                 NewKirk, and Prospectus'                  *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 02/23/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : inf_fundperf.ksh                          *
#COMMENT *  Script Calls : sendfile.ksh, ftp_put.ksh                 *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 2-3 min                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S  If run manually as a stand-alone, must give it a first   S
#COMMENT S  parameter of the original XJOB inf_fundperf.ksh created  S
#COMMENT S                                                           S
#COMMENT S  These tasks are done here in case inf_fundperf.ksh fails.S
#COMMENT S  Can run this to complete tasks if log errors deemed okay S
#COMMENT S  to continue. If not deemed okay, should be resolved and  S
#COMMENT S  inf_fundperf.ksh should be re-run instead. This script   S
#COMMENT S  will not be run if missing price errors in error log.    S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time.      R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 12/02/2003  CC: 5851      By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 05/07/2004  CC: 6054      By: Rick Sica             U
#COMMENT U Reason: Send Extract to MAAS                              U
#COMMENT U Date: 12/13/2004  CC: 7469      By: Mark Slinger          U
#COMMENT U Reason: Send FUNDPERF.TXT to PC4682 instead of PC4603.    U
#COMMENT U         Make use ini file instead of hardcoding.          U
#COMMENT U Date: 06/30/2006  CC: 10414     By: Rick Sica             U
#COMMENT U Reason: No longer need to send PRTFLEO to MAAS.           U
#COMMENT U Date: 07/23/2007  CC: 11728     By: Rick Sica             U
#COMMENT U Reason: Put Fund Perf file in $EBSXFER Bowne area         U
#COMMENT U Date: 06/30/2009 WMS: 02451     By: Louis Blanchette      U
#COMMENT U Reason: Put Fund Perf file in Informatica cifs area       U
#COMMENT U Date: 20110526   WMS: 2998      By: Tony Ledford          U
#COMMENT U Reason: Eliminate connect direct usage.                   U
#COMMENT U Date: 20111005   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT U Date: 20120424   Proj:  WMS5502        By: Rick Sica      U
#COMMENT U Annualized perf by AUL INCEPT date                        U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 2 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
clear
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX FUND PERFORMANCE SEND"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
######################
### setup generic variables
### to initial values
######################
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

export XJOB=$1
export rundate=$2
######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Move index file to area accessed by participant statements
### ******************************************************************
export dd_PRTRPTO=$XJOB/${EBSRPTPFX}FUNDPERFRPT
export dd_ERRLOGO=$XJOB/FUNDPERFLOG
export dd_PRTFLEO=$XJOB/PRTFLEO
export dd_PRTFLIO=$XJOB/PRTFLIO
export dd_PRTFLPO=$XJOB/PRTFLPO
export FUNDPERF=$XJOB/$fundperf_file

if [[ ! -s $dd_PRTFLEO ]] ; then
  export JDESC="Script Ending due to empty FundPerf extract file at $tmptime"
  display_message nobanner
  return 99
fi

mv $dd_PRTFLIO $AULDATA
cat $dd_PRTFLEO > $FUNDPERF
if [[ -s $dd_PRTFLPO ]] ; then
   cat $dd_PRTFLPO >> $FUNDPERF
fi

### ******************************************************************
### TASK 2 - Send fund performance file to account services & LAN
### ******************************************************************
if $send_to_AS || $send_extracts_to_LAN ; then 
  fnc_ftpq $FUNDPERF
fi

### ******************************************************************
### TASK 3 - Send fund performance file backup
### ******************************************************************
# This one is for Web
if $send_extracts_to_LAN ; then
# Send backup
  tofile="$XJOB/FUNDPERF.$rundate.TXT"
  cp $FUNDPERF $tofile
  fnc_ftpq $tofile
fi

### ******************************************************************
### TASK 4 - Ready file for Enrollment Kits
### ******************************************************************
if $EK_extracts ; then
  cp $dd_PRTFLEO $EBSXFER/$ekxferdir/FUNDPERF.txt
fi

### ******************************************************************
### TASK 5 - # Copy file for Informatica to pick up
### ******************************************************************
cp $FUNDPERF $INF_path_file/FUNDPERF.TXT

### ******************************************************************
### TASK 6 - Send performance reports to LAN
### ******************************************************************
if $send_reports_to_LAN ; then
  if [[ $OMNIsysID = "prod" ]] ; then
    fromfile=$dd_ERRLOGO
    tofile="$XJOB/FUNDPERFLOG.$rundate.TXT"
    cp $fromfile $tofile
    fnc_ftpq $tofile
    
    fromfile=$dd_PRTRPTO
    tofile="$XJOB/FUNDPERFRPT.$rundate.TXT"
    cp $fromfile $tofile
    fnc_ftpq $tofile
  fi
fi

### ******************************************************************
### TASK 7 - Print performance reports
### ******************************************************************
if $print_reports ; then
  typeset prefix=""
  [[ $OMNIsysID = "dev" ]] && prefix="TEST."
  typeset filename="$XJOB/${prefix}RS.CDFT.RS\$\$2605.D$(date +%m%d).T$(date +%H%M%S)"
  cp $dd_PRTRPTO $filename
  fnc_ftpq $filename
fi

### ******************************************************************
### TASK 8 - Send reports to a document manager
### ******************************************************************

$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK END - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
