#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXCOM712                CALL            *
#COMMENT *  Schedule Name: rsuxcom712.ksh            OMNI-ASU        *
#COMMENT *  Script Name  : com_todss3000.ksh                         *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to create ABC feed to the DSS         *
#COMMENT *                 commission system.                        *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 08/31/2001                                *
#COMMENT *  Environment  : ENVINTF                                   *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : setpath omniprof MKOUTDIR JOBDEFINE       *
#COMMENT *  COBOL Calls  : COM3000E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  varies                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R  *** If restored from archive directory for rerunning,    R
#COMMENT R      file must be re-archived when finished.              R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task Sequence:                                           T
#COMMENT T  TASK 1 - Retrieve ABC files delivered from DSS via CDFT  T
#COMMENT T  TASK 2 - Prepare to process input files                  T
#COMMENT T  TASK 3 - Run the COBOL program to extract ABC data       T
#COMMENT T  TASK 4 - Append comm interface files to a common file    T
#COMMENT T  TASK 5 - clean up and say goodbye                        T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 08/31/2001  SMR: PEN3279  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program COM3000E               U
#COMMENT U                                                           U
#COMMENT U Date: 03/02/2004  CC: 5882     By: Mike Lewis             U
#COMMENT U Reason: Check for .done file in loop before aborting.     U
#COMMENT U                                                           U
#COMMENT U Date: 05/12/2005  CC: 8191     By: Mike Lewis             U
#COMMENT U Reason: Changed delivery location for pickup by CDFT5037; U
#COMMENT U         functionalize copy of extracts to POSTOUT.        U
#COMMENT U                                                           U
#COMMENT U Date: 07/31/2006  SMR: CC5403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 05/01/2008  SMR: CC12755  By: Mike Lewis            U
#COMMENT U Reason: Implement Parallel Processing of input file       U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function retrieve_ABC_file
{
 JDESC="Retrieving ABC files from DSS..."
 display_message

 export time_limit=600
 export delay_time=60

 if [[ $OMNIsysID = prod ]]
 then
    fromibm.cd RS.VCS.ABCEXT\(0\) $XJOB/ABCRCIN
 else
    fromibm.cd TEST.RS.VCS.ABCEXT\(0\) $XJOB/ABCRCIN
 fi

 while [[ ! -s $XJOB/ABCRCIN.done ]] ; do
   sleep 1
   ((time_limit = time_limit -1))
   [ $time_limit = 0 ] && { print "$XJOB/ABCRCIN did not arrive"; break; }
 done


 if [ ! -s $XJOB/ABCRCIN ]
 then
   JDESC="$XJOB/ABCRCIN file error "
   display_message
   return 99
 fi

}
function process_input
{
  JDESC="Preparing to create split input"
  display_message nobanner
  
  fnc_determine_group_sizes    ## calculate number of records per input file
  oldIFS="$IFS"
  IFS=""
  case "$split_type" in
    "row") fnc_split_plans_by_row ;;
    "column") fnc_split_plans_by_column  ;;
    "*")  JDESC="Unknown split type [$split_type]"; abend ;;
  esac
  
  IFS="$oldIFS"
  
  JDESC="Split input files created Ok!"
  display_message nobanner
  
}
#######################################################
## local function called by the generic functions
## fnc_split_plans_by_row or fnc_split_plans_by_column
## to populate split workfiles
#######################################################
function generate_card
{
  plan=$1
  echo $plan
}
function run_program
{
  JDESC="Running Cobol program on each split input file in background"
  display_message nobanner
  
  export integer segment=1
  while (( $segment <= $NUMGROUPS ))
  do
    SplitInput="${split_name}${segment}.${rundate}"
    if [[ -s $XJOB/$SplitInput ]]
    then
       run_splitinput $SplitInput &
       sleep $split_delay
    else
       JDESC="Invalid input [$SplitInput]"
       abend
    fi
    ((segment+=1))
  done

  wait  ## for all background run_splitinput calls to be completed

  ####-----------------------------------------
  ### check if all processes wrote "OK" flag file
  ####-----------------------------------------
  segment=1
  while (( $segment <= $NUMGROUPS ))
  do
    SplitInput="${split_name}${segment}.${rundate}"
    [[ ! -f $XJOB/$SplitInput.OK ]] && JDESC="Problem running $SplitInput"
    [[ ! -f $XJOB/$SplitInput.OK ]] && display_message                    
    [[ ! -f $XJOB/$SplitInput.OK ]] && abend
    ((segment+=1))
  done
}
function run_splitinput
{
  cp $EBSCTRL/DUMBTRAN.DAT $XJOB/DUMBTRAN${segment}.DAT
  cp $EBSCTRL/DUMBTRN2.DAT $XJOB/DUMBTRN2${segment}.DAT
  touch $XJOB/DUMMY3${segment}.DEF
  export dd_RSTROUT=$XJOB/rs${segment}.tmp
  export dd_TRANIN=$XJOB/DUMBTRAN${segment}.DAT
  export dd_TRANOUT=$XJOB/DUMBTRN2${segment}.DAT
  export dd_SEQAHST=$XJOB/SEQAHST
  export dd_ABCRCIN=$XJOB/ABCRCIN${segment}.${rundate}
  export dd_ABCRCOT=$XJOB/ABCRCOT$segment
  export Mid=$segment
  
  PNAME=COM3000E
  JDESC="Run COBOL program $PNAME using $dd_ABCRCIN"
  execute_program

  JDESC="Extract file $dd_ABCRCOT written"
  display_message nobanner

  touch $XJOB/$SplitInput.OK

}
function send_and_copy
{
  for file in ABCRCOT*
  do
    cat $file >> $EBSXFER/comm/COMMFILE
  done
    
  $copy_to_postout && copy_to_postout
}
function copy_to_postout
{
  cp $XJOB/ABCRCIN* $AULOUT
  cp $XJOB/ABCRCOT* $AULOUT
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
  prog=$(basename $0)
  export integer NumParms=$#
  export JUSER=${JUSER:-$LOGNAME}
  export JGROUP=${JGROUP:-01}
  export JNAME=${prog%.ksh}
  export JDESC="OMNIPLUS/UX ABC FEED TO DSS COMMISSION SYSTEM"
  export ENVFILE=ENVINTF
  export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories
export MKOUTDIR=current

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_daily_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
  . JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Retrieve ABC files delivered from DSS via CDFT
### ******************************************************************

$retrieve_ABC_file && retrieve_ABC_file

if [[ -s $EBSINPUT/comm/ABCRCIN ]]; then
   case "$OMNIsysID" in
     "dev") cp $EBSINPUT/comm/ABCRCIN $XJOB ;;
     "prod") mv $EBSINPUT/comm/ABCRCIN $XJOB ;;
   esac
else
   JDESC="$XJOB/ABCRCIN file not found  - touching file"
   display_message
   touch $XJOB/ABCRCIN
fi

### ******************************************************************
### TASK 2 - Prepare to process input files
### ******************************************************************

$process_input && process_input

### ******************************************************************
### TASK 3 - Run the COBOL program to extract ABC data
###          creating extract files.
### ******************************************************************

$run_program && run_program

### ******************************************************************
### TASK 4 - Append commission interface files to a common file
### This common file will be sent to the IBM via connect direct
### ******************************************************************

$send_and_copy && send_and_copy

### ******************************************************************
### TASK 5 - clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  return 0
