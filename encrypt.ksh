#!/bin/ksh

function usage
{
    echo usage: encrypt.ksh DSN password 1>&2
    exit 1
}

dsn="$1"
password="$2"

if [ -z "$dsn" ] || [ -z "$password" ]; then
    usage
fi

if [ -z "$ODBCINI" ]; then
    echo ODBCINI is not defined 1>&2
    usage
fi

padcrypt.pl -key $dsn -txt "$password" encrypt

exit $?

