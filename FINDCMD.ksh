#!/bin/ksh

if [[ "$LOGNAME" != "omnitest" ]];
then
  print "Sorry, omnitest is the only authorized user of this script"
  return 99
fi

cd /opt/ECS6/home/ctmagnto/ctm/sysout

grep -l "$1" CMD*
                     
return 0
