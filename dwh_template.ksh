#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *  CNTL-M Job Name   : RSUXDWH003              NON-CALL               *
#COMMENT *  UNIX Pointer Name : rsuxdwh003.ksh          OMNI-ASU               *
#COMMENT *  UNIX Script Name  : dwh_template.ksh                               *
#COMMENT *                                                                     *
#COMMENT *  Runtime syntax    : dwh_template.ksh X                             *
#COMMENT *  Description       : Run DWH-TEMPLATE against specified planlist    *
#COMMENT *                      where X=number of plan list splits.            *
#COMMENT *                                                                     *
#COMMENT *  Author            : AUL - Steve Loper                              *
#COMMENT *  Created           : 12/15/2011                                     *
#COMMENT *  Called by         : Scheduled or Manual                            *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       : None                                           *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : daily                                          *
#COMMENT *  Est. Run Time     : Depends on number of plans in single segment.  *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S  None                                                               S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Should be looked at by ASU support person prior to resbumission.   R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT T  Task Sequence -----------------                                    T
#COMMENT T  1 - Run Omniscript program                                         T
#COMMENT T  2 - Standard end-of-job processes                                  T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Steve Loper      WMS5501   12/15/2011  original                    U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   03/06/2012                              U
#COMMENT U  Reason: add functionality to process ATTR* files                   U
#COMMENT U  Reason: add functionality to split HOP and Prod recs; for testing, U
#COMMENT U          manipulate EBSFSET to send HOP file to test hop cifs.      U
#COMMENT U          Mod to use DTYPE, NSEGS, EXTFILE. Use fnc_log and fnc_exit U
#COMMENT U          functions to standardize messaging. Fix planlist location. U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   04/02/2012                              U
#COMMENT U  Reason: Fix error with fnc_die: change to fnc_exit.                U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
################################################################################
#### FUNCTIONS ####### functions section ##################### SECTION #########
################################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 1 ]] && mno=2
  check_mno
  touch $XJOB/Input_parm_is_${group}
  fnc_log "Input_parm_is_${group}"
  [[ $group = ALL ]] && group=1
  echo $chkSeqNum | { grep -q $group || mno=3; }
  check_mno
  ls -1 $LISTDIR | grep "${split_name}_${group}_" >$planlist
  ### if the planlist file is not populated, there is nothing to do - so quit.
  [[ -s $planlist ]] || fnc_exit "No plan list files found; Nothing to process..."
}
function check_overrides
{
   export EBSDATA=$EBSDATAovrd
   fnc_log "Derive data from: $EBSDATA"
}
function run_all_option
{
  [[ $extract_data != true ]] && { fnc_log "Skipped data extraction."; return 0; }
  fnc_log "Running ALL option"
  export grpNo=1 segNo=1
  export PLANLISTFILE=$LISTDIR/${split_name}_${grpNo}_${segNo}
  export DWHOUT=$XJOB/${tem_name_out}_contributor_${segNo}.tsv
  export sysoutfile=$XJOB/jobcalc.${grpNo}_${segNo}.out
  fnc_log "Running $OSname calculator for ALL plans"
  run_jobcalc > $sysoutfile 2>&1
}
function run_split_option
{
  [[ $extract_data != true ]] && { fnc_log "Skipped data extraction."; return 0; }
  fnc_log "Running split option: $group segments"
  cat $planlist |&
  export grpNo=$group
  while read -p fname; do
    export segNo=$(echo $fname|awk -F "_" '{print $5}')
    export PLANLISTFILE=$LISTDIR/$fname
    export DWHOUT=$XJOB/${tem_name_out}_contributor_${segNo}.tsv
    export sysoutfile=$XJOB/jobcalc.${grpNo}_${segNo}.out
    fnc_log "Running $OSname for plan list $fname"
    run_jobcalc > $sysoutfile 2>&1 &
    sleep 1
  done
  fnc_log "Waiting for completion of all submitted segments..."
  wait  ## for segment jobs to complete
  fnc_log "All segments completed."
  check_exit_status_ofSegments
}
function run_jobcalc
{
   typeset PMSG="In function: $0"
   fnc_log "Running $OSname with $PLANLISTFILE"

   export JNAME_DESC="${grpNo}_${segNo}"
   export PNAME=JOBCALC
   export JDESC="Extract data via $OSname"
   export JPRM1=$JUSER
   export JPRM2=$OSname
   export MKOUTDIR=child
   execute_job && touch $XJOB/${JNAME}_${JNAME_DESC}.ok

   [[ -f $XJOB/${JNAME}_${JNAME_DESC}.ok ]] && compStat="OK" || compStat="ERROR"
   fnc_log "Execution of $OSname for $PLANLISTFILE completed: $compStat"
   unset PMSG JNAME_DESC
}
function check_exit_status_ofSegments
{
  typeset -i segNo=1
  typeset cond=0
  set -A segErr
  until [ $segNo -gt $group ]; do
     [[ -f $XJOB/${JNAME}_${group}_${segNo}.ok ]] || cond=1
     [[ -f $XJOB/${JNAME}_${group}_${segNo}.ok ]] || segErr[${#segErr[*]}]=$segNo
     ((segNo+=1))
  done
  [[ $cond != 0 ]] && fnc_exit "ERROR: The following segments did not end Ok! [${segErr[@]}]"
  fnc_log "All job segments ran Ok"
}
################################################################################
####   MAIN    #######   main  section   ##################### SECTION #########
################################################################################
export integer NumParms=$#
export JNAME=$(basename $0 .ksh)
export JUSER=${JUSER:-$LOGNAME}
export JDESC="ONEAMERICA DWH Template Extraction Process"
export ENVFILE=ENVNONE
export group=$1

# standard script setup
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export planlist=$XJOB/$planlist
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
##############################################
# TASK 1 - Run Omniscript program
##############################################
[[ $group = 1 ]] && run_all_option || run_split_option

##############################################
# Task 2 - build single extract file without nulls
##############################################
$sed_and_consolidate && dwh_sed_and_consolidate $EXTFILE
$sed_and_consolidate || log "filter and consolidate function: OFF"

##############################################
# TASK 3 - copy single extracted datafile to share directory
##############################################
$copy_file && dwh_copy_file $EXTFILE
$copy_file || log "copy EXTFILE file to cifs: OFF"

##############################################
# TASK LAST - Standard end-of-job processes
##############################################
touch rs.tmp
eoj_housekeeping
return 0
