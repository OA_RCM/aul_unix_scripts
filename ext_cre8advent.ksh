#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXADV001                CALL            *
#COMMENT *  UNIX POINTER : rsuxadv001                OMNI-ASU        *
#COMMENT *  UNIX RX NAME : ext_cre8advent.ksh                        *
#COMMENT *                                                           *
#COMMENT *  Description  :                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.80                        *
#COMMENT *  Author       : Sungard - DRH                             *
#COMMENT *  Created      : 08/09/2010                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  : RIA2000E and RIA2001E                     *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  5 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can be resumitted as needed.     R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T                                                           T
#COMMENT T Task 1: call RIA2000E to create ADV Reconciliation and    T
#COMMENT T         Plan/Part files                                   T
#COMMENT T Task 2: Copy Reconciliation and Plan/Part files to Advent T
#COMMENT T         XFER directory                                    T
#COMMENT T Task 3: Create Plan/Part ndexed file for Advent           T
#COMMENT T         transactions file input                           T
#COMMENT T Task 4: Sort Sequential History file                      T
#COMMENT T Task 5: Create Advent transactions file input             T
#COMMENT T Task 6: Copy Advent Transaction file to xfer              T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  tstuid=`echo $JUSER|tr "[:upper:]" "[:lower:]"`
  [[ $tstuid = "help" || $tstuid = "?" ]] && mno=1
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function get_prev_date
{
  cat $AULDATA/DATENIGHT | \
  while read cycdate
  do
     cat $AULDATA/DATEHOLIDAY | cut -c10-45 >> MYHOLIDAY.txt
     findlastbusdate $cycdate
     rm MYHOLIDAY.txt
     JDESC="Previous business date - $prevdate"
      display_message nobanner
  done

}

function get_plan_list
{
  cd $XJOB
  ls /cifs/infa_data/Infa/TgtFiles/RS_Warehouse/Advent_PlanList.txt
  cp /cifs/infa_data/Infa/TgtFiles/RS_Warehouse/Advent_PlanList.txt $XJOB
  if [ 0 != $? ]; then
     export JDESC="Copy from TgtFiles of Advent_PlanList.txt failed "
     abend
  else
     export dd_SORTIN=$XJOB/Advent_PlanList.txt
     export dd_SORTOUT=$XJOB/Plan_List.txt
     print "Plan List Sort Beginning| $dd_SORTIN"
     sort -o $dd_SORTOUT -k .11,.16 $dd_SORTIN
     if [ 0 != $? ]; then
        export JDESC="Sort $dd_SORTIN failed ***"
        abend
     fi
     print "Plan List Sort Ending| $dd_SORTOUT";
     mv $dd_SORTOUT $XJOB/Advent_PlanList.txt
  fi
  cp $XJOB/Advent_PlanList.txt $EBSXFER/advent/out/.
}

function send_part_list
{
  cp $AULDATA/DATENIGHT  /cifs/infa_data/Infa/SrcFiles/RS_Warehouse/.
  if [ 0 != $? ]; then
     export JDESC="Copy to SrcFiles of DATENIGHT failed "
     abend
  fi
  cp $XJOB/Advent_PartList.txt /cifs/infa_data/Infa/SrcFiles/RS_Warehouse/.
  if [ 0 != $? ]; then
     export JDESC="Copy to SrcFiles of Advent_PlanList.txt failed "
     abend
  else
     export JDESC="Copy to SrcFiles of Advent_PlanList.txt completed"
  fi
  ll /cifs/infa_data/Infa/SrcFiles/RS_Warehouse/Advent*
  ll /cifs/infa_data/Infa/SrcFiles/RS_Warehouse/DATENIGHT
}

function findlastbusdate
{
prevdate=$cycdate
    
while true; do
    prevdate=$(calc_date $prevdate -1)
    dow=$(calc_dow $prevdate)
    if [[ "$dow" != "Sun" && "$dow" != "Sat" ]]; then
       if grep -q $prevdate MYHOLIDAY.txt; then
	:
      else
        echo $prevdate
        return
      fi
   fi
done
}

###########################################################################
###########################  main section  ################################
###########################################################################
######################
## setup variables
######################
prog=$(basename $0)
export integer NumParms=$#

export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX ADVENT RECONCILIATION "
export ENVFILE=ENVBATCH;
### ******************************************************************
### standard job set up
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
. JOBDEFINE
fnc_log_standard_start

$check_input && check_input

### ******************************************************************
### Task 1: call RIA2000E to create ADV Reconciliation and Plan/Part files
### ******************************************************************
if $cre8_reco ; then
  cd $XJOB
  $get_prev_date &&  get_prev_date
  $get_plan_list && get_plan_list
  export ADVPLAN=$XJOB/Advent_PlanList.txt
  export ADVPART=$XJOB/Advent_PartList.txt
  export ADVPARTIC=$XJOB/Advent_Part_IC.txt
  export ADVTRANF=$XJOB/Advent_tran_temp.txt
  PARM=$prevdate
  PNAME=RIA2000E 
  JDESC="Run program to extract the Advent Reconiliation $cycdate  $prevdate "
  execute_program
else
  JDESC="Skipped Reconciliation file create process"
   display_message nobanner
fi
### ******************************************************************
### Task 2: Copy Reconciliation and Plan/Part files to Advent XFER directory
### ******************************************************************
if $cpy_lst ; then
   JDESC="Copy files to advent out"
   display_message nobanner
##   cp $XJOB/Advent_PartList.txt $EBSXFER/advent/out/.
   cp $XJOB/*.ps1 $EBSXFER/advent/out/.
   $send_part_list && send_part_list
else
  JDESC="Skipped - Copy files to advent out"
   display_message nobanner
fi
### ******************************************************************
### Task 3: Create Agent/Plan/Part ndexed file for Advent transactions file input 
### ******************************************************************
if $rebld_lst ; then
  JDESC="Create indexed Plan/Part file"
  display_message nobanner
  rebuild $XJOB/Advent_Part_IC.txt, $XJOB/Advent_Part_IC.indx -o:lseq,ind -k:1+31d -r:f91
  cp  $XJOB/Advent_Part_IC.indx  $POSTOUT/Advent_Part_IC.indx
fi
### ******************************************************************
### Task 4: Sort SEQAHST.COMBINE.srt for Advent input
### ******************************************************************
    export dd_SORTIN=$AULOUT/SEQAHST.COMBINE.srt
    export dd_SORTOUT=$XJOB/SEQAHST.COMBINE.srt
        print "Seq Hist Sort Beginning..."
OTSORT $dd_SORTIN $dd_SORTOUT "/S(2,9,CH,A,11,17,CH,A,61,3,CH,A,67,2,CH,A) F(TXT) W($TMPDIR/) CORE(40000)"
        cond=$?
        print "Sequential History Sort Ending";
### ******************************************************************
### Task 5: Create Advent transactions file input
### ******************************************************************
if $cre8_tran ; then
  cd $XJOB
  export SEQAHST_FILE=$dd_SORTOUT
  PNAME=RIA2001E
  JDESC="Run $PNAME to extract the Advent Transaction files"
  execute_program
fi
### ******************************************************************
### Task 6: Copy Advent Transaction file to xfer
### ******************************************************************
   if [[ -s $ADVTRANF ]]; then
      cat $XJOB/*.tr1 >> $ADVTRANF 
      export dd_SORTIN=$ADVTRANF
      export dd_SORTOUT=$XJOB/ADVTRANFS
        print "Sort Beginning..."
OTSORT $dd_SORTIN $dd_SORTOUT "/S(1,33,CH,A,70,12,CH,A) F(TXT) W($TMPDIR/) CORE(40000)"
        cond=$?
        print "Sort Ending";
    mv $dd_SORTOUT $XJOB/*.tr1
   fi
   cp $XJOB/*.tr1 $EBSXFER/advent/out/.

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0

