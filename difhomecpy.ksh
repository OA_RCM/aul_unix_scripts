#!/bin/ksh
function do_diff
{
 diff $HOME/copy/$file1 $diffdir/$file1 > $HOME/tmpfile 2> $HOME/tmperr
 cond=$?
 diff_cond_check
}
function diff_cond_check
{
 if [[ $cond = "1" ]]
 then
   echo "--- Compare home to $toarea ---" > $HOME/TMPFILE
   echo "--------- $file1 ---------" >> $HOME/TMPFILE
   cat $HOME/tmpfile  >> $HOME/TMPFILE
   cat $HOME/TMPFILE
 else
   if [[ $cond = "0" ]]
   then
     echo "--- No Differences in $file1 ---" > $HOME/TMPFILE
     cat $HOME/TMPFILE
   else
     if [[ $cond = "2" ]] && [[ $diffcntr -le 2 ]]
     then
       if [[ $diffcntr = 1 ]]
       then
          diffdir=$EBSRCUCPY
          toarea="release"
       else
          diffdir=$EBSCUSCPY
	  toarea="custom"	
       fi
       ((diffcntr=diffcntr + 1))
       do_diff
     else
       echo "--- Bad Return Condition = $cond ---" > $HOME/TMPFILE
       cat $HOME/TMPFILE
     fi
   fi
 fi
}
 clear
 echo "  "
 echo "\tComparing Copybook from $HOME/copy ... \c"
 echo "  "
 echo "\tCopybook name to compare (no extension needed): \c"
 read file1?"  > "
 file1=${file1}.CPY
 if [ -z "$file1" ]
 then
   echo "Invalid response - script terminating...."
   return
 fi
 diffcntr=1
 diffdir=$EBSTCUCPY
 toarea="test"
 do_diff
 echo
 echo "\tPrint results? (Y/N): \c"
 read prt1
 if [[ $prt1 = "Y" ]]
 then
   echo
   echo "\tEnter printer number 4=ISPRT4 or 6=ISPRT6:  \c"
   read prtr
   if [[ $prtr = "4" ]]
   then
     lp -dISPRT_PS4_2 $HOME/TMPFILE
   fi
   if [[ $prtr = "6" ]]
   then
     lp -disprt6 $HOME/TMPFILE
   fi
 fi
 rm $HOME/TMPFILE
 rm $HOME/tmpfile
 rm $HOME/tmperr
