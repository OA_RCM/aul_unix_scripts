#!/usr/local/bin/perl
# DRH WMS4481v2 Extract 10/29/10
# RPS WMS8043v3 2 to 4  09/12/13
use Getopt::Std;
use Text::ParseWords;
use File::Basename;
use File::Path;
use AUL::Util::Forker;

$| = 1;

%opts = ();

getopts('P:', \%opts);

$maxjobs = $opts{P} || 2;

$forker = AUL::Util::Forker->new(maxjobs => $maxjobs, start_hook => \&jobstart, finish_hook => \&jobend);

$exitstat = 0;
$n = 1;
$auldata = $ENV{AULDATA};
$cobrun = $ENV{COBRUN};
$nlen = length(scalar(@ARGV));
$xjobfmt = "%0${nlen}d";
while (@ARGV)
{
  $seqfile = shift;
  next unless (-s $seqfile);
  if ($forker->start($seqfile))
  {
    ++$n;
    next;
  }
  $bseqfile = basename($seqfile);
  $xjob = $ENV{XJOB} . "/" . sprintf($xjobfmt, $n);
  $ENV{dd_SEQAHST} = "$seqfile";
  $ENV{dd_RPTOUT} = "$xjob/RPTOUT";
  $ENV{dd_RPTOUTE} = "$xjob/RPTOUTE";
  $ENV{dd_RSTROUT} = "$xjob/rs.tmp";
  $ENV{dd_SEQAHSTE} = "$xjob/SEQAHSTE";
  $ENV{dd_TRXOUT} = "$xjob/TRXOUT";
  $ENV{dd_TRANOUT} = "$xjob/tranout";
  $ENV{TEMPFILE} = "$xjob/TEMPFILE";
  $ENV{dd_RIAHSTOUT} = "$xjob/RIAHISTOUT";
  $ENV{dd_DATEBUSEND} = "$auldata/DATEBUSEND";
  $ENV{dd_AGTPLCYIDX} = "$auldata/AGTPLCYIDX";
  $ENV{dd_AGTADDRIDX} = "$auldata/AGTADDRIDX";
  mkpath($xjob);
  $logfile = $ENV{XJOB} . "/AULEXPRT.$n.log";
  $errfile = $ENV{XJOB} . "/AULEXPRT.$n.err";
  open(STDOUT, ">$logfile") || die "$logfile: $!\n";
  open(STDERR, ">$errfile") || die "$errfile: $!\n";
#  exec("$cobrun AULEXPRT \"00000000000000\"");
  exec("$cobrun AULEXPRT 00000000000000");
}
print "all segments submitted - waiting on remaining segments to complete...\n";
$forker->wait_children();
exit($exitstat);

sub jobstart
{
  my $file = shift;
  print "starting segment for $file...\n";
  return 1;
}

sub jobend
{
  my $child = shift;
  my $rc = $child->{exit};
  $rc >>= 8;
  $exitstat = 1 if ($rc > 4);
}

__END__

=head1 NAME

aulexprt_run.pl - 

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 OPTIONS

=over

=back
