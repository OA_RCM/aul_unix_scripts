#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTM Job Name  : RSUXINF716                  CALL         *
#COMMENT *  SchedXref Name: rsuxinf716.ksh           OMNI-ASU        *
#COMMENT *  Unix Script   : inf_AGTfiles.ksh                         *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Retrieve agent flat files from DSS;       *
#COMMENT *                 transfer flat files to ODS;               *
#COMMENT *                 build OMNI Agent Index files.             *
#COMMENT *                                                           *
#COMMENT *  Format       : rsuxinf716.ksh                            *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 11/04/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *                 rsux000csr.ksh                            *
#COMMENT *  COBOL Calls  : BLDAGTFL                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 5 min                                     *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Must check with application       R
#COMMENT R         support personnel before restart                  R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 10/01/2001  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Created to retrieve, build, transfer Agent files. U
#COMMENT U Date: 10/04/2002  SMR: CC3522   By: Mike Lewis            U
#COMMENT U Reason: Clean up of large files after copies completed.   U
#COMMENT U Date: 07/24/2002  CC: 4818     By: Patty Wamsley          U
#COMMENT U Reason: Performance Plus project                          U
#COMMENT U Date: 11/18/2003  CC: 5544     By: Patty Wamsley          U
#COMMENT U Reason: Die if POLICYFLAT or ADDRESSFLAT fail to          U
#COMMENT U         arrive before timeout                             U
#COMMENT U Date: 01/14/2004  CC: 5816     By: Mike Lewis             U
#COMMENT U Reason: Expand the time loop before causing an abort.     U
#COMMENT U Date: 03/02/2004  CC: 5882     By: Mike Lewis             U
#COMMENT U Reason: Check for .done file in loop before aborting.     U
#COMMENT U Date: 12/21/2004  CC: 7596     By: Mike Lewis             U
#COMMENT U Reason: Send special version of AGTPOLCY to CSR.          U
#COMMENT U Date: 06/30/2006  CC: 8735     By: Gary Pieratt           U
#COMMENT U Reason: Include 'NF' role code & standardize (rml).       U
#COMMENT U Date: 10/26/2007  CC: 12259    By: Mark Slinger           U
#COMMENT U Reason: Send ODS files to NT server                       U
#COMMENT U Date: 02/15/2008  CC: 12498    By: Rick Sica              U
#COMMENT U Reason: Agent file process flow change                    U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##functions################### FUNCTIONS #############################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  ## for future use
  return 0
}
######################################################################
##main###################### MAIN SECTION ############################
######################################################################    

### ******************************************************************
### define job variables
### ******************************************************************
  prog=$(basename $0)
  export JUSER=${JUSER:-$LOGNAME}
  export JNAME=${prog%.ksh}
  export JDESC="Retrieve and build Agent Index files from DSS"
  export ENVFILE=ENVBATCH

 ### ******************************************************************
 ### create output directories
 ### ******************************************************************
 . FUNCTIONSFILE
 set_generic_variables
 make_output_directories
 export AULOUT=$POSTOUT
 cd $XJOB

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
  . JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1: retrieve agent files 
### ******************************************************************
export dd_POLICYFLAT=$XJOB/POLICYFLAT
export dd_ADDRESSFLAT=$XJOB/ADDRESSFLAT

if $retrievefiles ; then
  JDESC="retrieving agent files from DSS..."
  display_message nobanner
  
  JDESC="Waiting for ADDRESSFLAT to be retrieved..."
  display_message nobanner
  cp $addressfromfile $dd_ADDRESSFLAT || { JDESC="Error copying $addressfromfile"; abend; }
  
  JDESC="Waiting for POLICYFLAT to be retrieved..."
  display_message nobanner
  cp $policyfromfile $dd_POLICYFLAT || { JDESC="Error copying $policyfromfile"; abend; }
fi

### ******************************************************************
### TASK 2: transfer flat files to CSR System
### ******************************************************************

if $sendtocsr ; then
  if [[ -s $dd_POLICYFLAT ]] && [[ -s $dd_ADDRESSFLAT ]] ; then
    export FROMFIL=$XJOB/AGTPOLCY.Txt
    grep ${csrtyp} $dd_POLICYFLAT | grep 99999999 > $FROMFIL
    grep ${csrtyp2} $dd_POLICYFLAT | grep 99999999 >> $FROMFIL
    export TOFIL=AGTPOLCY.Txt

    export FROMFIL2=$XJOB/AGTADDR.Txt
    cp $dd_ADDRESSFLAT $FROMFIL2
    export TOFIL2=AGTADDR.Txt
    
    PNAME=rsux000csr.ksh
    JDESC="Sending files to CSR System"
    execute_job
  fi
fi

### ******************************************************************
### TASK 3: define files needed by AUL programs
### ******************************************************************
export dd_ERRORFILE=$XJOB/BLDAGTERR
export dd_AGTPLCYIDX=$AULOUT/AGTPLCYIDX
export dd_AGTADDRIDX=$AULOUT/AGTADDRIDX

if $converttoidx ; then
  export dd_RSTROUT=$XJOB/rs.tmp
  
  export JDESC="run COBOL program $CPROG"
  export PNAME=BLDAGTFL
  execute_program
fi

### ******************************************************************
### TASK 4: save files
### ******************************************************************
if $savefiles ; then
  cp $dd_POLICYFLAT $AULOUT
  cp $dd_ADDRESSFLAT $AULOUT
  cp $dd_ERRORFILE $AULOUT

  if [[ -s $dd_ERRORFILE ]] ; then
    JDESC="Errors encountered during index build of Agent files"
    display_message
  else
    JDESC="Successful index build of Agent files"
    display_message nobanner
    cp $dd_AGTPLCYIDX $AULDATA
    cp $dd_AGTADDRIDX $AULDATA
    rm $dd_POLICYFLAT
    rm $dd_ADDRESSFLAT
  fi
fi

### ******************************************************************
### TASK 5: clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  copy_to_masterlog
  return 0
