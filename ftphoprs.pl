#!/opt/perl5/bin/perl
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U Date: 08/03/2006  CC: 9057      By: Mark Slinger          U
#COMMENT U Reason: Modified version for HOP - Initial.               U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

$RS03="$ENV{'RS03FILE'}";
$RS18="$ENV{'RS18FILE'}";
$CTRL="$ENV{'EBSCTRL'}";
$LOGS="$ENV{'EBSLOGS'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
&check2;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d $LOGS/ftpputrs.log -f $CTRL/ftpputods.cfg -E -a ./SrcHop $RS03`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $RS03 file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $RS03 file to ODS\n"; 
    }
}

sub check2
{
    $result=`$NCFTPPUT -d $LOGS/ftpputrs2.log -f $CTRL/ftpputods.cfg -E -a ./SrcHop $RS18`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $RS18 file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $RS18 file to ODS\n"; 
    }
}
