#!/usr/local/bin/perl

use FindBin;
use lib "$FindBin::Bin/modules";
use File::Basename;
use File::Spec::Functions qw(catfile);
use IO::File;
use MIME::Lite;
use XML::Simple;
use Planlist qw(plan_list_html);

$name = basename($0, '.pl');
$xmlfile = catfile($ENV{EBSCTRL}, "$name.xml");
$planlist = shift || die "No plan list file";

$email = XMLin($xmlfile);

my $msg = MIME::Lite->new(
  From => $email->{head}->{from},
  To => $email->{head}->{to},
  Cc => $email->{head}->{cc},
  Subject => $email->{head}->{subject},
  Type => 'multipart/mixed'
);

$msg->attach(
  Type => $email->{body}->{type},
  Data => $email->{body}->{content}
);

$fh = IO::File->new($planlist, "r");

$msg->attach(
  Type => 'text/html',
  Data => plan_list_html($fh, "Conversion Plan List Update", [2, 4, 6]),
  Filename => 'planlist.html',
  Disposition => 'attachment'
);

close($fh);

$msg->send();
