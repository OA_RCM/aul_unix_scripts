#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Job Name          : harris_trust.ksh                               *
#COMMENT *                                                                     *
#COMMENT *  Description       :  Generate Harris Trust Letters                 *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.20                             *
#COMMENT *  Resides           : $AULPROC                                       *
#COMMENT *  Author            : AUL - Tony Ledford                             *
#COMMENT *  Created           : 04/19/2002                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         :                                                *
#COMMENT *  Script Calls      :                                                *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         :                                                *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     :                                                *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U                                                                     U
#COMMENT U Date: 04/19/2002   SMR:                By: Tony Ledford             U
#COMMENT U Initial version                                                     U
#COMMENT U                                                                     U
#COMMENT U Date: 05/12/2004   SMR: CC3068         By: Mike Lewis               U
#COMMENT U Reason: Added alternate date and standardize.                       U
#COMMENT U                                                                     U
#COMMENT U Date: 06/23/2004   SMR: CC3067         By: Mike Lewis               U
#COMMENT U Reason: Modified ability to rerun for one plan.                     U
#COMMENT U                                                                     U
#COMMENT U Date: 01/11/2005   SMR: CC3270         By: Danny Hagans             U
#COMMENT U Reason: Modified ability to print to IBM and/or Vistaplus           U
#COMMENT U                                                                     U
#COMMENT U Date: 20111005   Proj:  WMS3939        By: Paul Lewis               U
#COMMENT U Standardize document manager calls to a function.                   U
#COMMENT U                                                                     U
#COMMENT U Date: 20111212   Proj:  WMS3939        By: Paul Lewis               U
#COMMENT U Send all reports produced from XJOB to a document manager.          U
#COMMENT U                                                                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function gen_create_card
{
  {
    echo "00200   \$TIHDR ${JPLAN}"
    echo "00201   DEFINE    HARRISTRUST"
    echo "00202                            PROD      US"
    echo "96600   \$TIHDR ${JPLAN}000000000           $JRUN_DATE"
    echo "96601   $calc_name          $JQTR_DATE        001"
  } >$1
}

function gen_prop_card
{
  {
    echo "00200   \$TIHDR 000001000000000           $JRUN_DATE"
    echo "00201   VTRNCM    HARRISTRUST         PROPAGATEVTH"
    echo "PARAMETER-DATA=*P**"
  } >$1
}

function create_harris_trust
{
  export JDESC="Create HARRIS TRUST folder"
  export PNAME=JOBVTUT
  export JPRM1=$JUSER
  export JPRM2=$(basename $1)
  execute_job
}

function prop_harris_trust
{
  export JDESC="Propagate HARRIS TRUST folder"
  export PNAME=JOBVTUT
  export JPRM1=$JUSER
  export JPRM2=$(basename $1)
  execute_job
}

function process_harris_trust
{
  export JUSER=$JUSER
  export JPLAN=$JPLAN
  export JFILEID=HARRISTRUST
  export JSYSTEM=*
  export JPROC=US
  ### RunDate should be in CCYYMMDD format
  export JRDATE=$JRUN_DATE
  ### RunTime should be in HHMMSS00 format
  export JRTIME=00000000
  export JPOST=Y
  export JUCOM=Y
  export JVTEX=Y
  export JSEQH=Y
  export JPRINT=N
  export JPRINTCTL=A
  export JBANRPGE=N
  export OVRD_SEQAHST=NEVER
  export JDESC="Process HARRIS TRUST Plans"
  export PNAME=JOBUNIV
  execute_job
}

function copy_files
{
  for f in "${files[@]}" ; do
    [ -f "$XJOB/$f" ] && { cp $XJOB/$f $AULOUT; cp $XJOB/$f $XJOB/$EBSRPTPFX$f; }
  done
  return 0
}

function ibm_print_file
{
  typeset file=$1
  typeset jobnum=$2
  typeset prefix=""
  [[ $OMNIsysID = "dev" ]] && prefix="TEST."
  typeset filename="${prefix}RS.CDFT.RS\$\$$jobnum.D$(date +%m%d).T$(date +%H%M%S)"
  cp $file $XJOB/$filename
  fnc_ftpq $XJOB/$filename
  ctmcontb -ADD "$ctrlm_signal" ODAT
  return 0
}

function print_files
{
  if [[ "$JSND_RPT" = "Y" ]]; then
    fnc_send2DocMgr
  fi
  if [[ "$JPRT_IBM" = "Y" ]]; then
     if [[ -s "$AULOUT/LTRRATES" ]]; then
        ibm_print_file $AULOUT/LTRRATES 2123
        cond=$?
        return $cond
     fi
  fi
}

function harris_trust
{
  gen_create_card $file1 || return 1
  if [[ "$JPLAN" = "000001" ]]; then
     gen_prop_card $file2 || return 1
  fi
  create_harris_trust $file1
  if [[ "$JPLAN" = "000001" ]]; then
     prop_harris_trust $file2
  fi
  if [[ "$JPLAN" = "000001" ]]; then
     export JPLAN="******"
  fi
  process_harris_trust
  copy_files || return 1
  print_files || return 1
  return 0
}
######################################################################
##MAIN##################### main section  #################SECTION####
######################################################################
export integer NumParms=$#
prog=$(basename $0)

export JUSER=${JUSER:-$LOGNAME}
export JPLAN=${JPLAN:-000001}
export JGROUP=${JGROUP:-01}
export JNAME=${prog%.ksh}
export JDESC="Generate Harris Trust Letters"
export AULOUT=$POSTOUT
export JPRT_IBM=${JPRT_IBM}
export JSND_RPT=${JSND_RPT}
### ******************************************************************
### standard job setup procedure
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

cd $XJOB
export EBSCARD=$XJOB
file1=$XJOB/harris_trust.card.1
file2=$XJOB/harris_trust.card.2
typeset -L15 calc_name
set -A files LTRRATES HTQLYERR HTANNERR

case $JHTL_TYPE in
  qtr)
    calc_name="LTR-QLY-RATES"
    ;;
  ann)
    calc_name="LTR-ANN-RATES"
    ;;
esac


harris_trust

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
cd $XJOB
eoj_housekeeping
copy_to_masterlog
create_html
return 0
