#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXINF712                CALL            *
#COMMENT *  UNIX POINTER : rsuxinf712.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : inf_elecDflt.ksh                           *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce election report     *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 10/08/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : INF1060R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  30 min                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Define files needed by Sungard and AUL routines.T
#COMMENT T  TASK 2 - Run the COBOL program to produce deposit        T
#COMMENT T           confirmations.                                  T
#COMMENT T  TASK 3 - Prepare files for transfer to IBM.              T
#COMMENT T  TASK 4 - Send reports to document manager.               T
#COMMENT T  TASK 5 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 08/01/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U Date: 07/18/2007  SMR: CC11966  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U Date: 05/04/2009  WMS: 1629     By: Rick Sica             U
#COMMENT U Reason: Report to cifs area                               U
#COMMENT U Date: 07/20/2009  WMS: 2776     By: Rick Sica             U
#COMMENT U Reason: Need blank files created when no report           U
#COMMENT U Date: 20110318   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function run_program
{
  cd $XJOB
  if [[ -s $dd_ELECTIONRCRD ]]
  then
    PNAME=INF1060R
    JDESC="Run $PNAME to produce participant election default report"
    execute_program
  fi
}
function prepare_files_for_transfer
{
  JDESC="Preparing Participant Election Default Report for Transfer"
  display_message nobanner

  if [[ $OMNICUST = "omnihop" ]] ; then
     cp $dd_ELECTNOT /cifs/infa_data/Infa/SrcFiles/RS_HOPODS
  else
     cp $dd_ELECTNOT /cifs/infa_data/Infa/SrcFiles/RS_Warehouse
  fi
    
  create_file
  JDESC="Copying Participant Election Default report to $AULOUT for ftp job "
  display_message nobanner

  cat $dd_ELECTNOT >> $XJOB/$CDFILE
  cp $XJOB/$CDFILE $AULOUT

  if [[ ! -s $dd_ELECTNOT ]] ; then
    JDESC="No Participant Election Default report created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH1360
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP1360
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ1360
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"
  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
function end_of_job
{
  cd $XJOB
  eoj_housekeeping
  copy_to_masterlog
  create_html
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Printing Election Default Report"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

cd $XJOB
#######################################################################
### TASK 1 - Define files needed by Sungard and AUL routines
#######################################################################
cp $EBSCTRL/DUMBTRAN.DAT $XJOB
cp $EBSCTRL/DUMBTRN2.DAT $XJOB
touch $XJOB/DUMMY3.DEF
export dd_RSTROUT=$XJOB/rs.tmp
export dd_TRANIN=$XJOB/DUMBTRAN.DAT
export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
if [ "$JSTN" = "yes" ]; then
  export dd_ELECTIONRCRD=$AULOUT/elecdefault.${JUSER}
else
  export dd_ELECTIONRCRD=$AULOUT/elecdefault.txt
fi

export dd_ELECTNOT=$XJOB/ELECTNOT
export dd_ELECTNOTERROR=$XJOB/${EBSRPTPFX}ELECTNOTERROR

######################################################################
### TASK 2 - Run the COBOL program to produce deposit confirmations
######################################################################

$run_program && run_program

######################################################################
### TASK 3 - Prepare files for transfer to IBM
######################################################################

$prepare_files && prepare_files_for_transfer

######################################################################
### TASK 4 - Send reports to document manager.
######################################################################

$send2DocMgr && fnc_send2DocMgr

################################################################
### TASK 5 - clean up and say goodbye
################################################################
end_of_job
return 0
