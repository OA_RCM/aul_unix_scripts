#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXCNF707                CALL            *
#COMMENT *  UNIX POINTER : rsuxcnf707.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : cnf_addrcng.ksh                           *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce address             *
#COMMENT *                 change confirmations                      *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 10/08/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : NA                                        *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  30 min                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Prepare files for transfer to IBM.              T
#COMMENT T  TASK 2 - Send reports to document manager.               T
#COMMENT T  TASK 3 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 01/21/2004  SMR:CC5641    By: Mike Lewis            U
#COMMENT U Reason: Initial creation of script.                       U
#COMMENT U                                                           U
#COMMENT U Date: 08/03/2006  SMR: CC8403   By: Steve Loper           U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 07/18/2007  SMR: CC11966  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U                                                           U
#COMMENT U Date: 20110317   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function prepare_files_for_transfer
{
  JDESC="Preparing Participant Address Change Confirmations for Transfer"
  display_message nobanner

  if [ -f $AULOUT/address.txt ]
  then
    cp $AULOUT/address.txt $XJOB
  else
    touch $XJOB/address.txt
    print " No address confirmations available" >> $XJOB/address.txt  ##Need a space in the first position--rml
    JDESC="No address confirmations available -- dummy created"
    display_message
  fi

  export FRFILE=$XJOB/address.txt

  create_file
  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying Participant Address Change Confirmation report to $AULOUT for ftp job "
    display_message nobanner
    ##cat $AULDATA/jcl/$JCL >> $XJOB/$CDFILE   ### Not needed by ControlM ...rml
    cp $FRFILE $XJOB/${EBSRPTPFX}ADDRESS.txt
    cat $FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No Deposit Confirmation report created -- ftp job using empty $CDFILE "
    display_message nobanner
    touch $XJOB/${EBSRPTPFX}ADDRESS.txt
    print "No participant address change confirmations available" >> $XJOB/${EBSRPTPFX}ADDRESS.txt
  fi
}
function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH1220
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP1220
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ1220
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"
  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
function end_of_job
{
  cd $XJOB
  eoj_housekeeping
  copy_to_masterlog
  create_html
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Printing Participant Address Change Confirmations"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

cd $XJOB
######################################################################
### TASK 1 - Prepare files for transfer to IBM
######################################################################

$prepare_files && prepare_files_for_transfer

######################################################################
### TASK 2 - Send reports to document manager.
######################################################################

$send2DocMgr && fnc_send2DocMgr

################################################################
### TASK 3 - clean up and say goodbye
################################################################
end_of_job
return 0
