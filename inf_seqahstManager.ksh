#!/usr/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  CNTL-M JOBNAME: RSUXINF700                               *
#COMMENT *  UNIX Pointer :  rsuxinf700.ksh                           *
#COMMENT *  UNIX Script  :  inf_seqahstManager.ksh                   *
#COMMENT *                                                           *
#COMMENT *  Description  : Scan input SEQAHST file:  Write records   *
#COMMENT *        reflecting non-financial activity - based on P1/P2 *
#COMMENT *        transactions list - to a "nonFinancial" SEQAHST    *
#COMMENT *        file (flat file).  All other records are assumed   *
#COMMENT *        to reflect financial activity and are written to   *
#COMMENT *        a "Financial" SEQAHST file.                        *
#COMMENT *                                                           *
#COMMENT *  ********************** N O T E ************************  *
#COMMENT *  **   The SOP of creating a SEQAHST.COMBINE.srt file  **  *
#COMMENT *  **   (sorted total records from all SEQAHST files)   **  *
#COMMENT *  **    has not been modified.                         **  *
#COMMENT *  *******************************************************  *
#COMMENT *                                                           *
#COMMENT *  Author       : ONEAMERICA - S Loper                      *
#COMMENT *  Created      : 09/25/2007                                *
#COMMENT *  Environment  : N/A                                       *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : None                                      *
#COMMENT *  COBOL Calls  : N/A                                       *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Weekdays (standard NYSE workdays)         *
#COMMENT *  Est.Run Time : Up to 5  minutes                          *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S     Will not work without:                                S
#COMMENT S     List of indicative transactions (p1 / p2 tran nbrs)   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T  1 - Check if all indicative transactions ID files noted  T
#COMMENT T       in array are readable                               T
#COMMENT T  2 - Create master indicative transaction ID file from    T
#COMMENT T      those noted in array                                 T
#COMMENT T  3 - Sort master indicative txn list for readability -    T
#COMMENT T      does not affect subsequent use                       T
#COMMENT T  4 - reformat combined sequential history                 T
#COMMENT T  5 - sort combined sequential history                     T
#COMMENT T  6 - Call PERL script to scan SEQAHST file and write out  T
#COMMENT T       SEQAHST.FIN with only non-indicative records        T
#COMMENT T  7 - Archive sequential history                           T
#COMMENT T  LAST - Standard end of job with housekeeping             T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2007  CC:  XXXX     By: Steve Loper           U
#COMMENT U Reason: Initial version.                                  U
#COMMENT U                                                           U
#COMMENT U Date: 20090817   WMS: 2532      By: Tony Ledford          U
#COMMENT U Reason: Converted this program to be a sequential history U
#COMMENT U         manager.  Added tasks to create sorted combined   U
#COMMENT U         file as well as archiving sequential history      U
#COMMENT U         files.                                            U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################################
####FUNCTIONS################  functions section  ######################SECTION#######
######################################################################################
function check_input 
{
  mno=0
  tstuid=`echo $JUSER|tr "[:upper:]" "[:lower:]"`
  [[ $tstuid = "help" || $tstuid = "?" ]] && mno=1
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
  [[ ! -f $SEQAHST_IN ]] && mno=4
  check_mno
}
function check_overrides
{
  export MKOUTDIR=current
}
function make_master_indicative_ID_file
{
  for file in ${IND_TXNfiles[*]}
  do
    [[ -r $file ]] || { JDESC="File $file NOT Readable"; display_message nobanner; }
    [[ -s $file ]] || { JDESC="File $file NOT Populated"; display_message nobanner; }
    JDESC="FILE ERROR: $file - see above"   ## this is default message in case either of above tests failed
    [[ -r $file && -s $file ]] && JDESC="File $file is readable and populated" || abend
    display_message nobanner

    ## create master indicative transaction list from each member identified in array
    cat $file >> $TMPDIR/master.txns || { JDESC="ERROR during cat of $file to TMPDIR master.txns - Abort"; abend; }
  done
}
function sort_master_indicative_ID_file
{
  cd $TMPDIR
  sort -u -o $sorted_indicative_master master.txns || \
     { JDESC="ERROR on sort of master txn ID file (master.txns)"; abend; }
  JDESC="Successfully created sorted master indicative transaction file ($sorted_indicative_master)"
  display_message nobanner
  cd $OLDPWD
}
function make_split_seqahst_file
{
  PMSG="function 'create_seqahst_financial_file'"
  JDESC="In $PMSG"; display_message nobanner

  PNAME="inf_bildFinSEQAHST.pl"
  JPRM1="$sorted_indicative_master"
  JPRM2="$SEQAHST_IN"
  std_output="$seqahst_financial_file"
  std_error=""

  JDESC="Call $PNAME to output non-indicative SEQAHST records"; display_message nobanner
  JDESC="Input SEQAHST records are in $JPRM2"; display_message nobanner
  JDESC="Output financial records  to $std_output"; display_message nobanner
  JDESC="SKIP Output of Indicative records"

  if [[ $debug = DEBUG ]]
  then
    std_error=$(eval "echo \$seqahst_indicative_file")
    JDESC="[in DEBUG mode] Output Indicative records to $std_error"
    std_error="2> $std_error"
  fi
  display_message nobanner

  $PNAME $JPRM1 $JPRM2 >$std_output $std_error && cond=0 || cond=1
  cond_limit=0
  check_status

  JDESC="Process to output non-indicative SEQAHST records complete!"; display_message nobanner
  JDESC="Successfully exit $PMSG"; display_message nobanner
  unset PMSG
}
######################################################################################
######MAIN###################    main  section    ######################SECTION#######
######################################################################################
prog=$(basename $0)
export integer NumParms=$#

##### setup local variables
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JGROUP=${JGROUP:-01}
export JDESC="OMNIPLUS/UX EXTRACT SEQAHST FINANCIAL INTERFACE"

#### standard script setup tasks
. FUNCTIONSFILE
. SEQAHSTFUNCTIONS
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
## TASK 1 - Check if all indicative transactions ID files noted in array are readable
## TASK 2 - Create master indicative transaction ID file from those noted in array
## following function accomplishes both task1 and task2
$make_master_indicative_ID_file && make_master_indicative_ID_file

## TASK 3 - Sort master indicative txn list for readability - does not affect subsequent use 
$sort_master_indicative_ID_file && sort_master_indicative_ID_file

## TASK 4 - reformat combined sequential history
$reformat_comb_seqahst_file && seq_reformat_comb_seqahst_file

## TASK 5 - sort combined sequential history
$sort_comb_seqahst_file && seq_sort_seqahst_file

## TASK 6 - Call PERL script to scan SEQAHST file and write out SEQAHST.FIN with only non-indicative records
$make_split_seqahst_file && make_split_seqahst_file

## TASK 7 - Archive sequential history
$archive_seqahst_files && seq_arch_seqahst_files

## TASK LAST - Standard end of job with housekeeping
eoj_housekeeping
return 0
