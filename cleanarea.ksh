#!/usr/bin/ksh
#
################################################################################
#  update:
#    S.Loper  20040617  function restore_master_files. if in conv1, but not
#                       conv1, rebuild symbolic link for SECU/VIOL to conv1.
#             20040617  main section. add code to check if user is in correct
#                       dataset.
#             20040727  SKL make directory 'jsubs' if removing EBSOUT contents.
#    T. Ledford 20050808 Added port control logic
#
################################################################################
#
# functions

function exit_routine
{
  print "User elected to exit [answer=$answer]"
  exit 0
}

function clean_directories
{
  for dir in "${remove_dirs[@]}"
  do
    cd $dir
    rm -r * && print "Removed $dir contents successfully"
    [[ `pwd` = $EBSOUT ]] && mkdir -p jsubs
  done
}

function clean_files
{
  for dir in "${remove_files[@]}" ; do
    (
      cd $dir
      for f in $(ls) ; do
        [ -f "$f" ] && rm $f
      done
    )
  done
}

function save_master_files
{
  (cd $EBSDATA; cp "${master_files[@]}" $XJOB)
}

function remove_master_files
{
  (
    cd $EBSDATA
    for f in $(ls) ; do
      [ -f "$f" ] && rm $f
    done
  )
}

function build_master_files
{
  (cd $EBSDATA; JOBFBLD $JUSER XX)
}

function restore_master_files
{
  cp "${master_files[@]}" $EBSDATA
  region_type=$(echo $EBSFSET|cut -c1-4)
  region_nbr=$(echo $EBSFSET|cut -c5)
  [[ $region_type = conv ]] && rebuild_symbolic_links
}

function rebuild_symbolic_links
{
  cd $EBSDATA
  rm -f SECU VIOL SYSM
  ln -s /omniconv/mstr/comConv/SECU SECU
  ln -s /omniconv/mstr/comConv/VIOL VIOL
  ln -s /omniconv/mstr/comConv/SYSM SYSM
  cd $OLDDATA
}
function rebuild_dir_list
{
  for dir in $(echo $EBSDIRLIST | tr ':' ' ')
  do
    mkdir -p $dir
  done
}

function stop_ports
{
  stopports $EBSFSET
}

function start_ports
{
  startports $EBSFSET
}
# main
prog=$(basename $0)
export JNAME=${prog%.ksh}
export JUSER=${JUSER:-$LOGNAME}

### make sure we are in desired area to initialize before continuing
clear;banner $EBSFSET
print "Is this the dataset you wish to initialize? (YES/NO)"
read answer
[[ $answer != YES && $answer != yes ]] && exit_routine

[ -f $EBSCTRL/$JNAME.ini ] || { echo "No ini file. Check \$EBSCTRL"; exit 99; }

### standard script setup calls
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start

### sequential process following function calls
stop_ports
clean_directories
clean_files
save_master_files
remove_master_files
build_master_files
restore_master_files
rebuild_dir_list
start_ports
eoj_housekeeping
