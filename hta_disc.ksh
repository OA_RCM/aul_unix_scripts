#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXHTA701                                *
#COMMENT *  Script Name  : hta_disc.ksh                CALL          *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Calls each step in Harris Trust Annual    *
#COMMENT *                 disclosure letters                        *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 02/26/2002                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : hta_disc2.ksh                             *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : after each daily edit                     *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : depends on # of plans in htanndisctl.txt  *
#COMMENT *                 and size of plan                          *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 01/30/07 CC10567  Name: Danny Hagans                U
#COMMENT U Reason: Modify script find the correct htannct file       U
#COMMENT U        for HOP ERISA letters.                             U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
###************ FUNCTIONS ********************************************
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}


### ******************************************************************
### define job variables
### ******************************************************************
  prog=$(basename $0)
  export JUSER=${JUSER:=$LOGNAME}
  export JNAME=${prog%.ksh}
  export JDESC="$JNAME.ksh to create and send Harris Trust Annual Disclosure letters"
  export tmptime=`date +%H%M%S`
  export today=`date +%Y%m%d`
  export ANNRPTDIR=$EBSINPUT/wizard

### ******************************************************************
### standard job setup procedure
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start

cd XJOB

### ******************************************************************
### Step 1
### ******************************************************************
  JDESC="Check to see if any disclosure letters need printing"
  display_message nobanner
  rptlist=/tmp/annrpt$$
  ls $ANNRPTDIR | grep htannctl >$rptlist
  if [[ -s $rptlist ]]
  then
     #
     # sleep just to make sure no FTP session is going on any of the files
     #
     sleep 5
     for f in $(cat $rptlist) ; do
       cat $ANNRPTDIR/$f
     done >$XJOB/annctl.txt
     for f in $(cat $rptlist) ; do
       mv $ANNRPTDIR/$f $XJOB
     done
    rm -f $rptlist
  else
     echo
     echo "Nothing to process"
     JDESC="No disclosure letters to process"
     display_message nobanner
     rm -f $rptlist
     return 0
  fi

  PNAME=hta_disc2.ksh
  JDESC="Call script to print Harris Trust Disclosure Letters"
  execute_job

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  eoj_housekeeping
  return
