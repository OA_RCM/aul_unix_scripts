#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *  CONTRLM JOBNAME   : RSUXDWH002                 NON-CALL            *
#COMMENT *  UNIX Pointer Name : rsuxdwh002.ksh             OMNI-ASU            *
#COMMENT *  UNIX Script Name  : dwh_driver.ksh                                 *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run DWH-XXXX.txt                               *
#COMMENT *                                                                     *
#COMMENT *  Author            : AUL - Gary Pieratt                             *
#COMMENT *  Created           : 05/23/2011                                     *
#COMMENT *  Called by         : Scheduled or manually run.                     *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       : None.                                          *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Daily                                          *
#COMMENT *  Est. Run Time     : Depends on volumn. Less than 60 min.           *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S  None                                                               S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Must be looked at by ASU support personnel prior to restart.       R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT T  TASK SEQUENCE -------------------------------------------------    T
#COMMENT T    1 - process_attr_files                                           T
#COMMENT T    2 - run calculater X times                                       T
#COMMENT T    3   filter extract and consolidate into master extract           T
#COMMENT T    4 - copy extract file to cifs                                    T
#COMMENT T    LAST - standard exit function                                    T
#COMMENT T                                                                     T
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Gary Pieratt     WMS5501   05/18/2011  original                    U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   12/20/2011                              U
#COMMENT U  Reason: correct header. allow for LISTDIR. run jobcalc function in U
#COMMENT U  Reason: background to trap exit code. add routine to check exit    U
#COMMENT U  Reason: status for segments. Allow EBSDATA override (BCV)          U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   01/18/2012                              U
#COMMENT U  Reason: correct display_messsage misspelling error.                U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   01/23/2012                              U
#COMMENT U  Reason: fix filter, consolidate, other misc errors                 U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   03/06/2012                              U
#COMMENT U  Reason: add functionality to process ATTR* files                   U
#COMMENT U  Reason: add functionality to split HOP and Prod recs; for testing, U
#COMMENT U          manipulate EBSFSET to send HOP file to test hop cifs.      U
#COMMENT U          Fix planlist issue; Mod to use DTYPE, NSEGS, EXTFILE.      U
#COMMENT U          Use fnc_log & fnc_exit functions to standardize messaging. U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   03/26/2012                              U
#COMMENT U  Reason: Allow for copy of SACI extract to test cifs area for HOP.  U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   04/02/2012                              U
#COMMENT U  Reason: Fix error with fnc_die: change to fnc_exit.                U
#COMMENT U  Reason: Fix ATTRFILE3 typo in function process_attr_files.         U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
################################################################################
#### FUNCTIONS ####### functions section ##################### SECTION #########
################################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 2 ]] && mno=2
  check_mno
  touch $XJOB/"Input_parm_is_${DTYPE}_${NSEGS}"
  fnc_log "Input_parm_is_${DTYPE}_${NSEGS}"
  [[ $NSEGS = ALL ]] && NSEGS=1
  echo $chkSeqNum | { grep -q $NSEGS || mno=3; }
  check_mno
  ls -1 $LISTDIR | grep "${split_name}_${NSEGS}_" >$planlist
  ### if the planlist file is not populated, there is nothing to do - so quit.
  [[ -s $planlist ]] || fnc_exit "No plan list files found; Nothing to process..."
}
function check_overrides
{
   export EBSDATA=$EBSDATAovrd
   export PXJOB=$XJOB
   fnc_log "Derive data from: $EBSDATA"
   fnc_log "Parent XJOB is..: $PXJOB"
   echo ${commonType[*]}|grep -q $DTYPE && export HOPFILE=${DTYPE}_${suffix}.${extension}
}
function extract_data
{
  export EXTFILE=${DTYPE}_${suffix}.tsv
  export JOBCMPNT=DWH-${DTYPE}.txt
  sed "s/XXXX/$DTYPE/g" $calc_dir/DWH-XXXX.txt > $JOBCMPNT
  export CALCPATH=$XJOB
  export MKOUTDIR=child

  if [[ $NSEGS != ALL ]]; then
     grp=0
     cat $planlist |&
     while read -p filename; do
        ((grp+=1))
	export JNAME_DESC="${NSEGS}_${grp}"
        export PLANLIST=$LISTDIR/$filename
        export JOBEXTRACT=${DTYPE}_${suffix}_${grp}.tsv
	export flagfile=$XJOB/JOBCALC_${NSEGS}_${grp}.ok
	export sysoutfile=$XJOB/JOBCALC_${NSEGS}_${grp}.out
        fnc_log "Running $JOBCMPNT calculator for plan list $filename"
        nohup JOBCALC $JUSER $JOBCMPNT > $sysoutfile && touch $flagfile &
        sleep 1
     done

     fnc_log "Waiting on all submitted job segments to complete processing..."
     wait
     fnc_log "Resume processing: All segments completed."
     typeset -i grp=1 cond=0
     set -A segErr
     rm -f $planlist

     until [[ $grp -gt $NSEGS ]]; do
	[[ -f $XJOB/JOBCALC_${NSEGS}_${grp}.ok ]] && { cond=0; fnc_log "Job $grp Ended Ok."; }
	[[ -f $XJOB/JOBCALC_${NSEGS}_${grp}.ok ]] || { cond=1; segErr[${#segErr[*]}]=$segNo; }
        ((grp+=1))
     done
     [[ $cond != 0 ]] && fnc_exit "ERROR: The following segments did not end Ok! [${segErr[@]}]"
  else
     export JNAME_DESC="JOBCALC_${NSEGS}"
     export PLANLIST=$PLANLISTFILE
     export JOBEXTRACT=${DTYPE}_${suffix}.tsv
     run_jobcalc
  fi
}
function run_jobcalc
{
  fnc_log "Call jobcalc to run $JOBCMPNT for all plans..."
  PNAME=JOBCALC
  JDESC="Exec $JOBCMPNT calculator for all plans..."
  JPRM1=$JUSER
  JPRM2=$JOBCMPNT
  CALCPATH=$XJOB
  execute_job
  fnc_log "$JOBCMPNT complete for all plans..."
}
function process_attr_files
{
  typeset savepath=$PWD cond=0 msg="" HOPtestCIFS=""
  cd $CPATH
  sed -e '1d' $ATTRFILE1 > $XJOB/$ATTRFILE1 || { msg="$ATTRFILE1"; cond=99; }
  sed -e '1d' $ATTRFILE2 > $XJOB/$ATTRFILE2 || { msg="$ATTRFILE2"; cond=99; }
  sed -e '1d' $ATTRFILE3 > $XJOB/$ATTRFILE3 || { msg="$ATTRFILE3"; cond=99; }
  [[ $cond != 0 ]] && fnc_exit "ERROR: $msg not created in XJOB!"

  cd $XJOB
  awk -F"\t" '{print $12}' $ATTRFILE1 > $ATTRFILE_TOTAL || { msg="$ATTRFILE1"; cond=99; }
  awk -F"\t" '{print $5}' $ATTRFILE2 >> $ATTRFILE_TOTAL || { msg="$ATTRFILE2"; cond=99; }
  awk -F"\t" '{print $1}' $ATTRFILE3 >> $ATTRFILE_TOTAL || { msg="$ATTRFILE3"; cond=99; }
  [[ $cond != 0 ]] && fnc_exit "ERROR: $msg not appended to total!"

  sort -u -o XXX $ATTRFILE_TOTAL && mv XXX $ATTRFILE_TOTAL || cond=99
  [[ $cond != 0 ]] && fnc_exit "ERROR: Sort to final ATTR totals file failed!"

  # copy ATTRFILE_TOTAL file to cifs
  $copy_attr_total_file && dwh_copy_file $ATTRFILE_TOTAL 
  $copy_attr_total_file || fnc_log "copy ATTRFILE_TOTAL file to cifs: OFF"
  cd $savepath
}
################################################################################
####   MAIN    #######   main  section   ##################### SECTION #########
################################################################################
export integer NumParms=$#
export JNAME=$(basename $0 .ksh)
export JUSER=${JUSER:-$LOGNAME}
export JDESC="ONEAMERICA DWH Object Extraction Process"
export ENVFILE=ENVNONE
export DTYPE=$1
export NSEGS=$2

# standard script setup
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export planlist=$XJOB/$planlist
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
export RSTROUT=$XJOB/rs.tmp

# Task 1 - create attr_totals from *attr* files in cifs
touch $XJOB/$ATTRFILE_TOTAL  ## just in case this task is set to false
$process_attr_files && process_attr_files
$process_attr_files || fnc_log "processing of ATTR files: OFF"

# Task 2 - run jobcalc to extract data
$extract_data && extract_data
$extract_data || fnc_log "extract_data function: OFF"

# Task 3 - build single extract file without nulls
$sed_and_consolidate && dwh_sed_and_consolidate $EXTFILE
$sed_and_consolidate || fnc_log "filter and consolidate function: OFF"

# Task 4 - copy contributor extract file to cifs
$copy_file && dwh_copy_file $EXTFILE
$copy_file || fnc_log "copy EXTFILE file to cifs: OFF"

# Task Last - standard exit function
touch rs.tmp
eoj_housekeeping
return 0
