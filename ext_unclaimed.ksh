#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS JOBNAME    : RSUXEXT715              NO-CALL         *
#COMMENT *  SchedXref Name : rsuxext715.ksh                          *
#COMMENT *  UNIX Script    : ext_unclaimed.ksh       OMNI-DEV        *
#COMMENT *                                                           *
#COMMENT *  Description  : Extract participant data to be loaded     *
#COMMENT *                 into tables and run against a Federal     *
#COMMENT *                 death master file to check if may have    *
#COMMENT *                 passed away                               *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 09/28/2011                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxext715.ksh                            *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  : EXT1070E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Monthly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : about 1 hour                              *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  can be restarted at any time     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task List:                                               T
#COMMENT T  1. run process                                           T
#COMMENT T  2. copy file to FTP transfer directory                   T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/28/11    wms: 6206     By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

######################################################################
############################# main code ##############################
######################################################################

### ******************************************************************
### define job variables
### ******************************************************************
prog=$(basename $0)
  export JUSER=${JUSER:=$LOGNAME}
  export JNAME=${prog%.ksh}
  export JDESC="$JNAME.ksh to Extract data for Unclaimed funds check"
  export JGROUP=${JGROUP:=01}
  export ENVFILE=ENVBATCH

### ******************************************************************
### source in common functions
### ******************************************************************
  . FUNCTIONSFILE
### ******************************************************************
### set variables to initial values
### ******************************************************************
  set_generic_variables
  make_output_directories
  cd $XJOB

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### Task 1 - Create unclaimed extract
### ******************************************************************

export UNCLAIMEDEXTNM=Unclaimed.RS.OMNI.psv
export UNCLAIMEDEXT=$XJOB/$UNCLAIMEDEXTNM
export UNCLAIMEDERRORS=$XJOB/${EBSRPTPFX}UNCLAIMEDERRORS
export UNCLAIMEDSUM=$XJOB/${EBSRPTPFX}UNCLAIMEDSUM

if $runprocess ; then
  export dd_RSTROUT=$XJOB/rs.tmp
  
  export PNAME=EXT1070E
  export JDESC="run COBOL program $PNAME"
  execute_program
fi

### ******************************************************************
### Task 2 - Send files where they need to go
### ******************************************************************

$sendext && cp $UNCLAIMEDEXT $CIFSarea/$UNCLAIMEDEXTNM

$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
