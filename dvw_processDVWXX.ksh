#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  Cntl-M Job Name : none                                   *
#COMMENT *  UNIX Job Pointer: none                                   *
#COMMENT *  UNIX Script Name: dvw_processDVWXX.ksh                   *
#COMMENT *                                                           *
#COMMENT *  CNTLM Job Type:  n/a                                     *
#COMMENT *  Runtime syntax:  dvw_processDVWXX.ksh filename           *
#COMMENT *                                                           *
#COMMENT *  Description  : RUN A JOB IN THE DVW BATCH TODO QUEUE     *
#COMMENT *    Execute the DVW job specified on the command line;     *
#COMMENT *    Track execution status. At successful EOJ, move the    *
#COMMENT *    executed job from 'todo' folder to the 'done' folder.  *
#COMMENT *                                                           *
#COMMENT *  Author       : OA/SKL                                    *
#COMMENT *  Created      : 05/07/2013                                *
#COMMENT *  Environment  : N/A                                       *
#COMMENT *  Called by    : dvw_sunrise.ksh or manually as needed     *
#COMMENT *                                                           *
#COMMENT *  Tools utilzed: standard omni undefined functions         *
#COMMENT *  Script Calls : FUNCTIONSFILE, specified DVWXX.TIME file  *
#COMMENT *  COBOL Calls  : None                                      *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Standard workdays                         *
#COMMENT *  Dependent on : see 'called by' above                     *
#COMMENT *  Est.Run Time : One to five minutes                       *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions : None                              S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                   R
#COMMENT R   Must be looked at by application support personnel      R
#COMMENT R   before being re-streamed.                               R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE: ----------------------------------------- T
#COMMENT T   03 - End of job housekeeping                            T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U  Date: 05/07/2013  WMS: 6167      By: Steve Loper         U
#COMMENT U  Reason: Initial version.                                 U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
##############################################################################
##  functions
##############################################################################
##############################################################################
### standard function to check script input
##############################################################################
function check_input
{
  typeset mno=0
  dvw_set_batch_variables
  [[ $NumParms -lt 1 ]] && { mno=2; check_mno; }
  [[ -z $JFILE ]] && { mno=3; check_mno; }
  [[ -x $BAT_TODO_DIR/$JFILE ]] || { mno=4; check_mno; }
}
##############################################################################
### standard function to set overrides to current environment
##############################################################################
function check_overrides
{
  export PATH=$BAT_TODO_DIR:$PATH
  fnc_log "Prepended $BAT_TODO_DIR to standard PATH"
}
##############################################################################
### execute input file [DVWXX.timestamp] specified on command line call
##############################################################################
function process_dvwxx_file
{
  fnc_log "in function: $0"
  export PNAME=$JFILE
  execute_job || process_error abend
  fnc_log "return from $JFILE execution ok"
  mv $BAT_TODO_DIR/$JFILE $BAT_DONE_DIR || process_error move
  fnc_log "Moved $JFILE to processed folder Ok"
}
##############################################################################
### execute if error on processing or final move of DVWXX file
##############################################################################
function process_error
{
  fnc_log "in function: $0"
  case "$1" in 
    "abend")
       touch $EBSLOGS/$JFILE.$today.abend.err
       fnc_exit "$prog:$0:Job $JFILE abended"
       ;;
    "move")
       touch $EBSLOGS/$JFILE.$today.move.err
       fnc_exit "$prog:$0:$JFILE move-to-done error"
       ;;
  esac
}
##############################################################################
####MAIN##################   main section   ######################SECTION#####
##############################################################################
prog=$(basename $0)
integer NumParms=$#

export JUSER=${JUSER:-DVWBATCH}
export JNAME=${prog%.ksh}
export JDESC="EXECUTE DVW JOBS IN BATCH TODO DIRECTORY"
export JFILE=$1

#### standard script setup
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides
cd $XJOB

########################
### Processing logic ###
########################
##TASK 01 - Remove batchflag file
process_dvwxx_file

##TASK 03 - End of job housekeeping
eoj_housekeeping
return 0
