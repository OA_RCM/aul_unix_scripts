#!/bin/ksh

function usage
{
    echo usage: decrypt.ksh dsn 1>&2
    exit 1
}

dsn="$1"

if [ -z "$dsn" ]; then
    usage
fi

if [ -z "$ODBCINI" ]; then
    echo ODBCINI is not defined 1>&2
    usage
fi

if [ ! -f $ODBCINI ]; then
    echo $ODBCINI is not a file 1>&2
    usage
fi

password="$(procini.pl -k Password $dsn)"

if [ 0 -ne $? ]; then
    echo Could not process the ini 1>&2
    exit 1
fi

padcrypt.pl -key $dsn -txt "$password" decrypt

exit $?

