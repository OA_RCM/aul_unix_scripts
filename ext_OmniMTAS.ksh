#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : HSUXEXT003             CALL            *
#COMMENT *  Schedule Name   : rsuxext003.ksh         OMNI-ASU        *
#COMMENT *  Unix Script     : ext_OmniMTAS.ksh                       *
#COMMENT *                                                           *
#COMMENT *  Description  : Omni - SEC Rule 22-C2 Extractor           *
#COMMENT *                                                           *
#COMMENT *  Author       : LQB                                       *
#COMMENT *  Created      : 04/10/2007                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : EXT1035E.CBL and EXT1040E.CBL             *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily, On Demand                          *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : Dependant on number of requests           *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T    1 - Reinitialize Responses files                       T
#COMMENT T    2 - Create HOP workfile                                T
#COMMENT T    3 - Create PROD workfile                               T
#COMMENT T    4 - Split HOP  workfile                                T
#COMMENT T    5 - Split PROD workfile                                T
#COMMENT T    6 - Run split extract on the HOP and PROD side         T
#COMMENT T    7 - Standard EOJ housekeeping tasks                    T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U  Louis Blanchette   08/18/2008  PROJ00000972              U
#COMMENT U  Added  EBSFSET and $ishopsplit valued from INI file      U
#COMMENT U  in function extract_data                                 U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
###   function declarations
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function check_overrides
{
  ## for future use
  return 0
}
function create_workfile
{
   PMSG="function create_workfile"
   ## set variables based on region specification
   dataSet=$1
   case "$dataSet" in
      "HOP")
         export dd_INVESTIDX=$AULDATAHOP/INVFUNDPLANX
         export dd_SRTWORK=$XJOB/SRTWORKHOP
         export dd_WORKFILE=$XJOB/WORKFILEHOP
	  ;;
      "PROD")
         export dd_INVESTIDX=$AULDATAPROD/INVFUNDPLANX
         export dd_SRTWORK=$XJOB/SRTWORKPROD
         export dd_WORKFILE=$XJOB/WORKFILEPROD
	  ;;
      *)
          JDESC="${PMSG}: Bad parm - Region [$dataSet]"
	  abend
	  ;;
   esac
   ## JOBDEFINE - two major functions:
   ## sets ENVfile for current process
   ## sets master file dd's to specified region
   . JOBDEFINE

   export dd_REQUEST=$AULDATA/MQREQUESTS

   Mid=$dataSet
   PNAME=EXT1035E
   JDESC="Run COBOL program $PNAME - $dataSet workfile creation"
   execute_program

   JDESC="Completed $PMSG - $dataSet workfile creation"
   display_message nobanner
   unset PMSG
}
#######################################################
## local function called by the generic functions
## fnc_split_plans_by_row or fnc_split_plans_by_column
## to populate split workfiles
#######################################################
function generate_card
{
  plan=$1
  echo "$plan"
}
function generate_split_workfile
{
  PMSG="function generate_split_workfile"
  JDESC="Execute function to create split workfile(s)"
  display_message nobanner

  fnc_determine_group_sizes   ## calculate number of plans per run
  fnc_split_plans_by_column   ## create actual split workfiles based on indicated method
  [[ $? != 0 ]] && { JDESC="ERROR creating split workfiles!"; abend; }

  JDESC="Completed function to create split workfile(s) Ok!"
  display_message nobanner
  unset PMSG
}
# Check if all workfiles  exist if not create an empty one
function check_workfiles
{
   PMSG="function check_workfiles"
   ## set variables based on region specification
   export grpNum=1
   dataSet=$1
   case "$dataSet" in
      "HOP")
          while (( $grpNum <= $NUMGROUPS_hop ))
          do
            if [[ ! -f WKFILEHOP$grpNum.$rundate ]]; then
               touch WKFILEHOP$grpNum.$rundate
            fi
            (( grpNum=$grpNum +1 ))
          done
	  ;;
      "PROD")
          while (( $grpNum <= $NUMGROUPS_prod ))
          do
            if [[ ! -f WKFILEPROD$grpNum.$rundate ]]; then
               touch WKFILEPROD$grpNum.$rundate
            fi
            (( grpNum=$grpNum +1 ))
          done
	  ;;
      *)
          JDESC="${PMSG}: Bad parm - Region [$dataSet]"
	  abend
	  ;;
   esac

   JDESC="Completed $PMSG - $dataSet"
   display_message nobanner
   unset PMSG

}
function extract_data
{
   PMSG="function extract_data"
   ## set variables based on region specification
   case "$dataSet" in
      "HOP")
          export FlagFile=hop_extract_ok.flag$grpNum
          export EBSDATA=$EBSDATAHOP
          export EBSFSET=$EBSFSETHOP
          $ishopsplit && unset DONT_DEFINE_AHST_SEGMENTS_
          export dd_RESPONSES=MQHOPRESP$grpNum
          export dd_WORKFILE=$datafile
          export dd_TEMPFILE=TEMPFILEHOP$grpNum
          export dd_RSTROUT=rs_hop.tmp$grpNum
	  ;;
      "PROD")
          export FlagFile=prod_extract_ok.flag$grpNum
          export EBSDATA=$EBSDATAPROD
          export EBSFSET=$EBSFSETPROD
          $isprodsplit && unset DONT_DEFINE_AHST_SEGMENTS_
          export dd_RESPONSES=MQPRODRESP$grpNum
          export dd_WORKFILE=$datafile
          export dd_TEMPFILE=TEMPFILEPROD$grpNum
          export dd_RSTROUT=rs_prod.tmp$grpNum	
	  ;;
      *)
          JDESC="${PMSG}: Bad parm - Region [$dataSet]"
	  abend
	  ;;
   esac
   ## JOBDEFINE - two major functions:
   ## sets ENVfile for current process
   ## sets master file dd's to specified region
   . JOBDEFINE

   export dd_TRANSMAP=$AULDATA/MQOMNI2SDR

   Mid=$dataSet$grpNum
   PNAME=EXT1040E
   JDESC="Run COBOL program $PNAME - $dataSet$grpNum extraction"
   execute_program

   JDESC="Completed $PMSG - $dataSet$grpNum extraction"
   display_message nobanner
   unset PMSG

   touch $FlagFile
}
###********************************************************************
### Driver to run generated split workfiles in background
###********************************************************************
function run_split_extract
{
  ####-----------------------------------------
  #### run extract in background for each group
  ####-----------------------------------------
  export dataSet="HOP"
  for file in $splitted_files_hop*
  do
    export grpNum=$(print $file|cut -c10)
    JDESC=" Calling extract process for HOP "
    display_message nobanner
    #call extract_process in BG to promote correct checking of the ok files.
    export datafile=$file
    $extract_hop && extract_data &
    sleep 5
  done

  export dataSet="PROD"
  for file in $splitted_files_prod*
  do
    export grpNum=$(print $file|cut -c11)
    JDESC=" Calling extract process for PROD "
    display_message nobanner
    #call extract_process in BG to promote correct checking of the ok files.
    export datafile=$file
    $extract_prod && extract_data &
    sleep 5
  done

#############################################
# Wait for all backgound process to complete
#############################################
  wait

###################################
# Look for background flags
###################################
  $extract_hop  && check_bkgnd_flag HOP
  $extract_prod && check_bkgnd_flag PROD

##############################################
# Merge all responses file for HOP and PROD
##############################################
  cat MQHOPRESP* >> $AULDATA/MQHOPRESP
  cat MQPRODRESP* >> $AULDATA/MQPRODRESP
}
##############################################
###  Check if specified background process  ##
###  completed successfully. Abend if not.  ##
##############################################
function check_bkgnd_flag
{
   export grpNum=1
   dataSet=$1
   case "$dataSet" in
      "HOP")
          while (( $grpNum <= $NUMGROUPS_hop ))
          do
            if [[ -f hop_extract_ok.flag$grpNum ]]; then
               JDESC="Extract process for HOP${grpNum} successful..."
               display_message nobanner
            else
               JDESC="Extract process for HOP${grpNum} unsuccessful..."
               display_message nobanner
	       abend
            fi
            (( grpNum=$grpNum +1 ))
          done
	  ;;
      "PROD")
          while (( $grpNum <= $NUMGROUPS_prod ))
          do
            if [[ -f prod_extract_ok.flag$grpNum ]]; then
               JDESC="Extract process for PROD${grpNum} successful..."
               display_message nobanner
            else
               JDESC="Extract process for PROD${grpNum} unsuccessful..."
               display_message nobanner
	       abend
            fi
            (( grpNum=$grpNum +1 ))
          done
	  ;;
      *)
          JDESC="${PMSG}: Bad parm - Region [$dataSet]"
	  abend
	  ;;
   esac
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}

export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX SEC RULE 22-C2 OMNI MTAS EXTRACTOR MODULE"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides

###########################
# Move to the JOB directory
###########################
cd $XJOB

### ******************************************************************
### TASK 1 - Reinitialize Responses files
### ******************************************************************
rm -f $AULDATA/MQHOPRESP $AULDATA/MQPRODRESP
touch $AULDATA/MQHOPRESP
touch $AULDATA/MQPRODRESP

### ******************************************************************
### TASK 2 - Create HOP workfile
### ******************************************************************
$extract_hop && create_workfile HOP

### ******************************************************************
### TASK 3 - Create PROD workfile
### ******************************************************************
$extract_prod && create_workfile PROD

### ******************************************************************
### TASK 4 - Split HOP workfile
### ******************************************************************
export split_name=$splitted_files_hop
export PLANLISTFILE=$split_name_hop
export NUMGROUPS=$NUMGROUPS_hop
$extract_hop && generate_split_workfile && check_workfiles HOP

### ******************************************************************
### TASK 5 - Split PROD workfile
### ******************************************************************
export split_name=$splitted_files_prod
export PLANLISTFILE=$split_name_prod
export NUMGROUPS=$NUMGROUPS_prod
$extract_prod && generate_split_workfile && check_workfiles PROD

### ******************************************************************
### TASK 6 - Run split extract on the HOP and PROD side
### ******************************************************************
run_split_extract

### ******************************************************************
### TASK 7 - Standard EOJ housekeeping tasks
### ******************************************************************
eoj_housekeeping
return 0
