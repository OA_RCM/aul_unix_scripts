#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXINF003                   CALL         *
#COMMENT *  Script Name  : inf_EXQpir.ksh               ASU          *
#COMMENT *  Xref Name    : rsuxinf003.ksh                            *
#COMMENT *                                                           *
#COMMENT *  Description  : batch job for Data Express                *
#COMMENT *                 qpiro file.                               *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 02/04/2008                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  : DERUN.gnt OPMSGSORT.gnt                   *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Once    a Day                             *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 - 45 min                               *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK LIST:                                               T
#COMMENT T                                                           T
#COMMENT T  TASK 1 -- define files needed by all programs            T
#COMMENT T  TASK 2 -- Run the COBOL driver program for Data Express  T
#COMMENT T  TASK 3 -- load output file to OMNI Vtran System          T
#COMMENT T  TASK 4 -- Sort the Message Log                           T
#COMMENT T  TASK 5 -- archive input files                            T
#COMMENT T  TASK 6 -- send files to document manager                 T
#COMMENT T  TASK 7 -- clean up and say goodbye                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U  Date: 02/04/2008  CC: 12475     By: Mike Lewis           U
#COMMENT U  Reason:  Add archiving of input files to prevent         U
#COMMENT U           duplications.                                   U
#COMMENT U                                                           U
#COMMENT U  Date: 07/08/2008  PROJ00000680  By: Zera Holladay        U
#COMMENT U  Reason: Need to replace Reflections as the file transfer U
#COMMENT U  tool for ExCCEPT.                                        U
#COMMENT U                                                           U
#COMMENT U  Date: 20110318   Proj: WMS3939  By: Paul Lewis           U
#COMMENT U  Reason: Standardize document manager calls to a function.U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
###########################   functions   ############################
######################################################################

function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function run_dataexpress_program
{
  if [ ! -s $QPFILE ]; then
    JDESC="file $QPFILE does not exist - no further action possible"
    display_message
    return 99
  else
    PNAME=DERUN
    JDESC="Running Cobol program $PNAME"
    export cond_limit=11
    execute_program
    JDESC="Program $PNAME completed successfully"
    display_message nobanner
  fi
}

function load_cards
{
  if [ ! -s $OPTRANFILE ]; then
    JDESC="file $OPTRANFILE does not exist - nothing to load"
    display_message
    return 99
  else
    JDESC="Loading $OPTRANFILE to OMNI Vtran System"
    display_message nobanner
    cp -p $OPTRANFILE $EBSCARD
    PNAME=JOBVTUT
    JPRM1=$JUSER
    JPRM2=OPTRANQP.DAT
    execute_job
    JDESC="$OPTRANFILE Loaded successfully"
    display_message nobanner
  fi
}

function sort_log
{
  if [ ! $DEMSGLOG ]; then
    JDESC="file $DEMSGLOG does not exist - nothing to sort"
    display_message
  else
    JDESC="sorting $DEMSGLOG for clarity"
    display_message nobanner
    PNAME=OPMSGSORT
    export cond_limit=11
    execute_program
    JDESC="$DEMSGLOG sorted successfully"
    display_message nobanner
  fi   
}

function archive_files
{
    for label in $archList; do
        export archDir=$XJOB
        mv $label ${label}.$rundate.$tmptime
        archive ${label}.$rundate.$tmptime
        JDESC="$archName Completed"
        display_message nobanner
    done
}

function send2DocMgr
{
  [[ ! -s $DEMSGLOG ]] &&  print "No entries found in $DEMSGLOG" >> $DEMSGLOG
  [[ ! -s $DEJOBLOG ]] &&  print "No entries found in $DEJOBLOG" >> $DEJOBLOG  
 
  JDESC="look in Visa to find pertinent logs such as $DEMSGLOG or $DEJOBLOG"
  display_message
  
  fnc_send2DocMgr

}

###########################################################################
###########################  main section  ################################
###########################################################################
export integer NumParms=$#
prog=$(basename $0)
export JUSER=$LOGNAME
export JNAME=${prog%.ksh}
export JDESC="DE Qpir Processing"
export runmode=POST
export AULOUT=$EDITOUT
export ENVFILE=ENVBATCH

############################
## source in common functions
############################
. FUNCTIONSFILE

######################
## setup generic variables
## to initial values
######################
set_generic_variables

######################
## check for required parameters
## and request for help
######################
check_input

######################
## create output directories
######################
make_output_directories
export SLOG=$XJOB/$JNAME.log
cd $XJOB

######################
### define master files
######################
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1 -- define files needed by all programs
### ******************************************************************

cd $EBSCNV || { JDESC="Could not cd to EBSCNV CIFS directory"; display_message nobanner; exit 99; }
mv qpiro.$today.txt $XJOB
cd $XJOB

export QPFILE=$XJOB/qpiro.$today.txt
export DEJOBLOG=$XJOB/${EBSRPTPFX}DEJOBLOGQP
export DEMSGLOG=$XJOB/${EBSRPTPFX}DEMSGLOGQP
export DEPGMNAME=$IBSEDE/QPIR101.EDE
export OPTRANFILE=$XJOB/OPTRANQP.DAT
export DESORTWK=$TMPDIR/DESORTWK

### ******************************************************************
### TASK 2 -- Run the COBOL driver program for Data Express
### ******************************************************************
$run_dataexpress_program && run_dataexpress_program
 
### ******************************************************************
### TASK 3 -- load output file to OMNI Vtran System
### ******************************************************************
$load_cards && load_cards

rm -f $EBSCARD/OPTRANQP.DAT

### ******************************************************************
### TASK 4 -- Sort the Message Log
### ******************************************************************
$sort_log && sort_log

### ******************************************************************
### TASK 5 -- archive input files
### ******************************************************************
$archive_files && archive_files

### ******************************************************************
### TASK 6 -- send files to document manager
### ******************************************************************
$send2DocMgr && send2DocMgr

### ******************************************************************
### TASK 7 -- clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0

