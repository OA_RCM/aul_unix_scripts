#!/opt/perl5/bin/perl

$TRX="$ENV{'TRXFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputtrx.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $TRX`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $TRX file to ODS successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $TRX file to ODS\n"; 
    }
}
