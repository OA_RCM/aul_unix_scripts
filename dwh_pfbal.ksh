#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *  CNTL-M Job Name   : RSUXDWH006              NON-CALL               *
#COMMENT *  UNIX Pointer Name : rsuxdwh006.ksh          OMNI-ASU               *
#COMMENT *  UNIX Script Name  : dwh_pfbal.ksh                                  *
#COMMENT *                                                                     *
#COMMENT *  Runtime syntax    : dwh_pfbal.ksh x                                *
#COMMENT *  Description       : Run DWH-PFBAL against specified planlist       *
#COMMENT *                      Extracts OMNI participant fund balances.       *
#COMMENT *                      x=number of plan splits to run against         *
#COMMENT *                                                                     *
#COMMENT *  Author            : AUL - Steve Loper                              *
#COMMENT *  Created           : 03/06/2012                                     *
#COMMENT *  Called by         : Scheduled or Manual                            *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       : None                                           *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : daily                                          *
#COMMENT *  Est. Run Time     : Depends on number of plans in single segment.  *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S  None                                                               S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Should be looked at by ASU support person prior to resbumission.   R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT T  Task Sequence -----------------                                    T
#COMMENT T   1  - Run Omniscript program to extract data                       T
#COMMENT T   2  - build single extract file without nulls                      T
#COMMENT T   3  - copy single extracted datafile to share directory            T
#COMMENT T LAST - Standard end-of-job processes                                T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U  Steve Loper      WMS5501   03/06/2012  original                    U
#COMMENT U                                                                     U
#COMMENT U  Steve Loper      WMS5501   04/02/2012                              U
#COMMENT U  Reason: Fix error with fnc_die: change to fnc_exit.                U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
################################################################################
#### FUNCTIONS ####### functions section ##################### SECTION #########
################################################################################
function check_input
{
  touch $XJOB/"Input_parms_are_${group}"
  fnc_log "Input_parms_are_${group}"
  typeset mno=0

  ## check for required number of input parms [1]
  [[ $NumParms -lt 1 ]] && { mno=2; check_mno; }

  ## check group input [parm 1]
  [[ $group = ALL ]] && group=1
  echo $chkSeqNum | { grep -q $group || mno=3; }
  check_mno

  ### if planlist file is not found populated, there is nothing to do - so quit.
  ls -1 $LISTDIR | grep "${split_name}_${group}_" >$planlist
  [[ -s $planlist ]] || { mno=4; check_mno; }

  ### check that each plan list file for the group is populated
  cat $planlist |&
  while read -p $fname; do
    [[ -s $LISTDIR/$fname ]] || fnc_exit "Empty plan list file! [$fname]"
  done
  check_mno
}
function check_overrides
{
   export EBSDATA=$EBSDATAovrd
   fnc_log "Derive data from: $EBSDATA"
}
function extract_data
{
  [[ $group = 1 ]] && run_all_option || run_split_option
}
function run_all_option
{
  fnc_log "Running ALL option"
  export grpNo=1 segNo=1
  export PLANLISTFILE=$LISTDIR/${split_name}_${grpNo}_${segNo}
  export DWHOUT=$XJOB/${tem_name_out}_${suffix}_${segNo}.tsv
  export JDESC="Running $OSname calculator for ALL plans"
  export sysoutfile=$XJOB/jobcalc.${grpNo}_${segNo}.out
  run_jobcalc $JDESC > $sysoutfile
}
function run_split_option
{
  fnc_log "Running split option: $group segments"
  export grpNo=$group
  cat $planlist |&
  while read -p fname; do
    export segNo=$(echo $fname|awk -F "_" '{print $5}')
    export PLANLISTFILE=$LISTDIR/$fname
    export DWHOUT=$XJOB/${tem_name_out}_${suffix}_${segNo}.tsv
    export JDESC="Running $OSname for plan list $fname"
    export sysoutfile=$XJOB/jobcalc.${grpNo}_${segNo}.out
    run_jobcalc $JDESC > $sysoutfile &
    sleep 1
  done
  fnc_log "Waiting for all submitted segments to complete processing..."
  wait  ## for segment jobs to complete
  fnc_log "Resume processing: All segments completed."
  check_exit_status_ofSegments
}
function run_jobcalc
{
   typeset PMSG="In function: run_jobcalc"
   fnc_log "Running $OSname with $PLANLISTFILE"

   export JNAME_DESC="${grpNo}_${segNo}"
   export PNAME=JOBCALC
   export JDESC="$1"
   export JPRM1=$JUSER
   export JPRM2=$OSname
   export MKOUTDIR=child
   execute_job && touch $XJOB/${JNAME}_${JNAME_DESC}.ok

   [[ -f $XJOB/${JNAME}_${JNAME_DESC}.ok ]] && compStat="OK" || compStat="ERROR"
   JDESC="Execution of $OSname for $PLANLISTFILE completed: $compStat"
   display_message nobanner
   unset PMSG JNAME_DESC
}
function check_exit_status_ofSegments
{
  typeset -i segNo=1
  typeset cond=0
  set -A segErr
  until [ $segNo -gt $group ]; do
     [[ -f $XJOB/${JNAME}_${group}_${segNo}.ok ]] || cond=1
     [[ -f $XJOB/${JNAME}_${group}_${segNo}.ok ]] || segErr[${#segErr[*]}]=$segNo
     ((segNo+=1))
  done
  [[ $cond != 0 ]] && fnc_exit "ERROR: The following segments did not end Ok! [${segErr[@]}]"
  fnc_log "All job segments ran Ok"
}
################################################################################
####   MAIN    #######   main  section   ##################### SECTION #########
################################################################################
export integer NumParms=$#
export JNAME=$(basename $0 .ksh)
export JUSER=${JUSER:-$LOGNAME}
export JDESC="ONEAMERICA DWH PFBAL Record Extraction Process"
export ENVFILE=ENVNONE
export group=$1

# standard script setup
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export planlist=$XJOB/$planlist
fnc_log_standard_start
check_input
check_overrides

cd $XJOB
##############################################
# TASK 1 - Run Omniscript program to extract data
##############################################
$extract_data && extract_data
$extract_data || fnc_log "extract_data function: OFF"

##############################################
# Task 2 - build single extract file without nulls
##############################################
$sed_and_consolidate && dwh_sed_and_consolidate $EXTFILE
$sed_and_consolidate || fnc_log "filter and consolidate function: OFF"

##############################################
# TASK 3 - copy single extracted datafile to share directory
##############################################
$copy_file && dwh_copy_file $EXTFILE
$copy_file || fnc_log "copy EXTFILE file to cifs: OFF"

##############################################
# TASK LAST - Standard end-of-job processes
##############################################
touch rs.tmp
eoj_housekeeping
return 0
