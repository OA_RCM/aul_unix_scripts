#!/opt/perl5/bin/perl

$HEBAFIL="$ENV{'HEBAFILE'}";
$PROC="$ENV{'AULPROC'}";
$JOBDIR="$ENV{'XJOB'}";
$NCFTPGET="/opt/ncftp/bin/ncftpget";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPGET -d $JOBDIR/ftpgetheb.log -f $PROC/ftpgetheb.cfg -E . $HEBAFIL`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $HEBAFIL file from 3000 successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $HEBAFIL file from 3000\n"; 
    }
}
