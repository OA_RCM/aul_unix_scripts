#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : ind_post_pvr.ksh                          *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Description  : CC12139 - SC47 Report                     *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 11/19/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE JOBCALC                         *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On demand                                 *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 15 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Cannot restream without          R
#COMMENT R                          programmer intervention          R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date:09/02/2008   SMR: WMS6259   By:  Gary Pieratt        U
#COMMENT U Reason: Original                                          U
#COMMENT U                                                           U
#COMMENT U Date: 20111214   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 1 ]] && mno=2
  check_mno
}

function generate_card
{
  echo $1
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}

prog=$(basename $0)
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/CC12139 SC47 Summary Report"
export ENVFILE=ENVBATCH

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start
JDESC="OLDCOBCODE=$OLDCOBCODE"
display_message
cd $XJOB
XJOB_save=$XJOB

export PLANLISTFILE=PVR_PLIST
fnc_get_plan_list

fnc_determine_group_sizes   ## calculate number of plans per JOBUNIC run (per deck)
export split_name=pvr_plist_split
fnc_split_plans_by_row   ## create actual card decks based on indicated method

grp=0
while (( $grp < $NUMGROUPS )) ; do
  let grp=$grp+1
  split_name_grp=$split_name$grp.$rundate
  split_name_final=${split_name}_${grp}
  mv $split_name_grp $split_name_final
done

### ******************************************************************
### TASK 1 - Create Step 1 Report and T966 cards
### ******************************************************************
planlist=planlist.txt
ls * | grep pvr_plist_split >$planlist
if [[ -s $planlist ]]; then
   grp=0
   for f in $(cat $planlist) ; do
     let grp=$grp+1
     export PLANLIST=$XJOB_save/$f
     export CC12139SC47=$XJOB_save/CC12139-SC47_${grp}.TXT
     export CC12139T966=$XJOB_save/CC12139-T966_${grp}.txt
     PNAME=JOBCALC
     JPRM1=$JUSER
     JPRM2=IND-POST-PVR1.txt
     XJOB=JOBCALC_${grp}
     mkdir $XJOB
     export JNAME=ind_post_pvr${grp}
     JDESC="Running $JOBCMPNT calculator for plan list $f..."
     display_message
     nohup $PNAME "$JPRM1" "$JPRM2" > $XJOB/${PNAME}_${grp}.out &
     sleep 1
   done

   wait

   export XJOB=$XJOB_save
   export JNAME=ind_post_pvr
   cd $XJOB
   rm -f $planlist

   grp=0
   while (( $grp < $NUMGROUPS )) ; do
     let grp=$grp+1
     export CC12139SC47=$XJOB_save/CC12139-SC47_${grp}.TXT
     export CC12139T966=$XJOB_save/CC12139-T966_${grp}.txt
     cat $CC12139SC47 >> CC12139-SC47.TXT
     cat $CC12139T966 >> CC12139-T966.txt
     rm $CC12139SC47
     rm $CC12139T966
   done
fi

### ******************************************************************
### TASK 2 - Run T966 cards created in step 1
### ******************************************************************
cp CC12139-T966.txt $EBSCARD
PNAME=JOBUNIC
JPRM1=$JUSER
JPRM2=CC12139-T966.txt
JDESC="Run JOBUNIC at $(date +%H:%M)"
display_message nobanner
execute_job
JDESC="JOBUNIC complete at $(date +%H:%M)"
display_message nobanner

### ******************************************************************
### TASK 3 - Create Final Report
### ******************************************************************
export JDESC="Call JOBCALC to create Final Report"
display_message nobanner
PNAME=JOBCALC
JPRM1=$JUSER
JPRM2=IND-POST-PVR2.txt
JDESC="Run JOBCALC $JPRM2 at $(date +%H:%M)"
display_message nobanner
execute_job
JDESC="JOBCALC $JPRM2 complete at $(date +%H:%M)"
display_message nobanner

$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 2 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
