#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxinf717.ksh            CALL            *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : FIDELITY TRANSMISSION FILE                *
#COMMENT *                 This job reads COFILE and creates two     *
#COMMENT *                 files to be transmitted to Fideltiy.      *
#COMMENT *                 VIPOUT and LATEOUT                        *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $AULPROC                                  *
#COMMENT *  Author       : P. Garza                                  *
#COMMENT *  Created      : 11/2003                                   *
#COMMENT *  Environment  : ENVGL                                     *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  : FIDFILE                                   *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S    Parameter is either supplied in full or none.          S
#COMMENT S    Has 2 parms: plan number - 6 digits, followed          S
#COMMENT S      by date in CCYYMMDD format.                          S
#COMMENT S    Either field may be set to "all" option by zeros.      S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 12/05/2003  SMR: CC       By: Patti Garza           U
#COMMENT U Reason: Initial Development.                              U
#COMMENT U                                                           U
#COMMENT U Date: 12/07/2003  SMR: CC       By: Steve Loper           U
#COMMENT U Reason: Minor changes for standards. Copy files for later U
#COMMENT U         transfer to STN and Fidelity. Add display msgs    U
#COMMENT U         and error checking.                               U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
###   function declarations                                         ##
######################################################################
function check_input 
{
  mno=0  tstuid=`echo $JUSER|tr "[:upper:]" "[:lower:]"`
  [[ $tstuid = "help" || $tstuid = "?" ]] && mno=1
  check_mno
  [[ $NumParms -lt 1 ]] && mno=2
  check_mno
}
function process_COFILE
{
 cd $XJOB
 ### check if COFILE exists in $XPDTRF yet before the move
 [[ ! -f $XPDTRF/COFILE ]] && exit 99 || mv $XPDTRF/COFILE $XJOB/COFILEBKP
 [[ ! -f $XJOB/COFILEBKP ]] && abend_error COFILE

 touch FIDFILETMP 
 touch COFILENEW
 grep -E "^.{91}ADV|^.{91}VIP" COFILEBKP > $XJOB/FIDFILETMP 
 grep -E "^.{91}VAN|^.{91}DLJ" COFILEBKP > $XJOB/COFILENEW
 sort -o $XJOB/NEWCOFIL -k .92,.95 -k .35,.41 $XJOB/FIDFILETMP
}
function abend_error
{
 fname=$1
 JDESC="ERROR: Could not copy $fname properly! (ABEND)"
 display_message nobanner
 exit 99
}
###########################################################################
###########################  main section  ################################
###########################################################################
######################
## setup variables
######################
export integer NumParms=$#
export override_date=$1
export JUSER=$LOGNAME
export JNAME=fidfile
export JDESC="FIDELITY TRADES"
export ENVFILE=ENVBATCH   

######################
### source in common functions
######################
. FUNCTIONSFILE

######################
## setup generic variables
## to initial values
######################
set_generic_variables
 
######################
## create output directories
######################
make_output_directories

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
display_message 
export JDESC="$JNAME Submitted by: $JUSER In $EBSFSET On: $(date)"
display_message nobanner

##**********************************************************************
##  TASK 1 - Pull non-Vangard Trades out of COFILE 
##*********************************************************************
process_COFILE

##**********************************************************************
##  TASK 2 - Create Fidelity trade files
##---------------------------------------------------------------------
##   READ SORTED FIDELITY TRADES (AS EXTRACTED FROM COFILE) AND 
##   GENERATE TWO OUTPUT FILES FOR TRANSMISSION TO FIDELITY             
##*********************************************************************
export PNAME=FIDFILE
export JDESC="Reads COFILE and creates file of Fidelity VIP and Late Trades"
export dd_COFILE=$XJOB/NEWCOFIL
export dd_TFILE=$AULDATA/FIDFUNDS
export dd_RPTOUT=$XRPT/FID.TRADE.RPT
export dd_RSTROUT=$XJOB/rs.tmp
export dd_LATEOUT=$XJOB/LATEOUT
export dd_VIPOUT=$XJOB/VIPOUT
execute_program

#########################################################################
##  TASK 3 - copy and send trade files
#########################################################################
cd $XJOB
cp LATEOUT $POSTOUT || abend_error LATEOUT
cp VIPOUT $POSTOUT || abend_error VIPOUT
cp LATEOUT $XPDTRF/LATEOUT.$rundate
cp VIPOUT $XPDTRF/VIPOUT.$rundate
cp COFILENEW $XPDTRF/COFILE || abend_error COFILENEW

#########################################################################
##  TASK 4 - End of job processing
#########################################################################
eoj_housekeeping
rm -f $XJOB/rsfile.txt
return 0
