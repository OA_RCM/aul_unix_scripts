#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTM Job Name  : RSUXINF009.ksh           CALL            *
#COMMENT *  SchedXref Name: rsuxinf009.ksh           OMNI-ASU        *
#COMMENT *  Unix Script   : inf_pnvLoad.ksh                          *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will use a card file created by  *
#COMMENT *                 ProNvest and call JOBVTUT in order        *
#COMMENT *                 to load the Card deck into OMNI.          *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $EBSPROC                                  *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 09/23/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBVTUT                                   *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : 7 am cycle jobs                           *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT #############################################################
#COMMENT #   Task List                                               #
#COMMENT # TASK 1 - Load transactions to OMNI                        #
#COMMENT # TASK 2 - Archive Card Files                               #
#COMMENT # TASK 3 - clean up and say goodbye                         #
#COMMENT #############################################################
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/26/2005  SMR: CC9105   By: Mike Lewis            U
#COMMENT U Reason: Created to run JOBVTUT to load ProNvest cards and U
#COMMENT U         if successful, archive files.                     U
#COMMENT U                                                           U
#COMMENT U Date: 09/24/2007  SMR: CC11234  By: Glen McPherson        U
#COMMENT U Reason: Make sure trade date equal to today's date        U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##FUNCTIONS################   functions   ############################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function copy_to_EBSCARD
{
  cd $LOADDIR
  for INFILE in *
  do
    export INFILE2=PNVTEMP
    export INFILE3=PNVTEMP2
    export JDESC="Reviewing $INFILE dates"
    display_message nobanner
    perl -pe 'next if (/^002/); next unless (/^\d{3}00/); substr($_, 41, 8) = $ENV{today};' $INFILE >$INFILE2
    perl -pe 'if (/^00201/) {substr($_, 18, 8) = $ENV{today}}' $INFILE2 >$INFILE3
    cp $INFILE3 $EBSCARD
    (load_cards)
    export JDESC="Remove $INFILE3 from $EBSCARD"
    display_message nobanner
    rm $EBSCARD/$INFILE3
  done
}

function load_cards
{
  cd $XJOB
  export JDESC="Loading card file $INFILE into Omni"
  export PNAME=JOBVTUT
  export JPRM1=$JUSER
  export JPRM2=$INFILE3
  execute_job
}
######################################################################
##########################  MAIN CODE ################################
######################################################################
export integer NumParms=$#
prog=$(basename $0)

export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Load ProNvest Cards into Omni"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

###################################
## call standard setup functions
###################################
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
. JOBDEFINE
fnc_log_standard_start

cd $XJOB
##########################################
## TASK 1 - Copy and Load Input Card Files
##########################################
$load_files && (copy_to_EBSCARD)

###################################
## TASK 2 - Archive Card Files
###################################
$archive_pnv_files && utl_archive.ksh arc_pnvLoad

## ******************************************************************
## TASK 3 - clean up and say goodbye
## ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
