#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS Job Name : RSUXFOR001                CALL            *
#COMMENT *  ECS Job Call : rsuxfor001.ksh            OMNI ASU        *
#COMMENT *  UNX Script   : for_maturity.ksh                          *
#COMMENT *                                                           *
#COMMENT *  Description  : Forfeiture Maturity; set up T404's        *
#COMMENT *                 for participants who need non-vested      *
#COMMENT *                 money forfeitured to Mr Forfeiture        *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 08/02/2006                                *
#COMMENT *  Environment  : ENVPRCE_BACK                              *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBDEFINE JOBUNIC                         *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily starting at 8:30 am                 *
#COMMENT *                (This includes weekends and holidays,      *
#COMMENT *                 however, they will be skipped because     *
#COMMENT *                 the logic below will skip them)           *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : <10 min most days; <90 min calendar       *
#COMMENT *                 quarter-ends; <4 hours on year-end        *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R  T404F folders are created on plans that have part's      R
#COMMENT R  that need forfeitures matured.  So if restart from       R
#COMMENT R  beginning, need to get rid of T404F folders.  Can also   R
#COMMENT R  be restarted from a particular or do any range of plans  R
#COMMENT R  using "ini" settings startplan and endplan. If startplan R
#COMMENT R  is left set to "", it will default to A00000. If endplan R
#COMMENT R  is left set to "", it will default to ZZZZZZ.            R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 08/02/2006  CC:9586       By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 06/12/2008  CC:12275      By: Gary Pieratt          U
#COMMENT U Reason: Mature forfeitures two months in advance          U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
##############################################################################
##  Functions
##############################################################################
function check_input
{
  return
}

function run_card
{
  PNAME=JOBUNIC
  JPRM1=$JUSER
  JPRM2=$forfcardname
  execute_job
  touch $XJOB/${forfcardname}.ok
}

##############################################################################
##  Main MAIN main Code
##############################################################################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX Forfeiture Maturity"
# Need to do Price Look back since running during the day
export ENVFILE=ENVPRCE_BACK

#####################################
## Standard script setup routines  ##
#####################################
. FUNCTIONSFILE          ## source in common functions
set_generic_variables    ## define standard variables for all scripts
check_input              ## standard command line parameter check
make_output_directories  ## make report and output directories for process
fnc_log_standard_start   ## inform user that job has begun

########################################################################
### call standard omniplus master file / environment definition script
########################################################################
. JOBDEFINE

cd $XJOB

### ******************************************************************
### TASK 1 - Create T966 cards to run calculator FOR-MATURXFER
### ******************************************************************
. propgeneric.func

if $matforf_date_set; then
   ### CC 12275 begin (set rundate two months into the future [60 days ahead]) ###
#  rdate_plus_60=$(calc_date $rundate 60)

   dayofwk=$(calc_dow $rundate)
   # WK043 will be blank if in middle of a holiday date range from DATEHOLIDAY or on a weekend
   export WK043=$(get_rundates $rundate $dayofwk $AULDATA/DATEHOLIDAY | xargs -n 1 echo | sort | head -n 1)

   # If WK043 is valued, process forfeitures for the day, otherwise, don't process
   if [[ -n "$WK043" ]]; then
      WK043=$(calc_date $WK043 60)
      export WK044=$(calc_date $(get_rundates $rundate $dayofwk $AULDATA/DATEHOLIDAY | xargs -n 1 echo | sort | tail -n 1) 60)
   else
      export matforf_create_card=false
      export runjobunic=false
   fi
   ### CC 12275 end ###
fi

### ******************************************************************
### TASK 2 - Create T966 cards to run calculator FOR-MATURXFER
### ******************************************************************

$runjobunic && rm $POSTOUT/FOR_MATURITY_EXTRACT_*

export loopcnt=0

for matforfplans in "${matforfplanlist[@]}" ; do
   let loopcnt=$loopcnt+1
   forfcardname=forf966.$loopcnt

   if $matforf_create_card; then
      startplan=`echo $matforfplans | cut -c1-6`
      endplan=`echo $matforfplans | cut -c7-12`
      typeset plan=000001
      gentemp.pl -a -v -o $XJOB/$forfcardname RUNCALC "\$date=\"$rundate\"" "\$jobname=\"FOR-MATURXFER\"" "\$planid=\"$plan\""
      print "96602         WK043=$WK043;WK044=$WK044;" >> $XJOB/$forfcardname
      print "96603         TX011='$startplan';TX012='$endplan';TX013='$loopcnt';" >> $XJOB/$forfcardname
      cp $XJOB/$forfcardname $EBSCARD
   fi

### ******************************************************************
### TASK 3 - Run T966 cards
### ******************************************************************

   if $runjobunic; then 
      JDESC="Starting job to process $forfcardname"
      display_message nobanner
      run_card &
   fi
done

### ******************************************************************
### TASK 4 - Check for successful completion(s)
### ******************************************************************

if $runjobunic; then
  JDESC="Waiting on forfeiture maturity jobs to complete"
  display_message nobanner
  wait
  num_ok_cards=$(ls | grep '.ok$' | wc -l)
  if ((num_ok_cards != loopcnt)) ; then
    ((num_failed = loopcnt - num_ok_cards))
    JDESC="$num_failed forfeiture maturity card(s) failed"
    display_message nobanner
    msg="$JDESC"
    send_job_abend_notification
    abend
  else
    JDESC="all forf cards finished ok"
    display_message nobanner
  fi
fi

### ******************************************************************
### TASK 5 - Send extracts via mailx 
### ******************************************************************

if $sendreports; then
   JDESC="Concantenating and sending reports via mailx"
   display_message nobanner
   cd $POSTOUT
   cat FOR_MATURITY*.csv >> for_maturity_extract.csv
   cat for_maturity_extract.csv | mailx -s "Forfeiture Maturity Extract" $sendperson
   cd $XJOB
fi

### ******************************************************************
### TASK 6 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0