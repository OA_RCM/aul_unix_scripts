#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS Job Name : RSUXINF751               CALL             *
#COMMENT *  UNIX Job Name: rsuxinf751.ksh                            *
#COMMENT *  Script Name  : inf_getDW.ksh                             *
#COMMENT *                                          OMNI-ASU         *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will find all SEQDWVT files and  *
#COMMENT *                 cat into one file; dwvtall.txt.           *
#COMMENT *                 Then create indicative.txt for MIR jobs   *
#COMMENT *                 From dwvtall.txt. Send indicative.txt &   *
#COMMENT *                 and SEQAHST.COMBINE.srt to aulhpux2.      *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - MSS                                 *
#COMMENT *  Created      : 12/03/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : seqdwvt.pl, seqahst.pl & rs03.pl          *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 12/03/2004  SMR: SMR????  By: Mark Slinger          U
#COMMENT U Reason: Gather all SEQDWVT files into one file.           U
#COMMENT U Date: 03/14/2006  SMR: CC9735   By: Mark Slinger          U
#COMMENT U Reason: Get only SEQDWVT since the last time this job ran.U
#COMMENT U Date: 06/01/2006  SMR: CC9840  By: Mark Slinger           U
#COMMENT U Reason: Get all DWHSRENT.TXT & DWHRETIR.TXT since the lastU
#COMMENT U         time this job ran.                                U
#COMMENT U Date: 08/01/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U Date: 09/07/2006  SMR: CC10622  By: Mark Slinger          U
#COMMENT U Reason: Fix bug in retrieving DWHSRENT & DWHRETIR         U
#COMMENT U Date: 01/29/2007  SMR: CC9268 & CC10916 By: M. Slinger    U
#COMMENT U Reason: Add an "801" to indicative.txt for any 395 recordsU
#COMMENT U         found in SEQAHST.COMBINE.srt & for any 860 recordsU
#COMMENT U         found in rs03out.txt.                             U
#COMMENT U Date: 09/25/2007  CC12197   By: Zera Holladay             U
#COMMENT U Reason: Changed insecure protocols to secure.             U
#COMMENT U Date: 10/31/2007  CC12259   By: Mark Slinger              U
#COMMENT U Reason: send files to NT                                  U
#COMMENT U Date: 11/23/2009  WMS2669   By: Mark Slinger              U
#COMMENT U Reason: Sort indicative.txt                               U
#COMMENT U Date: 06/16/2011   By: Nabin Timsina                      U
#COMMENT U Reason: The current location of indicative file was       U
#COMMENT U   $POSTOUT.  This location is not safe as the contents    U
#COMMENT U   are erased by some other jobs. So the path is now       U
#COMMENT U   changed to $TGT where the indicative file will be placedU
#COMMENT U   changes have occured only in the func cat_indicative    U
#COMMENT U Date: 04/10/2013  WMS6167   By: Rick Sica                 U
#COMMENT U Reason: dwvtall file to informatica                       U
#COMMENT U                                                           U
######################################################################
##functions##################  FUNCTIONS  ############################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function send_email_notification
{
  [[ -s $goodmsgfile ]] && fnc_notify re_success
  [[ -s $badmsgfile ]] && fnc_notify re_errors
}

function send_file_to_informatica
{
  export ODSFILE=$FILE_LOC
  $send_to_unix && ftpputods.pl
  $send_to_nt && cp $ODSFILE $TGT
}

function get_dw
{
  find_dw
  [[ -s dwvtfind.txt ]] && cat_dw || default_dw
  create_indicative
  add_to_indicative
  add_rs03
  cat_indicative_upr
  mv $POSTOUT/indicative.txt $POSTOUT/ind_pre_sort.txt
  sleep 15
  sort -u $POSTOUT/ind_pre_sort.txt > $POSTOUT/indicative.txt
  sleep 30
  copy_indicative
  rm $POSTOUT/ind_pre_sort.txt
}

function find_dw
{
  find "${dw_paths[@]}" . -name SEQDWVT -newer $EBSCTRL/dw_flag > dwvtfind.txt
}

function default_dw
{
  find $EBSOUT . -name SEQDWVT > dwvtfind.txt
  [[ -s dwvtfind.txt ]] && cat_dw || touch dwvtall.txt
}

function cat_dw
{
  while read oneline then; do
    if [[ $oneline != *.gz ]]; then
      cat $oneline >> dwvtall.txt
    fi
  done < dwvtfind.txt
}

function create_indicative
{
  cp dwvtall.txt $POSTOUT/dwvtall.txt
  sleep 30
  JDESC="Creating indicative.txt from SEQDWVT"
  PNAME=seqdwvt.pl
  JPRM1=$dwvt_loc
  execute_job
}

function add_to_indicative
{
  JDESC="Adding SEQAHST.COMBINE.srt data to indicative.txt"
  PNAME=seqahst.pl
  JPRM1=$seqahst_loc
  execute_job
}

function add_rs03
{
  JDESC="Adding rs03out data to indicative.txt"
  PNAME=rs03.pl
  JPRM1=$rs03out_loc
  execute_job
}

function copy_indicative
## put one copy in $POSTOUT for MIR scripts and a copy in
## .SrcFiles for Informatica maps to use as source.
{
  [[ $OMNIsysID = prod ]] && \
    scp $POSTOUT/indicative.txt ${Rlogin}@${Rhost}:${Rdir}
  export FILE_LOC="$indicative_loc"
  send_file_to_informatica
}

function cat_indicative_upr
{
  if [ -r $TGT/indicative.upr ]; then
    cat $TGT/indicative.upr >> $POSTOUT/indicative.txt
    sleep 15
    rm $TGT/indicative.upr
  fi
}

function get_seqahst
{
  if [ ! -r $POSTOUT/SEQAHST.COMBINE.srt ]; then
    touch $POSTOUT/SEQAHST.COMBINE.srt
    chmod 777 $POSTOUT/SEQAHST.COMBINE.srt
  fi
  [[ $OMNIsysID = prod ]] && \
    scp $POSTOUT/SEQAHST.COMBINE.srt ${Rlogin}@${Rhost}:${Rdir}
  export FILE_LOC="$seqahst_loc"
  send_file_to_informatica
  export FILE_LOC="$dwvt_loc"
  send_file_to_informatica
}

function get_dwhsrent
{
  find "${dw_paths[@]}" . -name DWHSRENT.TXT -newer $EBSCTRL/dw_flag > dwhsfind.txt
  [[ -s dwhsfind.txt ]] && cat_dwhsrent || default_dwhsrent
  if [ ! -r $POSTOUT/dwhsrent.txt ]; then
    touch $POSTOUT/dwhsrent.txt
    chmod 777 $POSTOUT/dwhsrent.txt
  fi
  export FILE_LOC="$dwhsrent_loc"
  send_file_to_informatica
}

function cat_dwhsrent
{
  while read oneline then; do
    if [[ $oneline != *.gz ]]; then
      cat $oneline >> dwhsrent.txt
      cp dwhsrent.txt $POSTOUT/dwhsrent.txt
    fi
  done < dwhsfind.txt
}

function default_dwhsrent
{
  find $EBSOUT . -name DWHSRENT.TXT > dwhsfind.txt
  [[ -s dwhsfind.txt ]] && cat_dwhsrent || touch dwhsrent.txt
  cp dwhsrent.txt $POSTOUT/dwhsrent.txt
}

function get_dwhretir
{
  find "${dw_paths[@]}" . -name DWHRETIR.TXT -newer $EBSCTRL/dw_flag > dwhrfind.txt
  [[ -s dwhrfind.txt ]] && cat_dwhretir || default_dwhretir
  if [ ! -r $POSTOUT/dwhretir.txt ]; then
    touch $POSTOUT/dwhretir.txt
    chmod 777 $POSTOUT/dwhretir
  fi
  export FILE_LOC="$dwhretir_loc"
  send_file_to_informatica
}

function cat_dwhretir
{
  while read oneline then; do
    if [[ $oneline != *.gz ]]; then
      cat $oneline >> dwhretir.txt
      cp dwhretir.txt $POSTOUT/dwhretir.txt
    fi
  done < dwhrfind.txt
}

function default_dwhretir
{
  find $EBSOUT . -name DWHRETIR.TXT > dwhrfind.txt
  [[ -s dwhrfind.txt ]] && cat_dwhretir || touch dwhretir.txt
  cp dwhretir.txt $POSTOUT/dwhretir.txt
}

######################################################################
##main##################  MAIN SECTION  ##############################
######################################################################

### ******************************************************************
### define job variables
### ******************************************************************
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Gathering all deferred work files into one file"
export ENVFILE=ENVBATCH

### ******************************************************************
### source in common functions
### ******************************************************************
. FUNCTIONSFILE

### ******************************************************************
### setup generic variables to initial values
### ******************************************************************
set_generic_variables

### ******************************************************************
### check for required parameters and request for help
### ******************************************************************
check_input

### ******************************************************************
### create output directories
### ******************************************************************
make_output_directories
export goodmsgfile=$XJOB/good_messages
export badmsgfile=$XJOB/bad_messages
export AULOUT=$POSTOUT

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

### ******************************************************************
### log job startup
### ******************************************************************
fnc_log_standard_start

### ******************************************************************
### Step 1 - prep work
### ******************************************************************
cd $XJOB
rm $POSTOUT/indicative.txt
rm $POSTOUT/dwvtall.txt

### ******************************************************************
### Step 2 - a)find desired SEQDWVT files and create indicative.txt
###          b)add "801" record to indicative.txt for any 395 records
###            found in SEQAHST.COMBINE.srt
###          c)add "801" record to indicative.txt for any 860 records
###            found in rs03out.txt
### ******************************************************************
$get_DW && get_dw

### ******************************************************************
### Step 3 - get copy of SEQAHST.COMBINE.srt
### ******************************************************************
$get_SEQAHST && get_seqahst

### ******************************************************************
### Step 4 - reset dw_flag to today
### ******************************************************************
touch $EBSCTRL/dw_flag

### ******************************************************************
### Step 5 - get DWHSRENT.TXT
### ******************************************************************
$get_DWHSRENT && get_dwhsrent

### ******************************************************************
### Step 6 - get DWHRETIR.TXT
### ******************************************************************
$get_DWHRETIR && get_dwhretir

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
