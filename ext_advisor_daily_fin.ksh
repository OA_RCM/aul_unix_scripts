#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXEXT716              NOCALL            *
#COMMENT *  Script Name  : ext_advisor_daily_fin.ksh                 *
#COMMENT *                                                           *
#COMMENT *  Description  : A daily financial extract for Advisors    *
#COMMENT *                 according to PM401 and ini                *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 11/02/2012                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxext716.ksh                            *
#COMMENT *  Script Calls : JOBCALC                                   *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily Business Days                       *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : under 2 hours but varies                  *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must contact ASU personnel       R
#COMMENT R                          before restart.                  R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 11/02/2012  Proj:WMS8565   By: Rick Sica            U
#COMMENT U Reason: Initial creation.                                 U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export JNAME=${prog%.ksh}
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:=01}
export ENVFILE=ENVBATCH

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables

######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input

make_output_directories
export MKOUTDIR=current     ### rml - had to add this so that the JOBCALC is ran in same output directory.

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### Task 1: Create input file of Advisors from ini setting
### ******************************************************************
export AdvisorListFile=$XJOB/PM401LIST
echo $AdvisorsList > $AdvisorListFile

### ******************************************************************
### Task 2: Run OmniScript to create extract files by Advisor
### ******************************************************************

if $runprocess ; then
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=EXT-ADVISOR-DAILY-FIN.txt
  JDESC="Running $PNAME for $JPRM2"
  execute_job
fi

### ******************************************************************
### Task 3: Move files to Transfer out area
### ******************************************************************

if $copyfilesout ; then
  mkdir -p $ADVISOROUT
  cp *.TXT $ADVISOROUT
fi

### ******************************************************************
### Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0
