#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxinf711.ksh            CALL            *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to send the deferred work file from   *
#COMMENT *                 JOBUNIF to the Power Image System         *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 07/23/2001                                *
#COMMENT *  Environment  : ENVINTF                                   *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : setpath omniprof MKOUTDIR                 *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : 4 times a day                             *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 07/23/2001  SMR: PEN????  By: Mike Lucas            U
#COMMENT U Reason: Created to send deferred work file to Power Image U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

### ******************************************************************
### define job variables
### ******************************************************************
  SLOG=rsuxinf711.log;       			export SLOG
  SPROG=rsuxinf711.ksh;		       		export SPROG
  JUSER=$LOGNAME;                               export JUSER
  tmptime=`date +%H%M%S`;                       export tmptime
  today=`date +%Y%m%d`;                         export today
  JNAME=inf711;                                 export JNAME
  ENVFILE=ENVINTF;                              export ENVFILE

### ******************************************************************
### set up the environment 
### ******************************************************************
  . /omniplus/setpath.txt
  . /omniplus/omniprof.520it

### ******************************************************************
### create output directories
### ******************************************************************
  export AULOUT=$EDITOUT
  . MKOUTDIR
  cd $XJOB

print "***************************************************************"
print "*    $SPROG Submitted by:" $JUSER "On:" $(date)
print "***************************************************************"
print "$SPROG Starting at " $tmptime > $XJOB/$SLOG

### ******************************************************************
### send deferred work file to Power Image via ncftp  
### ******************************************************************
     export DWFILE=$XJOB/DEFWORKFILE_"$today"_"$tmptime"
     cp $AULOUT/SEQDWVT $DWFILE
      ftpputdwf.pl                                     

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
tmptime=`date +%H%M%S`
print "$SPROG Ending at " $tmptime >> $XJOB/$SLOG
return 0  
