#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM Job name:   RSUXINF745               NO-CALL       *
#COMMENT *  Sched Job name:   rsuxinf745.ksh           OMNI-ASU      *
#COMMENT *  UNIX Script name: inf_annprosp.ksh                       *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will create the Annual/Semi-     *
#COMMENT *                 Annual Prospectus files.                  *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 08/09/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : INF1002R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : 5th day of each Month, AM job.            *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 2 hours; depending on # of plans &        *
#COMMENT *                 participants                              *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :   None                            S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time.      R
#COMMENT R                          This is only a report.           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 11/05/2003  CC: 4712      By: Rick Sica             U
#COMMENT U Reason: Original                                          U
#COMMENT U Date: 03/12/2004  SMR: CC5940   By: Mike Lewis            U
#COMMENT U Reason: Mods to LAN dirs forced C:D destination change.   U
#COMMENT U Date: 03/15/2004  SMR: CC5940   By: Mike Lewis            U
#COMMENT U Reason: More Mods to LAN directories for C:D.             U
#COMMENT U Date: 06/28/2004  SMR: CC6677   By: Mike Lewis            U
#COMMENT U Reason: Bring up to current standards. Use .ini file.     U
#COMMENT U Date: 08/01/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U Date: 12/21/2006  SMR: CC10951  By: Gary Pieratt          U
#COMMENT U Reason: Copy EXTRACT.PAPER to POSTOUT                     U
#COMMENT U Date: 09/13/2007   CC: 11951    By: Steve Loper           U
#COMMENT U Reason: Modify for file transfer - change in target.      U
#COMMENT U Reason: Utilize new enterprise CM/AFT transfer process.   U
#COMMENT U Reason: Obsolete and remove all transfer logic.           U
#COMMENT U Reason: Obsolete and remove all email notification logic. U
#COMMENT U Reason: cc10951 requirements now obsolete. Remove code.   U
#COMMENT U Reason: "A"nnual or "S"emi designation not applicable.    U
#COMMENT U Date: 02/21/2008  SMR: CC12580  By: Judy Stewart          U
#COMMENT U Reason: Replace investment table file                     U
#COMMENT U Date: 20110318   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U Date: 20130730   Proj:  WMS8043        By: Rick Sica      U
#COMMENT U 2 to 4 char inv id                                        U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function local_error_handling
{
  ## skl 11951 remove prior code since this is called via generic routine
  ## skl 11951 now for future use
  return 0
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
######################
## setup variables
######################
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Omni Annual/Semi-Annual Prospectus File Interface"
export ENVFILE=ENVBATCH

###############################
### standard script startup
###############################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
check_input
. JOBDEFINE
fnc_log_standard_start

cd $XJOB
### ******************************************************************
### TASK 1 - Run program to create prospectus reports
### ******************************************************************
PNAME=INF1110E
JDESC="Program for creating Annual/Semi-Annual Prospectus Files 02.21.2008 version"
export dd_RSTROUT=$XJOB/rs.tmp

## type extract mapped to physical file name
export EXTPAPER=EXTRACT.PAPER
export EXTCD=EXTRACT.CD
export ERRORS=PROSPECTUS.ERRORS
export SUMMARY=PROSPECTUS.SUMMARY
export LISTPAPER=LISTING.PAPER
export LISTCD=LISTING.CD
export INTERNET=LISTING.INTERNET
export DEFINITIONS=ID.DEFINITIONS
export BATCHCOUNT=BATCH.COUNT

## select id mapped to fully qualified physical member
## these members get transfered to remote location based on
## CM/FTP job setup by PCD group
export dd_ANNPROSEXTPAPER=$XFERLOC/$EXTPAPER
export dd_ANNPROSEXTCD=$XFERLOC/$EXTCD
export dd_ANNPROSERRORS=$XFERLOC/$ERRORS
export dd_ANNPROSSUMMARY=$XFERLOC/$SUMMARY
export dd_ANNPROSLISTPAPER=$XFERLOC/$LISTPAPER
export dd_ANNPROSLISTCD=$XFERLOC/$LISTCD
export dd_ANNPROSINTERNET=$XFERLOC/$INTERNET
export dd_ANNPROSDEFINITIONS=$XFERLOC/$DEFINITIONS
export dd_ANNPROSBATCHCOUNT=$XFERLOC/$BATCHCOUNT

export dd_INVTABLE=$AULDATA/investment_tbl.txt
export dd_EXCLINV=$AULDATA/EXCLINV
execute_program

### ******************************************************************
### TASK 2 - Send specified files to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK LAST - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
