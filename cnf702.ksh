#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxcnf702.ksh            CALL            *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce participant         *
#COMMENT *                 deposit confirmations                     *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 01/30/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : setpath omniprof MKOUTDIR JOBDEFINE       *
#COMMENT *  Script Calls : rsux00120d.ksh                            *
#COMMENT *  COBOL Calls  : CNF1003R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 30 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2000  SMR: PEN 3312 By: Mike Lucas            U
#COMMENT U Reason: Created to run the program CNF1003R               U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2001  SMR: ******** By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U                                                           U
#COMMENT U Date: 04/29/2002  SMR: CC2944   By: Mike Lewis            U
#COMMENT U Reason: Breakout to individual CD script.                 U
#COMMENT U                                                           U
#COMMENT U Date: 08/03/2006  SMR: CC8403   By: Steve Loper           U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

### ******************************************************************
### define job variables
### ******************************************************************
  CPROG=CNF1003R;     				export CPROG
  SLOG=rsuxcnf702.log;				export SLOG
  SPROG=rsuxcnf702.ksh;				export SPROG
  JUSER=$LOGNAME;                               export JUSER
  tmptime=`date +%H%M%S`;                       export tmptime
  today=`date +%Y%m%d`;                         export today
  JNAME=cnf702;                                 export JNAME
  ENVFILE=ENVBATCH;                             export ENVFILE
                                            
### ******************************************************************
### set up the environment
### ******************************************************************
#dir=$(cd $(dirname $0); pwd)
#. $dir/omniprof.txt

#  . /omni_prod/setpath.txt
#  . /omni_prod/omniprof.520prod
  export rundate=`cat $AULDATA/DATEDAILY`

### ******************************************************************
### create output directories
### ******************************************************************
export AULOUT=$POSTOUT
. MKOUTDIR

print "***************************************************************"
print "*    $SPROG Submitted by:" $JUSER "On:" $(date)
print "***************************************************************"
print "$SPROG Starting at " $tmptime > $XJOB/$SLOG

### ******************************************************************
### call standard omni_prod master file / environment definition script
### ******************************************************************
print "call JOBDEFINE" >> $XJOB/$SLOG
  . JOBDEFINE

### ******************************************************************
### define files needed by Sungard I/O routines
### ******************************************************************
print "define Sungard local files..." >> $XJOB/$SLOG
  cp $EBSCTRL/DUMBTRAN.DAT $XJOB
  cp $EBSCTRL/DUMBTRN2.DAT $XJOB
  touch $XJOB/DUMMY3.DEF
  dd_RSTROUT=$XJOB/rs.tmp;               export dd_RSTROUT
  dd_TRANIN=$XJOB/DUMBTRAN.DAT;          export dd_TRANIN
  dd_TRANOUT=$XJOB/DUMBTRN2.DAT;         export dd_TRANOUT

### ******************************************************************
### define files needed by AUL programs
### ******************************************************************
print "define AUL local files..." >> $XJOB/$SLOG
  dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX;     export dd_AGTPLCYIDX
  dd_AGTADDRIDX=$AULDATA/AGTADDRIDX;     export dd_AGTADDRIDX
  dd_DEPOSITS=$AULOUT/deposits.txt;      export dd_DEPOSITS
  dd_DEPCONF=$XJOB/DEPCONF;              export dd_DEPCONF
  dd_ERROR=$XJOB/${EBSRPTPFX}DEPCONFERR; export dd_ERROR

### ******************************************************************
### take a peek at the environment variables
### ******************************************************************
###  env | sort

### ******************************************************************
### Run the COBOL program to produce deposit confirmations
### ******************************************************************
print "run COBOL program $CPROG" >> $XJOB/$SLOG
  $COBRUN $CPROG > $XJOB/$CPROG.log 2> $XJOB/$CPROG.err

### ******************************************************************
### check job status
### ******************************************************************
  cond=$?
  if [ $cond -gt 4 ]
  then
     print "$SPROG Abended with cond=" $cond >> $XJOB/$SLOG
     print "***************************************"
     print "**                                   **"
     print "**   $SPROG Ended Abnormally         **"
     print "**           Error Code = $cond      **"
     print "**                                   **"
     print "***************************************"
     return $cond
  fi

### ******************************************************************
### send Deposit Report files to IBM via connect direct
### ******************************************************************
  export REPORT1=$XJOB/DEPCONF
  export ERRRPT1=$XJOB/${EBSRPTPFX}DEPCONFERR
  rsux00120d.ksh


### ******************************************************************
### clean up and say goodbye
### ******************************************************************
  rm  $XJOB/*.tmp
  rm  $XJOB/DUM*
  print "Run of COBOL Program $CPROG Complete..."
  cp $XJOB/DEPCONF $AULOUT
  cp $XJOB/${EBSRPTPFX}DEPCONFERR $AULOUT
  tmptime=`date +%H%M%S`
print "$SPROG Ending at " $tmptime >> $XJOB/$SLOG
return 0
