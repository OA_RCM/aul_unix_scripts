#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXCNF702                CALL            *
#COMMENT *  UNIX POINTER : rsuxcnf702.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : cnf_deposits.ksh                          *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce participant         *
#COMMENT *                 deposit confirmations                     *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 10/08/2001                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : CNF1003R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  30 min                                   *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE -------------------------------------------T
#COMMENT T  TASK 1 - Define files needed by Sungard and AUL routines.T
#COMMENT T  TASK 2 - Run the COBOL program to produce deposit        T
#COMMENT T           confirmations.                                  T
#COMMENT T  TASK 3 - Prepare files for transfer to IBM.              T
#COMMENT T  TASK 4 - Send reports to document manager                T
#COMMENT T  TASK 5 - clean up and say goodbye.                       T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2000  SMR: PEN 3312 By: Mike Lucas            U
#COMMENT U Reason: Created to run the program CNF1003R               U
#COMMENT U                                                           U
#COMMENT U Date: 08/21/2001  SMR: ******** By: Mike Lewis            U
#COMMENT U Reason: Standardization of scripts                        U
#COMMENT U                                                           U
#COMMENT U Date: 04/29/2002  SMR: CC2944   By: Mike Lewis            U
#COMMENT U Reason: Breakout to individual CD script.                 U
#COMMENT U                                                           U
#COMMENT U Date: 08/03/2006  SMR: CC8403   By: Steve Loper           U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 07/18/2007  SMR: CC11966  By: Mike Lewis            U
#COMMENT U Reason: Convert from C:D to CM/AFT & standardize.         U
#COMMENT U                                                           U
#COMMENT U Date: 04/16/2009 WMS: 1943      By: Gary Pieratt          U
#COMMENT U Reason: Vista-only confirmations                          U
#COMMENT U                                                           U
#COMMENT U Date: 20110317  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
########################  functions  #################################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function run_program
{
  cd $XJOB
  PNAME=CNF1003R
  JDESC="Run $PNAME to produce deposit confirmations"
  execute_program
}
function prepare_files_for_transfer
{
  JDESC="Preparing Deposit Confirmations for Transfer"
  display_message nobanner

### WMS 1943 begin
  export FRFILE=$1
  export V=$2
  create_file 
### WMS 1943 end

  cp $XJOB/$CDFILE $AULOUT

  if [[ -s $FRFILE ]]
  then
    JDESC="Copying Deposit Confirmation report to $AULOUT for ftp job "
    display_message nobanner
    ##cat $AULDATA/jcl/$JCL >> $XJOB/$CDFILE   ### Not needed by ControlM ...rml
    cat $XJOB/$FRFILE >> $XJOB/$CDFILE
    cp $XJOB/$CDFILE $AULOUT
    if [[ -s $AULOUT/$CDFILE ]]
    then
      JDESC="Copied $CDFILE to $AULOUT"
      display_message nobanner
    else
      JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
      display_message nobanner
      exit 99
    fi
  else
    JDESC="No Deposit Confirmation report created -- ftp job using empty $CDFILE "
    display_message nobanner
  fi
}
function create_file
{
  case $EBSFSET in
    "prod") JCLFILE=RSPP120D
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ120D
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE="${PFX}.$JCLFILE"

### WMS 1943 begin
  if $V ; then
     CDFILE="${CDFILE}V"
  fi
### WMS 1943 end

  rm -f $XJOB/$CDFILE
  touch $XJOB/$CDFILE
}
function end_of_job
{
  cd $XJOB
  eoj_housekeeping
  copy_to_masterlog
  create_html
}
######################################################################
##MAIN##################  main code  #####################SECTION#####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Printing Participant Deposit Confirmations"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard job setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

cd $XJOB
#######################################################################
### TASK 1 - Define files needed by Sungard and AUL routines
#######################################################################
cp $EBSCTRL/DUMBTRAN.DAT $XJOB
cp $EBSCTRL/DUMBTRN2.DAT $XJOB
touch $XJOB/DUMMY3.DEF
export dd_RSTROUT=$XJOB/rs.tmp
export dd_TRANIN=$XJOB/DUMBTRAN.DAT
export dd_TRANOUT=$XJOB/DUMBTRN2.DAT
export dd_AGTPLCYIDX=$AULDATA/AGTPLCYIDX
export dd_AGTADDRIDX=$AULDATA/AGTADDRIDX
if [ "$JSTN" = "yes" ]; then
  export dd_DEPOSITS=$AULOUT/deposits.${JUSER}
else
  export dd_DEPOSITS=$AULOUT/deposits.txt
fi

export dd_DEPCONF=DEPCONF
### WMS 1943 begin
export dd_DEPCONFV=DEPCONFV
### WMS 1943 end
export dd_ERROR=${EBSRPTPFX}DEPCONFERR

######################################################################
### TASK 2 - Run the COBOL program to produce deposit confirmations
######################################################################

$run_program && run_program

######################################################################
### TASK 3 - Prepare files for transfer to IBM
######################################################################

### WMS 1943 begin
$prepare_files && prepare_files_for_transfer $dd_DEPCONF false
$prepare_files && prepare_files_for_transfer $dd_DEPCONFV true
### WMS 1943 end

######################################################################
### TASK 4 - Send reports to document manager.
######################################################################

$send2DocMgr && fnc_send2DocMgr

################################################################
### TASK 5 - clean up and say goodbye
################################################################
end_of_job
return 0
