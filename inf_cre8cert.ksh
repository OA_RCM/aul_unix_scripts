#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : RSUXINF750              NOCALL            *
#COMMENT *  Script Name  : inf_cre8cert.ksh                          *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will produce the OMNI Registered *
#COMMENT *                 business certificates.                    *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RML                                 *
#COMMENT *  Created      : 02/24/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : rsuxinf750.ksh                            *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : INFCERT, RPT1150R.CBL                     *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily                                     *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must contact ASU personnel       R
#COMMENT R                          before restart.                  R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 02/25/2005  SMR: CC7715   By: Mike Lewis            U
#COMMENT U Reason: Initial creation.                                 U
#COMMENT U                                                           U
#COMMENT U Date: 05/05/2010  WMS00002525 By: Joe Young               U
#COMMENT U Reason: DLS to xPression conversion                       U
#COMMENT U                                                           U
#COMMENT U Date: 06/21/2010  SMR: WMS04459 By: Louis Blanchette      U
#COMMENT U Reason: Added validation for Expected Certificates        U
#COMMENT U                                                           U
#COMMENT U Date: 20110318   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function email_warning_msg
{
if grep -q "NO DIFFERENCE" $dd_VALCERTOUT ; then
  echo 'NO DIFFERENCE'
else
  PMSG="function email_warning_msg"
  cat $dd_VALCERTOUT | mailx -s "[$EBSFSET]:EXPECTED CERTIFICATE REPORT !" $CERTNOTIFY
  JDESC="Completed function to email warning message(s)!"
  display_message nobanner
  unset PMSG
fi  
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export JNAME=${prog%.ksh}
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:=01}
export JDESC="OMNI Registered Business Certificate Creation"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### source in common functions
######################
. FUNCTIONSFILE
set_generic_variables
 
######################
### check for -options and
### required parameters
### and request for help
######################
#fnc_check_options
check_input 

make_output_directories

######################
### Override daily date and rundate 
### if one is passed in
######################
fnc_set_daily_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Gather up and merge control files.                                  
### ******************************************************************

  export dd_CERTIN=$XJOB/certin.txt
  touch $dd_CERTIN
  cp $AULOUT/$processed_file $XJOB
  cat $XJOB/$processed_file >> $dd_CERTIN
  mv $wizard_loc/rpcert* $XJOB
  cat $XJOB/rpcert* >> $dd_CERTIN  
  sort certin.txt > tempcert.txt 
  mv tempcert.txt certin.txt

### ******************************************************************
### TASK 2 - Run the COBOL program to produce Certificates           
### ******************************************************************
### ******************************************************************
### define files needed by Sungard I/O routines
### ******************************************************************
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_CERTOUT=$XJOB/certout.txt 

  if [[ "$JSTN" = "YES" ]]; then
    export dd_RSTRIN=$AULOUT/rsfile.${JUSER}
  else
    export dd_RSTRIN=$AULOUT/rsfile.txt
  fi

  PNAME=INFCERT 
  JDESC="run COBOL program $PNAME"
  execute_program

### ******************************************************************
### TASK 3 - Place certificates into proper transfer directory.          
### ******************************************************************
  cp $dd_CERTOUT $cert_pickup_dir
  cp $dd_CERTOUT $infa_src_dir

### ******************************************************************
### TASK 4 - Run the COBOL program to get Expected Certificates           
### ******************************************************************
  PMSG="Get Expected Certificates"
  JDESC="Starting $PMSG - using ODBC"
  display_message nobanner

  export COBSAVE=$COBRUN
  export COBRUN=$(dirname $COBRUN)/$AUL_RTS
  
  export dd_VALCERTOUT=$XJOB/${EBSRPTPFX}ValidCertout.txt 
  export ODSDSN="ODS"
  export BWIZDSN="BATCH_WIZARD"
  PNAME=RPT1150R 
  JDESC="run COBOL program $PNAME"
  execute_program
  
  export COBRUN=$COBSAVE

  JDESC="Completed $PMSG - using ODBC"
  display_message nobanner
  unset PMSG

### ******************************************************************
### TASK 5 - Email differences if any
### ******************************************************************  
$email_warning_msg && email_warning_msg

### ******************************************************************
### TASK 6 - Send difference report to document manager
### ******************************************************************  
$send2DocMgr && fnc_send2DocMgr
  
### ******************************************************************
### Clean up and say goodbye
### ******************************************************************
JDESC="Process complete !"
display_message nobanner

eoj_housekeeping
return 0
