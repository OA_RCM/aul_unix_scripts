#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  ECS Job Name : RSUXINF752               CALL             *
#COMMENT *  UNIX Job Name: rsuxinf752.ksh                            *
#COMMENT *  Script Name  : inf_getstmt.ksh                           *
#COMMENT *                                          OMNI-ASU         *
#COMMENT *                                                           *
#COMMENT *  Description  : Retrieves Stmt_Option_Categories.xls &    *
#COMMENT *                 Stmt_Investment_Types.xls from $AULDATA   *
#COMMENT *                 and places them in Informatica SrcFiles   *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Author       : AUL - MSS                                 *
#COMMENT *  Created      : 09/22/2005                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls :                                           *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 1 min                                     *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must check with Application      R
#COMMENT R                          support personnel before restart.R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/22/2005  SMR: CC9066   By: Mark Slinger          U
#COMMENT U Reason: Get statement investment data files.              U
#COMMENT U                                                           U
#COMMENT U Date: 08/01/2006  SMR: CC8403   By: Mike Lewis            U
#COMMENT U Reason: Changes required to work in OMNI Env model 2.     U
#COMMENT U                                                           U
#COMMENT U Date: 10/26/2007  CC12259  By: Mark Slinger               U
#COMMENT U Reason: Send files to NT server                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##functions##################  FUNCTIONS  ############################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function send_email_notification
{
  [[ -s $goodmsgfile ]] && fnc_notify re_success
  [[ -s $badmsgfile ]] && fnc_notify re_errors
}
# Send file to Informatica can be turned off in the ini file
function send_file_to_informatica
{
  export ODSFILE=$STMT_FILE
  [[ $OMNICUST = "omnihop" ]] && ftphopods.pl || ftpputods.pl
}
# Send file to NT can be turned off in the ini file
function send_file_to_nt_informatica
{
  cond=0
  JDESC="Copying $STMT_FILE to shared path"
  display_message nobanner
  case $EBSFSET in
    "hop")
      cp $STMT_FILE $HOPTGT || cond=98
    ;;
    *)
      cp $STMT_FILE $TGT || cond=99
    ;;
  esac
  case $cond in
    "0")
      JDESC="Sucessfully sent file $STMT_FILE to NT"
      display_message nobanner
    ;;
    "98")
      JDESC="ERROR - $JDESC (ABORT!) $HOPTGT"
      display_message nobanner
      return $cond
    ;;
    "99")
      JDESC="ERROR - $JDESC (ABORT!) $TGT"
      display_message nobanner
      return $cond
    ;;
  esac
  display_message nobanner
}
######################################################################
##main##################  MAIN SECTION  ##############################
######################################################################

### ******************************************************************
### define job variables
### ******************************************************************
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Getting statement investment info files from $AULDATA"
export ENVFILE=ENVBATCH

### ******************************************************************
### source in common functions
### ******************************************************************
. FUNCTIONSFILE

### ******************************************************************
## setup generic variables to initial values
### ******************************************************************
set_generic_variables

### ******************************************************************
### check for required parameters and request for help
### ******************************************************************
check_input

### ******************************************************************
## create output directories
### ******************************************************************
make_output_directories
export goodmsgfile=$XJOB/good_messages
export badmsgfile=$XJOB/bad_messages

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

### ******************************************************************
### log job startup
### ******************************************************************
fnc_log_standard_start
cd $XJOB

### ******************************************************************
### Put $AULDATA Stmt files to Informatica SrcFiles location
### ******************************************************************
# send Stmt_Investment_Types.prn
export STMT_FILE="$stmt1_loc"
$send_to_unix && send_file_to_informatica
$send_to_nt && send_file_to_nt_informatica
# send Stmt_Option_Categories.prn
export STMT_FILE="$stmt2_loc"
$send_to_unix && send_file_to_informatica
$send_to_nt && send_file_to_nt_informatica

### ******************************************************************
### clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
