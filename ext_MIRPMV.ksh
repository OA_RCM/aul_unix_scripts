#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *  CTM  JOB NAME     : RSUXEXT200                       CALL          *
#COMMENT *  UNIX SYMBOLIC LINK: rsuxext200.ksh                   OMNI-ASU      *
#COMMENT *  UNIX SCRIPT NAME  : ext_MIRPMV.ksh                                 *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run MIR-PMV extract                            *
#COMMENT *                       (internal omniscript version )                *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.20ff                           *
#COMMENT *  Resides           : Found based on PATH                            *
#COMMENT *  Author            : AUL - Mark Slinger                             *
#COMMENT *  Created           : 08/20/2007                                     *
#COMMENT *  Environment       : ENVBASE                                        *
#COMMENT *  Called by         : Manually or as scheduled                       *
#COMMENT *  Script Calls      : FUNCTIONSFILE, JOBDEFINE, [sendfile.ksh]       *
#COMMENT *  COBOL Calls       : None directly.                                 *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : Every Day                                      *
#COMMENT *  Est. Run Time     : Up to 3 hours in production                    *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R  Can be restarted anytime - MIR extract only.                       R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE ---------------------------------------------------- T
#COMMENT T    1  - create 966 card for execution                               T
#COMMENT T    2  - execute 966 card just created                               T
#COMMENT T    3  - send mirpmv.txt to shared path (NT server)                  T
#COMMENT T  LAST - end of job cleanup routine                                  T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information : N/A                                         P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U Update History:                                                     U
#COMMENT U Date: 08/20/2007   CC: 12077   By: Mark Slinger                     U
#COMMENT U Initial version. Uses internal omniscript MIR-PMV. Alternative      U
#COMMENT U method to running JOBCALC with external omniscript.                 U
#COMMENT U                                                                     U
#COMMENT U Date: 11/08/2007   CC: 12259   By: Mark Slinger                     U
#COMMENT U Reason: Send mirpmv.txt to NT server for Informatica                U
#COMMENT U                                                                     U
#COMMENT U Date: 04/28/2008   CC: 12762   By: Christy Wieringa                 U
#COMMENT U Reason: Updated to run job in multiple threads to decrease overall  U
#COMMENT U         run time                                                    U
#COMMENT U                                                                     U
#COMMENT U Date:09/02/2008    wms00001040 By: Christy Wieringa                 U
#COMMENT U Reason:  Add error handling.  fnc_process_T966_cards will create    U
#COMMENT U          .OK files.  Verify number of .OK files = number of splits. U
#COMMENT U          If one or more .OK files is missing, Abend and display     U
#COMMENT U          message "One or more segments failed".                     U 
#COMMENT U                                                                     U
#COMMENT U Date: 03/06/2012   WMS: 5501   By: Steve Loper                      U
#COMMENT U Reason: Add code to copy ODSFILE to additional CPATH location.      U
#COMMENT U         CPATH depends on OMNI region - see ini file for setting.    U
#COMMENT U                                                                     U
#COMMENT U Date: 03/26/2012   WMS: 5501   By: Steve Loper                      U
#COMMENT U Reason: Allow for record split based on HOP/Prod plan ID. (dev)     U
#COMMENT U Reason: Copy HOP file to HOP test cifs location (dev only).         U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
################################################################################
###FUNCTION################# function declarations #################SECTION#####
################################################################################
##*********************************************************
## check script input parameters if applicable
##*********************************************************
function check_input
{
  return 0
}
function check_overrides
{
  export JPOST=N
}
function transfer_file
{
  fname=$1
  PMSG="function $0"
  [[ -z $fname ]] && fnc_exit "No filename provided to transfer_file function "
  
  fromfile=$XJOB/$fname
  tofile="${cd_target_loc}$fname.$rundate"

  fnc_log "Call sendfile to build/execute transfer script to transfer file [$fname]"
  PNAME=sendfile.ksh
  JDESC="Build/Execute transfer script to send $fromfile to $tofile"
  JPRM1=$jobExt
  JPRM2=$fromfile
  JPRM3=$tofile
  execute_job
  fnc_log "Sendfile process returned Ok"
}
function send_file_to_nt_informatica
{
  copy_to_cifs $ODSFILE $TARGET
  #-- Prod plan recs to prod, HOP plan recs to HOP (dev only)
  #-- Only set HOPtestCIFS location if intend to split and deliver records there.
  [[ -n $HOPtestCIFS ]] && $split_function
  [[ -z $HOPtestCIFS ]] && fnc_log "Skip function: $split_function"
  [[ -z $HOPtestCIFS ]] && copy_to_cifs $ODSFILE $CPATH
}
function split_planlist
{
  export PMSG="function $0"
  fnc_log "Executing function split_planlist"
  
  PLANLISTFILE=$XJOB/$PLANLISTFILE
  fnc_get_plan_list
  fnc_determine_group_sizes   ## calculate number of plans per JOBUNIC run (per deck)
  fnc_split_plans_by_row      ## create actual card decks based on indicated method
  [[ $? != 0 ]] && fnc_exit "ERROR creating T966 assign cards!"
}
  function run_T966_card_decks
{
  ####-----------------------------------------
  #### run cards in background for each group
  ####-----------------------------------------
  export integer segment=1
  while (( $segment <= $NUMGROUPS ))
  do
    export MIRINDIR=$XJOB
    $create_t966_cards && fnc_create_t966_cards $templName $omniRxPfx
    echo 96602 '   0    TX100="$MIRINDIR/'${split_name}$segment.$rundate'" ;' >> $EBSCARD/$templName.$rundate
    echo 96603 '   0    TX101="$EBSXFER/mirpmv/mirpmv.'$segment'"; ' >> $EBSCARD/$templName.$rundate
    
    cp $EBSCARD/$templName.$rundate $EBSCARD/$templName.$segment.$rundate 
   
    $process_t966_cards && fnc_process_t966_cards $templName.$segment $jobExt $rptName &
    ((segment+=1))
    sleep $delay
    
  done
  wait  ## for all background run_966_cards calls to be completed
  rm -f $ODSFILE
  export integer segment=1
  export integer okfiles=0
  while (( $segment <= $NUMGROUPS ))
  do
     ## check for Ok flag - there should be 1 file for each split 
     [[ -f $templName.$segment.OK ]] && ((okfiles+=1))  
     cat $EBSXFER/mirpmv/mirpmv.$segment >> $ODSFILE 
     rm $EBSXFER/mirpmv/mirpmv.$segment
     ((segment+=1))
     sleep $delay
  done
  
  if (( $okfiles < $NUMGROUPS )); then
     fnc_exit "One or more segments failed"
  fi   
  unset PMSG
}
#######################################################
## local function called by the generic functions
## fnc_split_plans_by_row or fnc_split_plans_by_column
#######################################################
function generate_card
{
  plan=$1
  echo $plan
}
#######################################################
## Following only applies to dev server for testing
#######################################################
function deliver_split_hop_records
{
  typeset PMSG="function $0" cond fname=$(basename $ODSFILE)
  cd $XJOB
  ## get all HOP and Prod plan records into respective single files
  grep ^Y $ODSFILE > HFILE || fnc_log "ERROR getting records into HFILE"     # get HOP records
  grep -v ^Y $ODSFILE > PFILE || fnc_log "ERROR getting records into PFILE"  # get Prod records (non-HOP)
  copy_to_cifs HFILE $HOPtestCIFS/$fname                                     ## copy HOP data extract to its share
  copy_to_cifs PFILE $CPATH/$fname                                           ## copy Prod data extract to its share
  cd $OLDPWD
  unset PMSG
}
function copy_to_cifs
{
  cp $1 $2 && cond=0 || cond=99
  [[ $cond != 0 ]] && fnc_log "ERROR: cp $1 $2"
  [[ $cond = 0 ]] && { fnc_log "Sucessfully copied $1 to the following shared path:"; fnc_log "[$2]"; }
}
###########################################################################
###########################  main section  ################################
###########################################################################
prog=$(basename $0)
integer NumParms=$#

###################################################
## setup local script variables
###################################################
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="MIR PMV EXTRACT"             
export ENVFILE=ENVBATCH
export MKOUTDIR=child
###################################################
## load global functions and variables; 
## verify input parameters
###################################################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
. JOBDEFINE
fnc_log_standard_start
check_input 
check_overrides
###################################################
cd $XJOB  ## everything is ready - start processing
###################################################
## TASK 1 - split plan list
$create_split_planlist && split_planlist
$create_split_planlist || fnc_log "Skip function split_planlist"

## TASK 2 - create template card and run 
$run_t966_card_decks && run_T966_card_decks
$run_t966_card_decks || fnc_log "Skip function run_T966_card_decks"

## Task 3 - send mirpmv.txt to shared path for Informatica usage.
$send_to_nt && send_file_to_nt_informatica
$send_to_nt || fnc_log "Skip function send_file_to_nt_informatica"

## TASK LAST - end of job cleanup routine
touch rs.tmp
eoj_housekeeping
return 0
