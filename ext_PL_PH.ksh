#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : ext_PL_PH.ksh             NOCALL          *
#COMMENT *  SchdXrf Name : rsuxext708.ksh            APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Build extracts of PL/PH addresses         *
#COMMENT *                 (CC 12066)                                *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - GJP                                 *
#COMMENT *  Created      : 08/14/2007                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE JOBCALC EXT-PH-BALS.txt         *
#COMMENT *  COBOL Calls  :                                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On demand                                 *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can be restarted any time        R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: __/__/____   SMR: ______   By:  __________          U
#COMMENT U Reason: ________________________________________          U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input 
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=ext_PL_PH
export JDESC="OMNIPLUS/UX build PL/PH address extracts"
export ENVFILE=ENVBATCH

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
check_input 
make_output_directories
export MKOUTDIR=current

######################
### Override daily date and rundate 
### if one is passed in
######################
fnc_set_monthend_date

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

### ******************************************************************
### TASK 1 - Run script to update PH662
### ******************************************************************

cd $XJOB
export JDESC="Call JOBCALC to run PL/PH extract build"
display_message nobanner
PNAME=JOBCALC
JPRM1=$JUSER
JPRM2=EXT-PH-BALS.txt
JDESC="Running JOBCALC $JPRM2 at $(date +%H:%M)"
execute_job

JDESC="JOBCALC $JPRM2 complete at $(date +%H:%M)"
display_message nobanner

### ******************************************************************
### TASK 2 - Prep transfer of extracts to secure server/folder
### ******************************************************************
mv CC12066* $POSTOUT

### ******************************************************************
### TASK 3 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
return 0