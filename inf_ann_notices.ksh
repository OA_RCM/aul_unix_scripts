#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CTRL-M Job Name : RSUXINF812             NO CALL         *
#COMMENT *  Schedule Name   : rsuxinf812.ksh         ASU-OMNI        *
#COMMENT *  Unix Script     : inf_ann_notices.ksh                    *
#COMMENT *                                                           *
#COMMENT *  Description  : Loads annual notice requests into         *
#COMMENT *                 SQL table PPA_PLAN_NOTIFICATIONS          *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - GJP                                 *
#COMMENT *  Created      : 05/15/2009                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    :                                           *
#COMMENT *  Script Calls : JOBDEFINE, ODBC (RTS32.AUL)               *
#COMMENT *  COBOL Calls  : INF1300L                                  *
#COMMENT *  Frequency    : Daily                                     *
#COMMENT *  Est.Run Time : 30 minutes                                *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time.      R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 05/15/2009  WMS:  843     By: Gary Pieratt          U
#COMMENT U Reason: Original                                          U
#COMMENT U                                                           U
#COMMENT U Date: 20110318   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT U Date: 20130502   Proj: WMS8240  By: Louis Blanchette      U
#COMMENT U Reason: Feedback - Add new T891 card file.                U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

function load_trans2VTRAN
{
  JDESC="Function load_trans2VTRAN..."
  display_message nobanner

  if [ -s $XJOB/$dd_S891CARDS ]
  then
     JDESC="Load $dd_S891CARDS output file to OMNI Vtran System"
     display_message nobanner
     cp $XJOB/$dd_S891CARDS $EBSCARD
     export MKOUTDIR=child
     PNAME=JOBVTUT
     JPRM1=$JUSER
     JPRM2=$dd_S891CARDS
     execute_job
     export MKOUTDIR=current
     JDESC="Completed $PMSG - Load file $dd_S891CARDS to VTRAN"
     display_message nobanner
  else
     JDESC="File $dd_S891CARDS is empty...no T891 to load"
     display_message nobanner
  fi
}

###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS ELECTRONIC ACA/QDIA PROCESS"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE

set_generic_variables
make_output_directories
export MKOUTDIR=current
fnc_log_standard_start
check_input

. JOBDEFINE
###########################
# Move to the JOB directory
###########################
cd $XJOB

### ******************************************************************
### TASK 1 - Define files needed
### ******************************************************************
JDESC="Defining files needed by Cobol program..."
display_message nobanner

export dd_RSTROUT=$XJOB/rs.tmp
export dd_NOTICEPLANLIST=$EBSINPUT/wizard/NoticePlanlist.txt
if [[ ! -s $dd_NOTICEPLANLIST ]] ; then
   JDESC="File $dd_NOTICEPLANLIST doesn't exist; creating empty..."
   display_message nobanner
   touch $dd_NOTICEPLANLIST
fi

mv $dd_NOTICEPLANLIST $XJOB
export dd_NOTICEPLANLIST=NoticePlanlist.txt
export dd_SRTPLANLIST=NoticePlanlist.srt
export dd_NOTICEERRORS=${EBSRPTPFX}ANN_NTC_ERR.log
export dd_S891CARDS=S891ACAFILE.$rundate
    
### ***********************************************************************
### TASK 2 - Run the COBOL program for ACA/QDIA.
### ***********************************************************************
export COBSAVE=$COBRUN
export COBRUN=$(dirname $COBRUN)/$AUL_RTS
   
PNAME=INF1300L
JDESC="Run COBOL program $PNAME - ACA/QDIA"
$run_program && execute_program
export COBRUN=$COBSAVE

### ******************************************************************
### TASK 3 - Load transactions to VTRAN
### ******************************************************************
$load_trans2VTRAN && load_trans2VTRAN

### ******************************************************************
### TASK 4 - Send error log to document manager
### ******************************************************************
JDESC="Sending error log to document manager..."
$send2DocMgr && display_message nobanner && fnc_send2DocMgr

### ******************************************************************
### TASK 5 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
