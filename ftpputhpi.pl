#!/opt/perl5/bin/perl

$HPI="$ENV{'HPIFILE'}";
$PROC="$ENV{'AULPROC'}";
$NCFTPPUT="/opt/ncftp/bin/ncftpput";
$TIME_MIN=`/usr/bin/date +%M`;
$tody=`/usr/bin/date +%m.%d.%E`;
chop($tody);

&check;
exit(0);

sub check
{
    $result=`$NCFTPPUT -d ftpputhpi.log -f $PROC/ftpputods.cfg -E -a ./SrcFiles $HPI`;
    print $result;
    if ( $? == 0 )
    {
      print "$tody: ftp of $HPI file to 9000 successful\n";
    }
    else
    {
      print OUT "$tody ERROR: Could not ftp $HPI file to 9000\n"; 
    }
}
