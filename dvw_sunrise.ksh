#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  Cntl-M Job Name : RSUXDVW001             CALL            *
#COMMENT *  UNIX Job Pointer: rsuxdvw001.ksh         OMNI-ASU        *
#COMMENT *  UNIX Script Name: dvw_sunrise.ksh                        *
#COMMENT *                                                           *
#COMMENT *  CNTLM Job Type:  job                                     *
#COMMENT *  Runtime syntax:  dvw_sunrise.ksh                         *
#COMMENT *                                                           *
#COMMENT *  Description  : EXECUTES JOBS IN DVW BATCH TODO QUEUE     *
#COMMENT *    Simply executes all jobs that have accumulated in the  *
#COMMENT *    batch todo directory. Jobs are submitted in parallel   *
#COMMENT *    job streams in order to process them all ASAP.         *
#COMMENT *                                                           *
#COMMENT *  Author       : OA/SKL                                    *
#COMMENT *  Created      : 05/07/2013                                *
#COMMENT *  Environment  : N/A                                       *
#COMMENT *  Called by    : ECS Scheduler; may be run manually        *
#COMMENT *                                                           *
#COMMENT *  Tools utilzed: parallel                                  *
#COMMENT *  Script Calls : FUNCTIONSFILE                             *
#COMMENT *  COBOL Calls  : None                                      *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Daily (in omnicycle)                      *
#COMMENT *  Dependent on : UTL010                                    *
#COMMENT *  Est.Run Time : Depends on volume and size                *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions : None                              S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                   R
#COMMENT R   Must be looked at by application support personnel      R
#COMMENT R   before being re-streamed.                               R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE: ----------------------------------------- T
#COMMENT T   01 - Remove batchflag file                              T
#COMMENT T   02 - Run accumulated jobs in batch todo directory       T
#COMMENT T   03 - Check for errors from background DVWXX jobs        T
#COMMENT T   04 - Check batched_member_list vs processed_member_list T
#COMMENT T LAST - End of job housekeeping                            T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U  Date: 05/07/2013  WMS: 6167      By: Steve Loper         U
#COMMENT U  Reason: Initial version.                                 U
#COMMENT U                                                           U
#COMMENT U  Date: 12/03/2013  WMS: 6167      By: Steve Loper         U
#COMMENT U  Reason: Call Unix service to gunzip any files found in   U
#COMMENT U      'todo' directory.                                    U
#COMMENT U                                                           U
#COMMENT U  Date: 02/26/2014  WMS: 6167      By: Steve Loper         U
#COMMENT U  Reason: new function to check members in BAT_TODO_DIR    U
#COMMENT U    against list of those members supposed to be here.     U
#COMMENT U  Reason: Only attempt processing when files are present.  U
#COMMENT U                                                           U
#COMMENT U  Date: 03/17/2014  WMS: 6167      By: Steve Loper         U
#COMMENT U  Reason: Fix miss-spelled batch indicator flag variable.  U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
##############################################################################
##  functions
##############################################################################
##############################################################################
### standard function to check script input
##############################################################################
function check_input
{
  return 0  ## standard; for future use
}
##############################################################################
### standard function to set overrides to current environment
##############################################################################
function check_overrides
{
  ## set all DVW batch location and file parameters for working
  ## in sunrise / sunset batch window
  dvw_set_batch_variables   
}
##############################################
## Remove the file used as a "batch process flag"
## This allows subsequent JOBLTIUT jobs to run realtime
## Name of file is set in JOBLTIUT.ini file
##############################################
function remove_batch_flag
{
  fnc_log "in function: $0"
  cd $EBSCTRL
  typeset cond=0   ## cond: 0=nothing to do (no found found); 1=file removed ok; 2=error on file removal
  [[ -f $dvwBatIndFlagFile ]] && { rm $dvwBatIndFlagFile && cond=1 || cond=2; }
  case "$cond" in
    "0") fnc_log "$dvwBatIndFlagFile not found: removal not needed" ;;
    "1") fnc_log "Removed $dvwBatIndFlagFile Ok" ;;
    "2") fnc_exit "$prog:$0: rm: could not remove $dvwBatIndFlagFile from EBSCTRL!" ;;
  esac
  cd $OLDPWD
}
#########################################################################
## use the tool 'parallel' to run parallel dvw_processDVWXX.ksh executions;
## each with a separate DVWXX.TIMESTAMP file supplied on the command line. 
#########################################################################
function process_dvwxx_files
{
  fnc_log "in function: $0"
  cd $BAT_TODO_DIR
  fnc_unzip_files $CPUnum
  fnc_log "The files listed in $JNAME.list were pending for processing today [$today]"
  ls -1 DVW*|grep -v gz > $XJOB/$JNAME.list
  if [[ -s $XJOB/$JNAME.list ]]; then
     ls -1 DVW* | parallel -P $CPUnum -t dvw_processDVWXX.ksh {} >>$EBSLOGS/dvwxx_processed_${today}.out 
     fnc_log "All parallel processing complete"
  else
     fnc_log "No DVW files found to process: Nothing to do."
  fi
  cd $OLDPWD
}
#########################################################################
## check for any err files from todays batch DVW cycle; abend if found
#########################################################################
function check_dvwxx_errors
{
  fnc_log "in function: $0"
  typeset default_errMsg="$prog:$0:One-or-more batch DVW errors found for $today: Must investigate!"
  ## sanity check 1
  cd $EBSLOGS
  [[ $(ls -1|grep .err|grep DVW|grep $today|wc -l) -gt 0 ]] && fnc_exit "$default_errMsg"
  ## sanity check 2
  cd $BAT_LOCATION/$dvwPendingDir
  [[ $(ls -1 *|grep -c DVW) -gt 0 ]] && fnc_exit "$default_errMsg"
  cd $OLDPWD
  fnc_log "All batched DVW files processed Ok"
}
#########################################################################
## First: move the EBSCARD/$dvwBatMemberList file to EBSLOGS/name.sunrise.date.
## Next:  check if all members recorded in the batch_member_list file were actually
## found in the BAT_TODO_DIR location for processing (they would be in XJOB/JNAME.list).
## Build an exceptions-list and mail the list to specified groups/individuals.
#########################################################################
function check_batMember_list
{
  fnc_log "in function: $0"
  mv $EBSCARD/$dvwBatMemberList $EBSLOGS/$dvwBatMemberList.$rundate
  cat $EBSLOGS/$dvwBatMemberList.$rundate |&
  while read -p fname;do
     grep -q $fname $XJOB/$JNAME.list || print $fname >> $XJOB/$badmsgfile
  done
}
##############################################################################
####MAIN##################   main section   ######################SECTION#####
##############################################################################
prog=$(basename $0)
integer NumParms=$#

export JUSER=${JUSER:-DVWBATCH}
export JNAME=${prog%.ksh}
export JDESC="EXECUTE DVW JOBS IN BATCH TODO DIRECTORY"
export MKOUTDIR=child

#### standard script setup
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
check_overrides
cd $XJOB

########################
### Processing logic ###
########################
##TASK 01 - Remove batchflag file
$remove_batch_flag && remove_batch_flag
$remove_batch_flag || fnc_log "Skipped removal of batch flag file"

##TASK 02 - Run all accumulated jobs in batch todo directory in parallel
$process_dvwxx_files && process_dvwxx_files
$process_dvwxx_files || fnc_log "Skipped execution of dvwxx files [todo directory]"

##TASK 03 - Check for any errors from background DVWXX.timestamp jobs
$check_dvwxx_errors && check_dvwxx_errors
$check_dvwxx_errors || fnc_log "Skipped check for any DVWXX process errors"

##TASK 04 - Check batched_member_list against processed_member_list
$check_batMember_list && check_batMember_list
$check_batMember_list || fnc_log "Skipped check_batMember_list"

##TASK LAST - End of job housekeeping
eoj_housekeeping
[[ -s $XJOB/$badmsgfile ]] && fnc_notify re_errors
[[ -s $XJOB/$badmsgfile ]] && fnc_exit "Not all files were processed; see $badmsgfile"
return 0
