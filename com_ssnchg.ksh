#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : none needed - not a schedule job          *
#COMMENT *  Script Name  : com_ssnchg.ksh                            *
#COMMENT *                                                           *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : job to read sequential history and create *
#COMMENT *                 financial transactions to send to DSS     *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Special Note : the file pcr_hwm_extract.txt is           *
#COMMENT *                 transferred by CDPC1121                   *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $AULPROC                                  *
#COMMENT *  Author       : AUL - KLM                                 *
#COMMENT *  Created      : 01/06/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : FUNCTIONSFILE JOBDEFINE                   *
#COMMENT *  COBOL Calls  : COM4000E                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT #            Task List                                      #
#COMMENT # TASK 1 - Sort the Sequential History File                 #
#COMMENT # TASK 2 - Run the COBOL pgm to extract contrib information #
#COMMENT # TASK 3 - Send commission files to common area for transfer#
#COMMENT # TASK 4 - Clean up and say goodbye                         #
#COMMENT LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 03/10/2004  CC#: 2249     By: Karen Moss (pgm)      U
#COMMENT U                                     K Danielowski (script)U
#COMMENT U Reason: Created to split out contribution information     U
#COMMENT U         From the PC step.                                 U
#COMMENT U                                                           U
#COMMENT U Date: 03/31/2004  CC#: 2249     By: Steve Loper           U
#COMMENT U Reason: Rename and write to enhanced standards.           U
#COMMENT U                                                           U
#COMMENT U Date: 05/12/2005  CC#: 8191     By: Mike Lewis            U
#COMMENT U Reason: Functionalize copy to POSTOUT and change          U
#COMMENT U         delivery location for pickup by CDFT.             U
#COMMENT U                                                           U
#COMMENT U Date: 04/07/2006  CC#: 10082    By: Steve Loper           U
#COMMENT U Reason: Correct issue where UNIFT850 work directory is    U
#COMMENT U         sought during weekend jobs in wrong location.     U
#COMMENT U                                                           U
#COMMENT U Date: 02/02/2007  SMR: CC10698  By: Judy Stewart          U
#COMMENT U Reason: Change commission system to read additional input U
#COMMENT U         file that contains HWM & AAP amounts.             U
#COMMENT U                                                           U
#COMMENT U Date: 03/12/2008  SMR: CC11779  By: Danny Hagans          U
#COMMENT U Reason: Change the sort key for Seqential History to      U
#COMMENT U         match 5.50 record layout.                         U
#COMMENT U                                                           U
#COMMENT U Date: 20110607  Proj#: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
### ******************************************************************
### Internal Functions
### ******************************************************************
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function sort_seqahpc
{
   SRTI=$OUT850/SEQAHPC
   SRTO=$XJOB/SEQAHPC.srt
   sort -o $SRTO -k .2,.10 -k .11,.27 -k .36,.43 -k .28,.35 -k .61,.66r $SRTI
}
function send2DocMgr
{
  cd $XJOB
  ## write commission info to COMMFILE
  mkdir -p $EBSXFER/comm
  cat EXTRTNA >> $EBSXFER/comm/COMMFILE
  cat EXTRTMA >> $EBSXFER/comm/COMMFILE
  cat EXTRTFA >> $EBSXFER/comm/COMMFILE
  cat EXTRTTA >> $EBSXFER/comm/COMMFILE
  cat EXTRTEA >> $EBSXFER/comm/COMMFILE


  ## write COMMERRFILE to document manager
  COMMERRDocMgr=${EBSRPTPFX}COMMERRFILE
  mv COMMERRFILE $COMMERRDocMgr
  fnc_send2DocMgr $XJOB/$COMMERRDocMgr && msg="SUCCESS" || msg="ERROR"
  JDESC="$msg writing $COMMERRDocMgr to document manager"
  [[ $msg = ERROR ]] && print "[$prog:$(date +%X)] $JDESC" >> $master_err_log
  display_message nobanner
}
function copy_to_postout
{
  suffix=""
  [[ $JSTN = YES ]] && suffix=".$JUSER"

  ## copy local commissions info files to AULOUT location
  cp EXTRTNA $AULOUT/EXTRTNA$suffix
  cp EXTRTMA $AULOUT/EXTRTMA$suffix
  cp EXTRTFA $AULOUT/EXTRTFA$suffix
  cp EXTRTTA $AULOUT/EXTRTTA$suffix
  cp EXTRTEA $AULOUT/EXTRTEA$suffix
  cp COMMERRFILE $AULOUT/COMMERRFILE$suffix
}
### ******************************************************************
### MAIN                    main section                     SECTION
### ******************************************************************
prog=$(basename $0)
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

### ******************************************************************
### standard setup function calls
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
check_input
make_output_directories
fnc_log_standard_start

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE
export FILES=$(basename `ls -trd $EBSOUT/$JUSER/$today/UNIFT850* | tail -1`)
export OUT850=$EBSOUT/$JUSER/$today/$FILES
### ******************************************************************
### TASK 1 - Sort the Sequential History File
### on Plan/Plan-Sequence, Folder, Part-ID, Run-Date, Trade-Date
### ******************************************************************
sort_seqahpc

### ******************************************************************
### TASK 2 - Run the COBOL program to extract contribution information
### from the Omni Sequential History File to the Commission Files.
### ******************************************************************

cd $XJOB

if [[ -f $EBSXFER/comm/pcr_hwm_extract.txt ]] ;
then
  cp $EBSXFER/comm/pcr_hwm_extract.txt $XJOB
else
  JDESC="No $EBSXFER/comm/pcr_hwm_extract.txt - aborting"
  display_message
  return 99
fi


PNAME=COM4000E
JDESC="Extract contribution information"
export dd_RSTROUT=$XJOB/rs.tmp
export dd_SEQAHST=$XJOB/SEQAHPC.srt
export dd_HELPER2=$AULDATA/HELP2
export dd_EXTRTNA=$XJOB/EXTRTNA
export dd_EXTRTMA=$XJOB/EXTRTMA
export dd_EXTRTFA=$XJOB/EXTRTFA
export dd_EXTRTTA=$XJOB/EXTRTTA
export dd_EXTRTEA=$XJOB/EXTRTEA
export dd_COMMERRFILE=$XJOB/COMMERRFILE
export dd_RSTRIN=$OUT850/rsfile.txt
export dd_INEXTRT=$POSTOUT/planval.txt
export dd_HWMEXTIN=$XJOB/pcr_hwm_extract.txt
execute_program

JDESC="Extract files  EXTRTNA,MA,FA,TA,EA written"
display_message nobanner

### ******************************************************************
### TASK 3 - Send commission files to common area for pickup by C:D
### ******************************************************************
$send2DocMgr && send2DocMgr

### ******************************************************************
### TASK 4 - Clean up and say goodbye
### ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
