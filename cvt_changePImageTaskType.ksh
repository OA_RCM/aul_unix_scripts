#!/usr/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Job Name          : cvt_changePImageTaskType.ksh                   *
#COMMENT *                                                                     *
#COMMENT *  Description       : Send plan list file from conversion manager    *
#COMMENT *                      to power image server.                         *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.20                             *
#COMMENT *  Resides           : /home/test/scripts                             *
#COMMENT *  Author            : AUL - Tony Ledford                             *
#COMMENT *  Created           : 07/19/2004                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         :                                                *
#COMMENT *  Script Calls      :                                                *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         :                                                *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     :                                                *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                             R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U                                                                     U
#COMMENT U Date: 07/19/2004   SMR:                By: Tony Ledford             U
#COMMENT U Initial version                                                     U
#COMMENT U                                                                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# main

export JNAME=cvt_changePImageTaskType
export JUSER=$LOGNAME

. FUNCTIONSFILE

set_generic_variables
make_output_directories

JDESC="$JNAME starting at $(date)"
display_message

file=$XJOB/$(basename $localfile)

if [ -f $donefile ] ; then
  mv $localfile $file
  mv $donefile $XJOB
else
  touch $file
fi

if [ -s $file ] ; then
  JDESC="sending $localfile to $remhost"
  display_message nobanner
  ftp -n $remhost <<EOF
user $ftpuser $ftppass
ascii
cd $remdir
put $file $remfile
EOF
else
  JDESC="Nothing to process"
  display_message nobanner
fi

JDESC="$JNAME ending at $(date)"
display_message
