#!/bin/ksh
#COMMENT ***********************************************************************
#COMMENT *                    Standard HP Job Documentation                    *
#COMMENT ***********************************************************************
#COMMENT *                                                                     *
#COMMENT *  Job Name          : rsuxexp707.ksh                                 *
#COMMENT *  Script Name       : exp_egtrra_fees_rpt.ksh    NON-CALL            *
#COMMENT *                                                 OMNI-ASU            *
#COMMENT *                                                                     *
#COMMENT *  Description       : Run EGTRRA Fee Report process                  *
#COMMENT *                                                                     *
#COMMENT *  Version           : OmniPlus/UNIX 5.20                             *
#COMMENT *  Resides           :                                                *
#COMMENT *  Author            : AUL - Glen McPherson                           *
#COMMENT *  Created           : 01/08/2009                                     *
#COMMENT *  Environment       :                                                *
#COMMENT *  Called by         :                                                *
#COMMENT *  Script Calls      : FUNCTIONSFILE JOBCALC                          *
#COMMENT *  COBOL Calls       :                                                *
#COMMENT *                                                                     *
#COMMENT *  Frequency         : monthly                                        *
#COMMENT *                                                                     *
#COMMENT *  Est. Run Time     : 5 min.                                         *
#COMMENT *                                                                     *
#COMMENT *  Y2K Status        : Compliant                                      *
#COMMENT *                                                                     *
#COMMENT ***********************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                             S
#COMMENT S                                                                     S
#COMMENT S                                                                     S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions : Can be restarted anytime                    R
#COMMENT R                                                                     R
#COMMENT R                                                                     R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                                     T
#COMMENT T                                                                     T
#COMMENT T                                                                     T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information :                                             P
#COMMENT P RPT       REPORT             SPCL CO SPECIAL    DUE    BUZZ         P
#COMMENT P NAME     DESCRIPTION         FORM PY HANDLING   OUT    CODE         P
#COMMENT P                                                                     P
#COMMENT P                                                                     P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                                    U
#COMMENT U                                                                     U
#COMMENT U Date: 01/13/09  WMS: 572      By: Glen McPherson                    U
#COMMENT U                                                                     U
#COMMENT U Date: 07/01/09  WMS:1519C     By: Rick Sica                         U
#COMMENT U Reason: Omni 5.8 Upgrade                                            U
#COMMENT U                                                                     U
#COMMENT U Date: 04/15/10  WMS:3879      By: Danny Hagans                      U
#COMMENT U Reason: change logic to pull cifs file to postout for processing.   U
#COMMENT U                                                                     U
#COMMENT U Date: 20111025   Proj:  WMS3939        By: Paul Lewis               U
#COMMENT U Standardize document manager calls to a function.                   U
#COMMENT U                                                                     U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

# functions

function get_csv_file
{
  cat $AULDATA/DATENIGHT | \
  while read dnight
     do
     export f="EGTRRA$dnight.csv.sav"
     export fname="/cifs/billing/$f"
     export JDESC="getting input file...$f"
     display_message
     if [[ -s $fname  ]] then
        cp $fname $POSTOUT
        export JDESC="Copied input file...$f"
        display_message
     else
        export JDESC="File not found...$f"
        display_message
     fi
  done
}

function run_report
{
  export JDESC="Running EXP-EGTRRA-FEES-RPT calculator..."
  display_message
  export PNAME=JOBCALC
  export JPRM1=$JUSER
  export JPRM2=EXP-EGTRRA-FEES-RPT.txt
  execute_job
}

# main

prog=$(basename $0)
export JNAME=${prog%.ksh}
export JUSER=$LOGNAME

# load functions file into script
. FUNCTIONSFILE

# standard function calls
set_generic_variables
make_output_directories
fnc_log_standard_start

# do custom stuff here
$get_csv_file && get_csv_file
export pname=$POSTOUT/$f
# if cifs file has been copied to postout continue process
if [[ -s $pname ]] then
   $run_report && run_report
   $send2DocMgr && fnc_send2DocMgr $POSTOUT/${EBSRPTPFX}EGTRRA_FEERPT
fi
# standard exit function
eoj_housekeeping
