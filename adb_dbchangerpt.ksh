#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Job Name     : rsuxadb703.ksh            NO CALL         *
#COMMENT *  Script Name  : adb_dbchangerpt.ksh                       *
#COMMENT *                                           APP             *
#COMMENT *                                                           *
#COMMENT *  Description  : Disbursement Change Registry Report       *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - RPS                                 *
#COMMENT *  Created      : 09/29/2006                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : ECS Scheduler                             *
#COMMENT *  Script Calls : FUNCTIONSFILE JOBDEFINE                   *
#COMMENT *  COBOL Calls  : INF1015R                                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : On Request                                *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  1 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Can re-stream job any time       R
#COMMENT R                                                           R
#COMMENT R    As long as the report strings are still available      R
#COMMENT R    from the cycle in question.                            R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Tape or Datacomm Information :                           T
#COMMENT T                                                           T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 06/21/2001  SMR: xxxxxxx  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program INF1015R               U
#COMMENT U                                                           U
#COMMENT U Date: 09/29/2006  CC: 10406     By: Rick Sica             U
#COMMENT U Reason: Report to Vista; script to standards              U
#COMMENT U                                                           U
#COMMENT U Date: 04/07/2009  WMS:1519      By: Rick Sica             U
#COMMENT U Reason: Omni 5.8                                          U
#COMMENT U                                                           U
#COMMENT U Date: 20110317   Proj: WMS3939  By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U                                                           U
#COMMENT U Date: 20110705   Proj: WMS 2998 By: Tony Ledford          U
#COMMENT U Reason: Eliminate use of connect direct.                  U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

######################################################################
##FUNCTIONS################   functions   ############################
######################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}

######################################################################
##MAIN##################### main section  #################SECTION####
######################################################################
prog=$(basename $0)
export integer NumParms=$#
clear

[[ -z $JUSER ]] && export JUSER=$LOGNAME
[[ -z $JGROUP ]] && export JGROUP=01

export JNAME=${prog%.ksh}
export JDESC="OmniPlus/UX DB Change Report"
export ENVFILE=ENVBATCH

######################
## source in common functions
######################
. FUNCTIONSFILE
 
######################
## setup generic variables
## to initial values
######################
set_generic_variables

######################
## check for required parameters
## and request for help
######################
check_input

######################
## create output directories
######################
make_output_directories
export MKOUTDIR=current

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Run reports
### ******************************************************************

export dd_TAXDBCHGRPT=$XJOB/${EBSRPTPFX}TAXDBCHGRPT

if $runprog; then
  export dd_RSTRIN=$POSTOUT/rsfile.txt
  [[ "$JSTN" = "YES" ]] && export dd_RSTRIN=$POSTOUT/rsfile.${JUSER}
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_TAXDBCHGFILE=$XJOB/TAXDBCHGFILE.tmp

  PNAME=INF1015R
  JDESC="Run of COBOL Program $PNAME"
  execute_program
fi

### ******************************************************************
### TASK 2 - Send Report to document manager
### ******************************************************************
$send2DocMgr && fnc_send2DocMgr

### ******************************************************************
### TASK 3 - save in AULDATA for year-end microfiche
### ******************************************************************

if [ -s "$dd_TAXDBCHGRPT" ] ; then
  arcdir=$AULDATA/db_change_registry
  arcfile=$arcdir/reports.gz

  if $archiverpt; then
    [ -d $arcdir ] || mkdir -p $arcdir
    cat $dd_TAXDBCHGRPT | gzip >>$arcfile
  fi

### ******************************************************************
### TASK 4 - archive year-end microfiche file
### ******************************************************************

    if [ "$(echo $rundate | cut -c5-8)" = "1231" ] ; then
      gunzip -c $arcfile >$FRFILE
      export FRFILE=$XJOB/dbchgrpt
      export archName="Archive DB Change Report for $year"
      export archDir=$AULDATA
      export archType="AULDATA"
      archFile=$FRFILE.$rundate.$tmptime
      cp $FRFILE $archFile
      archive $archFile

      # remove file out of AULDATA
      rm $arcfile
    fi

fi

### ******************************************************************
### TASK Last - clean up and say goodbye
### ******************************************************************
eoj_housekeeping
return 0
