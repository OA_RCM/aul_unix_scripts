#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *  ECS Job Name : RSUXINF751               CALL             *
#COMMENT *  UNIX Job Name: rsuxinf751.ksh           OMNI-ASU         *
#COMMENT *  Script Name  : inf_getDW.ksh                             *
#COMMENT *                                                           *
#COMMENT *  Description  : This job will find all SEQDWVT files and  *
#COMMENT *                 cat into one file; dwvtall.txt.           *
#COMMENT *                 Also copies SEQAHST data to Informatica   *
#COMMENT *                 SrcFiles location for HOP.                *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.20                        *
#COMMENT *  Resides      : $EBSPROC                                  *
#COMMENT *  Author       : AUL - MSS                                 *
#COMMENT *  Created      : 12/03/2004                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M (scheduler)                     *
#COMMENT *  Script Calls : seqdwvt.pl                                *
#COMMENT *  COBOL Calls  : none                                      *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :                                   R
#COMMENT R  Check with Application support personnel before restart. R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  Task Sequence -------------------------------------------T
#COMMENT T   01 - Gather non-zip'd SEQDWVT files into dwvtall.txt    T
#COMMENT T   02 - Create indicative.txt from SEQDWVT                 T
#COMMENT T   03 - Send indicative file to Informatica                T
#COMMENT T   04 - Get copy of SEQAHST.COMBINE.srt to Informatica     T
#COMMENT T LAST - clean up and say goodbye                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U Update History:                                           U
#COMMENT U Date: 12/03/2004  SMR: SMR????  By: Mark Slinger          U
#COMMENT U Reason: Gather all SEQDWVT files into one file.           U
#COMMENT U                                                           U
#COMMENT U Date: 12/28/2005  CC9057  By: Mark Slinger                U
#COMMENT U Reason: Modifed version for HOP                           U
#COMMENT U                                                           U
#COMMENT U Date: 10/31/2007  CC12259  By: Mark Slinger               U
#COMMENT U Reason: Send files to NT                                  U
#COMMENT U                                                           U
#COMMENT U Date: 03/24/2011  WMS 3985   By: Steve Loper              U
#COMMENT U Reason: Send SEQAHST.COMBINE.srt file to Informatica.     U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
######################################################################
##functions##################  FUNCTIONS  ############################
######################################################################
function check_input
{
  mno=0
  check_mno
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function send_email_notification
{
  [[ -s $goodmsgfile ]] && fnc_notify re_success
  [[ -s $badmsgfile ]] && fnc_notify re_errors
}
function send_file_to_informatica
{
  export ODSFILE=$IND_FILE
  $send_to_unix && ftphopods.pl
  $send_to_nt && cp $ODSFILE $TGT
}
function get_seqahst
{
  cond=0
  [[ ! -r $SEQAHST_FILE ]] && { touch $SEQAHST_FILE; chmod 777 $SEQAHST_FILE; }
  [[ $OMNIsysID = prod ]] && { cp $SEQAHST_FILE $TGT || cond=99; }
  [[ $cond != 0 ]] && { JDESC="FATAL: Fail copy of $SEQAHST_FILE to $TGT"; abend; }
}
######################################################################
##main##################  MAIN SECTION  ##############################
######################################################################
prog=$(basename $0)
export integer NumParms=$#

export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="Gathering all deferred work files into one file"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

### ******************************************************************
### Standard script setup tasks
### ******************************************************************
. FUNCTIONSFILE
set_generic_variables
make_output_directories
fnc_log_standard_start
check_input
. JOBDEFINE

## last setup before processing
cd $XJOB
rm $POSTOUT/indicative.txt
rm $POSTOUT/dwvtall.txt
export goodmsgfile=$XJOB/good_messages
export badmsgfile=$XJOB/bad_messages

## ******************************************************************
## TASK 1 - gather all non zipped SEQDWVT files into dwvtall.txt
## ******************************************************************
find $EBSOUT -name SEQDWVT > dwvtfind.txt
while read oneline then; do
   if [[ $oneline != *.gz ]]; then
      cat $oneline >> dwvtall.txt
   fi
done < dwvtfind.txt
cp dwvtall.txt $POSTOUT/dwvtall.txt
sleep 3

## ******************************************************************
## TASK 2 - Create indicative.txt from SEQDWVT
## run perl script to extract needed data from SEQDWVT
## and create indicative.txt for indicative MIR jobs
## ******************************************************************
JDESC="Creating indicative.txt from SEQDWVT"
PNAME=seqdwvt.pl
JPRM1=$dwvt_loc
execute_job

## ******************************************************************
## TASK 3 - Send indicative file to Informatica
## ******************************************************************
export IND_FILE="$indicative_loc"
send_file_to_informatica

### ******************************************************************
### TASK 4 - get copy of SEQAHST.COMBINE.srt
### ******************************************************************
$get_seqahst && get_seqahst

## ******************************************************************
## TASK LAST - clean up and say goodbye
## ******************************************************************
eoj_housekeeping
copy_to_masterlog
create_html
return 0
