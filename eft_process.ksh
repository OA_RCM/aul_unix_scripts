#!/usr/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  Cntl-M Job Name : RSUXEFT050             CALL            *
#COMMENT *  UNIX Job Pointer: rsuxeft050.ksh         OMNI-ASU        *
#COMMENT *  UNIX Script Name: eft_process.ksh                        *
#COMMENT *  Format: JOBCAEFT eft-parm [group number]                 *
#COMMENT *                                                           *
#COMMENT *  Description  : Runs cash eft debit sweep prior to the    *
#COMMENT *                 nightly(post) unified job.                *
#COMMENT *                                                           *
#COMMENT *  Version      : OmniPlus/UNIX 5.50                        *
#COMMENT *  Author       : AUL - PRL                                 *
#COMMENT *  Created      : 05/07/2007                                *
#COMMENT *  Environment  : ENVBATCH2                                 *
#COMMENT *  Called by    : CTRL-M                                    *
#COMMENT *  Script Calls : FUNCTIONSFILE, JOBDEFINE                  *
#COMMENT *                 EXT-EFT-DATA-COLLCT.txt                     *
#COMMENT *  COBOL Calls  : CAEFT, EFTFORMT, BAEFTFMT, REFORMAT,      *
#COMMENT *                 EFTRPT1, BA700F                           *
#COMMENT *                                                           *
#COMMENT *  Frequency    : daily at 2:00 pm                          *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time : 10 min                                    *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R  Notes: 1. Accepted eft-parms are (E-EFT, P-PRENOTE,      R
#COMMENT R            A-ALL)                                         R
#COMMENT R         2. Group # only needed if VFS system used,        R
#COMMENT R            Default=01.                                    R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T Step Sequence:                                            T
#COMMENT T  1 - OMNIPLUS EFT extract process                         T
#COMMENT T  2 - Add Plan number to EFT files                         T
#COMMENT T  3 - OMNIPLUS EFT format process                          T
#COMMENT T  4 - Fix for v5.50 BAEFTFMT issue                         T
#COMMENT T  5 - Re-format EFTFMT to Meret System Modified            T
#COMMENT T  6 - Produce EFT file adding data collection method
#COMMENT T  7 - Produce custom EFT Report                            T
#COMMENT T  8 - Standard reporting process                           T
#COMMENT T  9 - FTP ACH file EFTOUT2 to mainframe                    T
#COMMENT T Last - End of job housekeeping                            T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 01/17/2001  SMR:         By: SKL                    U
#COMMENT U Reason: Modify 5.10 version to accept input parameter for U
#COMMENT U         CAEFT extraction process. Implement program       U
#COMMENT U         execution module.                                 U
#COMMENT U                                                           U
#COMMENT U Date: 06/11/2001  SMR:         By: SKL                    U
#COMMENT U Reason: 5.20 code standards adoption.                     U
#COMMENT U                                                           U
#COMMENT U Date: 11/22/2006  CC#: 11062   By: SKL                    U
#COMMENT U Reason: Correct EFTFMT file based on SG/SSR 20698.        U
#COMMENT U                                                           U
#COMMENT U Date: 05/07/2007  CC#: CC11460  By: Paul Lewis            U
#COMMENT U Reason: Rewrite to standards.                             U
#COMMENT U                                                           U
#COMMENT U Date: 06/28/2007  CC#: CC11695  By: Mike Lewis            U
#COMMENT U Reason: Converted C:D process to SFTP                     U
#COMMENT U                                                           U
#COMMENT U Date: 04/07/2009  WMS:1519      By: Rick Sica             U
#COMMENT U Reason: Omni 5.8 CAEFT to CAUTIL and remove '5' logic     U
#COMMENT U                                                           U
#COMMENT U Date: 20110610   Proj:  WMS3939        By: Paul Lewis     U
#COMMENT U Standardize document manager calls to a function.         U
#COMMENT U                                                           U
#COMMENT U Date: 20121231   Proj:  WMS6167        By: Danny Hagans   U
#COMMENT U Add Data Collection Method TO THE eft report.B            U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  tstuid=`echo $JUSER|tr "[:upper:]" "[:lower:]"`
  [[ $tstuid = "help" || $tstuid = "?" ]] && mno=1
  check_mno
  [[ $NumParms -lt 1 ]] && mno=2
  check_mno
  export JPARM=${JPARM:=X}
  case $JPARM in
    "E") JprmDesc=EFT ;;
    "P") JprmDesc=PRE-NOTE ;;
    "A") JprmDesc=ALL ;;
      *) mno=3 ;;
  esac
  check_mno
}
function extract_eft
{
  export PNAME=CAUTIL
  export PARM=$JPARM
  export JDESC="OMNIPLUS EFT EXTRACT PROCESS"
  export dd_RSTROUT=$XJOB/rs.tmp
  export dd_SEQAHST=$EBSDATA/SEQAHST.EFT
  export dd_EFT=$XJOB/EFT
  execute_program
}
function add_plan_number
{
  export PNAME=EFTFORMT
  export PARM=" "
  export JDESC="OMNIPLUS EFT ADD PLAN NUMBER"
  export dd_EFTIN=$XJOB/EFT
  export dd_EFTOUT=$XJOB/EFTOUT1
  execute_program
  cp $XJOB/EFTOUT1 $XJOB/EFTOUT1X
}
function format_4_omniplus
{
  export PNAME=BAEFTFMT
  JDESC="OMNIPLUS EFT FORMAT PROCESS"
  if [[ -s $XJOB/EFTOUT1 ]]
  then
    cp $EBSCTRL/DUMMY.DEF $XJOB
    export dd_EFT=$XJOB/EFTOUT1
    export dd_EFTFMT=$XJOB/EFTFMT
    export dd_IN80=$EBSCTRL/EFT.PARAMETERS
    export dd_IN802=$XJOB/DUMMY.DEF
    execute_program
  else
    print "NO EFT file created!!...Creating EFTOUT2!!"
    display_message nobanner
    touch $XJOB/EFTOUT2
    $create_ftp_file && create_ftp_file
    return 0
  fi
}
function correct_eftfmt_file
{
  [[ $correct_eftfmt_file != true ]] && { print "Skipped correct_eftfmt_file"; return 0; }
  cd $XJOB
  cond=0
  cp EFTFMT EFTFMT.orig || cond=1
  [[ $cond = 0 ]] && { head -2 EFTFMT > 1st.part || cond=1; }
  [[ $cond = 0 ]] && { grep -E -v '^1|^5' EFTFMT > 2nd.part || cond=1; }
  [[ $cond = 0 ]] && { cat 1st.part 2nd.part > EFTFMT || cond=1; }
  [[ $cond != 0 ]] && { JDESC="ERROR adjusting EFTFMT file - Please Investigate! "; mail_msg; exit 99; }
  cd $OLDPWD
}
function mail_msg
{
  print $JDESC | mailx -s "EFT process message" $ASUEMAIL
  display_message nobanner
}
function reformat_4_meret
{
  export PNAME=REFORMAT
  export PARM=" "
  export JDESC="OMNIPLUS EFT Re-format to Meret Modified"
  export dd_EFTIN=$XJOB/EFTFMT
  export dd_EFTOUT=$XJOB/EFTOUT2
  execute_program
  if [[ -f $dd_EFTOUT ]]
  then
    cp $dd_EFTOUT $POSTOUT
  fi
}
function produce_data_collection
{
  mv rs.tmp rs.tmpx
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=EXT-EFT-DATA-COLLCT.txt
  JDESC='OmniScript EFT Data Collection Method'
  JDESC="Run JOBCALC $JPRM2 at $(date +%H:%M)"
  execute_job
  JDESC="JOBCALC $JPRM2 complete at $(date +%H:%M)"
  display_message nobanner
  mv rs.tmpx rs.tmp

}
function produce_eft_report
{
  export PNAME=EFTRPT1
  export PARM=" "
  export JDESC="OMNIPLUS EFT Produce Custom Report"
  export dd_EFTIN=$XJOB/EFTOUT1X
  export dd_EFTRPT=$XJOB/${EBSRPTPFX}EFTREPORT
  execute_program

  if [[ -f $dd_EFTRPT ]]
  then
    cp $dd_EFTRPT $POSTOUT
    $print2DTU && lp -drpsprt34 -olandscape -oc -ofp12.66 -olpi8 $dd_EFTRPT
    $print2Treas && lp -dtreprt1 -olandscape -oc -ofp12.66 -olpi8 $dd_EFTRPT
    $print2Test && lp -dISPRT_PS4_2 -olandscape -oc -ofp12.66 -olpi8 $dd_EFTRPT
    $send2DocMgr && fnc_send2DocMgr
  fi
}
function create_ftp_file
{
  if [[ ! -f $XJOB/EFTOUT2 ]]
    then
      JDESC="NO EFTOUT2 file created!!...Creating a new EFTOUT2"
      display_message nobanner
      touch $XJOB/EFTOUT2
  fi

  JDESC="Starting ACH File Transmit at $tmptime"
  display_message nobanner
  export FRFILE=EFTOUT2

  case $EBSFSET in
    "prod") JCLFILE=RSPPACH1
                PFX=RS.CDFT ;;
         *) JCLFILE=RSZZACH1
           PFX=TEST.RS.CDFT ;;
   esac

   export CDFILE="${PFX}.$JCLFILE"
   rm -f $XJOB/$CDFILE
   touch $XJOB/$CDFILE
   ##cat $AULDATA/jcl/$JCLFILE >> $XJOB/$CDFILE  ## Not needed by ControlM...rml
   cat $XJOB/$FRFILE >> $XJOB/$CDFILE
   cp $XJOB/$CDFILE $POSTOUT
}
###########################################################################
###########################  main section  ################################
###########################################################################
######################
## setup variables
######################
export integer NumParms=$#
export prog=$(basename $0)
export JPARM=`echo $1|tr "[:lower:]" "[:upper:]"`
export JGROUP=$2

export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JGROUP=${JGROUP:=01}
export JDESC="OMNIPLUS/UX CASH EFT PROCESSING"
export ENVFILE=ENVBATCH2

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
check_input
. JOBDEFINE
export MKOUTDIR=current
fnc_log_standard_start

cd $XJOB
#####################################################
## Step 1 - OMNIPLUS EFT extract process
#####################################################
$extract_eft && extract_eft

#####################################################
## Step 2 - Add Plan number to EFT files
#####################################################
$add_plan_number && add_plan_number

#####################################################
## Step 3 - OMNIPLUS EFT format process
#####################################################
$format_4_omniplus && format_4_omniplus

#####################################################
## Step 4 - Fix for v5.50 BAEFTFMT issue
#####################################################
$correct_eftfmt_file && correct_eftfmt_file

#####################################################
## Step 5 - Re-format EFTFMT to Meret System Modified
#####################################################
$reformat_4_meret && reformat_4_meret

#####################################################
## Step 6 - Produce EFT file adding data collection method
#####################################################
$produce_data_collection && produce_data_collection

#####################################################
## Step 7 - Produce custom EFT Report
#####################################################
$produce_eft_report && produce_eft_report

#####################################################
## Step 8 - Standard reporting process
#####################################################
standard_reporting_process

#####################################################
## Step 9 - FTP ACH file EFTOUT2 to mainframe
#####################################################
$create_ftp_file && create_ftp_file

#####################################################
## Step Last - End of job housekeeping
#####################################################
eoj_housekeeping
return 0
