#!/usr/bin/ksh
#################################################################
## cvt_cycleEnd.ksh
## rsuxcvt47540.ksh
##
## Updated:
## --------------------------------------------------------------
## 20040821  S.Loper  Added code to run ODS update remotely and
##                    send email to Corp requesting final backup.
## --------------------------------------------------------------
#################################################################
###functions###########  FUNCTIONS ##############################
#################################################################

#################################################################
## MASTER FUNCTION WHICH DRIVES ALL OTHER FUNCTIONS
## --------------------------------------------------------------
## First - Make sure next function to be run is specified
## Next  - Check current err_level 
## Next  - Either run specified function or process error
#################################################################
function run_function
{
 next_function=$1
 [[ -z $next_function ]] && JDESC="Function not set"
 [[ -z $next_function ]] && { display_message nobanner; exit 99; }
 case $err_level in
   "abend") 
        errmsg="err_level='ABEND': function [$next_function] bypassed"
	print $errmsg >> $badmsgfile
	JDESC=$errmsg; display_message nobanner
	return 
	;;
     *) 
        export cond_limit=1
        $next_function 
	;;
 esac
}
#################################################################
## RUN JOBMIRROR TO TAKE FINAL DATASET BACKUP
## --------------------------------------------------------------
## First - run JOBMIRROR to take pre-cycle end data backup
#################################################################
function final_data_backup
{
  PNAME=JOBMIRROR
  JPRM1=$JUSER
  JPRM2=cycle_end
  JDESC="Run cycle-end dataset backup"
  execute_job || err_level=abend

  JDESC="Cycle-end dataset backup complete at $(date +%H:%M)"
  display_message nobanner
}
#################################################################
## CREATE LIST OF PROD AND HOLD PLANS IN CONVX DATASET
## --------------------------------------------------------------
## First - run JOBCALC to generate list of all plans in conv area
## Next  - create list of plan numbers from 9000/plan-task file
## Next  - create list of plans not migrating to production 
#################################################################
function create_plan_lists
{
  JDESC="Create plan list files at $(date +%H:%M)"
  display_message nobanner
  ## first remove all previous plan.list files
  rm -f $EBSCTRL/plan.list*

  ## next create list of all plans in dataset
  unset PLANLISTFILE
  JDESC="Create list of all plans in OMNI dataset in $EBSFSET"
  display_message nobanner
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=PLANLIST
  execute_job || { err_level=abend; return; }

  ## create effective full plan list less global plan
  grep -v 000001 $EBSCTRL/plan.list > $listall || { err_level=abend; return; }

  ## then create list of all plans to load to production
  ## sort the list uniquely just in case there are duplicates and to ensure
  ## that the plan list is in alphanumeric order.

  cat $taskInputFile | cut -c23-28|grep -v ' ' > $listprod
  sort -o $listprod.srt -u $listprod
  mv $listprod.srt $listprod || { err_level=abend; return; }

  ## then compare plan lists and get list of plans going to production and
  ## list of plans in error status (not going to production).  That is,
  ## difference between listall and listprod = listhold

  while read planno
  do
    grep $planno $listprod || print $planno >> $listhold
  done<$listall || err_level=abend

  JDESC="Plan list files created at $(date +%H:%M)"
  display_message nobanner
}
#################################################################
## COPY ALL CONVX SPECIFIC DATA TO CONV-ARCHIVE REGION
## --------------------------------------------------------------
## 1. copy current conversion area to archive area 
#################################################################
function archive_cycle
{
  ## <1> run job to copy current conv cycle data components to archive region
  PNAME=copyarea.ksh
  JPRM1=$destArea
  JDESC="Copy $EBSFSET region to $destArea for archival $(date +%H:%M)"
  execute_job || err_level=abend

  JDESC="$EBSFSET region archive to $destArea complete at $(date +%H:%M)"
  display_message nobanner
}
#################################################################
## DELETE HOLD PLANS FROM CONVX DATASET
## --------------------------------------------------------------
## create 051 delete cards for plans not going to production and
## run JOBUNIC to execute them.  This leaves only plans going to
## production in cycle dataset.
#################################################################
function delete_plans
{
  [[ -s $listhold ]] || return
  ## create T051 cards to delete plans not going to production
  JDESC="Create $delplansCard deck with T051's for plans held back"
  display_message nobanner

  while read planno
  do
    print "05100   \$TIHDR ${planno}000000000           $today" >> tmpfile
    print "05101   PL         22                      0000"     >> tmpfile
  done<$listhold || { err_level=abend; return; }
  mv tmpfile $EBSCARD/$delplansCard || { err_level=abend; return; }

  ## run job to use created cards and delete plans
  JDESC="Run $delplansCard deck to delete plans not moving to production"
  display_message nobanner
  PNAME=JOBUNIC
  JPRM1=$JUSER
  JPRM2=$delplansCard
  execute_job || err_level=abend
}
#################################################################
## EXTRACT PERSON RECORDS GOING TO PRODUCTION
## --------------------------------------------------------------
## First - pull all PH SSN's and PE SSN's
## Next  - compare lists and create T542 Person record delete cards
## Next  - delete person records via JOBUNIC w/ T542 cards
## Next  - run getperson calculater to dump remaining person records
#################################################################
function extract_persons
{
  ## <1> pull all ssn's on PH records and PE records
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=cvt_pepartlist
  execute_job || { err_level=abend; return; }
  
  ## <2> compare PH/PE SSN list and create delete PE cards 
  PNAME=cvt_gen_t542.pl
  JPRM1=$XJOB/partlist.dat
  JPRM2=$XJOB/pelist.dat
  JPRM3=$EBSCARD/$delpersonsCards
  execute_job || { err_level=abend; return; }

  ## <3> run JOBUNIC with T542 cards to delete unwanted person records
  unset MKOUTDIR
  PNAME=JOBUNIC
  JPRM1=$JUSER
  JPRM2=$delpersonsCards
  execute_job || { err_level=abend; return; }
  export MKOUTDIR=current

  ## <4> pull all remaining PE records from AHST file
  PNAME=JOBCALC
  JPRM1=$JUSER
  JPRM2=cvt_getperson
  execute_job || err_level=abend
}
#################################################################
## RUN ODS DATA EXTRACTION AND LOAD MASTER DRIVER
## --------------------------------------------------------------
## First - run ODS driver to extract and load OMNI data
#################################################################
function process_ods_updates
{
  grep "transfer_" $EBSCTRL/ods_loadjob.ini | grep -q true; cond=$?
  [[ $cond = 0 ]] && ods_xfer="and Transfer" || ods_xfer="(no transfer)"
  [[ $cond != 0 ]] && export ODS_UPDATE_METHOD="opsCall"
  [[ $cond = 0 ]] && ods_load="and Load" || ods_load="with delayed upload"
  JDESC="Extract $ods_xfer $ods_load OMNI data for(to) ODS"
  display_message nobanner

  extract_ods      ## get data for converted plans from OMNI
  update_ods       ## only if data was transfered and option enabled

  JDESC="Completed OMNI processing for ODS updates"
  display_message nobanner
}
function extract_ods
{
  update_time
  PNAME=ods_loadjob.ksh
  JDESC="Extract OMNI data for ODS at $hhxmm"
  execute_job || err_level=abend
  update_time
  JDESC="Completed Extract OMNI data for ODS at $hhxmm"
  display_message nobanner
}
function update_ods
{
  methodOk=false
  until [ $methodOk = true ]
  do
    case $ODS_UPDATE_METHOD in
      "remshCall")
         JDESC="ODS_UPDATE_METHOD=remshCall. Run ODS loads remotely"
         display_message nobanner
	 cond=0
         methodOk=true
	 run_remote_ODS_update
	 ;;
      "opsCall")
         JDESC="ODS_UPDATE_METHOD=opsCall. Call OPS to release ODSLOADCNV job"
         display_message nobanner
	 print $ODSmsg | mailx -s "ODS UPLOAD" $ODSToList
	 methodOk=true
	 ;;
      *)
         JDESC="ODS_UPDATE_METHOD not set. Assume OPS Call method."
         display_message nobanner
         export ODS_UPDATE_METHOD="opsCall"
	 ;;
     esac
  done
}
function run_remote_ODS_update
{
  ## <1> run remote program to load OMNI extracts which have 
  ##  been transfered to Informatica region on AULHPUX1 box.
  update_time
  JDESC="[$hhxmm] Call remote process to update Informatica with "
  JDESC="$JDESC extracted OMNI data."
  display_message nobanner

  Rlogin=$ODS_login
  Rhost=$ODS_host

  PNAME="$ODS_Xdir/$ODS_Xjob"
  run_remotely=yes
  IN_BACKGROUND=yes 
  cond=0
  execute_job || cond=99

  [[ $cond != 0 ]] && JDESC="ERROR: Load ODS remsh failed!"
  [[ $cond != 0 ]] && { display_message nobanner; exit 99; }

  JDESC="$ODS_Xmsg at $hhxmm"
  print $JDESC | mailx -s "ODS UPLOAD" $ODSToList
  display_message nobanner

  ### Don't check ODS load. Since ran remotely, will check later. Fallback
  ### will be to either call OPS and have them release ODSLOADCNV or simply
  ### login to aulhpux1 as inform1 and run the job directly. 
}
  
#################################################################
## TAKE APPLICATION BACKUPS OF VTRAN AND MASTER FILES
## --------------------------------------------------------------
## First - Create 030 plan backup and 002 tran backup cards
## Next  - Run OMNI master plan backup job in background
## Next  - Run OMNI transactions plan backup job in background
## Next  - wait for background jobs to complete before return
#################################################################
function backup_plans
{
  cd $EBSCTRL
  rm -f PGMBKUP.DEF VTBKUP.DEF RESTORE.DEF VTREST.DEF UCOBKUP.DEF UCOREST.DEF

  ## <1> Create backup control cards from prod.plan.list
  while read planno
  do
    ## master file bkup control card
    print "03000   \$TIHDR $planno" >> PGMBKUP.DEF || cond=71
    print "03001   $today         " >> PGMBKUP.DEF || cond=71
    print "01000   \$TIHDR ******" > RESTORE.DEF || cond=71
    print "01001               1 " >> RESTORE.DEF || cond=71

    ## tran file bkup control card
    print "00200   \$TIHDR $planno" >> VTBKUP.DEF || cond=72
    print "00201   BKUP           " >> VTBKUP.DEF || cond=72
    print "00200   \$TIHDR $planno" >> VTREST.DEF || cond=72
    print "00201   RESTALL        " >> VTREST.DEF || cond=72

    ## ucom file bkup control card> 
    print "09100   \$TIHDR $planno"  >> UCOBKUP.DEF || cond=73 
    print "09101                 "   >> UCOBKUP.DEF || cond=73
    print "09200   \$TIHDR ******"   >  UCOREST.DEF || cond=73
    print "09201                "    >> UCOREST.DEF || cond=73
  done<$listprod || { err_level=abend; return; }
  [[ $cond -ne 0 ]] && { MSG="backup_plans"; check_mno; }
  cd $OLDPWD

  ## <2> Run OMNI master files backup job
  JDESC="Backup master data for plans to load into OMNI production"
  display nobanner
  PNAME=JOBPBKP
  JPRM1=$JUSER
  IN_BACKGROUND=yes
  execute_job || { err_level=abend; return; }

  ## <3> Run OMNI transaction file backup job
  JDESC="Backup transactions for plans to load into OMNI production"
  display nobanner
  PNAME=JOBVTBK
  JPRM1=$JUSER
  IN_BACKGROUND=yes
  execute_job || { err_level=abend; return; }

  ## <4> Run OMNI UCOM file backup job
  JDESC="Backup UCOM for plans to load into OMNI production"
  display nobanner
  PNAME=JOBUCBK
  JPRM1=$JUSER
  IN_BACKGROUND=yes
  execute_job || { err_level=abend; return; }

  wait    ## for all backups to finish
}
#################################################################
## TAKE APPLICATION BACKUPS OF VTRAN AND MASTER FILES
## --------------------------------------------------------------
## First - pull all PH SSN's and PE SSN's
## Next  - compare lists and create T542 Person record delete cards
## Next  - delete person records via JOBUNIC w/ T542 cards
## Next  - run getperson calculater to dump remaining person records
#################################################################
function check_bkups
{
  cond=0

  ## <1> get name of all pre-production backups 
  cd $EBSBKUP
  JDESC="[load_production] Find backups"; display_message nobanner
  export last_Mstr_bkup=$(ls -1tr *.pbkp00 2>>/dev/null | tail -1)
  export last_Tran_bkup=$(ls -1tr *.vtbk 2>>/dev/null | tail -1)
  export last_Ucom_bkup=$(ls -1tr *.ucbk 2>>/dev/null | tail -1)
  export last_Prsn_bkup=$(basename $PERSON_DATFILE)

  ## <2> verify_backups
  JDESC="[load_production] Verify backups"; display_message nobanner
  msg="Error"
  [[ -z $last_Mstr_bkup ]] && { err_level=abend; msg="$msg:No Master Backup"; }
  [[ -z $last_Tran_bkup ]] && { err_level=abend; msg="$msg:No Trans  Backup"; }
  [[ -z $last_Ucom_bkup ]] && { err_level=abend; msg="$msg:No UCOM Backup"; }
  [[ -z $last_Prsn_bkup ]] && { err_level=abend; msg="$msg:No Person Backup"; }
  [[ $msg != "Error" ]] && { JDESC=$msg; display_message nobanner; exit 99; }
[[ -s $last_Mstr_bkup ]] || { err_level=abend; msg="$msg:Empty Master Backup"; }
[[ -s $last_Tran_bkup ]] || { err_level=abend; msg="$msg:Empty Trans  Backup"; }
[[ -s $last_Ucom_bkup ]] || { err_level=abend; msg="$msg:Empty UCOM Backup"; }
[[ -s $last_Prsn_bkup ]] || { err_level=abend; msg="$msg:Empty Person Backup"; }
  [[ $msg != "Error" ]] && { JDESC=$msg; display_message nobanner; exit 99; }

  ## reset Rlogin and Rhost based on both ODS and OMNI implications
  export Rlogin=$OMNI_login
  export Rhost=$OMNI_host
}

function pull4_production
{
  ## <3> copy all backups to production box
  JDESC="[load_production] Copy backups to ${Rregion} on ${Rhost}"
  display_message nobanner
 rcp $last_Mstr_bkup ${Rlogin}@${Rhost}:${Rdir} || { err_level=abend; return; }
 rcp $last_Tran_bkup ${Rlogin}@${Rhost}:${Rdir} || { err_level=abend; return; }
 rcp $last_Ucom_bkup ${Rlogin}@${Rhost}:${Rdir} || { err_level=abend; return; }
 rcp $last_Prsn_bkup ${Rlogin}@${Rhost}:${Rdir} || { err_level=abend; return; }
 rcp $EBSCTRL/RESTORE.DEF ${Rlogin}@${Rhost}:${Rdir2} || { err_level=abend; return; }
 rcp $EBSCTRL/VTREST.DEF ${Rlogin}@${Rhost}:${Rdir2}  || { err_level=abend; return; }
 rcp $EBSCTRL/UCOREST.DEF ${Rlogin}@${Rhost}:${Rdir2} || { err_level=abend; return; }
  cd $OLDPWD
}
function load_production
{
  ## <1> run remote program to load master file, tran file, UCOM file, 
  ## and person records for converted plans to OMNI production.
  JDESC="Restore Master plan, Tran, UCOM, and Persons records to $Rregion"
  display_message nobanner

  PNAME=$Xdir/cvt_restore_conv.ksh
  JPRM1=${last_Mstr_bkup%00}
  JPRM2=${last_Tran_bkup}
  JPRM3=${last_Ucom_bkup}
  JPRM4=${last_Prsn_bkup}
  run_remotely=yes
  execute_job         ## no use checking return code - not sent via rmsch

  ### <2> Exit Status: Attempt to fetch a file from remote system which will
  ###    only exist if the remote process completed properly. If not, exit.
  cond=0
  rcp ${Rlogin}@${Rhost}:$Xdir/cvt_restore_conv.ok . 1>/dev/null 2>&1
  cond=$?
  [[ $cond != 0 ]] && msg="ERROR restoring" || msg="Successfully restored"

  JDESC="$msg Master plan records, Tran, Ucom, and Persons to $Rregion"
  display_message nobanner
  [[ $cond != 0 ]] && { JDESC="Abort $prog!"; display_message nobanner; }
  [[ $cond != 0 ]] && exit 99
}
#################################################################
## REQUEST ECS TO RUN VERITAS FULL BACKUP OF CONVX REGION
## --------------------------------------------------------------
## First - Send job request to ECS to run Veritas backup of conv
##         region for final stored backup.
#################################################################
function final_tape_backup
{
  ## this is temporary code to be realized later.Remove this when done.
  JDESC="REMINDER: Send email to Josh to make final conversion backup"
  print $JDESC | mailx -s "FINAL CONV BKUP REQUEST" $ODSToList
  display_message nobanner
}
function update_time
{
  export hh=$(date +%H)
  export mm=$(date +%M)
  export hhmm=${hh}${mm}
  export hhxmm="${hh}:${mm}"
  export tmptime=$(date +%H%M%S)
}
#################################################################
###MAIN############## main section ##################SECTION#####
#################################################################
clear
prog=$(basename $0)
export JUSER=${JUSER:-$LOGNAME}
export JNAME=${prog%.ksh}
export JDESC="OMNIPLUS/UX AUL CONVERSION CYLCE-END PROCESS DRIVER"
export ENVFILE=ENVNONE

######################################## 
### Standard job startup routines
########################################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
. JOBDEFINE
fnc_log_standard_start

########################################
### begin driver process
########################################
cond=0
export err_level=none
$final_data_backup && run_function "final_data_backup"
$create_plan_lists && run_function "create_plan_lists"
$archive_cycle     && run_function "archive_cycle"
$delete_plans      && run_function "delete_plans"
$extract_persons   && run_function "extract_persons"
$update_ods        && run_function "process_ods_updates"
$backup_plans      && run_function "backup_plans"
$check_bkups       && run_function "check_bkups"
$pull4_production  && run_function "pull4_production"
$load_production   && run_function "load_production"
$final_tape_backup && run_function "final_tape_backup"

### end processing
[[ -s $badmsgfile ]] && notify re_errors
[[ $err_level = abend ]] && cond=99
eoj_housekeeping
return $cond
