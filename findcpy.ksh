#!/bin/sh 
#
if [[ -z $1 ]]; then
   echo "\tEnter Copybook name to associate:\c"
   echo
   read book
   if [[ -z "$book" ]]; then
      echo "You must enter a Copybook name - aborting"
      return 0
   fi
else
   book=$1
fi

### *****************************************************
### get names of programs to move and compile
### *****************************************************

# Use 'show s' to find programs copybook is used in 
integer sdirno=0
for envdir in `show s|grep Order|awk -F " " ' { print $4 }'`; do 
   if [[ "$envdir" != "." ]]; then
      (( sdirno = sdirno + 1 ))
      sdvar[$sdirno]="$envdir"
   fi
done
integer sdirno_last=$sdirno
sdirno=0
# Make a list of those programs
cblist=$HOME/cb.lst
rm $cblist
touch $cblist
until [[ ${sdirno} = ${sdirno_last} ]]; do  
   (( sdirno = sdirno + 1 )) 
   grep -l -s "COPY $book" ${sdvar[$sdirno]}/*.CBL >> $cblist
   grep -l -s "COPY "'"'"$book" ${sdvar[$sdirno]}/*.CBL >> $cblist
   grep -l -s "COPY ""'""$book" ${sdvar[$sdirno]}/*.CBL >> $cblist
done

echo
echo
echo LIST

# Remove duplicate source names   
cname2=" "
cat $cblist | \
while read cname; do
   cname1=`basename $cname`
   if [ "$cname1" != "$cname2" ]; then
      cname2=cname1
      print $cname
   fi
done

rm $cblist
