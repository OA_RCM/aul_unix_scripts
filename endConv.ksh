#!/bin/ksh
function show_conv_elections
{
  integer counter=1
  typeset -Z2 cnt=$counter
  until [ $cnt -gt $numJobs ]
  do
    x=runJob$cnt; y=Job$cnt; z=Job${cnt}Block; t=Job${cnt}Type
    eval jobname=\$$y
    eval block=\$$z
    eval type=\$$t
    eval \$$x &&  flag=YES || flag='+NO+'
    [[ $type = F ]] && type=Foreground
    [[ $type = B ]] && type=Background
    printf "Job%s: %-20s\t RunBlock: %s\t RunState: %s\t RunType: %s\n" $cnt $jobname $block $flag $type
    ((cnt+=1))
  done
}
function should_we_continue
{
  printf "Review above. Should processing continue? (only YES continues): "
  read answer
  [[ $answer = YES ]] && printf "\n" || { print User aborted - quit; exit 99; }
}
#### main
USERID=${1:-$LOGNAME}
event=${2:-$$}
logdir=$EBSLOGS/endConv
mkdir -p $logdir

print "[$(date)] BEGIN END CONVERSION PROCESS DRIVER"
. $EBSCTRL/endConv.ini

[[ -f $PLANLISTFILE ]] && lit1="Found" || lit1="NOT FOUND ERROR"
[[ -f $OMNIvcfFILE ]] && lit2="Found" || lit2="NOT FOUND ERROR"
[[ -f $EBSCARD/DMCV.PARMS ]] && lit3="Found" || lit3="NOT FOUND ERROR"

echo RUNTIME ENVIRONMENT
echo "EBSDATA dir: $EBSDATA"
printf "USERID.....: %s\t event: %s\t logdir: %s\n" $USERID $event $logdir
printf "OMNIrcfPLAN: %s\t OMNIvcfPLAN: %s\n" $OMNIrcfPLAN $OMNIvcfPLAN
echo "OMNIvcfFILE: $OMNIvcfFILE - $lit2"
echo "Plan List..: $PLANLISTFILE - $lit1"
echo "DMCV Input.: DMCV.PARMS (EBSCARD) - $lit3"
cat $EBSCARD/DMCV.PARMS

show_conv_elections
should_we_continue
 
###################################
####  BLOCK  A   ##################
###################################
if [ $runJob01 = "true" ]
then
  print "[$(date)] Task 1 - Run 550 TEXT FILE DELETE [INTERNAL550.DEL] in foreground"
  outfile=$logdir/jobtext.INTERNAL550_delete.$event.out
  JOBTEXT $USERID INTERNAL550.DEL > $outfile 2>>$outfile
else
  print "Skipped $Job01"
fi

if [ $runJob02 = "true" ]
then
  print "[$(date)] Task 2 - Run 580 TEXT FILE LOAD [INTERNAL580.LOAD] in foreground"
  outfile=$logdir/jobtext.INTERNAL580_load.$event.out
  JOBTEXT $USERID INTERNAL580.LOAD > $outfile 2>>$outfile
else
  print "Skipped $Job02"
fi
###################################
####  BLOCK  B   ##################
###################################
if [ $runJob03 = "true" ]
then
  print "[$(date)] Task 3 - Run ONE-LOAN-PRECONV in background"
  outfile=$logdir/jobcalc.ONE-LOAN-PRECONV.$event.out
  nohup JOBCALC $USERID ONE-LOAN-PRECONV.txt > $outfile 2>>$outfile &
else
  print "Skipped $Job03"
fi

print "[$(date)] Wait for BLOCK B background jobs to finish"
wait   ## for above to finish, cannot run DMCV until prior job finishes
print "[$(date)] BLOCK B background jobs finished - continue with SG conversion processes"

###################################
####  BLOCK  C   ##################
###################################
if [ $runJob04 = "true" ]
then
  print "[$(date)] Task 4 - Run JOBDMCV DMCV.PARMS in background"
  outfile=$logdir/jobdmcv.$event.out
  nohup JOBDMCV $USERID DMCV.PARMS > $outfile 2>> $outfile &
else
  print "Skipped $Job04"
fi

if [ $runJob05 = "true" ]
then
  print "[$(date)] Task 5 - Run JOBSGCV in background"
  outfile=$logdir/jobsgcv.$event.out
  nohup JOBSGCV $USERID > $outfile 2>> $outfile &
else
  print "Skipped $Job05"
fi

print "[$(date)] Wait for BLOCK C background jobs to finish"
wait   ## for both above to finish, since DMCV has to be done before post-Loan calc's run
print "[$(date)] BLOCK C background jobs finished - continue custom conversion processes"

###################################
####  BLOCK  D   ##################
###################################
if [ $runJob06 = "true" ]
then
  print "[$(date)] Task 6 - Run ONE-LOAN-POSTCONV in background"
  outfile=$logdir/jobcalc.ONE-LOAN-POSTCONV.$event.out
  nohup JOBCALC $USERID ONE-LOAN-POSTCONV.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job06"
fi

if [ $runJob07 = "true" ]
then
  print "[$(date)] Task 7 - Run ONE-LD-ICFCUPD in background"
  outfile=$logdir/jobcalc.ONE-LD-ICFCUPD.$event.out
  nohup JOBCALC $USERID ONE-LD-ICFCUPD.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job07"
fi

if [ $runJob08 = "true" ]
then
  print "[$(date)] Task 8 - Run ONE-FEOV-UPD in background"
  outfile=$logdir/jobcalc.ONE-FEOV-UPD.$event.out
  nohup JOBCALC $USERID ONE-FEOV-UPD.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job08"
fi

if [ $runJob09 = "true" ]
then
  print "[$(date)] Task 9 - Run ONE-T245-TO-T247 in background"
  outfile=$logdir/jobcalc.ONE-T245-TO-T247.$event.out
  nohup JOBCALC $USERID ONE-T245-TO-T247.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job09"
fi

if [ $runJob10 = "true" ]
then
  print "[$(date)] Task 10 - Run ONE-VCF-POSTCONV in background"
  outfile=$logdir/jobcalc.ONE-VCF-POSTCONV.$event.out
  nohup JOBCALC $USERID ONE-VCF-POSTCONV.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job10"
fi

if [ $runJob11 = "true" ]
then
  print "[$(date)] Task 11 - Run ONE-RCF-POSTCONV in background"
  outfile=$logdir/jobcalc.ONE-RCF-POSTCONV.$event.out
  nohup JOBCALC $USERID ONE-RCF-POSTCONV.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job11"
fi

if [ $runJob12 = "true" ]
then
  print "[$(date)] Task 12 - Run ONE-UPD-CTCOMPL2 in background"
  outfile=$logdir/jobcalc.ONE-UPD-CTCOMPL2.$event.out
  nohup JOBCALC $USERID ONE-UPD-CTCOMPL2.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job12"
fi

print "[$(date)] Wait for BLOCK D background jobs to finish"
wait   ## for both above to finish, driver job not complete until they are done
print "[$(date)] BLOCK D background jobs finished - complete driver routine"

###################################
####  BLOCK  E   ##################
###################################
if [ $runJob13 = "true" ]
then
  print "[$(date)] Task 13- Run ONE-RCF-POSTCONV2 in background"
  outfile=$logdir/jobcalc.ONE-RCF-POSTCONV2.$event.out
  nohup JOBCALC $USERID ONE-RCF-POSTCONV2.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job13"
fi

if [ $runJob14 = "true" ]
then
  print "[$(date)] Task 14 - Run ONE-LN-CLEANUP in background"
  outfile=$logdir/jobcalc.ONE-LN-CLEANUP.$event.out
  nohup JOBCALC $USERID ONE-LN-CLEANUP.txt > $outfile 2>> $outfile &
else
  print "Skipped $Job14"
fi

print "[$(date)] Wait for BLOCK E background jobs to finish"
wait   ## for both above to finish, driver job not complete until they are done
print "[$(date)] BLOCK E background jobs finished - complete driver routine"

print "[$(date)] END CONVERSION PROCESS DRIVER COMPLETE"
