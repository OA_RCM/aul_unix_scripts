#!/bin/ksh
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTLM JOBNAME: RSUXADB701                CALL            *
#COMMENT *  UNIX POINTER : rsuxadb701.ksh            OMNI-ASU        *
#COMMENT *  UNIX RX NAME : adb_cdsextract.ksh                        *
#COMMENT *                                                           *
#COMMENT *                                                           *
#COMMENT *  Description  : Check writing interface                   *
#COMMENT *                 This job pulls the payment file produced  *
#COMMENT *                 by OmniPlus and extracts payment data to  *
#COMMENT *                 send to the Check Disbursment System      *
#COMMENT *                                                           *
#COMMENT *  Author       : AUL - MBL                                 *
#COMMENT *  Created      : 09/29/2000                                *
#COMMENT *  Environment  : ENVBATCH                                  *
#COMMENT *  Called by    : Control-M                                 *
#COMMENT *  Script Calls : JOBDEFINE                                 *
#COMMENT *  COBOL Calls  : EXT1010E UTPYMT INF1002E                  *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Nightly                                   *
#COMMENT *                                                           *
#COMMENT *  Est.Run Time :  < 1 hour                                 *
#COMMENT *                                                           *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *                                                           *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S                                                           S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE--------------------------------------------T
#COMMENT T  TASK 1 - Create eft extract                              T
#COMMENT T  TASK 2 - Uncompress Payment file                         T
#COMMENT T  TASK 3 - Look for previous ACHWRITE file                 T
#COMMENT T  TASK 4 - Produce check write file extract                T
#COMMENT T  TASK 5 - Prepare Check Write file for transfer to CDS    T
#COMMENT T  TASK 6 - Clean up and say goodbye                        T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/29/2000  SMR: PEN3235  By: Mike Lucas            U
#COMMENT U Reason: Created to run the program UTPYMT, INF1002E       U
#COMMENT U Date: 08/21/2001  SMR: *******  By: Mike Lewis            U
#COMMENT U Reason: Standardization in scripts                        U
#COMMENT U Date: 11/08/2004  SMR: CC7373   By: Rick Sica             U
#COMMENT U Reason: Auto Refund functionality and standardization     U
#COMMENT U Date: 02/10/2005  SMR: CC7839   By: Rick Sica             U
#COMMENT U Reason: New EFT functionality                             U
#COMMENT U Date: 06/02/2005  SMR: CC8518   By: Mike Lewis            U
#COMMENT U Reason: Change the date used to get the T114refund input. U
#COMMENT U Date: 04/14/2006  SMR: 5.5      By: Rick Sica             U
#COMMENT U Reason: Changes necessary for 5.5 Upgrade.                U
#COMMENT U Date: 08/10/2007  SMR: CC11967  By: Mike Lewis            U
#COMMENT U Reason: Switch from C:D transfers to CM/AFT.              U
#COMMENT U Date: 20110317  Proj: WMS3939   By: Paul Lewis            U
#COMMENT U Reason: Standardize document manager calls to a function. U
#COMMENT U Date: 20120710  Proj: WMS7914   By: Rick Sica             U
#COMMENT U Reason: ACH for Omni                                      U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

############################################################################
###   function declarations
############################################################################
function check_input
{
  mno=0
  [[ $NumParms -lt 0 ]] && mno=2
  check_mno
}
function run_program
{
  cd $XJOB
  JDESC="Run COBOL program $PNAME"
  execute_program
}
function prepare_files_for_transfer
{
  JDESC="Preparing Check Write Extract for Transfer"
  display_message nobanner
  create_file

  echo "                          0000$rundate" >$CDFILE
  [[ -s $dd_EFTTMP ]] && cat $dd_EFTTMP >> $CDFILE
  [[ -s $dd_EFTEXTRACT ]] && cat $dd_EFTEXTRACT >> $CDFILE
  [[ -s $dd_CHKWRITE ]] && cat $dd_CHKWRITE >> $CDFILE
  if [[ -s $PREVACHWRITE ]]; then
    export PNAME=INF1002U
    run_program
  fi

  JDESC="Copying Check Write Extract to $AULOUT for ftp job "
  display_message nobanner
  cp $CDFILE $AULOUT
  if [[ -s $CDFILEOUT ]]
  then
    JDESC="Copied $CDFILE to $AULOUT"
    display_message nobanner
  else
    JDESC="Problem with copy of $CDFILE to $AULOUT -- ABORT"
    display_message nobanner
    exit 99
  fi
}

function create_file
{
  case $EBSFSET in
     "hop") JCLFILE=RSHH1010
            PFX=RS.CDFT ;;
    "prod") JCLFILE=RSPP1010
            PFX=RS.CDFT ;;
         *) JCLFILE=RSZZ1010
            PFX=TEST.RS.CDFT ;;
  esac

  export CDFILE=$XJOB/"${PFX}.$JCLFILE"
  export CDFILEOUT=$AULOUT/"${PFX}.$JCLFILE"
  rm -f CDFILE
  touch $CDFILE
}
###########################################################################
###########################  MAIN SECTION  ################################
###########################################################################
######################
## setup standard local variables
######################
prog=$(basename $0)
export integer NumParms=$#
export JUSER=${JUSER:-$LOGNAME}
export JGROUP=${JGROUP:-"01"}

export JNAME=adb_cds
export JDESC="OMNIPLUS/UX CDS EXTRACT"
export ENVFILE=ENVBATCH
export AULOUT=$POSTOUT

######################
### standard setup
######################
. FUNCTIONSFILE
set_generic_variables
make_output_directories
export MKOUTDIR=current
check_input

######################
### Override daily date and rundate
### if one is passed in
######################
fnc_set_daily_date

##### set rundate to date of last price file unless an override is present
if [[ -z $opt_d ]] ; then
  export rundate=$lastTRdate
  echo "rundate=$rundate"
fi

### ******************************************************************
### call standard omniplus master file / environment definition script
### ******************************************************************
. JOBDEFINE

######################
## tell user job started
######################
fnc_log_standard_start

cd $XJOB

### ******************************************************************
### TASK 1 - Create eft extract
### ******************************************************************

export dd_HELPER2=$AULDATA/HELP2
export dd_EFTEXTRACT=$XJOB/EFTTOCDS

if $Prenote_ACH; then
  export dd_RSTRIN=$POSTOUT/rsfile.txt
  [[ "$JSTN" = "YES" ]] && export dd_RSTRIN=$POSTOUT/rsfile.${JUSER}
  export PNAME=EXT1010E

  $runeft && run_program
fi
[[ ! -s $dd_EFTEXTRACT ]] && touch $dd_EFTEXTRACT

### ******************************************************************
### TASK 2 - Uncompress Payment file
### ******************************************************************
export dd_RSTROUT=$XJOB/rs.tmp
export dd_RS06OUT=$XJOB/PAYMENT
export dd_PAYMENTS=$AULOUT/PAYMENT
[[ "$JSTN" = "YES" ]] && export dd_PAYMENTS=$AULOUT/PAYMENT.${JUSER}
touch $XJOB/PAYMENT
export PNAME=UTPYMT

$uncompress_file && run_program

### ******************************************************************
### TASK 3 - Produce check write file extract
### ******************************************************************
export PREVACHWRITE=$XJOB/ACHWRITE.prev
if $Prenote_ACH; then
   rm $PREVACHWRITE
   touch $PREVACHWRITE
   mkdir -p $ACH_LOC
   ls $ACH_LOC/ACHWRITE.* >ACHlist
   if [[ -s ACHlist ]]; then
      for f in $(cat ACHlist) ; do
         cat $f >>$PREVACHWRITE
         rm $f
      done
   fi
else
   touch $PREVACHWRITE
fi

### ******************************************************************
### TASK 4 - Produce check write file extract
### ******************************************************************
export dd_PAYMENT=$XJOB/PAYMENT
export dd_CHKWRITE=$XJOB/CHKWRITE
touch $dd_CHKWRITE
export dd_ACHWRITE=$XJOB/ACHWRITE.$rundate
export dd_EFTTMP=$XJOB/ACHEXTRACT.tmp
export dd_T114REFUNDS=$POSTOUT/t114refunds.$rundate
[[ ! -s $dd_T114REFUNDS ]] && touch $dd_T114REFUNDS
export dd_REFUNDTEMP=$XJOB/REFUNDS.tmp
export EFTPRENOTEIDX=$XJOB/EFT_PRENOTE.idx

if $Prenote_ACH; then
   cp $AULDATA/EFT_PRENOTE.idx $EFTPRENOTEIDX
   cp $EFTPRENOTEIDX $XJOB/EFT_PRENOTE.idx.bak
   ACH_PRENOTE = "Y"
else
   ACH_PRENOTE = "N"
fi

export PNAME=INF1002E
export PARM=$ACH_PRENOTE

$produce_extract && run_program

[[ -s $dd_ACHWRITE ]] && cp $dd_ACHWRITE $ACH_LOC

### ******************************************************************
### TASK 5 - Prepare Check Write file for transfer to CDS
### ******************************************************************
$sendextract && prepare_files_for_transfer

### ******************************************************************
### TASK 6 - Clean up and say goodbye
### ******************************************************************
if $Prenote_ACH; then
   cp $EFTPRENOTEIDX $AULDATA/EFT_PRENOTE.idx 
fi
eoj_housekeeping
copy_to_masterlog
return 0
