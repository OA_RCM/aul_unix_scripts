#!/usr/local/bin/perl
use strict;
#COMMENT *************************************************************
#COMMENT *             Standard HP Job Documentation                 *
#COMMENT *************************************************************
#COMMENT *                                                           *
#COMMENT *  CNTL-M JOBNAME: N/A                                      *
#COMMENT *  UNIX Pointer :  N/A                                      *
#COMMENT *  UNIX Script  :  inf_bildFinSEQAHST.pl                    *
#COMMENT *                                                           *
#COMMENT *  Description  : PERL code to scan input SEQAHST file and  *
#COMMENT *                 write records reflecting non-indicative   *
#COMMENT *                 activity (i.e. "Financial") to a separate *
#COMMENT *                 file.  Will write out indicative records  *
#COMMENT *                 when run in debug mode (debug=DEBUG).     *
#COMMENT *                                                           *
#COMMENT *  Author       : ONEAMERICA - S Loper                      *
#COMMENT *  Created      : 09/25/2007                                *
#COMMENT *  Environment  : N/A                                       *
#COMMENT *  Called by    : Parent ksh or manually                    *
#COMMENT *  Script Calls : None                                      *
#COMMENT *  COBOL Calls  : N/A                                       *
#COMMENT *                                                           *
#COMMENT *  Frequency    : Weekdays (standard NYSE workdays)         *
#COMMENT *  Est.Run Time : Up to 5  minutes                          *
#COMMENT *  Y2K Status   : Compliant                                 *
#COMMENT *************************************************************
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT S  Special Instructions :                                   S
#COMMENT S     Will not work without:                                S
#COMMENT S     List of indicative transactions (tran numbers)        S
#COMMENT S                                                           S
#COMMENT SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT R  Restart Instructions :  Must be looked at by application R
#COMMENT R    support personnel before being re-streamed             R
#COMMENT R                                                           R
#COMMENT R                                                           R
#COMMENT RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT T  TASK SEQUENCE                                            T
#COMMENT T  1  - Make lookup table (hash) from master txn ID file    T
#COMMENT T  2  - Read-loop thru input SEQAHST file                   T
#COMMENT T  3a - Write "non-indicative" records to SEQAHST.FIN file  T
#COMMENT T  3b - Write Indicative records to SEQAHST.IND file (ONLY  T
#COMMENT T       IF in debug mode)                                   T
#COMMENT T                                                           T
#COMMENT TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT P  Printing Information:                                    P
#COMMENT P RPT       REPORT         SPCL CO SPECIAL    DUE    BUZZ   P
#COMMENT P NAME     DESCRIPTION     FORM PY HANDLING   OUT    CODE   P
#COMMENT P                                                           P
#COMMENT PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#COMMENT U  Update History:                                          U
#COMMENT U                                                           U
#COMMENT U Date: 09/25/2007  CC:  12144    By: Steve Loper           U
#COMMENT U Reason: Initial version.                                  U
#COMMENT U                                                           U
#COMMENT UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
#######################################################################
##  functions (subroutines)
#######################################################################
sub make_lookup_table($@)
{
  my $file = shift;
  my %txns = ();
  open FILE, "< $file" or die "Can't open $file : $!";
  while( <FILE> ) {
    chomp;
    $txns{$_}++;
  }
  close FILE;
  return \%txns;
}
MAIN: {
  my ($infile1, $infile2) = @ARGV;
  my $txns = make_lookup_table($infile1);
  open FILE2, "< $infile2" or die "Can't open $infile2: $!";
  while( <FILE2> ) {
    my $tranNbr=substr($_, 60, 3);
    print unless (defined $txns->{$tranNbr});
    print stderr "$_" if (defined $txns->{$tranNbr} && $ENV{debug} eq "DEBUG" );
  }
  close FILE2;
  exit 0;
}
